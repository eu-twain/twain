
# TWAIN
## Overview
This repository contains kustomize + helm solution for deploying Twain data processing solution to kubernetes cluster.
## Table of Contents
- [Prerequisites](#prerequisites)
- [Repository Structure](#repository-structure)
- [Kustomize Configuration](#kustomize-configuration)
- [Deployment](#deployment)
- [Troubleshooting](#troubleshooting)
- [Contributing](#contributing)
- [License](#license)
## Prerequisites
Before you begin, ensure you have the following installed:
- [helm](https://helm.sh)(v3.11.\*)
- [kubectl](https://kubectl.docs.kubernetes.io)(v1.29.\*)
- [kubernetes](https://kubernetes.io) cluster(v1.29.\*)
- [kustomize](https://kubectl.docs.kubernetes.io)(v5.0.\*) - part of kubectl
## Repository Structure
The repository is organised as follows:
```
.
├── README.md
├── apps
│   ├── airflow
│   │   └── base
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       │   ├── twain-airflow-postgres-additional-credentials.yaml
│   │       │   ├── twain-airflow-postgres-init.yaml
│   │       │   └── twain-airflow-trino-additional-credentials.yaml
│   │       │   └── twain-components-admins-secrets.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   ├── requirements.txt
│   │       │   ├── twain-airflow-scheduler.yaml
│   │       │   ├── twain-airflow-web.yaml
│   │       │   └── twain-airflow-worker.yaml
│   │       └── resources
│   │           ├── twain-airflow-admin-secrets.yaml
│   │           └── twain-airflow-dags-sync.yaml
│   ├── hive
│   │   └── base
│   │       ├── chart
│   │       │   ├── chart-resources
│   │       │   │   └── hive-metastore
│   │       │   │       └── templates
│   │       │   │           ├── configmap.yaml
│   │       │   │           ├── postgresql-init.yaml
│   │       │   │           ├── statefulset.yaml
│   │       │   │           └── svc.yaml
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       │   ├── twain-hive-metastore-postgres-init.yaml
│   │       │   └── twain-hive-postgres-additional-credentials.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   ├── configmap.yaml
│   │       │   └── statefulset.yaml
│   │       └── resources
│   │           └── serviceaccount.yaml
│   ├── hive_ps
│   │   └── base
│   │       ├── kustomization.yaml
│   │       ├── plain_kustomize
│   │       │   ├── core-site-configmap.yaml
│   │       │   ├── deployment.yaml
│   │       │   ├── external-patches
│   │       │   │   └── twain-postgres-additional-credentials.yaml
│   │       │   ├── hive-site-configmap.yaml
│   │       │   ├── patches
│   │       │   │   └── twain-hive-postgres-init.yaml
│   │       │   ├── service.yaml
│   │       │   └── serviceaccount.yaml
│   │       ├── readme.md
│   │       └── values.yaml
│   ├── jupyterhub
│   │   └── base
│   │       ├── chart
│   │       │   ├── chart-resources
│   │       │   │   └── jupyterhub
│   │       │   │       └── templates
│   │       │   │           ├── hub
│   │       │   │           │   ├── configmap.yaml
│   │       │   │           │   ├── deployment.yaml
│   │       │   │           │   ├── networkpolicy.yaml
│   │       │   │           │   ├── role.yaml
│   │       │   │           │   ├── rolebinding.yaml
│   │       │   │           │   ├── secret.yaml
│   │       │   │           │   ├── service-account.yaml
│   │       │   │           │   └── service.yaml
│   │       │   │           ├── image-puller
│   │       │   │           │   ├── daemonset.yaml
│   │       │   │           │   ├── networkpolicy.yaml
│   │       │   │           │   └── service-account.yaml
│   │       │   │           ├── proxy
│   │       │   │           │   ├── deployment.yaml
│   │       │   │           │   ├── networkpolicy.yaml
│   │       │   │           │   ├── service-account.yaml
│   │       │   │           │   ├── service-api.yaml
│   │       │   │           │   └── service-public.yaml
│   │       │   │           └── singleuser
│   │       │   │               ├── networkpolicy.yaml
│   │       │   │               └── service-account.yaml
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       │   ├── twain-jupyterhub-postgres-init.yaml
│   │       │   ├── twain-keycloak-config-cli-secret.yaml
│   │       │   └── twain-postgres-additional-credentials.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   └── hub
│   │       │       ├── configmap.yaml
│   │       │       ├── deployment.yaml
│   │       │       └── secret.yaml
│   │       ├── resources
│   │       │   └── singleuser-configmap.yaml
│   │       └── volume_enricher
│   │           └── pod_volume_enricher.yaml
│   ├── keycloak
│   │   ├── base
│   │   │   ├── chart
│   │   │   │   ├── kustomization.yaml
│   │   │   │   └── values.yaml
│   │   │   ├── external-patches
│   │   │   │   ├── twain-keycloak-postgres-init.yaml
│   │   │   │   └── twain-postgres-additional-credentials.yaml
│   │   │   ├── kustomization.yaml
│   │   │   ├── patches
│   │   │   │   └── twain-keycloak-config-cli-job.yaml
│   │   │   └── resources
│   │   │       ├── twain-components-admins-secrets.yaml
│   │   │       ├── twain-keycloak-config-cli-realms.yaml
│   │   │       ├── twain-keycloak-config-cli-secret.yaml
│   │   │       └── twain-keycloak-config.yaml
│   │   └── documentation
│   │       └── KeycloakTwainGroupSetup.md
│   ├── kustomization.yaml
│   ├── minio
│   │   └── base
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       │   └── twain-components-admins-secrets.yaml
│   │       │   └── twain-keycloak-config-cli-secret.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   ├── twain-minio-provisioning-configmap.yaml
│   │       │   └── twain-minio-provisioning-job.yaml
│   │       └── resources
│   │           ├── twain-minio-provisioning-buckets-configmap.yaml
│   │           └── twain-minion-keycloak-secrets.yaml
│   ├── nginx-ingress-controller
│   │   └── base
│   │       ├── kustomization.yaml
│   │       └── resources
│   │           └── twain-ingress-certificate.yaml
│   ├── openmetadata
│   │   ├── base
│   │   │   ├── README.md
│   │   │   ├── chart
│   │   │   │   ├── kustomization.yaml
│   │   │   │   └── values.yaml
│   │   │   ├── docs
│   │   │   │   └── c3484050-ae42-40da-93e7-12bdfd9e1c09.png
│   │   │   ├── external-patches
│   │   │   │   ├── twain-keycloak-config-cli-secret.yaml
│   │   │   │   ├── twain-omd-postgres-init.yaml
│   │   │   │   └── twain-postgres-additional-credentials.yaml
│   │   │   ├── kustomization.yaml
│   │   │   ├── patches
│   │   │   │   ├── delete_secret_reference.yaml
│   │   │   │   ├── twain-deployment-containers.yaml
│   │   │   │   ├── twain-deployment-init-containers.yaml
│   │   │   │   ├── twain-openmetadata-db-secret.yaml
│   │   │   │   ├── twain-openmetadata-jwt-issuer-password-overwrite-configmap.yaml
│   │   │   │   ├── twain-openmetadata-jwt-issuer-password-overwrite-job.yaml
│   │   │   │   ├── twain-openmetadata-pipeline-secret.yaml
│   │   │   │   └── twain-openmetadata-search-secret.yaml
│   │   │   └── resources
│   │   │       └── twain-allow-automatic-secretes-creation.yaml
│   │   └── base_dependencies
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── docs
│   │       │   └── TWAIN_2024-openmetadata-dependencies.drawio.png
│   │       ├── external-patches
│   │       │   ├── twain-airflow-postgres-init.yaml
│   │       │   └── twain-postgres-additional-credentials.yaml
│   │       ├── kustomization.yaml
│   │       └── patches
│   │           ├── twain-config-envs.yaml
│   │           └── twain-db-migrations.yaml
│   ├── postgresql
│   │   └── base
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   └── twain-postgres-primary.yaml
│   │       └── resources
│   │           ├── twain-postgres-additional-credentials.yaml
│   │           ├── twain-postgres-admin-secrets.yaml
│   │           └── twain-postgres-init-config.yaml
│   ├── superset
│   │   └── base
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       │   ├── twain-keycloak-config-cli-secret.yaml
│   │       │   ├── twain-keycloak-env-vars.yaml
│   │       │   ├── twain-superset-additional-credentials.yaml
│   │       │   └── twain-superset-postgres-init.yaml
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   ├── superset_bootstrap.sh
│   │       │   ├── superset_config.py
│   │       │   ├── superset_init.sh
│   │       │   ├── twain-superset-env.yaml
│   │       │   └── twain-superset.yaml
│   │       └── values_overrides.yaml
│   ├── trino
│   │   └── base
│   │       ├── chart
│   │       │   ├── kustomization.yaml
│   │       │   └── values.yaml
│   │       ├── external-patches
│   │       ├── kustomization.yaml
│   │       ├── patches
│   │       │   ├── coordinator-deployment.yaml
│   │       │   └── worker-deployment.yaml
│   │       └── resources
│   │           ├── twain-trino-certificate.yaml
│   │           └── twain-trino-secrets.yaml
│   └── trino_ps
│       └── base
│           ├── kustomization.yaml
│           ├── patches
│           │   └── twain-trino-ssl-internal-cert.pem
│           ├── resources
│           └── values.yaml
└── overlays
    ├── cluster
    │   ├── history
    │   ├── kustomization.yaml
    │   ├── patches
    │   │   ├── airflow
    │   │   │   ├── twain-airflow-redis-master.yaml
    │   │   │   ├── twain-airflow-scheduler.yaml
    │   │   │   ├── twain-airflow-web.yaml
    │   │   │   └── twain-airflow-worker.yaml
    │   │   ├── jupyterhub
    │   │   │   ├── twain-jupyterhub-hub-deployment.yaml
    │   │   │   ├── twain-jupyterhub-hub-secret.yaml
    │   │   │   ├── twain-jupyterhub-proxy-deployment.yaml
    │   │   ├── keycloak
    │   │   │   ├── twain-keycloak-config-cli.yaml
    │   │   │   └── twain-keycloak.yaml
    │   │   ├── minio
    │   │   │   ├── twain-minio-deployment.yaml
    │   │   │   └── twain-minio-pvc.yaml
    │   │   ├── nginx-ingress
    │   │   │   └── ingress-nginx-controller.yaml
    │   │   ├── openmetadata
    │   │   │   └── base
    │   │   │       ├── openmetadata-deployment.yaml
    │   │   │       └── openmetadata-pod.yaml
    │   │   ├── openmetadata-dependencies
    │   │   │   ├── twain-dags-pvc.yaml
    │   │   │   ├── twain-logs-pvc.yaml
    │   │   │   └── twain-omd-opensearch.yaml
    │   │   ├── postgresql
    │   │   │   └── twain-postgresql.yaml
    │   │   └── trino
    │   │       ├── twain-trino-coordinator-deployment.yaml
    │   │       └── twain-trino-worker-deployment.yaml
    │   ├── resources
    │   │   ├── ingress
    │   │   │   └── twain-ingress-rules.yaml
    │   │   ├── jupyterhub
    │   │   │   └── twain-jupyterhub-shared-models-pvc.yaml
    │   │   └── volumes
    │   │       ├── twain-persistent-jupyterhub-nfs-volume.yaml
    │   │       ├── twain-persistent-jupyterhub-shared-models-nfs-volume.yaml
    │   │       ├── twain-persistent-minio-nfs-volume.yaml
    │   │       ├── twain-persistent-omd-dags-nfs-volume.yaml
    │   │       ├── twain-persistent-omd-logs-nfs-volume.yaml
    │   │       ├── twain-persistent-opensearch-local-volume.yaml
    │   │       ├── twain-persistent-postgresql-local-volume.yaml
    │   │       └── twain-persistent-postgresql-nfs-volume.yaml
    └── dev_ps
        └── kustomization.yaml

```
- `apps/`: Contains base configuration for each of components/apps of the solution.
- `overlays/`: Contains environment-specific customisations and configurations.
  - `cluster/`: Example overlay configuration with custom storage definition and credentials workflow.
## Kustomize Configuration
### Apps
Apps directory contains base configuration for the components. Configuration created using publicly available helm charts and customised using Kustomize. Apps folder also contains base  `kustomization.yaml` file.
### Overlays
Each overlay directory contains Kustomize configuration files file that customises the base configuration for a specific environment.
#### Example: `overlays/cluster`
```sh
.
├── history
├── kustomization.yaml
├── patches
│   ├── airflow
│   │   ├── twain-airflow-redis-master.yaml
│   │   ├── twain-airflow-scheduler.yaml
│   │   ├── twain-airflow-web.yaml
│   │   └── twain-airflow-worker.yaml
│   ├── jupyterhub
│   │   ├── twain-jupyterhub-hub-deployment.yaml
│   │   ├── twain-jupyterhub-hub-secret.yaml
│   │   ├── twain-jupyterhub-proxy-deployment.yaml
│   │   └── values.yaml
│   ├── keycloak
│   │   ├── twain-keycloak-config-cli.yaml
│   │   └── twain-keycloak.yaml
│   ├── minio
│   │   ├── twain-minio-deployment.yaml
│   │   └── twain-minio-pvc.yaml
│   ├── nginx-ingress
│   │   └── ingress-nginx-controller.yaml
│   ├── openmetadata
│   │   └── base
│   │       ├── openmetadata-deployment.yaml
│   │       └── openmetadata-pod.yaml
│   ├── openmetadata-dependencies
│   │   ├── twain-dags-pvc.yaml
│   │   ├── twain-logs-pvc.yaml
│   │   └── twain-omd-opensearch.yaml
│   ├── postgresql
│   │   └── twain-postgresql.yaml
│   ├── secrets.yaml
│   └── trino
│       ├── twain-trino-coordinator-deployment.yaml
│       └── twain-trino-worker-deployment.yaml
└── resources
    ├── ingress
    │   └── twain-ingress-rules.yaml
    ├── jupyterhub
    │   └── twain-jupyterhub-shared-models-pvc.yaml
    └── volumes
        ├── twain-persistent-jupyterhub-nfs-volume.yaml
        ├── twain-persistent-jupyterhub-shared-models-nfs-volume.yaml
        ├── twain-persistent-minio-nfs-volume.yaml
        ├── twain-persistent-omd-dags-nfs-volume.yaml
        ├── twain-persistent-omd-logs-nfs-volume.yaml
        ├── twain-persistent-opensearch-local-volume.yaml
        ├── twain-persistent-postgresql-local-volume.yaml
        └── twain-persistent-postgresql-nfs-volume.yaml

```
- `patches`: Environment-specific configuration overrides.
- `resources`: Environment-specific resource(e.g. volumes).
NOTE: While creating own overlay you'd need to update/overwrite following secrets:
```yaml
secrets:
- name: twain-components-admins-secrets
  values:
  - keycloak-admin-password
  - keycloak-admin-username
  - keycloak-admin-update-instructions
  - airflow-admin-password
  - airflow-admin-username
  - airflow-admin-update-instructions
  - minio-admin-password
  - minio-admin-username
  - minio-admin-update-instructions
  - minio-service-account-password
  - minio-service-account-update-instructions
  - minio-service-account-username
- name: twain-keycloak-config-cli-secret
  values:
  - JUPYTERHUB_CLIENT_ID
  - JUPYTERHUB_CLIENT_SECRET
  - JUPYTERHUB_SECRET
  - KEYCLOAK_REALM
  - MINIO_CLIENT_ID
  - MINIO_CLIENT_SECRET
  - MINIO_SERVICE_PASSWORD
  - OPENMETADATA_CLIENT_ID
  - SUPERSET_CLIENT_ID
  - SUPERSET_CLIENT_SECRET
- name: twain-minio
  values:
  - root-password
  - root-user
- name: twain-postgres-additional-credentials
  values:
  - AIRFLOW_DB
  - AIRFLOW_PASSWORD
  - AIRFLOW_USER
  - HIVE_POSTGRES_DB
  - HIVE_POSTGRES_PASSWORD
  - HIVE_POSTGRES_USER
  - JUPYTERHUB_DB
  - JUPYTERHUB_PASSWORD
  - JUPYTERHUB_USER
  - KEYCLOAK_DB
  - KEYCLOAK_PASSWORD
  - KEYCLOAK_USER
  - OMD_DEPS_AIRFLOW_DB
  - OMD_DEPS_AIRFLOW_PASSWORD
  - OMD_DEPS_AIRFLOW_USER
  - OPENMETADATA_DB
  - OPENMETADATA_PASSWORD
  - OPENMETADATA_USER
  - SUPERSET_DB
  - SUPERSET_PASSWORD
  - SUPERSET_USER
- name: twain-postgres-admin-secrets
  values:
  - admin-password
  - password
  - replication-password
- name: twain-trino-secrets
  values:
  - INTERNAL_COMMUNICATION_SHARED_SECRET
- name: twain-trino-certificate
  values:
  - twain-trino-ssl-internal-cert.pem
- name: trino-file-authentication
  values:
  - password
  - password.db
- name: twain-jupyterhub-hub
  values:
  - hub.config.CryptKeeper.keys
  - hub.config.JupyterHub.cookie_secret
  - proxy-token
  - values.yaml
- name: twain-airflow-admin-secrets
  values:
  - $patch
  - airflow-fernet-key
  - airflow-password
  - airflow-secret-key
- name: twain-airflow-redis
  values:
  - redis-password
- name: twain-superset-secret
  values:
  - SUPERSET_SECRET_KEY
- name: twain-config-envs
  values:
  - AIRFLOW__API__AUTH_BACKENDS
  - AIRFLOW__CELERY__FLOWER_PORT
  - AIRFLOW__CORE__DAGS_FOLDER
  - AIRFLOW__CORE__EXECUTOR
  - AIRFLOW__CORE__FERNET_KEY
  - AIRFLOW__CORE__SQL_ALCHEMY_CONN_CMD
  - AIRFLOW__DATABASE__SQL_ALCHEMY_CONN_CMD
  - AIRFLOW__KUBERNETES__NAMESPACE
  - AIRFLOW__KUBERNETES__POD_TEMPLATE_FILE
  - AIRFLOW__KUBERNETES__WORKER_CONTAINER_REPOSITORY
  - AIRFLOW__KUBERNETES__WORKER_CONTAINER_TAG
  - AIRFLOW__KUBERNETES_EXECUTOR__NAMESPACE
  - AIRFLOW__KUBERNETES_EXECUTOR__POD_TEMPLATE_FILE
  - AIRFLOW__KUBERNETES_EXECUTOR__WORKER_CONTAINER_REPOSITORY
  - AIRFLOW__KUBERNETES_EXECUTOR__WORKER_CONTAINER_TAG
  - AIRFLOW__LOGGING__BASE_LOG_FOLDER
  - AIRFLOW__LOGGING__DAG_PROCESSOR_MANAGER_LOG_LOCATION
  - AIRFLOW__OPENMETADATA_AIRFLOW_APIS__DAG_GENERATED_CONFIGS
  - AIRFLOW__OPENMETADATA_SECRETS_MANAGER__AWS_ACCESS_KEY
  - AIRFLOW__OPENMETADATA_SECRETS_MANAGER__AWS_ACCESS_KEY_ID
  - AIRFLOW__OPENMETADATA_SECRETS_MANAGER__AWS_REGION
  - AIRFLOW__SCHEDULER__CHILD_PROCESS_LOG_DIRECTORY
  - AIRFLOW__TRIGGERER__DEFAULT_CAPACITY
  - AIRFLOW__WEBSERVER__SECRET_KEY
  - AIRFLOW__WEBSERVER__WEB_SERVER_PORT
  - DATABASE_CELERY_CMD
  - DATABASE_HOST
  - DATABASE_PASSWORD_CMD
  - DATABASE_PORT
  - DATABASE_PROPERTIES
  - DATABASE_PSQL_CMD
  - DATABASE_SQLALCHEMY_CMD
  - DATABASE_USER_CMD
  - DATABASE_USER
  - DATABASE_PASSWORD
  - TZ
- name: twain-openmetadata-authorizer-secret
  values:
  - AUTHORIZER_ADMIN_PRINCIPALS
  - AUTHORIZER_ALLOWED_REGISTRATION_DOMAIN
  - AUTHORIZER_CLASS_NAME
  - AUTHORIZER_ENABLE_SECURE_SOCKET
  - AUTHORIZER_ENFORCE_PRINCIPAL_DOMAIN
  - AUTHORIZER_PRINCIPAL_DOMAIN
  - AUTHORIZER_REQUEST_FILTER
- name: twain-openmetadata-jwt-secret
  values:
  - JWT_ISSUER
  - JWT_KEY_ID
  - RSA_PRIVATE_KEY_FILE_PATH
  - RSA_PUBLIC_KEY_FILE_PATH
- name: twain-openmetadata-pipeline-secret
  values:
  - AIRFLOW_TRUST_STORE_PATH
  - AIRFLOW_USERNAME
  - PIPELINE_SERVICE_CLIENT_CLASS_NAME
  - PIPELINE_SERVICE_CLIENT_ENABLED
  - PIPELINE_SERVICE_CLIENT_ENDPOINT
  - PIPELINE_SERVICE_CLIENT_HEALTH_CHECK_INTERVAL
  - PIPELINE_SERVICE_CLIENT_SSL_CERT_PATH
  - PIPELINE_SERVICE_CLIENT_VERIFY_SSL
  - PIPELINE_SERVICE_IP_INFO_ENABLED
  - SERVER_HOST_API_URL
- name: twain-openmetadata-authentication-secret
  values:
  - AUTHENTICATION_AUTHORITY
  - AUTHENTICATION_CALLBACK_URL
  - AUTHENTICATION_CLIENT_ID
  - AUTHENTICATION_ENABLE_SELF_SIGNUP
  - AUTHENTICATION_JWT_PRINCIPAL_CLAIMS
  - AUTHENTICATION_PROVIDER
  - AUTHENTICATION_PUBLIC_KEYS
  - AUTHENTICATION_RESPONSE_TYPE
- name: twain-airflow-trino-additional-credentials
  values:
  - AIRFLOW_TRINO_PASSWORD
  - AIRFLOW_TRINO_USER
```
## Deployment
To deploy to a Kubernetes cluster, use Kustomize with `kubectl`:
1. Navigate to the desired environment overlay directory:
```bash
cd overlays/cluster
```
2. Apply the configuration to the Kubernetes cluster:
```bash
kubectl kustomize --enable-helm . | kubectl apply -f -
```

## Troubleshooting
If you encounter issues, consider the following:
- Verify that `kustomize` and `kubectl` are properly installed and configured.
- Check the `kustomization.yaml` files for syntax errors or misconfigurations.
- Check rendered manifest `kubectl kustomize --enable-helm .` .
- Use `kubectl describe` and `kubectl logs` to debug Kubernetes resources.
## Contributing
Contributions are welcome! Please follow these guidelines:
1. Fork the repository.
2. Create a feature branch (`git checkout -b feature/your-feature`).
3. Commit your changes (`git commit -am Add new feature`).
4. Push to the branch (`git push origin feature/your-feature`).
5. Create a pull request.
## License
This project is licensed under the [MIT License](LICENSE).  <- TODO: Which license to use?

