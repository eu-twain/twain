Directory contains an approach of deploying hive on minikube from plain yaml files.

We cannot store postgres client jar as config map due to its size (limits described in https://kubernetes.io/docs/concepts/configuration/configmap/#:~:text=The%20data%20stored%20in%20a,separate%20database%20or%20file%20service.).
Copy did not work, too, as the container's image is not run as the root user and target directory has 'rw-r--r--' access rights and the root is the owner.
