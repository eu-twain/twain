apiVersion: apps/v1
kind: Deployment
metadata:
  name: twain-hive-metastore
  namespace: default
spec:
  template:
    metadata:
      labels:
        app: deep-backend
        app.kubernetes.io/instance: twain
        app.kubernetes.io/name: hive
        app.kubernetes.io/version: 1.0.0
        app.kubernetes.io/component: hive
    spec:
      serviceAccountName: twain-hive-metastore
      containers:
        - name: backend
          image: hive:4.0.0
          command:
            - /bin/bash
            - -c
            - |
              set -e;
              ls -la /opt/hive/lib
              # cp /opt/hive/lib-external/*.jar /opt/hive/lib
              sh -c /entrypoint.sh
          ports:
            - containerPort: 9083
              name: http
              protocol: TCP
          env:
            - name: MINIO_SERVICE_USER
              value: "twain_service_account"
            - name: MINIO_SERVICE_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: MINIO_SERVICE_PASSWORD
                  name: twain-keycloak-config-cli-secret
            - name: DB_DRIVER
              value: "postgres"
            - name: SERVICE_NAME
              value: "metastore"
            - name: HADOOP_OPTIONAL_TOOLS
              value: "hadoop-aws"
          volumeMounts:
            # shared volumes
            - mountPath: /opt/hive/conf
              name: shared-volume
            - mountPath: /opt/hadoop/etc/hadoop
              name: shared-volume-core-site
            - mountPath: /opt/hive/lib/postgresql-42.7.3.jar
              subPath: "postgresql-42.7.3.jar"
              name: shared-volume-postgres-jar
      initContainers:
        - name: waiting-until-minio-is-opertional
          command:
            - /bin/bash
            - -c
            - |
              set -e;
              echo "Waiting for Minio";
              wait-for-port \
                --host=twain-minio \
                --state=inuse \
                --timeout=120 \
                9000;
              echo "Minio is available";
          image: docker.io/bitnami/minio:2024.3.7-debian-12-r0
          imagePullPolicy: IfNotPresent
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
                - ALL
            privileged: false
            readOnlyRootFilesystem: false
            runAsGroup: 0
            runAsNonRoot: true
            runAsUser: 1001
            seccompProfile:
              type: RuntimeDefault
        - name: download-postgresql-jar
          image: alpine/curl:latest
          command:
            - /bin/sh
            - -c
            - |
              #!/bin/sh
              cd ${JAR_PATH}
              curl -O https://jdbc.postgresql.org/download/postgresql-42.7.3.jar
          env:
            - name: JAR_PATH
              value: /opt/jars
          volumeMounts:
            - mountPath: /opt/jars
              name: shared-volume-postgres-jar
        - name: prepare-hive-metastore-hive-site-xml
          image: alpine:latest
          command:
            - /bin/sh
            - -c
            - |
              #!/bin/sh
              set -e
              cd ${TEMPLATE_DIR}
              for file in $(ls -1 ./);
              do
              eval "cat <<EOF>${OUTPUT_DIR}/${file}
              $(cat ${file})
              EOF
              "
              echo "parsed file ${file}"
              done
          env:
            - name: TEMPLATE_DIR
              value: /opt/templates
            - name: OUTPUT_DIR
              value: /opt/outputs
            - name: HIVE_POSTGRES_DB
              valueFrom:
                secretKeyRef:
                  key: HIVE_POSTGRES_DB
                  name: twain-postgres-additional-credentials
            - name: HIVE_POSTGRES_USER
              valueFrom:
                secretKeyRef:
                  key: HIVE_POSTGRES_USER
                  name: twain-postgres-additional-credentials
            - name: HIVE_POSTGRES_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: HIVE_POSTGRES_PASSWORD
                  name: twain-postgres-additional-credentials
          volumeMounts:
            # shared volume
            - mountPath: /opt/outputs
              name: shared-volume
            - mountPath: /opt/templates
              name: hive-site-volume
        - name: prepare-hive-metastore-core-site-xml
          image: alpine:latest
          command:
            - /bin/sh
            - -c
            - |
              #!/bin/sh
              set -e
              cd ${TEMPLATE_DIR}
              for file in $(ls -1 ./);
              do
              eval "cat <<EOF>${OUTPUT_DIR}/${file}
              $(cat ${file})
              EOF
              "
              echo "parsed file ${file}"
              done
          env:
            - name: TEMPLATE_DIR
              value: /opt/templates
            - name: OUTPUT_DIR
              value: /opt/outputs
            - name: MINIO_SERVICE_USER
              value: twain_service_account
            - name: MINIO_SERVICE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: twain-keycloak-config-cli-secret
                  key: MINIO_SERVICE_PASSWORD
          volumeMounts:
            # shared volume
            - mountPath: /opt/outputs
              name: shared-volume-core-site
            - mountPath: /opt/templates
              name: core-site-volume
      volumes:
        # shared volume
        - name: shared-volume
          emptyDir: {}
        - name: shared-volume-core-site
          emptyDir: {}
        - name: shared-volume-postgres-jar
          emptyDir: {}
        - name: hive-site-volume
          configMap:
            name: twain-hive-hive-site-configmap
        - name: core-site-volume
          configMap:
            name: twain-hive-core-site-configmap
