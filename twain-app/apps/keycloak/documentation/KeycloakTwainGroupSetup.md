## Keycloak groups setup

### Abstract
This guide aims at providing detailed description on the concept of user groups, its practical application within Twain framework, its implementation as Infrastructure as Code (IaC) concept as well as showing the example of setting up a new group and provides details about deployment of the changes to the k8s cluter, be it local minikube for development or an actual k8s cluster like the one belonging to DTU.

### Concept
Groups play an important role within Twain environment as they define the role each Twain user occupies (is it a researcher or a wind farm operator) and define the legal entity the user is a member of (legal entity is a blanket term to encompass both for-profit companies and universities that are part of Twain project).

Whenever a user logs into any component of Twain using the Keycloak as Identity Provider, the Keycloak resends to the user's browser the JWT token that is tailored by Softserve Poland (SSP) developers to fit into the requirements of the component. It means that if the component expects the user's token to contain key 'membership', the SSP created the Twain realm in Keycloak in the way that the JWT token will have that key inside.

In order to achieve the purpose above it was not enough to just create a set of groups. Each group would have to comprise of their own set of attributes.

### Attributes
During the implementation of security within Twain framework SSP was tasked with creation of consistent security setup. As we have several components (among others: MinIO, Trino, JupyterHub) and each component has its own unique way of user authorization, the most straight-forward method of achieving this was to introduce a set of group attributes within Keycloak and tailoring Keycloak's JWT responses when a user logs into a component.
Each user group should contain those attributes:
* entity_name
* minio_entity_name
* policy

Attribute entity_name contains 2 values, one for the legal entity name and the other for the role of the users within the legal entity; it is used to create JWT token for Jupyterhub and Trino, as it is possible to pass a list of groups the user is member of. This list is then interpreted by the downstream systems (Trino, JupyterHub) to provide the role based data access security.

Attribute minio_entity_name contains only one value which is the name of the legal entity the users are members of. This is used to create JWT token for MinIO and MinIO is interpreting the value to provide role based data access security.

Attribute policy contains at least one value (but in practice it has always 2 values) which is/are the name of access policies that should be applied by MinIO to the user. Those policies are included in creation of JWT token for MinIO and MinIO is interpreting the value to provide role based data access security.

### Infrastructure as code
#### Keycloak IaC Context
As the goal of the grant is to create a deployable source code of data environment, the best way to achieve it is to follow the standard k8s deployment procedure by using the documented helm chart provided by the authors of the components or 3rd party (like other open source organizations - bitnami, apache - as those sometimes may have better approaches to deployment to k8s than the authors themselves).
At the moment of writing this docs, Twain using bitnami distribution of helm chart in the version 19.2.0. This was not enough to cover all the requirements placed before Twain consortium, as it does not provide sufficient level of security - plainly speaking, the passwords to Keycloak clients would have to be present in the source code and since the source code is public we would have to expose them to the public.

Instead of exposing the passwords, the DevOps team figured out the solution that would keep the passwords outside of the source code, but to achieve this there is quite a lot of kustomization scripts that alter the output of helm chart.


#### IaC implementation and development guide
In the case of Keycloak deployment, the setup of the realm is already embedded in the IaC. The Twain realm is uploaded into keycloak via a k8s Job called 'twain-keycloak-keycloak-config-cli', but it needed to be kustomized further in order to avoid putting passwords in the configuration.

The source code for Twain realm, including setting up the groups within Keycloak, is stored as kustomize patch in path "keycloak/base/resources/twain-config-cli-realms.yaml". This yaml contains the entire description of Twain realm but the sensitive information like passwords to Keycloak clients are written as shell variables.

Applying of aforementioned file 'twain-config-cli-realms.yaml' on k8s cluster creates a config map called 'twain-keycloak-config-cli-realms' which in turn is mounted as a volume of init container 'realms-templating' of the kubernetes job 'twain-keycloak-keycloak-config-cli'. This init container replaces the variables present in the Twain realm configuration file with actual values present in secret 'twain-keycloak-config-cli-secret'.

The step above produces the parsed definition of Twain realm that in turn is mounted as a file within path "/config" of container 'keycloak-config-cli', which is part of aforementioned job 'twain-keycloak-keycloak-config-cli'. This job is going to apply the Twain realm configuration into the Keyclaok instance.

### Setup example

#### How does the source code look like
After all the introduction above, we may finally take a look on the group setup 'living' example:
```json
{
    "name": "DTU_researchers_group",
    "path": "/DTU_researchers_group",
    "subGroups": [],
    "attributes": {
      "entity_name": [
        "researchers",
        "DTU"
      ],
      "minio_entity_name": [
        "DTU"
      ],
      "policy": [
        "read_all_manage_yours",
        "twain_public_bucket_policy_cr"
      ]
    },
    "realmRoles": [],
    "clientRoles": {}
},
```
The group contains only name and the attributes:
* entity_name has 2 values: researchers and DTU; which means the group is for a legal entity called DTU and their members are researchers; the JupyterHub will receive a JWT token that will contain this information and it would allow the applications that use the JupyterHub client to understand and interpret that information;
* minio_entity_name has one value: DTU, meaning the members of the group will be flagged as belonging to legal entity DTU when JWT token is generated for MinIO's Keycloak client;
* policy has 2 values: read_all_manage_yours and twain_public_bucket_policy_cr, meaning MinIO will receive a JWT token with key 'policy' and its value is a list of 2 strings 'read_all_manage_yours' and 'twain_public_bucket_policy_cr';

#### How to create new group
Assume Twain consortium grows by adding a new legal entity, for the sake of argument lets call this "Wind Farms Italy, Inc.". By gathering the information on the company, we surmise this is wind farm operator, they should not be able to see the non-public data from other WF-operators, but should be granted access to public data. The sample setup can look like this:
```json
{
    "name": "WFI_WF_operators_group",
    "path": "/WFI_WF_operators_group",
    "subGroups": [],
    "attributes": {
      "entity_name": [
        "wf_operators",
        "WFI"
      ],
      "minio_entity_name": [
        "WFI"
      ],
      "policy": [
        "generic_access_based_on_jwt_group",
        "twain_public_bucket_policy_cr"
      ]
    },
    "realmRoles": [],
    "clientRoles": {}
},
```
Assigning the group to MinIO policy 'twain_public_bucket_policy_cr' grants the members access to public ingestion while assignment to policy 'generic_access_based_on_jwt_group' will allow them to place files in their own subdirectory in the bucket 'wf-operators' (assuming nothing changes in the setup of the security policy on MinIO side).
Additionally, we have to add the company preferred abbreviation to attribute 'minio_entity_name' so MinIO would know which company we are assigning the user to.

To apply proper data security policies from JupyterHub/Trino perspective we have to set the attribute "entity_name" to values: "wf_operators" and company preferred abbreviation (same as in attribute 'minio_entity_name').

The json above ahs to be placed in proper place in source code in file 'twain-config-cli-realms.yaml' as nother element of the list of the key 'groups'.

#### How to apply new group into cluster/minikube
Running standard deployment described by DevOps is not enough to force the changes to Keycloak realm as k8s jobs are immutable and the person applying the deployment must delete the job 'twain-keycloak-keycloak-config-cli' before running deployment.

### How the security groups are interpreted by downstream Twain components
A reader of this doc may notice that the actual rules of security are not set in the Keycloak itself - those are governed by the components themselves.
Each component parses the JWT token in its own, unique way. It is sufficient to describe the data security on Trino/JupyterHub (those share the client and JWT token is identical in their case) and MinIO.

#### MinIO
MinIO requires 3 elements in the JWT token:
* policy - it is a list of policies that have to be applied upon the user login;
* picture - for the purpose of Twain this key was refurbished to host the company preferred abbreviation;
* preferred_username - the name of the user;

All keys above play a role in determining the access role for the user. at the moment of writing this doc there are 4 custom MinIO policies:
* twain_public_bucket_policy_cr - this policy allows RW operations into bucket 'ingestion-public' and RO access to bucket 'ingestion-results';
* read_all_manage_yours - this policy:
    * denies the access to datalake bucket;
    * allows RO access everywhere else;
    * allows RW access to ${put company preferred abbreviation name from JWT here}/${put preferred_username from JWT here} subdirectories on researchers bucket;
* generic_access_based_on_jwt_group - this policy:
    * denies the access to datalake bucket;
    * allows RW access to ${put company preferred abbreviation name from JWT here}/${put preferred_username from JWT here} subdirectories on wf-operators bucket;
    * allows RO access to ${put company preferred abbreviation name from JWT here}subdirectories on wf-operators bucket;
* generic_superuser_access_based_on_jwt_group - a super user for each of the legal entities, not used at the mometn of writing this comment;

##### Minio in practice
For example consider the following case:
```json
{
  "user name": "ABC",
  "user company": "MY_COMPANY",
  "user group": "MY_COMPANY_WF_operators_group",
  "user groups policies": [
    "generic_access_based_on_jwt_group", 
    "twain_public_bucket_policy_cr"],
  "minio_entity_name": "MY_COMPANY"
}
```
which means there is a user who is a wind farm operator, is called 'ABC', it belongs to company which preferred abbreviation name is 'MY_COMPANY' and its group provided 2 policies, one called 'generic_access_based_on_jwt_group' the other 'twain_public_bucket_policy_cr'.
Such a user would be able to access with read/write privilege the public bucket (access granted by the fact he has policy 'twain_public_bucket_policy_cr') and will be able to have RW access into directory "MY_COMPANY/ABC" within bucket wf-operators. Such a suer would also be able to browse, read and download the files put by their colleagues from the same legal entity (in this case called 'MY_COMPANY').

#### JupyterHub/Trino
TRino requires only one key inthe JWT token returned from Keyclaok after login:
* entities_names - this key in JWT token is mapped as group names in Trino by official Trino helm chart in values.yaml file in the value of key 'server.coordinatorExtraConfig'; SSP procures the JWT token in a way that it contains this key; its values are taken from entity_name attribute set up in Keycloak;

The Trino's access rules are set up in helm chart values.yaml file as a value of the key "accessControl.rules.access-rules\.json". The rules of parsing the setup are unique to Trino and can be understood by reading the official documentation (https://trino.io/docs/current/security/file-system-access-control.html). At the moment of writing this doc the access policy is configured as such:
1. on catalog level:
   1. users test and admin can do anything - but those are set up as interanl users and are not logging in via Keycloak;
   2. user public has read-only access to hive catalog;
   3. members of internal group twain_service_accounts can do anything - but those are set up as interanl users and are not logging in via Keycloak;
   4. members of groups users, wf_operators, researchers, viewers have RO access on any catalog;
2. on tables level:
   1. users test and admin have ability of DML, DDL and DCL commands on any catalog (what those mean can be determined by reading the article https://www.geeksforgeeks.org/sql-ddl-dql-dml-dcl-tcl-commands/) 
   2. it is possible to select from tables in system catalog;
   3. users belonging to groups wf_operators, researchers or viewers are granted SELECT on table common_data_model provided the data inside the table is marked as public or is intended for the user to see them; the person uploading the data into Twain can provide in avro schema the field twain_data_ro_allowed_companies with a list of companies preferred abbreviation names separated by semi-colon, if the user performing the read belongs to the company which preferred abbreviation name is present in the list, the user an see the row, otherwise the user cannot see the row;
   4. users belonging to groups wf_operators, researchers or viewers are granted SELECT on tables which name starts with prefix "static_info_";
   5. user public is granted SELECT on table common_data_model provided the data inside the table is marked as public (same logic as 2 rows above but with the word public instead of company preferred abbreviation);
   6. users belonging to group users are granted SELECT on table common_data_model provided the data inside the table is marked as belonging to the group the user is a member of;
3. on functions and procedures level:
   1. users test and admin are granted EXECUTE privilege on procedure/function sync_partition_metadata;

##### JupyterHub/Trino in practice
For example consider the same case as in MinIO example case:
```json
{
  "user name": "ABC",
  "user company": "MY_COMPANY",
  "user group": "MY_COMPANY_WF_operators_group",
  "user groups entity_name": [
    "wf-operator", 
    "MY_COMPANY"],
  "minio_entity_name": "MY_COMPANY"
}
```
which means there is a user who is a wind farm operator, is called 'ABC', it belongs to company which preferred abbreviation name is 'MY_COMPANY' and its group provided 2 records in entity_name, one called 'wf-operator' the other 'MY_COMPANY'.
Such a user would be able to access with read privilege the public data in common_data_model, data marked as visible by company 'MY_COMPANY' (access granted by the point '2.iii') and will be able to select data from tables which names start with "static_info_" (granted by point "2.iv").
