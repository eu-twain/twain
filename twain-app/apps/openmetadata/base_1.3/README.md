# Openmetadata quirks and features

Openmetadata is the least "kubernetesized" component of Twain (at least so far, as of June 2024). comparing to its helm dependencies, which install numerous deployments, services, containers, init containers etc., this component deploys only one k8s Deployment with one initcontainer and 1 container.

Most of the features of "Infrastructure as Code" cannot be reproduced by helm chart or kustomize, as even the official documentation requires a human intervention to set up the ecosystem (for example, the jwt token used by bots can only be retrieved from webUI - or brute forced).

In this document the author describes his concern regarding the features offered by Openmetadata.
1. **Creation of admin user**: admin user has to be created manually via Keycloak; its username must be equal to the value set in .Values.openmetadata.config.authorizer.initialAdmins or the value set in cluster overlay in overwrite of secret "openmetadata-authorizer-secret" (overlays have higher priority); please share the password as a manually created secret on te target k8s cluster; 
2. **Creation of jwt token**: jwt key is use in generating an "eternal" jwt token used by Airflow to communicate back the results of gathering of metadata from the target database/warehouse; the format fo the key is uuid in version 4; in the source code in patches the reader can observe the automatic creation of said jwt token key, however the time pressure forces us to abandon this approach and revert to relying on the DevOps to remember to set this value ".Values.openmetadata.config.jwtTokenConfiguration.keyId" in cluster overlay;
3. **Basic authentication**: minor nitpick about the basic authentication, which was investigated by ultimately not used in the Twain: there is no option to set up the password for the admin users as of version 1.3.x and 1.4.x, a proper issue was created in the Github of Openmetadata;
4. **After login action**: Openmetadata forces user to provide the full name after the last login: we should not do that and we are not supposed to process personal data; it is the opinion of the author of this note that we should investigate how to "turn this off"/"fake it";
5. **Manual creation of connection to Trino**: Openmetadata focuses on manual creation of resources, which means there is a need to click this connection out **after** the admin user is created. How to click it out? 
    1. go to settings (bottom left) → Services → Databases and set the connection to Trino by selecting its icon from the panel;
    2. get the password from k8s Secrets for trino from secret called 'twain-airflow-trino-additional-credentials';
    3. in “hostPort“ please omit the “http://”/ “https://” part - I do not know how but Openmetadata handles this on their own side and when I specified https:// it started failing; 
    4. in the “TrinoConnection Advanced Config“ drop down list please specify 2 “Connection Options“:
       1. “source“ with value “twain_metadata_airflow” → this is shown on Trino WebUI and we could identify the duration and costs of running OMD to adjust the schedule of metadata gathering;
       2. “verify“:  with value “false“ → this would allow Airflow to run queries to Trino hosted behind self-signed certificate

    After the setup the admin should see something like this: ![a trino setup](docs/c3484050-ae42-40da-93e7-12bdfd9e1c09.png)

6. **Phantom data present in the webUI**: Some data structures shown to the users in webUI are not stored in the Postgres DB; the list of tables is not stored on Postgres, while their details are stored there; restart of PostgresDB did not truncate the records from main overview page (as it takes the list form currently unknown source), but once user clicks them the error is shown as their details are supposed to be stored in DB;
7. **After logout action**: Openmetadata does not terminate the user session from Keycloak
8. **Eternal fernet keys**: we cannot change the fernet key once even one connection was "clicked out" by the OMD admins - the fernet key is used to encrypt connection details and if we change it OMD cannot load the connection into memory, resulting in teardown of the pod - and OMD simply does not work.
