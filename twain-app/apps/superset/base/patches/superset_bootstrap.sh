#!/bin/sh
pip install Authlib trino psycopg2 Flask-OIDC flask-openid
if [ ! -f ~/bootstrap ]; then
    echo "Running Superset with uid 0" > ~/bootstrap
fi
