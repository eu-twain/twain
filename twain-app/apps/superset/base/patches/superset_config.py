import logging
import os
import sys
from datetime import timedelta
from distutils.util import strtobool
from urllib.parse import quote

from typing import Any
from celery.schedules import crontab
from flask import redirect, request, session
from flask_appbuilder.security.manager import (
    AUTH_DB,
    AUTH_LDAP,
    AUTH_OAUTH,
    AUTH_OID,
    AUTH_REMOTE_USER,
)

from flask_appbuilder.security.views import AuthOIDView
from flask_appbuilder.views import ModelView, SimpleFormView, expose
from flask_caching.backends.filesystemcache import FileSystemCache
from flask_login import login_user
from flask_oidc import OpenIDConnect
from superset.security import SupersetSecurityManager
from superset.utils.core import RowLevelSecurityFilterType


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


client_id = os.getenv("KEYCLOAK_CLIENT_ID")
client_secret = os.getenv("KEYCLOAK_CLIENT_SECRET")
host = os.getenv("KEYCLOAK_HOST")
port = os.getenv("KEYCLOAK_PORT")

realm_name = os.getenv("KEYCLOAK_REALM")
hostname = os.getenv("HOSTNAME")

keycloak_tls = os.getenv("KEYCLOAK_USE_TLS", "false")
schema = "https" if strtobool(keycloak_tls) else "http"

redis_host = os.getenv("REDIS_HOST")
redis_port = os.getenv("REDIS_PORT")

superset_port = os.getenv("TWAIN_SUPERSET_SERVICE_PORT")
if superset_port:
    superset_port = f":{superset_port}"
else:
    superset_port = ""

ENABLE_PROXY_FIX = True
AUTH_TYPE = AUTH_OID


OIDC_ID_TOKEN_COOKIE_SECURE = False
OIDC_OPENID_REALM = os.getenv("KEYCLOAK_REALM")
OIDC_INTROSPECTION_AUTH_METHOD = "client_secret_post"
OIDC_SCOPES = "openid profile email roles"

# Map Authlib roles to superset roles
AUTH_ROLE_ADMIN = "Admin"
AUTH_ROLE_PUBLIC = "Public"
# Will allow user self registration, allowing to create Flask users from Authorized User
AUTH_USER_REGISTRATION = True
# The default user self registration role
AUTH_USER_REGISTRATION_ROLE = "Gamma"
AUTH_ROLES_SYNC_AT_LOGIN = True


if port:
    port = f":{port}"
else:
    port = ""

logger.info(f"Our keycloak connection url is: {schema}://{host}{port}/realms/{realm_name}.")

OIDC_CLIENT_SECRETS: dict = {
    "keycloak": {
        "issuer": f"{schema}://{host}{port}/realms/{realm_name}",
        "auth_uri": f"{schema}://{host}{port}/realms/{realm_name}/protocol/openid-connect/auth",
        "client_id": client_id,
        "client_secret": client_secret,
        "redirect_uris": [
            f"{schema}://twain-superset{superset_port}/oauth-authorized/keycloak"
        ],
        "userinfo_uri": (
            f"{schema}://{host}{port}/realms/{realm_name}/protocol/openid-connect/userinfo"
        ),
        "token_uri": (
            f"{schema}://{host}{port}/realms/{realm_name}/protocol/openid-connect/token"
        ),
        "token_introspection_uri": (
            f"{schema}://{host}{port}/realms/{realm_name}/protocol/openid-connect/token/introspect"
        ),
    }
}


class AuthOIDCView(AuthOIDView):

    @expose("/login/", methods=["GET", "POST"])
    def login(self, flag=True):
        sm = self.appbuilder.sm
        oidc = sm.oid

        def get_user_info_roles():
            info = oidc.user_getinfo(
                [
                    "preferred_username",
                    "given_name",
                    "family_name",
                    "email",
                    "groups",
                    "roles",
                    "role_list",
                    "twain_groups"
                ]
            )
            realm_roles = info.get('twain_groups', [])
            superset_roles = []

            for rr in realm_roles:
                superset_roles.extend(
                    AUTH_ROLES_MAPPING.get(rr, [])
                )

            if not superset_roles:
                superset_roles = [AUTH_USER_REGISTRATION_ROLE]
            return info, [sm.find_role(r) for r in superset_roles]

        def _get_fake_email_address(user_info):
            return user_info.get("preferred_username") + "@fake_twain.com"

        @self.appbuilder.sm.oid.require_login
        def handle_login():
            self.ensure_roles()
            self.update_rls()

            # problem with connection to Keycloak stems from the fact the keycloak has different unique key for users;
            # Keycloak considers username as unique and email as optional, while Superset requires unique email address.
            # assumption: Twain user accounts do not change their email address
            user_info, roles = get_user_info_roles()
            email_from_identity_provider = oidc.user_getfield("email")
            if not email_from_identity_provider:
                email_from_identity_provider = _get_fake_email_address(user_info)
            user = sm.auth_user_oid(email_from_identity_provider)
            if not user:
                logger.info(f"User {user_info.get('preferred_username')} has not yet logged in Superset,"
                            f"their groups are: {roles}")
                user = sm.add_user(
                    username=user_info.get("preferred_username"),
                    first_name=user_info.get("given_name", ""),
                    last_name=user_info.get("family_name", ""),
                    email=user_info.get("email", _get_fake_email_address(user_info)),
                    role=roles,
                )
            else:
                logger.info(f"User {user_info.get('preferred_username')} has logged in into superset before,"
                            f" updating his access rights to {roles}.")
                user.roles = roles
                sm.update_user(user)

            login_user(user, remember=False)
            return redirect(self.appbuilder.get_url_for_index)

        return handle_login()

    @expose("/logout/", methods=["GET", "POST"])
    def logout(self):
        oidc = self.appbuilder.sm.oid
        oidc.logout()
        super().logout()
        # fix for https://github.com/apache/superset/issues/24713
        logger.info(f"Clearing sessions")
        session.clear()
        logger.info(f'Clearing ended; redirecting user to '
                    f'{"".join((oidc.client_secrets.get("issuer"), "/protocol/openid-connect/logout", ))}')
        return redirect(
            "".join(
                (
                    oidc.client_secrets.get("issuer"),
                    "/protocol/openid-connect/logout",
                )
            )
        )

    def _get_rls_rules(self):
        from superset.connectors.sqla.models import RowLevelSecurityFilter

        sm = self.appbuilder.sm
        roles = (
            sm.get_session()
            .query(
                RowLevelSecurityFilter.id,
                RowLevelSecurityFilter.name,
            )
            .filter(
                RowLevelSecurityFilter.filter_type == RowLevelSecurityFilterType.REGULAR
            )
            .all()
        )
        return {name: id for id, name in roles}

    def ensure_roles(self):
        default_role = "Gamma"
        sm = self.appbuilder.sm
        gamma_role = sm.find_role(default_role)
        for r in AUTH_ROLES_MAPPING:
            superset_role = sm.find_role(r)
            if not superset_role:
                sm.copy_role(
                    role_from_name=default_role,
                    role_to_name=r,
                    merge=True,
                )

    def update_rls(self):
        from superset.commands.security.create import CreateRLSRuleCommand
        from superset.commands.security.update import UpdateRLSRuleCommand
        from superset.row_level_security.schemas import RLSPostSchema

        sm = self.appbuilder.sm
        superset_roles: list[str] = list(AUTH_ROLES_MAPPING.keys())
        RLSSchema = RLSPostSchema()
        old_roles = self._get_rls_rules()
        for role in AUTH_ROLES_MAPPING:
            member = role.split("_")[0]
            name = f"autogenerated RLS policy for {role} group"
            policy: dict[str, Any] = {
                "name": name,
                "description": f"RLS policy for role {role} added by superset_config",
                "filter_type": "Regular",
                "tables": [1, 2],
                "roles": [sm.find_role(role).id],
                "group_key": "",
                "clause": f"ro_access_companies like '%{member}%'",
            }
            item = RLSSchema.load(policy)
            try:
                if name in old_roles:
                    UpdateRLSRuleCommand(old_roles[name], item)
                else:
                    CreateRLSRuleCommand(item).run()
            except Exception as e:
                logger.error("Could not create RLS rule: '%s' due to %s", (name, e))


class CustomSecurityManager(SupersetSecurityManager):
    def __init__(self, appbuilder):
        super().__init__(appbuilder)
        if self.auth_type == AUTH_OID:
            self.oid = OpenIDConnect(self.appbuilder.get_app)
        self.authoidview = AuthOIDCView


CUSTOM_SECURITY_MANAGER = CustomSecurityManager

AUTH_ROLES_MAPPING = {
    "CENER_researchers_group": ["Gamma"],
    "DTU_researchers_group": ["Alpha", "sql_lab"],
    "EDF_WF_operators_group": ["Gamma"],
    "ENGIE_GREEN_FRANCE_WF_operators_group": ["Gamma"],
    "ENGIE_LABORELEC_WF_operators_group": ["Gamma"],
    "RAMBOLL_DANMARK_AS_researchers_group": ["Gamma"],
    "RAMBOLL_researchers_group": ["Gamma"],
    "SSP_group": ["Admin"],
    "TUD_researchers_group": ["Gamma"],
    "TUM_researchers_group": ["Gamma"],
}

RESULTS_BACKEND = FileSystemCache("/app/superset_home/sqllab")

CACHE_CONFIG = {
    "CACHE_TYPE": "RedisCache",
    "CACHE_DEFAULT_TIMEOUT": 300,
    "CACHE_KEY_PREFIX": "superset_",
    "CACHE_REDIS_HOST": redis_host,
    "CACHE_REDIS_PORT": redis_port,
    "CACHE_REDIS_DB": 1,
}

DATA_CACHE_CONFIG = CACHE_CONFIG

CELERY_BEAT_SCHEDULER_EXPIRES = timedelta(weeks=1)


class CeleryConfig:
    broker_url = f"redis://{redis_host}:{redis_port}/0"
    imports = ("superset.sql_lab", "superset.tasks.scheduler")
    result_backend = f"redis://{redis_host}:{redis_port}/1"
    worker_prefetch_multiplier = 1
    task_acks_late = False
    beat_schedule = {
        "reports.scheduler": {
            "task": "reports.scheduler",
            "schedule": crontab(minute="*", hour="*"),
            "options": {"expires": int(CELERY_BEAT_SCHEDULER_EXPIRES.total_seconds())},
        },
        "reports.prune_log": {
            "task": "reports.prune_log",
            "schedule": crontab(minute=0, hour=0),
        },
    }


CELERY_CONFIG = CeleryConfig

db_user = os.getenv("DB_USER")
db_pass = os.getenv("DB_PASS")
db_host = os.getenv("DB_HOST")
db_port = os.getenv("DB_PORT")
db_name = os.getenv("DB_NAME")


SQLALCHEMY_DATABASE_URI = (
    f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
)
SQLALCHEMY_ECHO = True
FLASK_DEBUG = True
