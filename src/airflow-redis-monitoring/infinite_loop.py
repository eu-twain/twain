import time
import logging
import pytz
import redis
import os
import sys
import subprocess
import datetime
import requests
from requests.exceptions import HTTPError
from http import HTTPStatus
import psycopg2

STANDARD_WAITTIME = 15  # 15 SECONDS
AIRFLOW_RESTART_TIME = 60 * 3  # 3 MINUTES
PERMANENT_TASKS_OPERATORS = ['S3KeySensor']


def _get_redis_queue_length(redis_host: str, redis_port: str, password: str, db=1):
    """
    Functions checks the length of Airflow task queue: in redis it means the length of list key called default.
    :param redis_host:
    :param redis_port:
    :param password:
    :param db:
    :return:
    """
    r = redis.Redis(host=redis_host, port=redis_port, password=password, db=db)
    redis_queue_length = r.llen("default")
    return redis_queue_length


def _restart_airflow_workers_pods(log_handle):
    """
    Functions deletes the pods labeled as airflow worker. If those are part of a k8s deployment/statefulSet,
    replicas will recreate them.
    :param log_handle:
    :return:
    """
    process = subprocess.run(
        [
            "kubectl", "delete", "pods",
            "-n", "default",
            "-l", "app.kubernetes.io/component=worker,twain.component=airflow"
        ],
        capture_output=True)
    if process.returncode > 0:
        raise KeyError("Deleting airflow worker pod failed: {0}".format(process.returncode))
    else:
        log_handle.info(f"Restarted airflow worker pods at {datetime.datetime.now(tz=pytz.UTC)}")


def _get_airflow_running_task_count(log_handle, airflow_host: str, airflow_port: int,
                                    airflow_admin_username, airflow_admin_password):
    """
    Function checks if there is any task in worker that is still running
    using API endpoint http://twain-airflow:8080/api/v1/dags/~/dagRuns/~/taskInstances?state=running.

    Raises http error if the connection fails or authentication could not be achieved
    (for example when the airflow REST API accepts only airflow.api.auth.backend.session authentication).
    :param log_handle:
    :param airflow_host:
    :param airflow_port:
    :param airflow_admin_username:
    :param airflow_admin_password:
    :return:
    """
    response = requests.request(
        "GET",
        f"http://{airflow_host}:{airflow_port}/api/v1/dags/~/dagRuns/~/taskInstances?state=running",
        auth=(airflow_admin_username, airflow_admin_password)
    )
    response.raise_for_status()
    # naive approach with just counting running tasks is wrong as some of them might have been submitted before
    # redis queue restarted, preventing the monitor from deleting worker pod
    # we need to count the non-permanent tasks instead of response.json()['total_entries']
    tasks_running_count = \
        len([x for x in response.json()['task_instances'] if x['operator'] not in PERMANENT_TASKS_OPERATORS])
    log_handle.info(f"There are currently {tasks_running_count} tasks running.")
    return tasks_running_count


def _as_sql_list(input_list: list):
    """
    Function renders the list of elements as sql list.
    :param input_list:
    :return:
    """
    output = ",".join([f"'{x}'" for x in input_list])
    return "(" + output + ")"


def _get_airflow_running_task_count_from_db(log_handle, postgres_host: str, postgres_port: int,
                                            postgres_db: str, postgres_airflow_username: str,
                                            postgres_airflow_password: str):
    """
    Function checks Airflow DB directly for a count of running tasks
    that are not supposed to run permanently (like s3sensors).
    :param log_handle:
    :param postgres_host:
    :param postgres_port:
    :param postgres_db:
    :param postgres_airflow_username:
    :param postgres_airflow_password:
    :return:
    """
    connection = psycopg2.connect(
        host=postgres_host,
        port=postgres_port,
        database=postgres_db,
        user=postgres_airflow_username,
        password=postgres_airflow_password)
    cursor = connection.cursor()
    rendered_query = f"select count(1) as task_count from task_instance " +\
                     f"where operator not in {_as_sql_list(PERMANENT_TASKS_OPERATORS)} and state = 'running';"
    log_handle.info(f"Prepared sql query: \"{rendered_query}\".")
    cursor.execute(rendered_query)
    record = cursor.fetchall()
    tasks_running_count = record[0][0]
    log_handle.info(f"There are currently {tasks_running_count} non-permanent tasks running.")
    if cursor:
        cursor.close()
    if connection:
        connection.close()
    return tasks_running_count


def run(log):
    log.info("Started monitoring airflow queued tasks to identify stuck airflow workers")
    redis_password = os.getenv("REDIS_PASSWORD")
    redis_host = os.getenv("REDIS_HOST")
    redis_port = os.getenv("REDIS_PORT_NUMBER")
    airflow_service_host = os.getenv("AIRFLOW_WEBSERVER_HOST")
    airflow_service_port = int(os.getenv("AIRFLOW_WEBSERVER_PORT_NUMBER"))
    airflow_admin_username = os.getenv("AIRFLOW_USER")
    airflow_admin_password = os.getenv("AIRFLOW_PASSWORD")
    postgres_host = os.getenv("AIRFLOW_POSTGRES_DB_HOST")
    postgres_port = int(os.getenv("AIRFLOW_POSTGRES_DB_PORT"))
    postgres_db = os.getenv("AIRFLOW_POSTGRES_DB_NAME")
    postgres_airflow_username = os.getenv("AIRFLOW_POSTGRES_USER")
    postgres_airflow_password = os.getenv("AIRFLOW_POSTGRES_PASSWORD")
    just_restarted = False
    while True:
        time.sleep(STANDARD_WAITTIME if not just_restarted else max([AIRFLOW_RESTART_TIME, STANDARD_WAITTIME]))
        just_restarted = False
        try:
            redis_queue_length = _get_redis_queue_length(redis_host, redis_port, redis_password)
        except redis.exceptions.ConnectionError as connection_error:
            log.error(f"redis service at {redis_host}:{redis_port} does not respond"
                      f" at {datetime.datetime.now(tz=pytz.UTC)}")
            continue
        log.info(f"There are {redis_queue_length} elements in redis queue.")
        if redis_queue_length >= 2:
            try:
                airflow_running_task_count = _get_airflow_running_task_count(
                    log, airflow_service_host, airflow_service_port, airflow_admin_username, airflow_admin_password)
            except HTTPError as exc:
                code = exc.response.status_code
                log.error(f"Error receiving count of running tasks from worker: {code}")
                if code == HTTPStatus.UNAUTHORIZED:
                    try:
                        airflow_running_task_count = _get_airflow_running_task_count_from_db(
                            log, postgres_host, postgres_port, postgres_db,
                            postgres_airflow_username, postgres_airflow_password
                        )
                    except Exception as e:
                        log.error(f"Error connecting to airflow database - action from admin required: {e}")
                        airflow_running_task_count = 1
                else:
                    log.error("Error connecting to airflow REST API - action from admin required.")
                    airflow_running_task_count = 1
            if airflow_running_task_count == 0:
                _restart_airflow_workers_pods(log)
                just_restarted = True


if __name__ == "__main__":
    logger = logging.getLogger('airflow-redis-monitoring')
    logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level="INFO", stream=sys.stdout)
    start_timestamp = datetime.datetime.now(tz=pytz.UTC)
    run(logger)
