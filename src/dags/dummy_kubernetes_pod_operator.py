import pendulum
from airflow.decorators import dag
from airflow.operators.empty import EmptyOperator
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from kubernetes.client import models as k8s


@dag(
    dag_id="dummy_kubernetes_pod_operator",
    description="Dummy dag to trigger basic kubernetes pod operator.",
    schedule=None,
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["MinIO", "Trino", "raw2bronze"]
)
def dummy_kubernetes_pod_operator() -> None:
    start = EmptyOperator(task_id='start')
    end = EmptyOperator(task_id='end')

    dummy_kpo = KubernetesPodOperator(
        # dag="dummy_kubernetes_pod_operator",
        name="hello-dry-run",
        image="debian",
        cmds=["bash", "-cx"],
        # arguments=["sleep 30", "echo hello", "echo", "10"],
        arguments=["echo hello && sleep 30 && echo hello_there && exit 0"],
        labels={"foo": "bar"},
        task_id="dry_run_demo",
        do_xcom_push=True,
        get_logs=True,
        retries=0,
        on_finish_action='keep_pod',
        in_cluster=True,
        startup_timeout_seconds=240,
        container_resources=k8s.V1ResourceRequirements(
            limits={"memory": "1G", "cpu": "2.0"}
        ),
        # onFinishAction.keep_pod # KEEP_POD
    )

    start >> dummy_kpo >> end


dummy_kubernetes_pod_operator()
