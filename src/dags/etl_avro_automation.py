import os
import sys
import pendulum
import logging

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")

from airflow.decorators import dag, task, task_group
from airflow.models.baseoperator import chain
from airflow.models import Variable
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
import botocore
from airflow import XComArg

from airflow.providers.amazon.aws.sensors.s3 import S3KeySensor
from airflow.providers.amazon.aws.operators.s3 import (
    S3ListOperator,
    S3DeleteObjectsOperator,
)
from airflow.utils.trigger_rule import TriggerRule
from avro_generator import csv_to_avro, TEN_MEGABYTE_LIMIT


BUCKET: str = Variable.get("avro_generation_bucket", "avro-generation-public")
KEY_SENSOR_WILDCARD_CSV: str = "*.csv"

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


@dag(
    dag_id="automatic_avro_generation",
    description="",
    schedule="@continuous",
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),

    catchup=False,
    tags=["MinIO", "Trino", "avsc"],
)
def automatic_avro_generation() -> None:

    sensor_csv = S3KeySensor(
        task_id="s3_key_sensor_csv",
        aws_conn_id="aws_minio",
        bucket_name=BUCKET,
        bucket_key=KEY_SENSOR_WILDCARD_CSV,
        wildcard_match=True,
    )

    list_csv_files = S3ListOperator(
        # Get Files List and store in the XCom
        task_id="list_csv_files",
        aws_conn_id="aws_minio",
        bucket=BUCKET,
        prefix=KEY_SENSOR_WILDCARD_CSV,
        # the delimiter marks key hierarchy, e.g. if "/" then doesn't traverse subfolders
        delimiter="",
        apply_wildcard=True,
    )

    def store_generation_error(s3_hook: S3Hook, original_file_name: str, error_reason: str):
        """
        Function stores a file on s3 and uses error_reason as its content and
        replaces its format as ".avsc_failed_generation.txt".
        :param s3_hook: s3 hook pointing to MinIO
        :param original_file_name: the original file name
        :param error_reason: the reason the avro schema generation failed
        :return:
        """
        avsc_path = original_file_name.replace('.csv', '.avsc_failed_generation.txt')
        s3_hook.load_string(
            string_data=error_reason,
            key=avsc_path,
            bucket_name=BUCKET,
            replace=True,
        )

    @task
    def generate_avsc(csv_files: list, **context):
        """
        Function generates the avsc schema or generation error report and stores it on MinIO.
        No "folder" structure is supported.
        Function downloads csv files smaller than TEN_MEGABYTE_LIMIT to Airflow worker to tmp directory
        and removes them after the schema was created.
        :param csv_files: a list of file names (no folders)
        :param context: standard Airflow context
        :return: None
        """
        source_s3 = S3Hook(aws_conn_id="aws_minio")
        for f in csv_files:
            try:
                file_metadata = source_s3.conn.head_object(Bucket=BUCKET, Key=f)
            except botocore.exceptions.ClientError as file_does_not_exist:
                error_reason = f"File {f} was removed from bucket {BUCKET} before the bot could start processing"
                logger.info(error_reason)
                store_generation_error(source_s3, original_file_name=f, error_reason=error_reason)
                continue
            if "ContentLength" in file_metadata.keys() and file_metadata["ContentLength"] > TEN_MEGABYTE_LIMIT:
                error_reason = f"File {f} is too large, it has exceeded 10MB limit"
                logger.info(error_reason)
                store_generation_error(source_s3, original_file_name=f, error_reason=error_reason)
                continue

            csv_data = source_s3.read_key(f, bucket_name=BUCKET)

            fw_path = os.path.join("/tmp/", f)
            with open(fw_path, "w") as fw:
                fw.write(csv_data)

            _, output_file_format = csv_to_avro(fw_path)

            avsc_path = fw_path.replace('.csv', '.' + output_file_format)
            print("avsc_path", avsc_path)

            with open(avsc_path) as avsc_f:
                source_s3.load_string(
                    string_data=avsc_f.read(),
                    key=avsc_path.replace('/tmp/', ''),
                    bucket_name=BUCKET,
                    replace=True,
                )
            if os.path.exists(avsc_path):
                source_s3.log.info(f"Removing worker's avro schema file: {avsc_path}")
                os.remove(avsc_path)
            if os.path.exists(fw_path):
                source_s3.log.info(f"Removing worker's data file: {avsc_path}")
                os.remove(fw_path)

    @task_group
    def delete_files(s3_files: XComArg) -> None:
        S3DeleteObjectsOperator.partial(
            trigger_rule=TriggerRule.NONE_SKIPPED,
            task_id="delete_files_from_origin",
            aws_conn_id="aws_minio",
            bucket=BUCKET,
        ).expand(keys=s3_files)

    generate_avro_schema = generate_avsc(csv_files=list_csv_files.output)
    delete_csv_files_from_origin = delete_files(list_csv_files.output)

    chain(
        sensor_csv,
        list_csv_files,
        generate_avro_schema,
        delete_csv_files_from_origin,
    )


automatic_avro_generation()
