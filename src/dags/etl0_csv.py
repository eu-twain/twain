"""
The DAG processes CSV file gets uploaded to the Temporary Storage.
It gets triggered by previous AVRO DAG:
if *.avsc files are processes successfully then process *.csv.
"""
# workaround for lack of PATH setup
import os
import sys
sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402


import datetime
import pytz
import pendulum
import json
import sys
import os
import logging
from typing import List
from airflow import XComArg
from airflow.configuration import AirflowConfigParser
from airflow.decorators import dag, task, task_group
from airflow.models import Variable, BaseOperator
from airflow.models.baseoperator import chain
from airflow.providers.amazon.aws.operators.s3 import S3ListOperator, S3CopyObjectOperator, S3DeleteObjectsOperator
from airflow.providers.amazon.aws.sensors.s3 import S3KeySensor
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.common.sql.operators.sql import SQLExecuteQueryOperator, SQLTableCheckOperator
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.utils.trigger_rule import TriggerRule
from src.common.utils.xcom_utilities import xcom_finished_ingestions
from src.common.operators.flatten_results_operator import FlattenResultsOperator
from src.common.operators.filter_non_applicable_csv_files_operator import FilterNonApplicableCSVFilesOperator
from src.common.functions.auxiliary_functions_csv import store_dead_letters, create_dir_if_not_exists, \
    get_csv_table_name, drop_bucket_name, transform_data_location_to_visible_by_spark
from src.common.utils.statics import DAGRUN_FAILED_PARENT_TASKS, DESTINATION_AVRO_FILE_PATH, TABLE_SUFFIX_PATTERN, \
    INGESTION_FAILURE_MODE, TABLE_SUFFIX_PATTERN

from utils.semi_avro_parsing import sanitize_filename

BUCKET_DATALAKE: str = Variable.get("bucket_datalake", "datalake")
BUCKET_PUBLIC: str = Variable.get("bucket_public", "ingestion-public")
BUCKET_QUARANTINE: str = Variable.get("bucket_quarantine", "quarantine")
BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")
AVRO_TABLE_PREFIX: str = Variable.get("avro_table_prefix", "avro_")
CSV_TABLE_PREFIX: str = Variable.get("csv_table_prefix", "csv_")

KEY_SENSOR_WILDCARD_CSV: str = "*.csv"

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


@dag(
    dag_id="etl0_csv",
    description="Move CSV file(s) to permanent storage and CREATE TABLE accordingly in Trino.",
    schedule=None,
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["MinIO", "Trino", "csv"],
    render_template_as_native_obj=True
)
def etl0_taskflow_api() -> None:
    """ ETL 0: move CSV file(s) uploaded to the temporary location (MinIO bucket)
    to permanent location (Ceph) and create Trino table(s) for it.
    """

    # [START tasks]

    def read_conf_populate_xcom(input_params):
        logger.info(f'input_params type: {type(input_params)}')
        if isinstance(input_params, AirflowConfigParser):
            input_params = input_params.as_dict()
        elif isinstance(input_params, str):
            logger.info(f"input_params: {input_params}")
            #  input_params = input_params.replace("\'", "\"")
            input_params = json.loads(input_params)
        for key, value in input_params.items():
            logger.info(f"    key: \'{key}\'; value: \'{value}\'")
        return input_params

    read_conf_populate_xcom_op = PythonOperator(
        task_id='parse_dagrun_config',
        python_callable=read_conf_populate_xcom,
        provide_context=True,
        op_kwargs={
            'input_params': '{{ dag_run.conf }}'
        }
    )

    def _get_expected_datafile_file_name(file_location_split: List):
        """ This function maps avsc file name with name of the file(s) containing data defined in said avcs.

        For PoC it is the same file name as avcs (but ending with its format) so only a one2one mapping.
        :param file_location_split:
        :return:
        """
        return ".".join(file_location_split[-1].split(".")[:-1])

    def get_expected_csv_names(input_config):
        logger.info(f"type input_config: {type(input_config)}")
        logger.info(f"input_config: {input_config}")
        output = list()
        for x_key, x_value in input_config.items():
            original_file_path_split = x_value['original_file_path'].split("/")
            expected_file_name = _get_expected_datafile_file_name(original_file_path_split)
            expected_file_location = "/".join(original_file_path_split[3:-1])
            if expected_file_location:
                output.append("/".join([expected_file_location, expected_file_name]) + ".csv")
            else:
                output.append(expected_file_name + ".csv")
        return output

    get_expected_csv_names_op = PythonOperator(
        task_id='get_expected_csv_names',
        python_callable=get_expected_csv_names,
        provide_context=True,
        op_kwargs={
            'input_config': "{{ ti.xcom_pull(task_ids='parse_dagrun_config') }}"
        }
    )

    def _download_3s_file(s3_file_path: str):
        file_path = "/".join(s3_file_path.split("/")[3:])
        bucket_name = s3_file_path.split("/")[2]
        logger.info(f"Downloading file {file_path} from bucket {bucket_name}.")
        hook = S3Hook("aws_minio")
        local_path = '/tmp/airflow/data/avro_schemas'
        create_dir_if_not_exists(local_path)
        file_name = hook.download_file(key=file_path, bucket_name=bucket_name, local_path=local_path)
        return file_name

    def _read_local_json_file(local_json_file: str):
        return json.load(open(local_json_file))

    @task()
    def build_sql_create_table(csv_file_config: dict) -> list:
        """ Build an SQL statement CREATE TABLE per each file.
        File list example:
            ['V52_SampleData.csv', 'DTU/blade_strain_gauge_data_sample.csv']
        """
        sql_list = []
        csv_separator = "\t"
        skip_header_line_count = 1
        for table_name, table_config in csv_file_config.items():
            fname = table_config["datalake_file_path"]
            enriched_file_location = table_config["enriched_file_path"]
            ingestion_timestamp = table_config['ingestion_timestamp']
            expiry_date = table_config.get('expiry_date', None)
            comment_text = ""
            if expiry_date:
                comment_text = json.dumps({'expiry_date': expiry_date})
            temp_local_enriched_file_location = _download_3s_file(enriched_file_location)
            logger.info(f"temp_local_enriched_file_location: {temp_local_enriched_file_location}")
            enriched_avsc_content = _read_local_json_file(temp_local_enriched_file_location)
            # TODO: remove the temporary file 'temp_local_enriched_file_location' once it outlives its usefulness
            logger.info(f"enriched_avsc_content: {json.dumps(enriched_avsc_content, indent=2)}")
            if 'csv_separator' in enriched_avsc_content.keys():
                csv_separator = enriched_avsc_content['csv_separator']
            if 'skip_header_line_count' in enriched_avsc_content.keys():
                skip_header_line_count = enriched_avsc_content['skip_header_line_count']
            elif 'header_line_count' in enriched_avsc_content.keys():
                skip_header_line_count = enriched_avsc_content['header_line_count']

            file_name = fname.split("/")[-1]
            sanitized_filename = sanitize_filename(file_name)
            csv_table = get_csv_table_name(sanitized_filename, ingestion_timestamp)
            csv_table_name = get_csv_table_name(sanitized_filename, ingestion_timestamp, table_name_mode=False)

            sanitized_avro_tablename = sanitized_filename.split('.')[0]
            # reference to AVRO table
            avro_table = f"{AVRO_TABLE_PREFIX}{sanitized_avro_tablename}_{ingestion_timestamp}"

            csv_file_path = f"s3://{BUCKET_DATALAKE}/{csv_table_name}"
            sql_list.append(f"""
                CREATE TABLE IF NOT EXISTS {csv_table} (
                    LIKE {avro_table}
                )
                COMMENT '{comment_text}'
                WITH (
                    format = 'CSV',
                    csv_separator = '{csv_separator}',
                    skip_header_line_count = {skip_header_line_count},
                    external_location = '{csv_file_path}'
                )
                """)
        return sql_list

    def copy_kwargs(file: str) -> dict:
        """ Utility function to make kwargs that matches signature of S3CopyObjectOperator. """
        # Pay attention to PATH dir_name/file_name.csv
        # Note!!! Trino Table must be pointed to enclosing DIRECTORY, not file_name.csv file!
        real_file_full_path = ".".join(file.split(".")[:-1])
        file_ingestion_timestamp = file.split(".")[-1]
        file_name = file.split("/")[-1]  # result: File_Name.csv.timestamp
        table_name = sanitize_filename(file_name.split('.')[0].lower()) + "_" + file_ingestion_timestamp  # result: file_name_timestamp
        csv_table_path = f"{CSV_TABLE_PREFIX}{table_name}/{file_name}"  # result: csv_file_name_timestamp/File_Name.csv.timestamp

        # TODO: some ideas to consider:
        # * lowercase file names: file.lower()
        # * replace spaces with underscores: file.replace(" ", "_")
        return {
            "source_bucket_key": real_file_full_path,
            "dest_bucket_key": csv_table_path
        }

    @task_group()
    def copy_files(s3_files: XComArg) -> None:
        S3CopyObjectOperator.partial(
            task_id="copy_files_to_datalake",
            aws_conn_id="aws_minio",
            source_bucket_name=f"{BUCKET_PUBLIC}",
            dest_bucket_name=f"{BUCKET_DATALAKE}",
        ).expand_kwargs(s3_files.map(copy_kwargs))

    @task_group()
    def delete_files(s3_files: XComArg) -> None:
        S3DeleteObjectsOperator.partial(
            task_id="delete_files_from_origin",
            aws_conn_id="aws_minio",
            bucket=f"{BUCKET_PUBLIC}",
        ).expand(keys=s3_files)

    # [END tasks]

    # A Sensor: monitor *.csv file(s) to be uploaded to INGESTION-PUBLIC bucket.
    sensor_csv = S3KeySensor(
        task_id="s3_key_sensor_csv",
        aws_conn_id="aws_minio",
        bucket_name=f"{BUCKET_PUBLIC}",
        bucket_key=KEY_SENSOR_WILDCARD_CSV,
        wildcard_match=True
    )

    s3_files = S3ListOperator(
        # Get Files List and store in the XCom
        task_id="list_s3_files",
        aws_conn_id="aws_minio",
        bucket=f"{BUCKET_PUBLIC}",
        prefix=KEY_SENSOR_WILDCARD_CSV,
        # the delimiter marks key hierarchy, e.g. if "/" then doesn't traverse subfolders
        delimiter="",
        apply_wildcard=True
    )

    filter_applicable_csv_files_op = FilterNonApplicableCSVFilesOperator(
        task_id='filter_applicable_csv_files',
        listing_files_task_id='list_s3_files',
        expected_files_task_id='get_expected_csv_names'
    )

    def get_ingestion_timestamp(input_config: dict):
        if not input_config:
            return f'{datetime.datetime.now(tz=pytz.UTC).strftime(TABLE_SUFFIX_PATTERN)}'
        return list(input_config.values())[0]['ingestion_timestamp']

    get_ingestion_timestamp_op = PythonOperator(
        task_id='get_ingestion_timestamp_op',
        python_callable=get_ingestion_timestamp,
        provide_context=True,
        op_kwargs={
            'input_config': "{{ ti.xcom_pull(task_ids='parse_dagrun_config') }}"
        }
    )

    def _get_applicable_csv_files(**context):
        """
        Function returns the list of applicable csv file names (csv files which name match avsc
        file names in the same path/URI on s3) extended with a dot and the ingestion timestamp.
        :param context: standard Airflow context
        :return: a list of applicable csv files which filenames are extended with a dot and ingestion timestamp
        """
        applicable_csv_files = context['ti'].xcom_pull(task_ids='filter_applicable_csv_files', key='expected_csv_files')
        ingestion_timestamp = context['ti'].xcom_pull(task_ids='get_ingestion_timestamp_op')
        return [x + "." + ingestion_timestamp for x in applicable_csv_files]

    get_applicable_csv_files_op = PythonOperator(
        task_id='get_applicable_csv_files',
        python_callable=_get_applicable_csv_files,
        provide_context=True
    )

    def _get_missing_csv_files(**context):
        """
        Function returns the list of missing csv file names (csv files names which would match avsc
        file names in the same path/URI on s3 if said csv file have existed).
        :param context: standard Airflow context
        :return: a list of applicable csv files which filenames are extended with a dot and ingestion timestamp
        """
        return context['ti'].xcom_pull(task_ids='filter_applicable_csv_files', key='missing_csv_files')

    get_missing_csv_files_op = PythonOperator(
        task_id='get_missing_csv_files',
        python_callable=_get_missing_csv_files,
        provide_context=True
    )

    store_dead_letters_missing_csv_files = PythonOperator(
        task_id='store_dead_letters',
        provide_context=True,
        python_callable=store_dead_letters,
        trigger_rule=TriggerRule.ALL_DONE,
        op_kwargs={
            'bucket_name': BUCKET_PUBLIC,
            'mode': INGESTION_FAILURE_MODE,
            'upstream_task_id': "get_missing_csv_files"
        }
    )

    filter_applicable_csv_files_op.set_upstream(s3_files)
    get_applicable_csv_files_op.set_upstream(filter_applicable_csv_files_op)
    get_missing_csv_files_op.set_upstream(filter_applicable_csv_files_op)
    store_dead_letters_missing_csv_files.set_upstream(get_missing_csv_files_op)

    copy_files_to_datalake = copy_files(get_applicable_csv_files_op.output)

    def _get_applicable_csv_files_config(**context):
        return context['ti'].xcom_pull(task_ids='filter_applicable_csv_files', key='expected_csv_files_config')

    get_applicable_csv_files_config_op = PythonOperator(
        task_id='get_applicable_csv_files_config',
        python_callable=_get_applicable_csv_files_config,
        provide_context=True
    )
    get_applicable_csv_files_config_op.set_upstream(filter_applicable_csv_files_op)
    sql_list = build_sql_create_table(get_applicable_csv_files_config_op.output)

    # TODO: consider NOT using CREATE TABLE IF NOT EXISTS
    # trino.exceptions.TrinoUserError: TrinoUserError(type=USER_ERROR, name=TABLE_ALREADY_EXISTS,
    # message="line 2:17: Table 'hive.twain.csv_v52_sampledata' already exists",
    # query_id=20240214_144115_00007_qbqmr)
    @task_group()
    def create_trino_tables(sql_text_list: XComArg) -> None:
        SQLExecuteQueryOperator.partial(
            task_id="trino_create_table_csv",
            conn_id="twain_trino",
        ).expand(sql=sql_text_list)

    create_trino_tables_tg = create_trino_tables(sql_list)

    def _flatten_get_applicable_csv_files_config(input_dict: dict):
        if not input_dict:
            return []
        return [
            get_csv_table_name(x_key, x_value['ingestion_timestamp'], force_lowercase=False)
            for x_key, x_value in input_dict.items()
        ]

    flatten_get_applicable_csv_files_config = PythonOperator(
        task_id='flatten_get_applicable_csv_files_config',
        python_callable=_flatten_get_applicable_csv_files_config,
        provide_context=True,
        op_kwargs={
            "input_dict": "{{ ti.xcom_pull(task_ids='get_applicable_csv_files_config') }}"
        }
    )
    flatten_get_applicable_csv_files_config.set_upstream(get_applicable_csv_files_config_op)

    class TwainSQLTableCheckOperator(SQLTableCheckOperator):
        """
        Class overwriting the SQLTableCheckOperator in order to push the table name to xcom for downstream processing
        """
        def execute(self, context):
            context['ti'].xcom_push('table_name', self.table)
            context['ti'].xcom_push(DESTINATION_AVRO_FILE_PATH, "dummy_value to be compliant")
            super().execute(context)

    @task_group()
    def validate_trino_tables(table_name_list: XComArg) -> None:
        TwainSQLTableCheckOperator.partial(
            task_id="validate_trino_table_csv",
            conn_id="twain_trino",
            checks={
                "row_count_check": {"check_statement": "COUNT(*) > 0"}
            }
        ).expand(table=table_name_list)

    validate_trino_tables_tg = validate_trino_tables(flatten_get_applicable_csv_files_config.output)
    mark_finished_str_csv_ingestions = PythonOperator(
        task_id='mark_finished_str_csv_ingestions',
        provide_context=True,
        python_callable=xcom_finished_ingestions,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        op_kwargs={
            'xcom_key_name': "table_name"
        }
    )
    flatten_avro_results = FlattenResultsOperator(
        task_id='flatten_str_csv_results_ingestions',
        # provide_context=True,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        marking_task_id='mark_finished_str_csv_ingestions'
    )

    def get_correct_tables_data_files(xcom_key: str, **context):
        """
        Function retrieves the s3 URIs of the files listed in xcom result xcom_key of
        task flatten_str_csv_results_ingestions
        :param xcom_key: the name of the result of flatten_str_csv_results_ingestions
        :param context: standard Airflow context
        :return: a list of s3 URI without the bucket name
        """
        output = list()
        csv_table_names = context['ti'].xcom_pull(
            task_ids='flatten_str_csv_results_ingestions', key=xcom_key)
        csv_files_config = context['ti'].xcom_pull(
            task_ids='get_applicable_csv_files_config')
        logger.info(f"csv_table_names {csv_table_names}")
        logger.info(f"csv_files_config: {csv_files_config}")
        sanitized_csv_files_config = {
            sanitize_filename(k): v
            for k, v in csv_files_config.items()
        }
        csv_files_config.update(sanitized_csv_files_config)
        for csv_table_name in csv_table_names:
            logger.info(f"csv_table_name: {csv_table_name}")
            reverted_csv_table_name = csv_table_name[len(CSV_TABLE_PREFIX):]
            logger.info(f"reverted_csv_table_name - {reverted_csv_table_name}")
            if reverted_csv_table_name in csv_files_config.keys():
                candidate = drop_bucket_name(
                    csv_files_config[reverted_csv_table_name]['original_data_path']
                )
                if candidate not in output:
                    output.append(candidate)
        return output

    get_correct_tables_data_files_op = PythonOperator(
        task_id='get_correct_tables_data_files',
        provide_context=True,
        python_callable=get_correct_tables_data_files,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        op_kwargs={
            "xcom_key": "correct_avro_files"
        }
    )

    get_incorrect_avros_op = PythonOperator(
        task_id='get_incorrect_avros',
        provide_context=True,
        python_callable=get_correct_tables_data_files,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        op_kwargs={
            "xcom_key": "incorrect_avro_files"
        }
    )

    get_correct_tables_data_files_op.set_upstream(flatten_avro_results)
    get_incorrect_avros_op.set_upstream(flatten_avro_results)

    delete_files_from_origin = delete_files(get_correct_tables_data_files_op.output)

    def procure_etl1_config(**context):
        """
        Function procures the configuration for downstream DAG (etl_step2_dag).
        It combines the names of the tables created with a successful ingestions with the applicable csv files config
        in order to combine a csv table with an enriched avro schema that defines it. As tables have suffixes
        indicating the ingestion timestamp, there is a need to transform table name back to original
        file name without that suffix.

        :param context: a standard Airflow context
        :return: a dictionary where the keys are the locations of enriched avro schema files and the values
                 are a list of tables for which the schema is applicable
        """
        output = dict()
        # correct_avro_tables is a list of table names, ending with suffixes, to get to the csv file name
        # the function has to get rid of them
        csv_table_names = context['ti'].xcom_pull(
            task_ids='flatten_str_csv_results_ingestions', key='correct_avro_tables')
        csv_files_config = context['ti'].xcom_pull(
            task_ids='get_applicable_csv_files_config')
        logger.info(f"csv_table_names {csv_table_names}")
        sanitized_csv_files_config = {
            sanitize_filename(k): v
            for k, v in csv_files_config.items()
        }
        csv_files_config.update(sanitized_csv_files_config)
        logger.info(f"csv_files_config {csv_files_config}")
        for csv_table_name in csv_table_names:
            reverted_csv_table_name = csv_table_name[len(CSV_TABLE_PREFIX):]
            reverted_csv_table_name = "_".join(reverted_csv_table_name.split("_")[:-1])
            logger.info(f"reverted_csv_table_name {reverted_csv_table_name}")
            if reverted_csv_table_name in csv_files_config.keys():
                s3a_enriched_file_location = transform_data_location_to_visible_by_spark(
                    csv_files_config[reverted_csv_table_name]['enriched_file_path'])
                csv_table_expiry_date = csv_files_config[reverted_csv_table_name].get("expiry_date", None)
                csv_table_name = csv_table_name.lower()
                csv_table_config = {
                    "csv_table_name": csv_table_name,
                    "csv_table_expiry_date": csv_table_expiry_date,
                }
                try:
                    output[s3a_enriched_file_location] = output[
                        s3a_enriched_file_location
                    ].append(csv_table_config)
                except KeyError:
                    output[s3a_enriched_file_location] = [csv_table_config]

        return output

    procure_etl1_config_op = PythonOperator(
        task_id='procure_etl1_config',
        provide_context=True,
        python_callable=procure_etl1_config,
        trigger_rule=TriggerRule.NONE_SKIPPED
    )

    # task [TRIGGERING DOWNSTREAM DAG] STARTS
    trigger_csv_dag = TriggerDagRunOperator(
        task_id="trigger_etl1_dag",
        trigger_dag_id="etl_step1_dag",
        conf=procure_etl1_config_op.output
    )

    # task [TRIGGERING DOWNSTREAM DAG] ENDS

    def copy_quarantine_kwargs(file: str) -> dict:
        """ Utility function to make kwargs that matches signature of S3CopyObjectOperator.
        Copy original AVSC file to quarantine. Remove s3://Bucket_name from paths
        """
        if file.startswith("s3://"):
            file = file[len("s3://"):]
            file = '/'.join(file.split("/")[1:])
        return {
            "source_bucket_key": file,
            "dest_bucket_key": file.split("/")[-1]
        }

    @task_group()
    def move_incorrect_csvs_to_quarantine(s3_files: XComArg) -> None:
        task_copy = S3CopyObjectOperator.partial(
            task_id="copy_incorrect_csvs_to_quarantine",
            aws_conn_id="aws_minio",
            source_bucket_name=f"{BUCKET_PUBLIC}",
            dest_bucket_name=f"{BUCKET_QUARANTINE}",
        ).expand_kwargs(s3_files.map(copy_quarantine_kwargs))
        task_delete = S3DeleteObjectsOperator.partial(
            task_id="remove_incorrect_csv_from_source_bucket",
            aws_conn_id="aws_minio",
            bucket=f"{BUCKET_PUBLIC}",
        ).expand(keys=s3_files)
        task_copy >> task_delete

    store_dead_letters_empty_csv_files = PythonOperator(
        task_id='store_dead_letters_empty_csv_files',
        provide_context=True,
        python_callable=store_dead_letters,
        trigger_rule=TriggerRule.ALL_DONE,
        op_kwargs={
            'bucket_name': BUCKET_PUBLIC,
            'mode': INGESTION_FAILURE_MODE,
            'upstream_task_id': "get_incorrect_avros"
        }
    )

    move_incorrect_csvs_to_quarantine_tg = \
        move_incorrect_csvs_to_quarantine(get_incorrect_avros_op.output)
    move_incorrect_csvs_to_quarantine_tg.set_upstream(get_incorrect_avros_op)
    store_dead_letters_empty_csv_files.set_upstream(get_incorrect_avros_op)

    # task [MARKING ETL0 DAGRUN STATUS] STARTS
    @task(trigger_rule=TriggerRule.ALL_DONE)
    def mark_run_end_state(validation_tasks_names: List, **context):
        failed_tasks_list = list()
        lookup_keys = [DAGRUN_FAILED_PARENT_TASKS, "missing_csv_files"]
        for lookup_key in lookup_keys:
            for x in validation_tasks_names:
                logger.info(f"Checking xcom key {lookup_key} for task {x}.")
                failed_tasks = context["ti"].xcom_pull(task_ids=x, key=lookup_key)
                if failed_tasks:
                    failed_tasks_list.extend(failed_tasks)
        logger.info(f'failed_tasks_list: {failed_tasks_list}')
        context['ti'].xcom_push('failed_transformations', failed_tasks_list)
        if len(failed_tasks_list) > 0:
            raise KeyError(f'Marking DAG as failed as {len(failed_tasks_list)} ingestion threads failed.')

    mark_run_end_state_task = mark_run_end_state(
        validation_tasks_names=[
            'mark_finished_str_csv_ingestions',
            'filter_applicable_csv_files'
        ]
    )
    mark_run_end_state_task.set_upstream(move_incorrect_csvs_to_quarantine_tg)
    mark_run_end_state_task.set_upstream(delete_files_from_origin)
    mark_run_end_state_task.set_upstream(trigger_csv_dag)

    chain(
        read_conf_populate_xcom_op,
        get_expected_csv_names_op,
        get_ingestion_timestamp_op,
        sensor_csv,
        s3_files,
        copy_files_to_datalake,
        sql_list,
        create_trino_tables_tg,
        validate_trino_tables_tg,
        mark_finished_str_csv_ingestions,
        flatten_avro_results,
        delete_files_from_origin,
        procure_etl1_config_op,
        trigger_csv_dag
    )


etl0_taskflow_api()
