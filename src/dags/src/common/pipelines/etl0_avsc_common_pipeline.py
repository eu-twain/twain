"""
The DAG continuously monitors AVSC file gets uploaded to the Temporary Storage.
"""
# workaround for lack of PATH setup
import os
import sys

import pytz

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402


import logging
import pendulum
import sys
import json
from json.decoder import JSONDecodeError
import datetime
import pytz
import random
from typing import Dict, List
from airflow.utils.session import provide_session
from airflow import XComArg
from airflow.decorators import dag, task, task_group
from airflow.models import Variable
from airflow.models.baseoperator import chain
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.amazon.aws.operators.s3 import S3ListOperator, S3CopyObjectOperator, S3DeleteObjectsOperator
from airflow.providers.amazon.aws.sensors.s3 import S3KeySensor
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.common.sql.operators.sql import SQLExecuteQueryOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.python import PythonOperator
from airflow.operators.empty import EmptyOperator
from src.common.utils.xcom_utilities import xcom_finished_ingestions, get_enriched_incorrect_avros,\
    get_enriched_correct_avros, comply_file_names_with_s3delete, comply_file_names_with_s3delete_tuple
from src.common.utils.statics import DESTINATION_AVRO_FILE_PATH, SOURCE_AVRO_FILE_PATH, DAGRUN_FAILED_PARENT_TASKS, \
    ENRICHMENT_FAILED, VALIDATE_AVRO_SCHEMAS_CORRECT_KEY, VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY,  \
    INGESTION_FAILURE_MODE, TABLE_SUFFIX_PATTERN
from src.common.operators.twain_s3_operators import TwainS3FileTransformOperator, TwainS3FileTransformOperatorEnhanced
from src.common.operators.flatten_results_operator import FlattenResultsOperator
from src.common.operators.flatten_enrichment_results_operator import FlattenEnrichmentResultsOperator
from src.common.functions.auxiliary_functions_avsc import store_dead_letters, get_incorrect_avros
from utils.exceptions import TwainCustomException, DATE_PARSING_EXCEPTION
from utils.semi_avro_parsing import parse_non_static_avro_schema, sanitize_filename
from utils.semi_avro_retention_policies import get_expiry_date_from_avsc


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


BUCKET_DATALAKE: str = Variable.get("bucket_datalake", "datalake")
BUCKET_WF_OPERATORS: str = Variable.get("bucket_wf_operators", "wf-operators")
BUCKET_QUARANTINE: str = Variable.get("bucket_quarantine", "quarantine")
BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")
AVRO_TABLE_PREFIX: str = Variable.get("avro_table_prefix", "avro_")
ENRICH_AVRO_TABLE_PREFIX: str = Variable.get("enrich_avro_table_prefix", "enriched_")
AVSC_W_STRINGS_PREFIX: str = Variable.get("avsc_file_with_all_types_strings_prefix", "str_")

KEY_SENSOR_WILDCARD_AVSC: str = "*.avsc"


def etl0_common_taskflow_api(bucket_name: str, downstream_dag_name: str) -> None:
    """ ETL 0: move AVSC file(s) uploaded to the temporary location (MinIO bucket)
    to permanent location (Ceph) and create Trino table(s) for it.
    """

    # A Sensor: monitor *.avsc file(s) to be uploaded to INGESTION-PUBLIC bucket.
    sensor_avsc = S3KeySensor(
        task_id="s3_key_sensor_avsc",
        aws_conn_id="aws_minio",
        bucket_name=f"{bucket_name}",
        bucket_key=KEY_SENSOR_WILDCARD_AVSC,
        wildcard_match=True
    )

    # A native s3 operator listing avro files in the INGESTION-PUBLIC bucket
    s3_files = S3ListOperator(
        # Get Files List and store in the XCom
        task_id="list_s3_files",
        aws_conn_id="aws_minio",
        bucket=f"{bucket_name}",
        prefix=KEY_SENSOR_WILDCARD_AVSC,
        # the delimiter marks key hierarchy, e.g. if "/" then doesn't traverse subfolders
        delimiter="",
        apply_wildcard=True
    )

    @task()
    def filter_out_identical_target_trino_table_names(avsc_file_list: list, **context):
        """
        Task filters out the file names that will have the same name in Trino and picks only one of them
        for further processing, the next ingestion cycles will ingest the duplicates, one file per ingestion cycle.
        :param avsc_file_list: a list of avro schema locations on the bucket
        :return: a list of tuples consisting of avro_path and ingestion_timestamp
        """
        ingestion_timestamp = context["ti"].xcom_pull("get_ingestion_timestamp")
        output = list()
        file_name_counter = dict()
        for avro_path in avsc_file_list:
            avro_name = sanitize_filename(avro_path.split('/')[-1])
            try:
                file_name_counter[avro_name.lower()]
            except KeyError as ke:
                file_name_counter[avro_name.lower()] = list()
            file_name_counter[avro_name.lower()].append(avro_path)
        logger.info(f"Results of counting target Trino tables duplicates: {file_name_counter}")
        for file_name_lower in file_name_counter.keys():
            if len(file_name_counter[file_name_lower]) == 1:
                output.append((file_name_counter[file_name_lower][0], ingestion_timestamp))
            else:
                logger.info(f"More than one option to choose for Trino table {file_name_lower}, "
                            f"selection at random commences for a list: {file_name_counter[file_name_lower]}.")
                random.seed()
                selected_index = random.randint(0, len(file_name_counter[file_name_lower]) - 1)
                output.append((file_name_counter[file_name_lower][selected_index], ingestion_timestamp))
        return output

    # A Task building parameters for downstream DAG
    @task(multiple_outputs=True)
    def build_avro_params(avsc_tuples_list: list, **context) -> dict:
        """ Build a dict of table name : AVSC file S3 path.
        File list example:
            ['V52_SampleData.avsc', 'ssp/blade_strain_gauge_data_sample.avsc']
        """
        avro_dict = {}
        ingestion_timestamp = context["ti"].xcom_pull("get_ingestion_timestamp")
        logger.info(f"ingestion_timestamp: {ingestion_timestamp}")
        for avsc_tuple in avsc_tuples_list:
            fname = avsc_tuple[0]
            expiry_date_string = avsc_tuple[1]
            datalake_file_path = f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{fname.split('/')[-1]}"
            enriched_file_path = f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{ENRICH_AVRO_TABLE_PREFIX}{fname.split('/')[-1]}"
            expected_table_name = fname.split("/")[-1].split(".")[0].replace("-", "_")
            avro_dict[expected_table_name] = {
                'original_file_path': fname,
                'datalake_file_path': datalake_file_path,
                'enriched_file_path': enriched_file_path,
                'ingestion_timestamp': ingestion_timestamp,
                'expiry_date': expiry_date_string
            }
        return avro_dict

    # task [GET INGESTION TIMESTAMP] STARTS
    def get_ingestion_timestamp():
        return f'{datetime.datetime.now(tz=pytz.UTC).strftime(TABLE_SUFFIX_PATTERN)}'

    get_ingestion_timestamp = PythonOperator(
        task_id='get_ingestion_timestamp',
        provide_context=True,
        python_callable=get_ingestion_timestamp
    )
    # task [GET INGESTION TIMESTAMP] ENDS

    # task [COPY FILES] STARTS
    def copy_kwargs(input_tuple: tuple) -> dict:
        """ Utility function to make kwargs that matches signature of S3CopyObjectOperator.
        Copy original AVSC file.
        """
        file = input_tuple[0]
        ingestion_timestamp = input_tuple[1]
        logger.info(f"File to process: {file}")
        logger.info(f"Source key: s3://{bucket_name}/{file}")
        logger.info(f"Destination key: s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{file.split('/')[-1]}")
        return {
            "source_bucket_key": file,
            "dest_bucket_key": f"{ingestion_timestamp}/{file.split('/')[-1]}"
        }

    def copy_sanitized_kwargs(input_tuple: tuple) -> dict:
        """ Utility function to make kwargs that matches signature of S3CopyObjectOperator.
        Copy original AVSC file.
        """
        # TODO: some ideas to consider:
        # * lowercase file names: file.lower()
        # * replace spaces with underscores: file.replace(" ", "_")
        file = input_tuple[0]
        ingestion_timestamp = input_tuple[1]
        logger.info(f"File to process: {file}")
        logger.info(f"Source key: s3://{bucket_name}/{file}")
        logger.info(f"Destination key: s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{sanitize_filename(file.split('/')[-1])}")
        return {
            "source_bucket_key": file,
            "dest_bucket_key": f"{ingestion_timestamp}/{sanitize_filename(file.split('/')[-1])}"
        }

    @task_group()
    def copy_files(s3_files: XComArg) -> None:
        S3CopyObjectOperator.partial(
            task_id="copy_files_to_datalake",
            aws_conn_id="aws_minio",
            source_bucket_name=f"{bucket_name}",
            dest_bucket_name=f"{BUCKET_DATALAKE}",
        ).expand_kwargs(s3_files.map(copy_kwargs))
    # task [COPY FILES] ENDS

    @task_group()
    def copy_sanitized_files(s3_files: XComArg) -> None:
        logger.info(f"copy_sanitized_files s3_files {s3_files}")
        S3CopyObjectOperator.partial(
            task_id="copy_sanitized_files_to_datalake",
            aws_conn_id="aws_minio",
            source_bucket_name=f"{bucket_name}",
            dest_bucket_name=f"{BUCKET_DATALAKE}",
        ).expand_kwargs(s3_files.map(copy_sanitized_kwargs))

    # task [VALIDATE AVRO SCHEMA] STARTS
    @task
    def validate_avro_schemas(avro_file_list: list, **context):
        """
        Function performs validation of semi-avro schema files.
        :param avro_file_list: a list of lists with relative paths to semi-avro schema files as first element
        :param context: standard Airflow context
        :return: via XCOM:
                correct_avsc - a dictionary which keys are file paths and values are tuples of file path,
                               ingestion timestamps and the expiry date retrieved from retention fields in avsc
                incorrect_avsc - a dictionary which keys are file paths and values are error messages
        """
        source_s3 = S3Hook(aws_conn_id="aws_minio")
        incorrect_avsc = dict()
        correct_avsc = dict()
        for avro_file_details in avro_file_list:
            avro_file_path = avro_file_details[0]
            avro_file_ingestion_timestamp = avro_file_details[1]
            logger.info(f"Validation of {avro_file_path} starts.")
            try:
                schema = json.loads(source_s3.read_key(avro_file_path, bucket_name=bucket_name))
            except JSONDecodeError as json_decode_error:
                error_message = f"Error parsing file {avro_file_path}. Are you sure it is a json?"
                logger.error(error_message)
                incorrect_avsc[avro_file_path] = error_message
                continue
            try:
                parse_non_static_avro_schema(schema)
            except TwainCustomException as tpe:
                logger.error(tpe.details)
                incorrect_avsc[avro_file_path] = tpe.details
                continue
            expiry_date = get_expiry_date_from_avsc(schema, avro_file_ingestion_timestamp)
            avro_file_details.append(expiry_date)
            correct_avsc[avro_file_path] = avro_file_details
        context["ti"].xcom_push(
            key=VALIDATE_AVRO_SCHEMAS_CORRECT_KEY, value=correct_avsc
        )
        context["ti"].xcom_push(
            key=VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY, value=incorrect_avsc
        )
    # task [VALIDATE AVRO SCHEMA] ENDS

    @task
    def get_correctly_validated_avro_schemas(**context):
        """
        Task procures the list of correct avsc files.
        :param context: standard Airflow context
        :return: a list of 2 element tuples which first element is relative path to avsc file and the second
                 is the string representation of ingestion timestamp.
        """
        correct_avscs = context["ti"].xcom_pull(
            task_ids='validate_avro_schemas', key=VALIDATE_AVRO_SCHEMAS_CORRECT_KEY)
        output = list()
        for _, correct_avsc in correct_avscs.items():
            output.append(correct_avsc)
        return output

    # task group [TRANSFORM AND COPY AVSC FILES] STARTS
    def transform_and_copy_kwargs(input_tuple: tuple) -> dict:
        """ Utility function to make kwargs that matches signature of S3FileTransformOperator.
        Copy transformed AVSC file: all fields have type="string".
        """
        file = input_tuple[0]
        ingestion_timestamp = input_tuple[1]
        logger.info(f"File to process: {file}")
        logger.info(f"Source key: s3://{bucket_name}/{file}")
        logger.info(f"Destination key: "
                    f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{AVSC_W_STRINGS_PREFIX}{file.split('/')[-1]}")
        return {
            "source_s3_key": f"s3://{bucket_name}/{file}",
            "dest_s3_key": f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{AVSC_W_STRINGS_PREFIX}{file.split('/')[-1]}"
        }

    # task group [TRANSFORM AND COPY AVSC FILES] STARTS
    def transform_sanitize_and_copy_kwargs(input_tuple: tuple) -> dict:
        """ Utility function to make kwargs that matches signature of S3FileTransformOperator.
        Copy transformed AVSC file: all fields have type="string".
        """
        file = input_tuple[0]
        ingestion_timestamp = input_tuple[1]
        logger.info(f"File to process: {file}")
        logger.info(f"Source key: s3://{bucket_name}/{file}")
        logger.info(f"Destination key: "
                    f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{AVSC_W_STRINGS_PREFIX}{file.split('/')[-1]}")
        return {
            "source_s3_key": f"s3://{bucket_name}/{file}",
            "dest_s3_key": f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{AVSC_W_STRINGS_PREFIX}{sanitize_filename(file.split('/')[-1])}"
        }

    @task_group()
    def transform_and_copy_files(s3_files: XComArg) -> None:
        """ A Task Group which reads all AVSC files, transforms (replace types to be "string")
        and uploads a copy with the prefix "str_" to the destination bucket.

        It turns out the problem with running the scripts with S3FileTransformOperator was caused by hash-bang
        in the script: it is supposed to point to the installation of Python on the worker;
        additionally, Bitnami installs its own Python venv and our scripts should point
        to that venv (at the moment it is in /opt/bitnami/airflow/venv/bin/python)
        """
        TwainS3FileTransformOperator.partial(
            task_id="copy_str_files_to_datalake",
            source_aws_conn_id="aws_minio",
            dest_aws_conn_id="aws_minio",
            replace=True,
            transform_script="/opt/bitnami/airflow/dags/git_ps-dags/scripts/transform.py",
        ).expand_kwargs(s3_files.map(transform_and_copy_kwargs))

        TwainS3FileTransformOperator.partial(
            task_id="copy_sanitized_str_files_to_datalake",
            source_aws_conn_id="aws_minio",
            dest_aws_conn_id="aws_minio",
            replace=True,
            transform_script="/opt/bitnami/airflow/dags/git_ps-dags/scripts/transform.py",
        ).expand_kwargs(s3_files.map(transform_sanitize_and_copy_kwargs))

    # task [TRANSFORM AND COPY AVSC FILES] ENDS

    # task [MARKING STATUS OF AVSC TO_STRING CONVERSION] STARTS
    mark_finished_str_avsc_ingestions = PythonOperator(
        task_id='mark_finished_str_avsc_ingestions',
        provide_context=True,
        python_callable=xcom_finished_ingestions,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        op_kwargs={
            'xcom_key_name': SOURCE_AVRO_FILE_PATH
        }
    )

    flatten_avro_results = FlattenResultsOperator(
        task_id='flatten_str_avsc_results_ingestions',
        # provide_context=True,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        marking_task_id='mark_finished_str_avsc_ingestions'
    )

    def get_correct_avros(**context):
        ingestion_timestamp = context['ti'].xcom_pull("get_ingestion_timestamp")
        correct_avro_files_list = context['ti'].xcom_pull(
            task_ids='flatten_str_avsc_results_ingestions', key='correct_avro_files')
        output = list()
        for correct_avro_file in correct_avro_files_list:
            output.append((correct_avro_file, ingestion_timestamp))
        return output

    get_correct_avros_op = PythonOperator(
        task_id='get_correct_avros',
        provide_context=True,
        python_callable=get_correct_avros,
        trigger_rule=TriggerRule.NONE_SKIPPED
    )

    get_incorrect_avros_op = PythonOperator(
        task_id='get_incorrect_avros',
        provide_context=True,
        python_callable=get_incorrect_avros,
        trigger_rule=TriggerRule.ALL_DONE,
        op_kwargs={
            'bucket_name': bucket_name
        }
    )

    store_dead_letters_op = PythonOperator(
        task_id='store_dead_letters',
        provide_context=True,
        python_callable=store_dead_letters,
        trigger_rule=TriggerRule.ALL_DONE,
        op_kwargs={
            'mode': INGESTION_FAILURE_MODE
        }
    )

    get_correct_avros_op.set_upstream(flatten_avro_results)
    get_incorrect_avros_op.set_upstream(flatten_avro_results)
    store_dead_letters_op.set_upstream(get_incorrect_avros_op)
    # task [MARKING STATUS OF AVSC TO_STRING CONVERSION] ENDS

    # BRANCH 1. 'processing incorrect avro schemas' STARTS  ###
    # task group [MOVE INCORRECT AVSC TO QUARANTINE] STARTS
    def copy_quarantine_kwargs(file: str) -> dict:
        """ Utility function to make kwargs that matches signature of S3CopyObjectOperator.
        Copy original AVSC file to quarantine. Remove s3://Bucket_name from paths
        """
        if file.startswith("s3://"):
            file = file[len("s3://"):]
            file = '/'.join(file.split("/")[1:])
        return {
            "source_bucket_key": file,
            "dest_bucket_key": file.split("/")[-1]
        }

    @task_group()
    def move_incorrect_avros_to_quarantine(s3_files: XComArg) -> None:
        task_copy = S3CopyObjectOperator.partial(
            task_id="copy_incorrect_avros_to_quarantine",
            aws_conn_id="aws_minio",
            source_bucket_name=f"{bucket_name}",
            dest_bucket_name=f"{BUCKET_QUARANTINE}",
        ).expand_kwargs(s3_files.map(copy_quarantine_kwargs))
        task_delete = S3DeleteObjectsOperator.partial(
            task_id="remove_incorrect_avros_from_source_bucket",
            aws_conn_id="aws_minio",
            bucket=f"{bucket_name}",
        ).expand(keys=s3_files.map(comply_file_names_with_s3delete))
        task_copy >> task_delete

    move_incorrect_avros_to_quarantine_task_group = \
        move_incorrect_avros_to_quarantine(get_incorrect_avros_op.output)
    move_incorrect_avros_to_quarantine_task_group.set_upstream(get_incorrect_avros_op)
    # task group [MOVE INCORRECT AVSC TO QUARANTINE] ENDS
    # BRANCH 1. 'processing incorrect avro schemas' ENDS  ###

    # BRANCH 2. 'processing correct avro schemas' STARTS  ###
    # task group [ENRICH AND COPY AVSC FILES] STARTS
    def _get_ownership_details(input_string: str):
        """
        Function retrieves owning company name and user from the MinIO file path
        Removes s3://Bucket_name from paths.
        Raises KeyError if the path does not consist of at least 3 subdirectories.
        :param input_string:
        :return:
        """
        if input_string.startswith("s3://"):
            input_string = input_string[len("s3://"):]
        input_split = input_string.split("/")
        if len(input_split) < 3:
            raise KeyError(f"Could not retrieve file ownership for file - missing metadata in path {input_string}.")
        return input_split[1], input_split[2]

    def enrich_and_copy_kwargs(input_tuple: tuple) -> dict:
        """ Utility function to make kwargs that matches signature of S3FileTransformOperator.
        Copy transformed AVSC file with added owner details, handled timestamps.
        """
        file = input_tuple[0]
        ingestion_timestamp = input_tuple[1]
        logger.info(f"File to process: {file}")
        source_s3_key = file
        dest_s3_key = f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{ENRICH_AVRO_TABLE_PREFIX}{file.split('/')[-1]}"
        logger.info(f"Source key: {source_s3_key}")
        logger.info(f"Destination key: {dest_s3_key}")
        owning_company, file_owner = _get_ownership_details(file)
        return {
            "source_s3_key": source_s3_key,
            "dest_s3_key": dest_s3_key
            , "owning_company": owning_company
            , "file_owner": file_owner
        }

    @task_group()
    def enrich_and_copy_avro_files(s3_files: XComArg) -> None:  # Dict:
        """ A Task Group which reads all AVSC files, transforms (handles owner, owning company)
        and uploads a copy with the prefix "enriched_" to the destination bucket.

        Consult function transform_and_copy_files to understand why the path is so convoluted.
        """
        TwainS3FileTransformOperatorEnhanced.partial(
            task_id="enrich_and_copy_avro_files_to_datalake",
            source_aws_conn_id="aws_minio",
            dest_aws_conn_id="aws_minio",
            replace=True,
            transform_script="/opt/bitnami/airflow/dags/git_ps-dags/scripts/enrich_private_avsc.py",
        ).expand_kwargs(s3_files.map(enrich_and_copy_kwargs))

    enrich_and_copy_avro_files_to_datalake = enrich_and_copy_avro_files(get_correct_avros_op.output)
    # task group [ENRICH AND COPY AVSC FILES] ENDS

    # task [MARKING STATUS OF ENRICH AND COPY AVSC FILES] STARTS
    mark_finished_enrich_avsc_ingestions = PythonOperator(
        task_id='mark_finished_enrich_avsc_ingestions',
        provide_context=True,
        python_callable=xcom_finished_ingestions,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        op_kwargs={
            'xcom_key_name': SOURCE_AVRO_FILE_PATH
        }
    )

    merge_enriched_avro_results = FlattenEnrichmentResultsOperator(
        task_id='merge_enriched_avsc_results_ingestions',
        # provide_context=True,
        trigger_rule=TriggerRule.NONE_SKIPPED,
        marking_task_id='mark_finished_enrich_avsc_ingestions'
    )

    get_enriched_correct_avros_op = PythonOperator(
        task_id='get_enriched_correct_avros_op',
        provide_context=True,
        python_callable=get_enriched_correct_avros,
        trigger_rule=TriggerRule.NONE_SKIPPED
    )
    # task [MARKING STATUS OF ENRICH AND COPY AVSC FILES] ENDS

    # BRANCH 2.1: 'processing incorrectly enriched avro schemas' STARTS  ###
    # task [MOVE INCORRECTLY ENRICHED AVRO SCHEMAS TO QUARANTINE] STARTS
    get_incorrect_avros_after_enrichment_op = PythonOperator(
        task_id='get_enriched_incorrect_avros_op',
        provide_context=True,
        python_callable=get_enriched_incorrect_avros,
        trigger_rule=TriggerRule.NONE_SKIPPED
    )
    get_incorrect_avros_after_enrichment_op.set_upstream(merge_enriched_avro_results)
    move_incorrect_avros_to_quarantine_task_group_after_enrichment = \
        move_incorrect_avros_to_quarantine(get_incorrect_avros_after_enrichment_op.output)
    move_incorrect_avros_to_quarantine_task_group_after_enrichment.set_upstream(get_incorrect_avros_after_enrichment_op)
    # task [MOVE INCORRECTLY ENRICHED AVRO SCHEMAS TO QUARANTINE] ENDS
    # BRANCH 2.1: 'processing incorrectly enriched avro schemas' ENDS  ###

    # BRANCH 2.2: 'processing correctly enriched avro schemas' STARTS  ###
    # task [BUILD SQL CREATE TABLE] STARTS
    @task()
    def build_sql_create_table(avsc_tuples_list: list, **context) -> list:
        """ Build an SQL statement CREATE TABLE per each file.
        File list example:
            ['V52_SampleData.avsc', 'ssp/blade_strain_gauge_data_sample.avsc']
        """
        sql_list = []
        ingestion_timestamp = context["ti"].xcom_pull("get_ingestion_timestamp")
        logger.info(f"ingestion_timestamp: {ingestion_timestamp}")
        for avsc_tuple in avsc_tuples_list:
            fname = avsc_tuple[0]
            expiry_date = avsc_tuple[1]
            comment_text = ""
            if expiry_date:
                comment_text = json.dumps({'expiry_date': expiry_date})
            file_name = fname.split("/")[-1]  # get rid of the PATH
            # get rid of the .avsc and underscores in the remaining file name, but maintain the original file name

            sanitized_filename = sanitize_filename(file_name)
            sanitized_table_name = sanitized_filename.split('.')[0]
            avro_table = f"{AVRO_TABLE_PREFIX}{sanitized_table_name}_{ingestion_timestamp}"
            avro_file_path = f"s3://{BUCKET_DATALAKE}/{ingestion_timestamp}/{AVSC_W_STRINGS_PREFIX}{sanitized_filename}"


            sql_list.append(f"""
                        CREATE TABLE IF NOT EXISTS {avro_table} (
                            id BIGINT
                        )
                        COMMENT '{comment_text}'
                        WITH (
                            format = 'AVRO',
                            avro_schema_url = '{avro_file_path}'
                        )
                        """
                            )
        return sql_list

    sql_list = build_sql_create_table(avsc_tuples_list=get_enriched_correct_avros_op.output)
    # task [BUILD SQL CREATE TABLE] ENDS

    # set of tasks [BRANCHING CREATION OF TRINO TABLES FOR EMPTY sql_list] STARTS
    sql_list_end = EmptyOperator(
        task_id="sql_list_end_join",
        trigger_rule="none_failed_min_one_success"
    )

    sql_list_alternative = EmptyOperator(
        task_id="sql_list_alternative",
        trigger_rule="none_failed_min_one_success"
    )

    @task.branch(task_id="branching_if_empty_sql_list")
    def branching_if_empty_sql_list(sql_list_input):
        if not sql_list_input:
            return ['sql_list_alternative']
        else:
            return ['trino_create_table_avro']

    branching_if_empty_sql_list_task = branching_if_empty_sql_list(sql_list)
    branching_if_empty_sql_list_task.set_upstream(sql_list)
    branching_if_empty_sql_list_task.set_downstream(sql_list_alternative)

    trino_create_table_avro = SQLExecuteQueryOperator(
        task_id="trino_create_table_avro",
        conn_id="twain_trino",
        sql=sql_list,
        split_statements=True,
        handler=list
    )

    sql_list_end.set_upstream([sql_list_alternative, trino_create_table_avro])
    # set of tasks [BRANCHING CREATION OF TRINO TABLES FOR EMPTY sql_list] ENDS

    # task group [DELETE CORRECT AVSC FILES] STARTS
    @task_group()
    def delete_files(avsc_tuples_list: XComArg) -> None:
        S3DeleteObjectsOperator.partial(
            task_id="delete_files_from_origin",
            aws_conn_id="aws_minio",
            bucket=f"{bucket_name}",
        ).expand(keys=avsc_tuples_list.map(comply_file_names_with_s3delete_tuple))

    delete_files_from_origin = delete_files(get_enriched_correct_avros_op.output)
    avro_params = build_avro_params(get_enriched_correct_avros_op.output)
    # task group [DELETE CORRECT AVSC FILES] ENDS
    # BRANCH 2.2: 'processing correctly enriched avro schemas' ENDS  ###

    # initializing tasks dependent on s3 file list
    filter_out_identical_target_trino_table_names_result = \
        filter_out_identical_target_trino_table_names(s3_files.output)
    copy_files_to_datalake = copy_files(filter_out_identical_target_trino_table_names_result)
    copy_sanitized_files_to_datalake = copy_sanitized_files(filter_out_identical_target_trino_table_names_result)
    validate_avro_schemas_op = validate_avro_schemas(filter_out_identical_target_trino_table_names_result)
    get_correctly_validated_avro_schemas_op = get_correctly_validated_avro_schemas()
    copy_str_files_to_datalake = transform_and_copy_files(get_correctly_validated_avro_schemas_op)
    get_ingestion_timestamp.set_upstream(s3_files)
    get_ingestion_timestamp.set_downstream(filter_out_identical_target_trino_table_names_result)

    # task [TRIGGERING DOWNSTREAM DAG] STARTS
    trigger_csv_dag = TriggerDagRunOperator(
        task_id="trigger_csv_dag",
        trigger_dag_id=downstream_dag_name,
        conf=avro_params
    )
    # task [TRIGGERING DOWNSTREAM DAG] ENDS

    # task [MARKING ETL0 DAGRUN STATUS] STARTS
    @task(trigger_rule=TriggerRule.ALL_DONE)
    def mark_run_end_state(validation_tasks_names: List, **context):
        failed_tasks_list = list()
        lookup_keys = [DAGRUN_FAILED_PARENT_TASKS, VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY]
        for lookup_key in lookup_keys:
            for x in validation_tasks_names:
                logger.info(f"Checking xcom key {lookup_key} for task {x}.")
                failed_tasks = context["ti"].xcom_pull(task_ids=x, key=lookup_key)
                if failed_tasks:
                    failed_tasks_list.extend(failed_tasks)
        logger.info(f'failed_tasks_list: {failed_tasks_list}')
        context['ti'].xcom_push('failed_transformations', failed_tasks_list)
        if len(failed_tasks_list) > 0:
            raise KeyError(f'Marking DAG as failed as {len(failed_tasks_list)} ingestion threads failed.')

    mark_run_end_state_task = mark_run_end_state(
        validation_tasks_names=[
                'mark_finished_str_avsc_ingestions',
                'mark_finished_enrich_avsc_ingestions',
                'validate_avro_schemas'
            ]
        )
    move_incorrect_avros_to_quarantine_task_group.set_downstream(mark_run_end_state_task)
    move_incorrect_avros_to_quarantine_task_group_after_enrichment.set_downstream(mark_run_end_state_task)
    sql_list_end.set_downstream(mark_run_end_state_task)
    # task [MARKING ETL0 DAGRUN STATUS] ENDS

    chain(
        sensor_avsc,
        s3_files,
        filter_out_identical_target_trino_table_names_result,
        copy_files_to_datalake,
        copy_sanitized_files_to_datalake,
        validate_avro_schemas_op,
        get_correctly_validated_avro_schemas_op,
        copy_str_files_to_datalake,
        mark_finished_str_avsc_ingestions,
        flatten_avro_results,
        enrich_and_copy_avro_files_to_datalake,
        mark_finished_enrich_avsc_ingestions,
        merge_enriched_avro_results,
        get_enriched_correct_avros_op,
        sql_list,
        branching_if_empty_sql_list_task,
        trino_create_table_avro
    )
    chain(
        trino_create_table_avro,
        delete_files_from_origin
    )
    chain(
        delete_files_from_origin,
        trigger_csv_dag
        , mark_run_end_state_task
    )
