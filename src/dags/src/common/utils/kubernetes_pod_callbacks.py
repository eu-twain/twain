import kubernetes.client as k8s
from airflow.providers.cncf.kubernetes.callbacks import KubernetesPodOperatorCallback


class TwainPodStartedCallback(KubernetesPodOperatorCallback):
    @staticmethod
    def on_pod_creation(*, pod: k8s.V1Pod, client: k8s.CoreV1Api, mode: str, **kwargs) -> None:
        """
        The function was an attempt at marking the state of the k8s pod in Airflow - unfortunately
        KubernetesPodOperator.execute_sync does not pass Airflow context here so we could not produce xcom message.
        :param pod:
        :param client:
        :param mode:
        :param kwargs:
        :return:
        """
        pass
