# processing statuses
ENRICHMENT_FAILED = 'enrichment_failed'

# xcom names
DESTINATION_AVRO_FILE_PATH = 'destination_avro_file_path'
SOURCE_AVRO_FILE_PATH = 'source_avro_file_path'

DAGRUN_FAILED_PARENT_TASKS = 'dagrun_failed_parent_tasks'
DAGRUN_FINISHED_PARENT_TASKS = 'dagrun_finished_parent_tasks'

VALIDATE_AVRO_SCHEMAS_CORRECT_KEY = 'correct_avsc'
VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY = 'incorrect_avsc'

# statuses for dead_letter
INGESTION_FAILURE_MODE = "INGESTION_FAILURE_MODE"
INGESTION_SUCCESS_MODE = "INGESTION_SUCCESS_MODE"

# table prefix timestamp pattern
TABLE_SUFFIX_PATTERN: str = "%Y%m%d%H%M%S"

# data expiration date pattern
DATA_EXPIRATION_DATE_PATTERN: str = "%Y-%m-%d"
# data expiration default retention in days
DEFAULT_DATA_RETENTION_IN_DAYS = 30

# memory allocation ratio between dataframe cells and GB of memory to be allocated
MEMORY_ALLOCATION_RATIO = 1000 * 1000 * 17
MEMORY_ALLOCATION_RATIO_STEP_2 = 1000 * 1000 * 20
MAX_MEMORY_ALLOCATION = 8

# tables protected from expiration
PROTECTED_TABLES = ['common_data_model', 'common_data_model_schema']
