import logging
import sys

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")

import json
from airflow.utils.state import State, DagRunState
from airflow.models.taskinstance import TaskInstance
from src.common.utils.statics import DESTINATION_AVRO_FILE_PATH, DAGRUN_FAILED_PARENT_TASKS, \
    DAGRUN_FINISHED_PARENT_TASKS, ENRICHMENT_FAILED

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


def _get_xcom_value(xcom_key_name: str, task_instance: TaskInstance, parent_task_instance: TaskInstance):
    xcom_key_value = None
    try:
        xcom_key_value = str(task_instance.xcom_pull(
            task_ids=str(parent_task_instance.task_id),
            map_indexes=str(parent_task_instance.map_index),
            key=xcom_key_name)
        ).replace("\'", "\"")
    except TypeError as e:
        logger.error(f"Failed to find xcom key \'{xcom_key_name}\' for task {parent_task_instance.task_id}"
                     f"/{parent_task_instance.map_index}. {e}")
    if not xcom_key_value:
        return {}
    a_dict = json.loads('{ "v": ' + xcom_key_value + '}')
    xcom_key_value = a_dict['v'][0]
    return xcom_key_value


def xcom_finished_ingestions(xcom_key_name: str, **context):
    """
    Function trying to get the status of upstream ingestion tasks.
    """
    this_dagrun_tasks = context['ti'].get_dagrun().get_task_instances()
    parent_task_ids = list(context['task'].upstream_task_ids)
    logger.info(f"parent task id list: {parent_task_ids}")
    parent_task_instances = list()
    for pti in [pti for pti in this_dagrun_tasks if pti.task_id in parent_task_ids]:
        parent_task_instances.append(pti)
    logger.info(f"Found {len(parent_task_instances)} parent tasks.")
    run_ids_map = dict()
    failed_run_ids_map = dict()
    for pti in parent_task_instances:
        logger.info(f"Trying to find xcom key \'{xcom_key_name}\' for task {pti.task_id}/{pti.map_index}.")
        xcom_key_value = _get_xcom_value(xcom_key_name, context['ti'], pti)
        logger.info(f"Flattened xcom value \'{xcom_key_value}\'.")

        if pti.state == State.SUCCESS:
            destination_avro_file_path_value = _get_xcom_value(DESTINATION_AVRO_FILE_PATH, context['ti'], pti)

            logger.info(f'destination_avro_file_path_value: {destination_avro_file_path_value}')
            xcom_extended_key_value = {
                'source': xcom_key_value,
                'destination': destination_avro_file_path_value
            }
            try:
                run_ids_map[str(pti.task_id) + "/" + str(pti.map_index)].append(xcom_extended_key_value)
            except KeyError as ke:
                run_ids_map[str(pti.task_id) + "/" + str(pti.map_index)] = [xcom_extended_key_value]
        else:
            try:
                failed_run_ids_map[str(pti.task_id) + "/" + str(pti.map_index)].append(xcom_key_value)
            except KeyError as ke:
                failed_run_ids_map[str(pti.task_id) + "/" + str(pti.map_index)] = [xcom_key_value]
    context['ti'].xcom_push(DAGRUN_FINISHED_PARENT_TASKS, run_ids_map)
    context['ti'].xcom_push(DAGRUN_FAILED_PARENT_TASKS, failed_run_ids_map)


def get_enriched_correct_avros(**context):
    """

    :param context: a standard Airflow context
    :return: a list of tuples where first element is the full s3 path of semi-avro schema file and the second is
             the data expiration date
    """
    output = list()
    correct_avro_files_input = context['ti'].xcom_pull(
        task_ids='merge_enriched_avsc_results_ingestions', key='correct_avro_files')
    correct_avscs_with_expiration_date = context["ti"].xcom_pull(task_ids='get_correctly_validated_avro_schemas')
    for avro_source, avro_object in correct_avro_files_input.items():
        data_expiration_date = None
        if 'enriched_avro' not in avro_object.keys() or 'string_avro' not in avro_object.keys():
            continue
        avro_source_file_path = "/".join(avro_source.split("/")[3:])
        for correct_avsc_with_expiration_date in correct_avscs_with_expiration_date:
            if correct_avsc_with_expiration_date[0] == avro_source_file_path:
                data_expiration_date = correct_avsc_with_expiration_date[2]
        output.append((avro_source, data_expiration_date))
    return output


def get_enriched_incorrect_avros(**context):
    output = list()
    correct_avro_files_input = context['ti'].xcom_pull(
        task_ids='merge_enriched_avsc_results_ingestions', key='incorrect_avro_files')
    for avro_source, avro_object in correct_avro_files_input.items():
        if 'enriched_avro' not in avro_object.keys() or 'string_avro' not in avro_object.keys():
            continue
        if avro_object['enriched_avro'] == ENRICHMENT_FAILED:
            output.append(avro_source)
    return output


def comply_file_names_with_s3delete(file_s3_path: str) -> str:
    if file_s3_path.startswith("s3://"):
        file_s3_path = file_s3_path[len("s3://"):]
        file_s3_path = file_s3_path.split("/")[1:]
    return "/".join(file_s3_path)


def comply_file_names_with_s3delete_tuple(file_s3_path_tuple: str) -> str:
    file_s3_path = file_s3_path_tuple[0]
    if file_s3_path.startswith("s3://"):
        file_s3_path = file_s3_path[len("s3://"):]
        file_s3_path = file_s3_path.split("/")[1:]
    return "/".join(file_s3_path)
