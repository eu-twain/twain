import logging
import sys
from .flatten_results_operator import FlattenResultsOperator, DAGRUN_FAILED_PARENT_TASKS, \
    DAGRUN_FINISHED_PARENT_TASKS, ENRICHMENT_FAILED


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


class FlattenEnrichmentResultsOperator(FlattenResultsOperator):
    def flatten_avro_results_function(self, xcom_key_name: str, task_instance_handle):
        output = dict()
        xcom_value = task_instance_handle.xcom_pull(
            task_ids=self.marking_task_id,
            key=xcom_key_name)
        logger.info(f"{self.__class__.__name__} for xcom_key_name {xcom_key_name} retrieved type "
                    f"{type(xcom_value)} xcom_value: {xcom_value}.")
        if not xcom_value:
            return {}
        elif isinstance(xcom_value, dict):
            enriched_avros_dict = xcom_value
        else:
            enriched_avros_dict = json.loads(xcom_value)
        logger.info(f'From task {self.marking_task_id} xcom_key_name: {xcom_key_name} '
                    f'received enriched_avros_dict: {enriched_avros_dict}')

        if xcom_key_name == DAGRUN_FAILED_PARENT_TASKS:
            xcom_key_name = DAGRUN_FINISHED_PARENT_TASKS
        mark_finished_str_avsc_ingestions_xcom = task_instance_handle.xcom_pull(
            task_ids='mark_finished_str_avsc_ingestions',
            key=xcom_key_name)
        logger.info(f"{self.__class__.__name__} for task \'mark_finished_str_avsc_ingestions\' xcom_key_name "
                    f"\'{xcom_key_name}\' retrieved type "
                    f"{type(mark_finished_str_avsc_ingestions_xcom)} "
                    f"xcom_value: {mark_finished_str_avsc_ingestions_xcom}.")
        if not mark_finished_str_avsc_ingestions_xcom:
            return {}
        elif isinstance(mark_finished_str_avsc_ingestions_xcom, dict):
            str_avros_dict = mark_finished_str_avsc_ingestions_xcom
        else:
            str_avros_dict = json.loads(mark_finished_str_avsc_ingestions_xcom)

        for str_avros_values in str_avros_dict.values():
            for str_avro_value in str_avros_values:
                output[str_avro_value['source']] = {'string_avro': str_avro_value['destination']}
        logger.info(f"output after getting str_avros_dict: {output}")

        for enriched_avros_values in enriched_avros_dict.values():
            logger.info(f"enriched_avros_values: {enriched_avros_values}")
            for enriched_avro_value in enriched_avros_values:
                if isinstance(enriched_avro_value, str):
                    str_avro_object = output[enriched_avro_value]
                    str_avro_object['enriched_avro'] = ENRICHMENT_FAILED
                    output[enriched_avro_value] = str_avro_object
                else:
                    str_avro_object = output[enriched_avro_value['source']]
                    str_avro_object['enriched_avro'] = enriched_avro_value['destination']
                    output[enriched_avro_value['source']] = str_avro_object
        return output

    def execute(self, context):
        self.correct_avro_files = self.flatten_avro_results_function(DAGRUN_FINISHED_PARENT_TASKS, context['ti'])
        self.incorrect_avro_files = self.flatten_avro_results_function(DAGRUN_FAILED_PARENT_TASKS, context['ti'])
        context['ti'].xcom_push("correct_avro_files", self.correct_avro_files)
        context['ti'].xcom_push("incorrect_avro_files", self.incorrect_avro_files)
