from airflow.providers.amazon.aws.operators.s3 import S3FileTransformOperator
from src.common.utils.statics import DESTINATION_AVRO_FILE_PATH, SOURCE_AVRO_FILE_PATH
from airflow.utils.decorators import apply_defaults
from airflow.exceptions import AirflowException
from airflow.hooks.S3_hook import S3Hook
from tempfile import NamedTemporaryFile
import subprocess


class TwainS3FileTransformOperator(S3FileTransformOperator):
    def execute(self, context):
        context['ti'].xcom_push(DESTINATION_AVRO_FILE_PATH, self.dest_s3_key)
        context['ti'].xcom_push(SOURCE_AVRO_FILE_PATH, self.source_s3_key)
        super().execute(context)


class TwainS3FileTransformOperatorEnhanced(S3FileTransformOperator):
    @apply_defaults
    def __init__(
            self,
            source_s3_key,
            dest_s3_key,
            owning_company,
            file_owner,
            transform_script=None,
            select_expression=None,
            source_aws_conn_id='aws_default',
            source_verify=None,
            dest_aws_conn_id='aws_default',
            dest_verify=None,
            replace=False,
            *args, **kwargs):
        super(TwainS3FileTransformOperatorEnhanced, self).__init__(
            source_s3_key=source_s3_key,
            source_aws_conn_id=source_aws_conn_id,
            source_verify=source_verify,
            dest_s3_key=dest_s3_key,
            dest_aws_conn_id=dest_aws_conn_id,
            dest_verify=dest_verify,
            replace=replace,
            transform_script=transform_script,
            select_expression=select_expression,
            *args,
            **kwargs)
        self.owning_company = owning_company
        self.file_owner = file_owner

    def execute(self, context):
        context['ti'].xcom_push(DESTINATION_AVRO_FILE_PATH, self.dest_s3_key)
        context['ti'].xcom_push(SOURCE_AVRO_FILE_PATH, self.source_s3_key)

        if self.transform_script is None and self.select_expression is None:
            raise AirflowException(
                "Either transform_script or select_expression must be specified")

        source_s3 = S3Hook(aws_conn_id=self.source_aws_conn_id,
                           verify=self.source_verify)
        dest_s3 = S3Hook(aws_conn_id=self.dest_aws_conn_id,
                         verify=self.dest_verify)

        self.log.info("Downloading source S3 file %s", self.source_s3_key)
        if not source_s3.check_for_key(self.source_s3_key):
            raise AirflowException(
                "The source key {0} does not exist".format(self.source_s3_key))
        source_s3_key_object = source_s3.get_key(self.source_s3_key)

        with NamedTemporaryFile("wb") as f_source, NamedTemporaryFile("wb") as f_dest:
            self.log.info(
                "Dumping S3 file %s contents to local file %s",
                self.source_s3_key, f_source.name
            )

            if self.select_expression is not None:
                content = source_s3.select_key(
                    key=self.source_s3_key,
                    expression=self.select_expression
                )
                f_source.write(content.encode("utf-8"))
            else:
                source_s3_key_object.download_fileobj(Fileobj=f_source)
            f_source.flush()

            if self.transform_script is not None:
                process = subprocess.Popen(
                    [self.transform_script, f_source.name, f_dest.name,
                     self.owning_company, self.file_owner],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    close_fds=True
                )

                self.log.info("Output:")
                for line in iter(process.stdout.readline, b''):
                    self.log.info(line.decode(self.output_encoding).rstrip())

                process.wait()

                if process.returncode > 0:
                    raise AirflowException(
                        "Transform script failed: {0}".format(process.returncode)
                    )
                else:
                    self.log.info(
                        "Transform script successful. Output temporarily located at %s",
                        f_dest.name
                    )

            self.log.info("Uploading transformed file to S3")
            f_dest.flush()
            dest_s3.load_file(
                filename=f_dest.name,
                key=self.dest_s3_key,
                replace=self.replace
            )
            self.log.info("Upload successful")
