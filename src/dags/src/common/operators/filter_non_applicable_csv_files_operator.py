import sys
import logging
import copy
from airflow.models.taskinstance import TaskInstance
from airflow.models import BaseOperator


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


class FilterNonApplicableCSVFilesOperator(BaseOperator):
    """
    The operator is responsible for determining the expected csv files based on the incoming avsc files
    and cross-referencing the actually existing csv files with the expected files.
    Those that match the expected file name are returned in xcom key "expected_csv_files" and for them
    the additional configuration for further processing is procured;
    those that should have existed but are not present in the bucket are returned in xcom key "missing_csv_files";
    those csv's that exist but were not expected to (as those do not match any avsc file name) are omitted
    from processing.
    """
    def __init__(self, listing_files_task_id, expected_files_task_id, **kwargs) -> None:
        super().__init__(**kwargs)
        self.not_expected_csv_files = None
        self.expected_csv_files = None
        self.expected_files_config = dict()
        self.listing_files_task_id = listing_files_task_id
        self.expected_files_task_id = expected_files_task_id
        self.missing_csv_files = list()

    def _procure_expected_files_config(self, ingestion_config: dict, csv_files_list):
        """
        Function procures one config for each csv file that is present in the list csv_files_list.
        The output config is based on the DAG run config and the difference is that the output point to csv
        file in key original_data_path instead of avsc file (as it is in DAG run config).
        :param ingestion_config: the config of the DAG run
        :param csv_files_list: the list of csv that applicable for further processing
        :return:
        """
        output_dict = dict()
        for csv_file in csv_files_list:
            avsc_file = csv_file.split(".")[0] + ".avsc"
            for table_name, table_config in ingestion_config.items():
                if table_config['original_file_path'].endswith(avsc_file):
                    table_config_copy = copy.copy(table_config)
                    original_bucket = '/'.join(table_config_copy['original_file_path'].split('/')[:3])
                    table_config_copy['original_data_path'] = '/'.join([original_bucket, csv_file])
                    output_dict[table_name] = table_config_copy
        return output_dict

    def _sort_csv_files(self, task_instance_handle: TaskInstance):
        """
        Function sorts expected csv files into 2 lists: first are applicable csvs that exist on the bucket,
        second is the list of missing csv files.
        :param task_instance_handle:
        :return: 2 lists, first is the list of csv files applicable for further processing, the second is the list
                 of missing csv files
        """
        initial_config = None
        try:
            initial_config = task_instance_handle.xcom_pull(task_ids='parse_dagrun_config')
        except TypeError as e:
            logger.error(f"Failed to find xcom for task 'parse_dagrun_config'. Error: {e}")
        if not initial_config:
            return {}
        logger.info(f"found initial_config: {initial_config}")

        listing_task_xcom_value = None
        try:
            listing_task_xcom_value = task_instance_handle.xcom_pull(task_ids=str(self.listing_files_task_id))
        except TypeError as e:
            logger.error(f"Failed to find xcom for task {self.listing_files_task_id}. Error: {e}")
        if not listing_task_xcom_value:
            return {}
        logger.info(f"listing_task_xcom_value: {listing_task_xcom_value}")
        expected_files_task_xcom_value = None
        try:
            expected_files_task_xcom_value = task_instance_handle.xcom_pull(
                task_ids=str(self.expected_files_task_id))
        except TypeError as e:
            logger.error(f"Failed to find xcom for task {self.listing_files_task_id}. Error: {e}")
        if not expected_files_task_xcom_value:
            return {}
        applicable_csv_files = [x for x in expected_files_task_xcom_value if x in listing_task_xcom_value]
        logger.info(f"applicable_csv_files: {applicable_csv_files}")
        self.expected_files_config = self._procure_expected_files_config(initial_config, applicable_csv_files)

        missing_csv_files = [x for x in expected_files_task_xcom_value if x not in listing_task_xcom_value]
        logger.info(f"missing_csv_files: {missing_csv_files}")
        return applicable_csv_files, missing_csv_files

    def execute(self, context):
        self.expected_csv_files, self.missing_csv_files = self._sort_csv_files(context['ti'])
        self.not_expected_csv_files = []  # TODO: csv cannot stay in temporary storage forever...
        context['ti'].xcom_push("expected_csv_files", self.expected_csv_files)
        context['ti'].xcom_push("missing_csv_files", self.missing_csv_files)
        context['ti'].xcom_push("not_expected_csv_files", self.not_expected_csv_files)
        context['ti'].xcom_push("expected_csv_files_config", self.expected_files_config)
