import datetime
from typing import Sequence
from airflow.models import BaseOperator
import json
import sys
sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")

from src.common.utils.statics import DAGRUN_FAILED_PARENT_TASKS, DAGRUN_FINISHED_PARENT_TASKS, ENRICHMENT_FAILED


class FlattenResultsOperator(BaseOperator):
    template_fields: Sequence[str] = ("incorrect_avro_files", "correct_avro_files")

    def __init__(self, marking_task_id, **kwargs) -> None:
        super().__init__(**kwargs)
        self.incorrect_avro_files = []
        self.correct_avro_files = []
        self.marking_task_id = marking_task_id
        self.correct_avro_tables = []
        self.incorrect_avro_tables = []

    def flatten_avro_results_function(self, xcom_key_name: str, task_instance_handle):
        xcom_value = task_instance_handle.xcom_pull(
            task_ids=self.marking_task_id,
            key=xcom_key_name)
        if not xcom_value:
            return []
        elif isinstance(xcom_value, dict):
            input_dict = xcom_value
        else:
            input_dict = json.loads(xcom_value)
        output_list = list()
        for values in input_dict.values():
            if DAGRUN_FINISHED_PARENT_TASKS == xcom_key_name:
                value = [x['source'] for x in values]
            else:
                value = values
            output_list.extend(value)
        return output_list

    def _generate_avro_files_from_table(self, input_list):
        output = []
        self.log.info("Generating avro files out of the table name")
        for avro_table in input_list:
            if avro_table.find("_") < 0:
                output.append(avro_table)
                continue
            avro_table_last_part = avro_table.split("_")[-1]
            try:
                datetime.datetime.strptime(avro_table_last_part, "%Y%m%d%H%M%S")
            except ValueError as e:
                self.log.info(f"Could not parse {avro_table_last_part} to date.")
                output.append(avro_table)
                continue
            output.append("_".join(avro_table.split("_")[:-1]))
        return output

    def execute(self, context):
        self.correct_avro_tables = self.flatten_avro_results_function(DAGRUN_FINISHED_PARENT_TASKS, context['ti'])
        self.correct_avro_files = self._generate_avro_files_from_table(self.correct_avro_tables)
        self.incorrect_avro_tables = self.flatten_avro_results_function(DAGRUN_FAILED_PARENT_TASKS, context['ti'])
        self.incorrect_avro_files = self._generate_avro_files_from_table(self.incorrect_avro_tables)
        context['ti'].xcom_push("correct_avro_files", self.correct_avro_files)
        context['ti'].xcom_push("correct_avro_tables", self.correct_avro_tables)
        context['ti'].xcom_push("incorrect_avro_files", self.incorrect_avro_files)
        context['ti'].xcom_push("incorrect_avro_tables", self.incorrect_avro_tables)
