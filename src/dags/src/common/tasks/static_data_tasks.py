import logging
import json
import os
import sys
from io import StringIO
from json.decoder import JSONDecodeError
from datetime import datetime
from airflow.models import Variable
from airflow.decorators import task
from airflow.utils.trigger_rule import TriggerRule
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.common.sql.operators.sql import SQLExecuteQueryOperator

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

from src.common.utils.statics import TABLE_SUFFIX_PATTERN
# from src.dags.utils.exceptions import TwainCustomException
from utils.trino_manager import TrinoManager
from avro_processor import trino_create_table_statement, normalize_table_name, parse_avro_schema
from csv_processor import (
    load_csv_with_schema,
    build_sql_with_parquet,
    parquet_data_from_dframe,
    DEFAULT_DATA_SHARING_AGREEMENT,
)
from utils.exceptions import TwainCustomException, DATE_PARSING_EXCEPTION, PARSING_ERROR

logger = logging.getLogger(__name__)
BUCKET_PUBLIC: str = Variable.get("static_bucket_public", "static-data-ingestion-public")
BUCKET_DATALAKE: str = Variable.get("bucket_datalake", "datalake")
BUCKET_RESULTS: str = Variable.get("static_bucket_results", "static-data-ingestion-results")


class TrinoSyncOperator(SQLExecuteQueryOperator):
    def execute(self, context):
        context['ti'].xcom_push(key="ingestion_stage", value="trino_create_table")
        super().execute(context)
        context['ti'].xcom_push(key="task_finished_successfuly", value=True)


def coalesce(arg: list):
    """
    Function returns first non-None element in the list
    :param arg: a list of elements
    :return: first not-none element
    """
    return next((a for a in arg if a is not None), None)


def failure_callback(context):
    print("FAIL")
    print(f"{context['ti']}")


def copy_kwargs(file: str) -> dict:
    """Utility function to make kwargs that matches signature of S3CopyObjectOperator.
    Copy original AVSC file.
    """
    logger.info(f"File to process: {file}")
    logger.info(f"Source key: s3://{BUCKET_PUBLIC}/{file}")
    logger.info(f"Destination key: s3://{BUCKET_DATALAKE}/{file.split('/')[-1]}")
    return {"source_bucket_key": file, "dest_bucket_key": file.split("/")[-1]}


@task(trigger_rule=TriggerRule.NONE_SKIPPED)
def ingestion_report(csv_files, avro_files, **context):
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    file_pair_ingestion_details = context['ti'].xcom_pull(
        key="file_pair_ingestion_details"
    )
    ingestion_status_success = context['ti'].xcom_pull(
        key='task_finished_successfuly'
    )
    now = datetime.now().strftime(TABLE_SUFFIX_PATTERN)
    if ingestion_status_success:
        logger.info("Task finished successfully, storing ingestion success dead letters.")
        for avro_file_path, ingestion_results in file_pair_ingestion_details.items():
            table_name = ingestion_results["expected_table_name"]
            csv_file_path = ingestion_results["paired_csv_date_file"]
            parquet_filepaths = ingestion_results["csv_to_parquet_map"][csv_file_path]
            parquet_file_list = '\n'.join(parquet_filepaths)

            content = f"""
            STATIC INGESTION SUCCESS. Finished at: {now}
            Generated table: {table_name}
            Generated .parquet files:
            {parquet_file_list}
            """

            source_s3.load_string(
                string_data=content,
                key=f"success_ingestion_{csv_file_path.replace(r'/', '_subpath_')}_{now}.txt",
                bucket_name=BUCKET_RESULTS,
                replace=True,
            )
    else:
        reported_files = []

        logger.info("Task finished unsuccessfully, storing ingestion failure dead letters.")
        ingestion_stages = context['ti'].xcom_pull(
            task_ids=['trino_create_table_parquet', 'build_sync_partitions', 'build_create_table'],
            key='ingestion_stage'
        )
        logger.info(f"Picking one of ingestion_stages: {ingestion_stages}.")
        ingestion_stage = coalesce(ingestion_stages)
        incorrect_avsc = context['ti'].xcom_pull(key='incorrect_avsc')
        if not ingestion_stage:
            ingestion_stage = 'UNKNOWN'
        if incorrect_avsc is None:
            incorrect_avsc = dict()
        if file_pair_ingestion_details is None:
            file_pair_ingestion_details = dict()

        at_least_one_correct_ingestion = False
        # handling missing ingestions
        content = f"""
        STATIC INGESTION FAILED at {now}.
        Ingestion failed on stage: {ingestion_stage}
        """
        for avro_file_path, ingestion_results in file_pair_ingestion_details.items():
            if ingestion_results["csv_to_parquet_map"]:
                csv_file_path = ingestion_results["paired_csv_date_file"]
                try:
                    if ingestion_results["csv_to_parquet_map"][csv_file_path]:
                        at_least_one_correct_ingestion = True
                        continue
                except KeyError:
                    logger.info(f"Avro file {avro_file_path} paired with {csv_file_path} has no valid "
                                f"parquet files, will be marked as ingestion failure.")
            if avro_file_path in incorrect_avsc.keys():
                continue
            csv_file_path = ingestion_results["paired_csv_date_file"]
            source_s3.load_string(
                string_data=content,
                key=f"failed_ingestion_{csv_file_path.replace(r'/', '_subpath_')}_{now}.txt",
                bucket_name=BUCKET_RESULTS,
                replace=True,
            )
            reported_files.append(avro_file_path)
        # handling incorrect (avsc parsing error and mismatching datetime parsing pattern
        # between avsc and csv) ingestions
        content_for_unparsable = """
        STATIC INGESTION FAILED at {}.
        Ingestion failed on stage: {}
        Error details: {}
        Incorrect file: {}
        """
        for avsc_file_path, avsc_error_message in incorrect_avsc.items():
            string_data = content_for_unparsable.format(now, ingestion_stage, avsc_error_message, avsc_file_path)
            source_s3.load_string(
                string_data=string_data,
                key=f"failed_ingestion_{avsc_file_path.replace(r'/', '_subpath_')}_{now}.txt",
                bucket_name=BUCKET_RESULTS,
                replace=True,
            )
            reported_files.append(avsc_file_path)
        # handling correct ingestions
        if at_least_one_correct_ingestion:
            logger.info("Whole batch finished unsuccessfully, but at least one file in the batch succeeded, "
                        "storing ingestion success dead letters.")
        for avro_file_path, ingestion_results in file_pair_ingestion_details.items():
            if not ingestion_results["csv_to_parquet_map"]:
                continue
            table_name = ingestion_results["expected_table_name"]
            csv_file_path = ingestion_results["paired_csv_date_file"]
            if not ingestion_results["csv_to_parquet_map"][csv_file_path]:
                continue
            parquet_filepaths = ingestion_results["csv_to_parquet_map"][csv_file_path]
            parquet_file_list = '\n'.join(parquet_filepaths)

            content = f"""
            STATIC INGESTION SUCCESS. Finished at: {now}
            Generated table: {table_name}
            Generated .parquet files:
            {parquet_file_list}
            """
            source_s3.load_string(
                string_data=content,
                key=f"success_ingestion_{csv_file_path.replace(r'/', '_subpath_')}_{now}.txt",
                bucket_name=BUCKET_RESULTS,
                replace=True,
            )
            reported_files.append(avro_file_path)

        # case where .avsc is invalid.
        # We may throw exception early with multiple file batches and the whole ingestion fails.
        # in such case we should go through list of files and generate relevant report
        # based on the one from first failed file of the batch
        content = f"""
            STATIC INGESTION FAILED at {now}.
            Ingestion failed on stage: {ingestion_stage}
            """
        for f in avro_files:
            if f not in reported_files:
                source_s3.load_string(
                    string_data=content,
                    key=f"failed_ingestion_{f.replace(r'/', '_subpath_')}_{now}.txt",
                    bucket_name=BUCKET_RESULTS,
                    replace=True,
                )

        raise Exception("Marking whole ingestion as a failure on Airflow.")


def _get_expected_ingestion_details(source_s3: S3Hook, avro_files: list, csv_files: list):
    """
    Function returns the expected names of the static ingestion tables and the expected count of parquet files
    per each table.
    If the table name cannot be procured because of invalid semi-avro schema, the file is stored in
    unparsable_avsc dictionary which values are the error messages.
    :param source_s3: s3 hook to locations where the input files are stored
    :param avro_files: the list of avro file paths
    :param csv_files: the list of csv file paths
    :return: a tuple which first element is a dictionary fo expected table names and their parquet files count
             and the second element is the dictionary of unparsable avro schema files
    """
    expected_table_ingestion_details = dict()
    unparsable_avsc = dict()
    for avro_f, csv_f in zip(avro_files, csv_files):
        logger.info(f"Getting expected table name for {avro_f} with csv file {csv_f}.")
        stripped_avro = avro_f.split('/')[-1]
        try:
            schema = json.loads(source_s3.read_key(stripped_avro, bucket_name=BUCKET_DATALAKE))
            expected_table_name = schema["name"]
        except JSONDecodeError as json_decode_error:
            error_message = f"Error parsing file {avro_f}. Are you sure it is a json?"
            logger.error(error_message)
            unparsable_avsc[avro_f] = error_message
            continue
        except KeyError as key_error:
            error_message = f"Error parsing file {avro_f}. Missing parameter 'name' responsible for " \
                            f"containing the target table name."
            logger.error(error_message)
            unparsable_avsc[avro_f] = error_message
            continue
        try:
            parse_avro_schema(schema)
        except TwainCustomException as tpe:
            logger.error(tpe.details)
            unparsable_avsc[avro_f] = tpe.details
            continue
        expected_table_ingestion_details[avro_f] = {
            "paired_csv_date_file": csv_f,
            "expected_table_name": normalize_table_name(schema["name"]),
            "csv_to_parquet_map": dict()
        }
    return expected_table_ingestion_details, unparsable_avsc


def get_table_partitions_as_strings(table_name: str, current_table_partitions: dict):
    """
    Function procures a list of partitions of te input table in format of string URI paths.
    :param table_name: the name of the table
    :param current_table_partitions: the dictionary containing the partitions of the tables
    :return: a list of strings, each representing one partition URI
    """
    logger.info(f"current_table_partitions: {current_table_partitions}")
    if (current_table_partitions[table_name]) is None or (current_table_partitions[table_name]['data'] is None):
        return None
    partition_count = \
        max(current_table_partitions[table_name]['data']
            [next(iter(current_table_partitions[table_name]['data']))].keys()) + 1
    logger.info(f"There are {partition_count} partitions in the table")
    partitions_full_strings = list()
    for x in range(0, partition_count):
        partition_strings = list()
        for partition_column_name in current_table_partitions[table_name]['columns']:
            partition_string_part = \
                f"{partition_column_name}=" + \
                current_table_partitions[table_name]['data'][partition_column_name][x]
            partition_strings.append(partition_string_part)
        partition_string = '/'.join(partition_strings)
        partitions_full_strings.append(partition_string)
    logger.info(f"Table {table_name} partitions in string format: {partitions_full_strings}.")
    return partitions_full_strings


@task
def build_sync_partitions(avsc_files: list, **context) -> list:
    context["ti"].xcom_push(
        key="ingestion_stage", value="build_sync_partitions"
    )

    source_s3 = S3Hook(aws_conn_id="aws_minio")
    sql_list: list[str] = []
    for avro_f in avsc_files:
        schema = json.loads(source_s3.read_key(avro_f, bucket_name=BUCKET_PUBLIC))
        table_name = normalize_table_name(schema["name"], lower_case=True)

        sql_list.append(
            f"CALL system.sync_partition_metadata('default', '{table_name}', 'DROP')"
        )

        sql_list.append(
            f"CALL system.sync_partition_metadata('default', '{table_name}', 'ADD')"
        )
    return sql_list


def infer_ingestion_status(table_name: str, parquet_files_paths: list):
    """
    Function infers the ingestion status based on the table name and the list of parquet file paths
    generated during the ETL.
    It gets the currently existing Trino partitions and if any of parquet files resides within this
    partition it is added to the output list
    :param table_name: the input table name
    :param parquet_files_paths: the full paths of parquets generated on s3 bucket
    :return: a tuple which first element is the boolean status of the table ingestion and the second element
             is the list of parquet file paths that reside within the already existing Trino partitions
    """
    output_status = False
    output_list = list()
    trinoHost = Variable.get("TWAIN_TRINO_SERVICE_URL")
    trinoPort = Variable.get("TWAIN_TRINO_SERVICE_PORT")
    TRINO_USER: str = Variable.get("TRINO_USER")
    TRINO_PASSWORD: str = Variable.get("TRINO_PASSWORD")
    tm = TrinoManager(
        host=trinoHost, port=trinoPort,
        username=TRINO_USER,
        password=TRINO_PASSWORD,
        catalog='hive',
        schema='default')
    current_table_partitions = tm.get_tables_partitions([table_name])
    if not current_table_partitions:
        return output_status, list()
    partition_strings = get_table_partitions_as_strings(table_name, current_table_partitions)
    if not partition_strings:
        return output_status, list()
    for parquet_file_path in parquet_files_paths:
        parquet_file_path_partition = "/".join(parquet_file_path.split("/")[1:-1])
        if parquet_file_path_partition in partition_strings:
            output_status = True
            output_list.append(parquet_file_path)
    logger.info(f"Correctly ingested parquet files for table {table_name}: {output_list}")
    return output_status, output_list


@task
def build_create_table(avsc_files: list, csv_files: list, sub_bucket: str, **context) -> list:
    """
    build create table SQL and set the data
    """
    context["ti"].xcom_push(
        key="ingestion_stage", value="build_create_table"
    )
    avro_files = sorted(avsc_files, key=lambda x: x.split(".")[0])
    csv_files = sorted(csv_files, key=lambda x: x.split(".")[0])

    source_s3 = S3Hook(aws_conn_id="aws_minio")

    file_pair_ingestion_details, incorrect_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    context["ti"].xcom_push(
        key="file_pair_ingestion_details", value=file_pair_ingestion_details
    )
    context["ti"].xcom_push(
        key="incorrect_avsc", value=incorrect_avsc
    )
    context["ti"].xcom_push(key="source_files", value=csv_files)

    sql_list = []
    # note: avro_f and csv_f are relative paths, not just file names
    for avro_f, csv_f in zip(avro_files, csv_files):
        csv_to_parquet_map = {}
        try:
            logger.info(f"processing {avro_f} with {csv_f}...")
            if avro_f in incorrect_avsc.keys():
                raise TwainCustomException(message=PARSING_ERROR, details=incorrect_avsc[avro_f])
            stripped_avro = avro_f.split('/')[-1]
            stripped_csv = csv_f.split('/')[-1]

            schema = json.loads(source_s3.read_key(stripped_avro, bucket_name=BUCKET_DATALAKE))
            csv_data = source_s3.read_key(stripped_csv, bucket_name=BUCKET_DATALAKE)
            table_name = schema["name"]
            table_name_normalized = normalize_table_name(table_name)

            parquet_files = parquet_data_from_dframe(
                load_csv_with_schema(StringIO(csv_data), schema=schema, owner=sub_bucket),
                schema=schema,
            )

            print(f"GENERATED PARQUET FILES: {parquet_files}")

            filename = stripped_csv.replace(".csv", ".parquet")
            parquet_files_names_on_table = list()
            for f in parquet_files:
                parquet_file_path_on_target_table = f.replace('/tmp/', '')
                parquet_files_names_on_table.append(parquet_file_path_on_target_table)
                data: bytes = open(f, "rb").read()
                source_s3.load_bytes(
                    bytes_data=data,
                    key=parquet_file_path_on_target_table,
                    bucket_name=BUCKET_DATALAKE,
                    replace=True,
                )
                csv_to_parquet_map[csv_f] = parquet_files_names_on_table
                os.remove(f)

            sql_list.append(
                "\n".join(
                    (
                        trino_create_table_statement(schema=schema),
                        build_sql_with_parquet(
                            path=f"s3a://{BUCKET_DATALAKE}/{table_name_normalized}/",
                            # trino/hive requires the partition key to be very last
                            # field of the table
                            # https://trino.io/docs/current/release/release-0.137.html
                            column="owning_company",
                        ),
                    )
                )
            )
            file_pair_ingestion_details[avro_f]["csv_to_parquet_map"] = csv_to_parquet_map
        except TwainCustomException as tce:
            if tce.args[0] in (DATE_PARSING_EXCEPTION, PARSING_ERROR):
                # adding datetime format parsing error to the incorrect file list
                incorrect_avsc[avro_f] = tce.details
                context["ti"].xcom_push(
                    key="incorrect_avsc", value=incorrect_avsc
                )
            context["ti"].xcom_push(
                key="file_pair_ingestion_details", value=file_pair_ingestion_details
            )
            all_parquets_in_minio = [x["csv_to_parquet_map"] for x in file_pair_ingestion_details.values()]
            logger.info(f"all_parquets_in_minio: {all_parquets_in_minio}")

            def flatten(xss):
                return [x for xs in xss for x in xs]

            if not flatten(all_parquets_in_minio):
                raise tce

            logger.info(
                f"Handling ingestion error, file_pair_ingestion_details = {file_pair_ingestion_details}")

            for avr_file_path, avro_file_ingestion_details in file_pair_ingestion_details.items():
                processed_table_name = avro_file_ingestion_details["expected_table_name"]
                for csv_file_path, parquet_file_paths_on_target_table in avro_file_ingestion_details[
                    "csv_to_parquet_map"].items():
                    inferred_ingestion_status, ingested_parquet_file_paths = \
                        infer_ingestion_status(processed_table_name, parquet_file_paths_on_target_table)
                    # if inferred_ingestion_status:
                    avro_file_ingestion_details["csv_to_parquet_map"][csv_file_path] = ingested_parquet_file_paths
            context["ti"].xcom_push(
                key="file_pair_ingestion_details", value=file_pair_ingestion_details
            )
            raise tce

        except Exception as e:
            context["ti"].xcom_push(
                key="file_pair_ingestion_details", value=file_pair_ingestion_details
            )
            all_parquets_in_minio = [x["csv_to_parquet_map"] for x in file_pair_ingestion_details.values()]
            logger.info(f"all_parquets_in_minio: {all_parquets_in_minio}")

            def flatten(xss):
                return [x for xs in xss for x in xs]

            if not flatten(all_parquets_in_minio):
                raise e
            # if at least one ingestion succeeded so far, the data may be visible to users,
            # meaning the ingestion of those files was a success and we should not prompt
            # user to resend those particular files
            logger.info(f"Handling ingestion error, file_pair_ingestion_details = {file_pair_ingestion_details}")
            # correctly_ingested_tables_so_far = dict()
            # for processed_table_name, parquet_file_paths_on_target_table in parquets_per_table.items():
            #     inferred_ingestion_status, ingested_parquet_file_paths = \
            #         infer_ingestion_status(processed_table_name, parquet_file_paths_on_target_table)
            #     if inferred_ingestion_status:
            #         correctly_ingested_tables_so_far[processed_table_name] = ingested_parquet_file_paths
            # if correctly_ingested_tables_so_far:
            #     context["ti"].xcom_push(
            #         key="generated_files", value=correctly_ingested_tables_so_far
            #     )
            for avr_file_path, avro_file_ingestion_details in file_pair_ingestion_details.items():
                processed_table_name = avro_file_ingestion_details["expected_table_name"]
                for csv_file_path, parquet_file_paths_on_target_table in avro_file_ingestion_details[
                    "csv_to_parquet_map"].items():
                    inferred_ingestion_status, ingested_parquet_file_paths = \
                        infer_ingestion_status(processed_table_name, parquet_file_paths_on_target_table)
                    # if inferred_ingestion_status:
                    avro_file_ingestion_details["csv_to_parquet_map"][csv_file_path] = ingested_parquet_file_paths
            context["ti"].xcom_push(
                key="file_pair_ingestion_details", value=file_pair_ingestion_details
            )
            raise e

    context["ti"].xcom_push(
        key="file_pair_ingestion_details", value=file_pair_ingestion_details
    )
    return sql_list
