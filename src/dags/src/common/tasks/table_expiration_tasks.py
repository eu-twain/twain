import datetime
import json
import logging
import pytz
import trino
from airflow.decorators import task
from airflow.exceptions import AirflowException
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from src.common.utils.statics import PROTECTED_TABLES, DATA_EXPIRATION_DATE_PATTERN
from utils.trino_manager import TrinoManager

GET_TABLE_ORIGINAL_FILES_QUERY = """
  select
    twain_original_file_name,
    twain_data_owning_company,
    twain_data_owning_member,
    count(1) row_count
  from {}
  group by
    twain_original_file_name,
    twain_data_owning_company,
    twain_data_owning_member
  order by 1 desc"""


@task()
def get_trino_tables_with_expiry(trinoHost: str, trinoPort: int, TRINO_USER: str, TRINO_PASSWORD: str) -> dict:
    tm = TrinoManager(
        host=trinoHost,
        port=trinoPort,
        username=TRINO_USER,
        password=TRINO_PASSWORD,
        catalog='hive',
        schema='default'
    )
    tm.log.info(f"Expiration starts at {datetime.datetime.now(tz=pytz.UTC)}")

    tables_with_comments = tm.run_query(
        """
        select 
          table_name,
          comment
        from
          system.metadata.table_comments
        where
          catalog_name='hive'
          and schema_name='default'
          and comment is not null
        """
    )

    tables_expiries = dict()
    for table_with_comment in tables_with_comments:
        table_comment_parsed = None
        table_name = table_with_comment[0]
        if table_name.lower() in PROTECTED_TABLES:
            continue
        try:
            table_comment = table_with_comment[1]
            table_comment_parsed = json.loads(table_comment)
        except json.decoder.JSONDecodeError as json_error:
            tm.log.error(f"Could not parse comment of table {table_name}")
            continue
        if 'expiry_date' in table_comment_parsed.keys():
            tables_expiries[table_name] = table_comment_parsed

    tm.log.info(tables_expiries)
    tm.connection.close()
    return tables_expiries


@task
def get_overdue_tables(tables_expiries: dict, logger: logging.Logger):
    overdue_tables = dict()
    for table_name, table_comment_with_expiry in tables_expiries.items():
        try:
            expiry_date = datetime.datetime.strptime(
                    table_comment_with_expiry['expiry_date'],
                    DATA_EXPIRATION_DATE_PATTERN
            )
            expiry_date = expiry_date.replace(tzinfo=pytz.UTC)
            if expiry_date <= datetime.datetime.now(tz=pytz.UTC):
                overdue_tables[table_name] = table_comment_with_expiry['expiry_date']
        except ValueError as ve:
            logger.error(f"Could not parse date in the comment of table {table_name}")
    logger.info(f"overdue_tables: {overdue_tables}")
    return overdue_tables


@task(multiple_outputs=True)
def split_bronze_table_from_the_rest(overdue_tables: dict, logger: logging.Logger, bronze_table_prefix: str):
    bronze_tables_to_be_expired = dict()
    standard_tables_to_be_expired = dict()
    for overdue_table, overdue_table_expiry_string in overdue_tables.items():
        if overdue_table.startswith(bronze_table_prefix) and not overdue_table.endswith("_schema"):
            bronze_tables_to_be_expired[overdue_table] = overdue_table_expiry_string
        else:
            standard_tables_to_be_expired[overdue_table] = overdue_table_expiry_string

    logger.info(f"standard_tables_to_be_expired: {standard_tables_to_be_expired}")
    logger.info(f"bronze_tables_to_be_expired: {bronze_tables_to_be_expired}")
    return {
        "standard_tables_to_be_expired": standard_tables_to_be_expired,
        "bronze_tables_to_be_expired": bronze_tables_to_be_expired
    }


@task(multiple_outputs=True)
def handle_expiration_of_standard_tables(input_dict: dict, trinoHost: str, trinoPort: int, TRINO_USER: str,
                                         TRINO_PASSWORD: str, aws_conn_id: str):
    tm = TrinoManager(
        host=trinoHost, port=trinoPort,
        username=TRINO_USER,
        password=TRINO_PASSWORD,
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    standard_tables_that_were_expired = list()
    failed_deletions = {
        "failed_trino_tables": [],
        "failed_minio_objects": []
    }
    standard_tables_to_be_expired = input_dict["standard_tables_to_be_expired"]
    for standard_table_to_be_expired in standard_tables_to_be_expired.keys():
        table_location = tm.get_file_location(standard_table_to_be_expired)
        tm.log.info(f"Table {standard_table_to_be_expired} resides in {table_location}")
        bucket, prefix = s3_hook.parse_s3_url(table_location)
        tm.log.info(f"Listing table {standard_table_to_be_expired} files in bucket {bucket} and path {prefix}")
        minio_keys = s3_hook.list_keys(
            bucket_name=bucket,
            prefix=prefix
        )
        tm.log.info(f"minio_keys: {minio_keys}")
        try:
            tm.drop_table(standard_table_to_be_expired)
            s3_hook.delete_objects(bucket=bucket, keys=minio_keys)
            standard_tables_that_were_expired.append(standard_table_to_be_expired)
            tm.log.info(f"Table {standard_table_to_be_expired} and its underlying data files removed.")
        except AirflowException as ae:
            failed_deletions["failed_trino_tables"].append(standard_table_to_be_expired)
        except trino.exceptions.TrinoUserError as tue:
            failed_deletions["failed_minio_objects"].append(minio_keys)
    if tm.connection:
        tm.connection.close()
    return {
        "failed_deletions": failed_deletions,
        "standard_tables_that_were_expired": standard_tables_that_were_expired
    }


def sanitize_ingestion_report_content(input_uris_string: str):
    if "*" in input_uris_string:
        return None
    return input_uris_string.split(",")


def expire_data_from_consumption_layer(tm: TrinoManager, s3_hook: S3Hook, bronze_table_name: str, bucket_results: str):
    results = {
        "bronze_table_data_files_removed_from_consumption": list(),
        "bronze_table_data_files_NOT_removed_from_consumption": list()
    }
    tm.log.info(f"Collecting original file names for table {bronze_table_name}")
    query = GET_TABLE_ORIGINAL_FILES_QUERY.format(bronze_table_name)
    original_file_name_details = tm.run_query(query)
    tm.log.info(f"Table {bronze_table_name} was created by {len(original_file_name_details)} files.")
    for original_file_name_detail in original_file_name_details:
        tm.log.info(f"{original_file_name_detail}")
        expected_ingestion_report_name = f"success_ingestion_{original_file_name_detail[0]}.txt"
        if not s3_hook.check_for_wildcard_key(expected_ingestion_report_name, bucket_results):
            tm.log.info(f"File {original_file_name_detail[0]} ingestion report not found. "
                        f"Cannot remove data from consumption layer.")
            results["bronze_table_data_files_NOT_removed_from_consumption"].append(original_file_name_detail[0])
            continue
        ingestion_report_content = s3_hook.read_key(expected_ingestion_report_name, bucket_results)
        tm.log.info(f"File {original_file_name_detail[0]} ingestion report contents: {ingestion_report_content}")
        minio_keys_to_remove = sanitize_ingestion_report_content(ingestion_report_content)
        if not minio_keys_to_remove:
            tm.log.info(f"File {original_file_name_detail[0]} ingestion report was not parsed or contained "
                        f"wildcards. Cannot remove data from consumption layer.")
            results["bronze_table_data_files_NOT_removed_from_consumption"].append(original_file_name_detail[0])
            continue
        bucket, _ = s3_hook.parse_s3_url(minio_keys_to_remove[0])
        minio_keys_to_remove = [s3_hook.parse_s3_url(x)[1] for x in minio_keys_to_remove]
        tm.log.info(f"minio_keys_to_remove after sanitization: {minio_keys_to_remove}")
        try:
            s3_hook.delete_objects(bucket=bucket, keys=minio_keys_to_remove)
        except AirflowException as ae:
            tm.log.info(ae)
            results["bronze_table_data_files_NOT_removed_from_consumption"].append(original_file_name_detail[0])
            continue
        results["bronze_table_data_files_removed_from_consumption"].append(original_file_name_detail[0])
    return results


@task(multiple_outputs=True)
def handle_expiration_of_bronze_tables(input_dict: dict, trinoHost: str, trinoPort: int, TRINO_USER: str,
                                       TRINO_PASSWORD: str, aws_conn_id: str, bucket_results: str):
    tm = TrinoManager(
        host=trinoHost, port=trinoPort,
        username=TRINO_USER,
        password=TRINO_PASSWORD,
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    bronze_tables_to_be_expired = input_dict["bronze_tables_to_be_expired"]
    tm.log.warning("Bronze layer tables must also check if they were ingested correctly by getting it "
                   "original file name, converting it to ingestion report and get the parquet files from ingestion"
                   " report to be deleted; disallow wildcards in that deletion!")
    failed_deletions = {
        "failed_trino_tables": [],
        "failed_minio_objects": []
    }
    consumption_layer_results = dict()
    bronze_tables_that_were_expired = list()
    for bronze_table_to_be_expired in bronze_tables_to_be_expired.keys():
        table_location = tm.get_file_location(bronze_table_to_be_expired)
        tm.log.info(f"Table {bronze_table_to_be_expired} resides in {table_location}")
        bucket, prefix = s3_hook.parse_s3_url(table_location)
        tm.log.info(f"Listing table {bronze_table_to_be_expired} files in bucket {bucket} and path {prefix}")
        minio_keys = s3_hook.list_keys(
            bucket_name=bucket,
            prefix=prefix
        )
        tm.log.info(f"minio_keys: {minio_keys}")
        consumption_layer_result = expire_data_from_consumption_layer(
            tm,
            s3_hook,
            bronze_table_to_be_expired,
            bucket_results
        )
        consumption_layer_results[bronze_table_to_be_expired] = consumption_layer_result
        try:
            tm.drop_table(bronze_table_to_be_expired)
            s3_hook.delete_objects(bucket=bucket, keys=minio_keys)
            bronze_tables_that_were_expired.append(bronze_table_to_be_expired)
            tm.log.info(f"Table {bronze_table_to_be_expired} and its underlying data files removed.")
        except AirflowException as ae:
            failed_deletions["failed_trino_tables"].append(bronze_table_to_be_expired)
        except trino.exceptions.TrinoUserError as tue:
            failed_deletions["failed_minio_objects"].append(minio_keys)
    if tm.connection:
        tm.connection.close()
    return {
        "failed_deletions": failed_deletions,
        "bronze_tables_that_were_expired": bronze_tables_that_were_expired,
        "consumption_layer_results": consumption_layer_results
    }
