import sys
import os
import logging
import datetime
import json
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.models import Variable
from src.common.utils.statics import TABLE_SUFFIX_PATTERN, INGESTION_FAILURE_MODE

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)
BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")
CSV_TABLE_PREFIX: str = Variable.get("csv_table_prefix", "csv_")


def create_dir_if_not_exists(input_dir) -> None:
    """
    Function recursively creates a directory on local file system.
    :param input_dir: a path to be created
    :return: None
    """
    if os.path.exists(input_dir):
        return
    try:
        os.mkdir(input_dir)
    except FileNotFoundError as fnf:
        create_dir_if_not_exists(os.path.dirname(input_dir))
        os.mkdir(input_dir)


def get_csv_table_name(file_name: str, ingestion_timestamp: str, table_name_mode=True, force_lowercase=True):
    """
    Function generates expected table name for incoming file's name.
    :param file_name: the name of the incoming file
    :param ingestion_timestamp: the string representation of ingestion timestamp
    :param table_name_mode: flag indicating of the resulting file name as to be compliant with SQL table name standard
    :param force_lowercase: flag indicating if the resulting table name has to be lowercase or it can have the same
                            letter cases as in the name of the incoming file
    :return: the expected csv table name
    """
    if force_lowercase:
        output = f"{CSV_TABLE_PREFIX}{file_name.split('.')[0].lower()}_{ingestion_timestamp}"
    else:
        output = f"{CSV_TABLE_PREFIX}{file_name.split('.')[0]}_{ingestion_timestamp}"
    if table_name_mode:
        return output.replace('-', '_').replace(' ', '_')  # get rid of .csv
    return output


def drop_bucket_name(file_s3_path: str):
    """
    Function gets rid of the s3 (or s3-like) protocol and bucket name from the full s3 file path.
    :param file_s3_path: the full s3 file path, example: "s3://ingestion-public/batch/fake_data.csv"
    :return: the relative path of the file, example: "batch/fake_data.csv"
    """
    return "/".join(file_s3_path.split("/")[3:])


def transform_data_location_to_visible_by_spark(input_location: str):
    """
    Function procures the full s3 path as accessible by downstream spark application (which uses s3a:// protocol).
    :param input_location: the path on s3/MinIO, example: "s3://ingestion-public/batch/fake_data.csv"
    :return: the path on s3/MinIO as accessible by spark "s3a://ingestion-public/batch/fake_data.csv"
    """
    search_string = 's3://'
    if input_location.startswith(search_string):
        input_location = 's3a://' + input_location[len(search_string):]
    return input_location


def _procure_dead_letter_details(
        bucket_name: str, upstream_task_id: str, upstream_task_output_key, upstream_task_output_value) -> tuple:
    """
    Function procures the file name and the content of the error message.
    :param bucket_name: name of the bucket the csv file originated from
    :param upstream_task_id: the name of the task that has some errors
    :param upstream_task_output_key: single key of the output of upstream_task_id
    :param upstream_task_output_value: single value for the key of the output of upstream_task_id
    :return: a 2 element tuple of file content as string and the name of the file that failed the ingestion
    """
    error_summary = dict()
    if upstream_task_id == "get_missing_csv_files":
        csv_file_name = upstream_task_output_key.split("/")[-1]
        avsc_file_name = ".".join(csv_file_name.split(".")[:-1]) + ".avsc"
        full_file_path = f"s3://{bucket_name}/{upstream_task_output_key}"
        error_summary['error_description'] = f"Missing corresponding csv file {full_file_path}"
        output_file_name = avsc_file_name
    elif upstream_task_id == "get_incorrect_avros":
        output_file_name = upstream_task_output_key.split("/")[-1]
        full_file_path = f"s3://{bucket_name}/{upstream_task_output_key}"
        error_text = f"We ingested the table but it has no records; either csv is empty or " \
                     f"avro schema setup pointed the ingestion to skip " \
                     f"more rows then there were in the incoming file {full_file_path}."
        logger.info(error_text)
        error_summary['error_description'] = error_text
    else:
        raise NotImplementedError(f"Dead letter contents for upstream task {upstream_task_id} not implemented.")
    dead_letter_file_content = json.dumps(error_summary, ensure_ascii=False, indent=4)
    return dead_letter_file_content, output_file_name


def store_dead_letters(bucket_name: str, mode: str, upstream_task_id: str, **context):
    """
    Function stores dead letters messages about failure of ingestion of semi-avro schema file
    due to errors in csv file processing (like the lack of corresponding csv file in the bucket
    or an empty csv file or avsc skipping more rows than here are in csv file.
    :param bucket_name: name of the bucket the csv file originated from
    :param mode: static value INGESTION_FAILURE_MODE
    :param upstream_task_id: name of the task for which dead letters have to be produced
    :param context: standard Airflow context
    :return:
    """
    now = datetime.datetime.now().strftime(TABLE_SUFFIX_PATTERN)
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    if mode == INGESTION_FAILURE_MODE:
        dead_letter_prefix = "failed_ingestion"
    else:
        raise NotImplementedError(f"Mode {mode} is not implemented.")
    upstream_task_id_output = context["ti"].xcom_pull(task_ids=upstream_task_id)
    logger.info(f"Started placing dead letters in minio with mode {mode}. "
                f"upstream_task_id output: {upstream_task_id_output}, "
                f"type of upstream_task_id output: {type(upstream_task_id_output)}")
    logger.info(f"There will be {len(upstream_task_id_output)} files.")
    if isinstance(upstream_task_id_output, list):
        upstream_task_id_output = {x: x for x in upstream_task_id_output}
    for upstream_task_id_output_key, upstream_task_id_output_value in upstream_task_id_output.items():
        dead_letter_file_content, file_name = _procure_dead_letter_details(
            bucket_name, upstream_task_id, upstream_task_id_output_key, upstream_task_id_output_value)
        source_s3.load_string(
            string_data=dead_letter_file_content,
            key=f"{dead_letter_prefix}_{file_name}.{now}.txt",
            bucket_name=BUCKET_RESULTS,
            replace=True,
        )
