import os
import sys
import logging
import datetime
import json
from src.common.utils.statics import TABLE_SUFFIX_PATTERN, INGESTION_FAILURE_MODE, \
    VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY, VALIDATE_AVRO_SCHEMAS_CORRECT_KEY
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.models import Variable

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)
BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")


def store_dead_letters(mode: str, **context):
    """
    Function stores dead letters messages about failure of ingestion of semi-avro schema file
    due to errors in semi-avro schema validation.
    :param mode: static value INGESTION_FAILURE_MODE
    :param context: standard Airflow context
    :return:
    """
    now = datetime.datetime.now().strftime(TABLE_SUFFIX_PATTERN)
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    if mode == INGESTION_FAILURE_MODE:
        dead_letter_prefix = "failed_ingestion"
        validation_xcom_key = VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY
    else:
        dead_letter_prefix = "ingestion_report"
        validation_xcom_key = VALIDATE_AVRO_SCHEMAS_CORRECT_KEY
    incorrect_avsc_dict = context["ti"].xcom_pull(task_ids='validate_avro_schemas', key=validation_xcom_key)
    logger.info(f"Started placing dead letters in minio with mode {mode}. "
                f"Incorrect_avsc_dict: {incorrect_avsc_dict}, "
                f"type of Incorrect_avsc_dict: {type(incorrect_avsc_dict)}")
    logger.info(f"There will be {len(incorrect_avsc_dict.keys())} files.")
    for table_relative_path, error_message in incorrect_avsc_dict.items():
        table_name = table_relative_path.split("/")[-1]
        error_summary = dict()
        error_summary['error_description'] = error_message
        dead_letter_file_content = json.dumps(error_summary, ensure_ascii=False, indent=4)
        source_s3.load_string(
            string_data=dead_letter_file_content,
            key=f"{dead_letter_prefix}_{table_name}.{now}.txt",
            bucket_name=BUCKET_RESULTS,
            replace=True,
        )


def get_incorrect_avros(bucket_name: str, **context) -> list:
    """
    Function gathers the full locations/paths (with the bucket name) of incorrect avro files
    from validate_avro_schemas and flatten_str_avsc_results_ingestions tasks
    :param bucket_name: name of the bucket where the avsc file originated from
    :param context: standard Airflow context
    :return:
    """
    # get dictionary
    incorrect_avro_files_after_validation = \
        context['ti'].xcom_pull(task_ids='validate_avro_schemas', key=VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY)
    if incorrect_avro_files_after_validation is None:
        incorrect_avro_files_after_validation = dict()
    # get list
    incorrect_avro_files_after_transform = \
        context['ti'].xcom_pull(task_ids='flatten_str_avsc_results_ingestions', key='incorrect_avro_files')
    output = set()
    if incorrect_avro_files_after_transform is None:
        incorrect_avro_files_after_transform = list()
    for avro_instance_key, _ in incorrect_avro_files_after_validation.items():
        avro_instance_key_with_bucket = f"s3://{bucket_name}/{avro_instance_key}"
        output.add(avro_instance_key_with_bucket)
    for avro_instance in incorrect_avro_files_after_transform:
        output.add(avro_instance[0])
    return list(output)
