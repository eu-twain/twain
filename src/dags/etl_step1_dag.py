# workaround for lack of PATH setup
import os
import sys

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402


import pendulum
import json
import logging
import sys
import math
import datetime
from airflow.decorators import dag
from airflow.models import Variable
from airflow.models import Connection
from airflow.operators.empty import EmptyOperator
from airflow.operators.python import PythonOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.trigger_dagrun import TriggerDagRunOperator
from airflow.providers.cncf.kubernetes.operators.pod import KubernetesPodOperator
from airflow.providers.cncf.kubernetes.utils.pod_manager import PodLaunchFailedException
from airflow.providers.cncf.kubernetes.secret import Secret
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.decorators import task
from kubernetes.client import models as k8s
from utils.trino_manager import TrinoManager
from src.common.utils.statics import MEMORY_ALLOCATION_RATIO, TABLE_SUFFIX_PATTERN, MAX_MEMORY_ALLOCATION, \
    INGESTION_FAILURE_MODE, VALIDATE_AVRO_SCHEMAS_INCORRECT_KEY
from src.common.utils.kubernetes_pod_callbacks import TwainPodStartedCallback


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)

BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")

secret_env_minio_user = Secret(
    deploy_type="env",
    deploy_target="MINIO_USER",
    secret="twain-components-admins-secrets",
    key="minio-service-account-username",
)
secret_env_minio_password = Secret(
    deploy_type="env",
    deploy_target="MINIO_PASSWORD",
    secret="twain-components-admins-secrets",
    key="minio-service-account-password",
)
secret_env_trino_user = Secret(
    deploy_type="env",
    deploy_target="TRINO_USER",
    secret="twain-airflow-trino-additional-credentials",
    key="AIRFLOW_TRINO_USER",
)
secret_env_trino_password = Secret(
    deploy_type="env",
    deploy_target="TRINO_PASSWORD",
    secret="twain-airflow-trino-additional-credentials",
    key="AIRFLOW_TRINO_PASSWORD",
)


def estimate_memory_consumption(input_data_config, trino_user: str, trino_password: str, **context):
    logger.info(f"input_data_config: {type(input_data_config)}: {input_data_config}")
    if isinstance(input_data_config, dict):
        input_config_dict = input_data_config
    else:
        try:
            input_config_dict = json.loads(input_data_config)
        except json.decoder.JSONDecodeError as json_decoder_error:
            input_config_dict = json.loads(input_data_config.replace("'", "\""))
    output = dict()
    trinoHost = Variable.get("TWAIN_TRINO_SERVICE_URL")   # 'https://172.29.112.1'
    trinoPort = Variable.get("TWAIN_TRINO_SERVICE_PORT")  # 10022
    tm = TrinoManager(
        host=trinoHost, port=trinoPort,
        username=trino_user,
        password=trino_password,
        catalog='hive',
        schema='default')
    logger.info(f"input_config_dict {input_config_dict}")
    for avro_entry_name, tables_configs_list in input_config_dict.items():
        logger.info(f"avro_entry_name {avro_entry_name} tables_configs_list {tables_configs_list}")
        tables_list = [x["csv_table_name"] for x in tables_configs_list]
        table_columns = tm.get_column_count(tables_list)
        table_rows = tm.get_table_row_count(tables_list)
        table_estimates = 0
        logger.info(f"table_columns {table_columns}")
        for table_name, table_column_count in table_columns.items():
            table_estimates = table_estimates + table_column_count*table_rows[table_name]
        output[avro_entry_name] = table_estimates
    print(f"avro_estimated_memory: {output}")

    temp_dict = {'fake_key': str(json.dumps(input_config_dict))}

    context['ti'].xcom_push("input_config_dict_as_string", temp_dict['fake_key'])
    return min([MAX_MEMORY_ALLOCATION, math.ceil(max(output.values()) / MEMORY_ALLOCATION_RATIO) + 1])


@dag(
    dag_id="etl_step1_dag",
    description="A dag to trigger kubernetes pod operator running step 1 of ETL.",
    schedule=None,
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["MinIO", "Trino", "raw2bronze"],
    # render_template_as_native_obj=True,
    params={
        'input_data_file': 's3a://datalake/output/raw_layer/schemas/fake_data_ps_001_avro_schema.avsc',
        'input_data_config': '{"s3a://datalake/output/raw_layer/schemas/fake_data_ps_001_avro_schema.avsc": ["sample_bronze_table_01"]}'
    }
)
def dummy_kubernetes_pod_operator() -> None:
    start = EmptyOperator(task_id='start')
    end = EmptyOperator(task_id='end')

    # # setting Airflow variables for minio is obsolete since we are accessing Kubernetes Secrets
    # MINIO_USER: str = Variable.get("MINIO_USER")
    # MINIO_PASSWORD: str = Variable.get("MINIO_PASSWORD")
    TRINO_USER: str = Variable.get("TRINO_USER")
    TRINO_PASSWORD: str = Variable.get("TRINO_PASSWORD")
    TWAIN_IMAGE_TAG: str = Variable.get('TWAIN_IMAGE_TAG', default_var="latest")

    minio_connection = Connection.get_connection_from_secrets("aws_minio")
    minio_endpoint_and_port = f"{minio_connection.host}:{minio_connection.port}"
    trino_connection = Connection.get_connection_from_secrets("twain_trino")
    trino_endpoint_and_port = f"{trino_connection.host}:{trino_connection.port}"

    memory_config_estimation = PythonOperator(
        task_id='estimate_memory_config',
        python_callable=estimate_memory_consumption,
        provide_context=True,
        op_kwargs={
            'input_data_config': "{{ dag_run.conf }}",
            'trino_user': TRINO_USER,
            'trino_password': TRINO_PASSWORD
        },
    )

    # using traditional syntax instead of a decorator introduced in 2.4 as
    # "The Docker image provided to the @task.kubernetes decorator must support executing Python scripts"
    # excerpt from https://docs.astronomer.io/learn/kubepod-operator#use-the-taskkubernetes-decorator
    # which is not 100% assured in Twain
    kpo = KubernetesPodOperator(
        name="ETL_step_1",
        random_name_suffix=True,
        image=f"registry.windenergy.dtu.dk/eu-twain/twain/infrastructure-spark-ingestion-step-one:{TWAIN_IMAGE_TAG}",
        image_pull_policy='Always',  # 'IfNotPresent',
        labels={"Twain": "etl_step_1"},
        task_id="run_etl_step_1",
        do_xcom_push=True,
        get_logs=True,
        retries=0,
        on_finish_action='delete_succeeded_pod',  # onFinishAction.keep_pod # KEEP_POD # in python: 'keep_pod'
        in_cluster=True,
        startup_timeout_seconds=240,
        container_resources=k8s.V1ResourceRequirements(
            limits={
                "memory": "{{ ti.xcom_pull(task_ids='estimate_memory_config') }}G",
                "cpu": "2.0"
            }
        ),
        env_vars={
            "INPUT_DATA_CONFIG":
                "{{ ti.xcom_pull(task_ids='estimate_memory_config', key='input_config_dict_as_string') }}",
            "INPUT_DATA_FILE": "{{ params.input_data_file }}",
            "MINIO_ENDPOINT_AND_PORT": minio_endpoint_and_port,
            "TRINO_ENDPOINT_AND_PORT": trino_endpoint_and_port,
            "ESTIMATED_MEMORY_ALLOCATION": "{{ ti.xcom_pull(task_ids='estimate_memory_config') }}"
        },
        secrets=[
            secret_env_minio_user,
            secret_env_minio_password,
            secret_env_trino_user,
            secret_env_trino_password
        ],
        callbacks=TwainPodStartedCallback
    )

    def _revert_enriched_to_csv_file_name(enriched_file_name: str):
        if enriched_file_name.startswith("enriched_"):
            enriched_file_name = enriched_file_name[len("enriched_"):]
        return ".".join(enriched_file_name.split(".")[:-1]) + ".csv"

    def _generate_failed_ingestion_report_file_name(csv_file_name: str, ingestion_timestamp: str):
        return f"failed_ingestion_{csv_file_name}.{ingestion_timestamp}.txt"

    @task(trigger_rule=TriggerRule.ONE_FAILED)
    def handle_pod_error_op(**context):
        """
        Task procures the list of csv files which should have been ingested but their ingestion failed
        due to k8s pod failure (like out-of-memory or pod eviction failures).
        If the csv file was already reported on due to "standard" application related error like type casting,
        then function omits the file from the output list.
        :param context: standard Airflow context
        :return: a list of 2 element tuples which first element is csv file name and the second
                 is the string representation of ingestion timestamp.
        """
        logger.info(f"ETL step 1 failed, marking all input files as failures unless already reported on.")
        output = list()
        source_s3 = S3Hook(aws_conn_id="aws_minio")
        input_config_dict_as_string = context['ti'].xcom_pull(
            task_ids="estimate_memory_config", key="input_config_dict_as_string")
        input_config_dict = json.loads(input_config_dict_as_string)
        for enriched_avsc_full_path, csv_table_name in input_config_dict.items():
            logger.info(f"Checking if file {enriched_avsc_full_path} is already reported on in {BUCKET_RESULTS}.")
            enriched_file_name = enriched_avsc_full_path.split("/")[-1]
            ingestion_timestamp = enriched_avsc_full_path.split("/")[-2]
            csv_file_name = _revert_enriched_to_csv_file_name(enriched_file_name)
            expected_report_file_name = _generate_failed_ingestion_report_file_name(csv_file_name, ingestion_timestamp)
            if not source_s3.check_for_key(key=expected_report_file_name, bucket_name=BUCKET_RESULTS):
                logger.info(f"File {csv_file_name} was not reported on yet.")
                output.append((csv_file_name, ingestion_timestamp))
        return output

    def store_dead_letters(mode: str, **context):
        """
        Function stores dead letters messages about failure of ingestion of csv file due to k8s pod failure
        (like out-of-memory or pod eviction failures); the user is guided to contact administrator with failed pod id
        if possible as the actual failure reason cannot be determined by Airflow itself.
        :param mode: static value INGESTION_FAILURE_MODE;
        :return:
        """
        source_s3 = S3Hook(aws_conn_id="aws_minio")
        if mode == INGESTION_FAILURE_MODE:
            dead_letter_prefix = "failed_ingestion"
        else:
            raise NotImplementedError(f"Mode {mode} is not implemented.")
        incorrect_csv_file_details = context["ti"].xcom_pull(task_ids='handle_pod_error_op')
        pod_id = context["ti"].xcom_pull(task_ids='run_etl_step_1', key="pod_name")
        logger.info(f"Started placing dead letters in minio with mode {mode}. "
                    f"incorrect_csv_file_details: {incorrect_csv_file_details}, "
                    f"type of incorrect_csv_file_details: {type(incorrect_csv_file_details)}")
        logger.info(f"There will be {len(incorrect_csv_file_details)} files.")
        for csv_file_name, ingestion_timestamp in incorrect_csv_file_details:
            error_summary = dict()
            error_message = "The ingestion failed on step 'etl_step1', responsible for date transformation " \
                            f"from raw to bronze data layers. Please contact administrator."
            if pod_id:
                error_message += f" Please use this ID \"{pod_id}\" as it would help them figure out" \
                                 f" the reason of error."
            error_summary['error_description'] = error_message
            dead_letter_file_content = json.dumps(error_summary, ensure_ascii=False, indent=4)
            source_s3.load_string(
                string_data=dead_letter_file_content,
                key=f"{dead_letter_prefix}_{csv_file_name}.{ingestion_timestamp}.txt",
                bucket_name=BUCKET_RESULTS,
                replace=True,
            )

    store_dead_letters_op = PythonOperator(
        task_id='store_dead_letters',
        python_callable=store_dead_letters,
        provide_context=True,
        op_kwargs={
            'mode': INGESTION_FAILURE_MODE
        },
    )

    # task [TRIGGERING DOWNSTREAM DAG] STARTS
    def procure_etl2_config(**context):
        output = dict()
        bronze_table_names = context['ti'].xcom_pull(
            task_ids='run_etl_step_1', key='return_value')
        logger.info(f"bronze_table_names type: {type(bronze_table_names)}, bronze_table_names: {bronze_table_names}")
        output['correct_tables'] = bronze_table_names['correct_tables']
        return output

    procure_etl2_config_op = PythonOperator(
        task_id='procure_etl2_config',
        provide_context=True,
        python_callable=procure_etl2_config,
        trigger_rule=TriggerRule.NONE_FAILED
    )

    trigger_etl2_dag = TriggerDagRunOperator(
        task_id="trigger_etl2_dag",
        trigger_dag_id="etl_step2_dag",
        conf=procure_etl2_config_op.output
    )
    # task [TRIGGERING DOWNSTREAM DAG] ENDS

    start >> memory_config_estimation >> kpo >> procure_etl2_config_op >> trigger_etl2_dag >> end
    kpo >> handle_pod_error_op() >> store_dead_letters_op


dummy_kubernetes_pod_operator()
