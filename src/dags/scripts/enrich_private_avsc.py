#!/opt/bitnami/airflow/venv/bin/python
import json
import logging
import sys

logger = logging.getLogger(__name__)


def _sanitize_path_traversal(input_file_path: str):
    temp_file_name = os.path.basename(input_file_path)
    new_dirname = os.path.dirname(input_file_path)
    if not os.path.isdir(new_dirname):
        raise KeyError("Temp file not in a directory.")
    input_file_path_split = input_file_path.split(os.sep)
    if ".." in input_file_path_split:
        raise KeyError(
            "Input file path contains path traversal ('..') instruction, "
            "violation of PCI DSS v4.0: 6.2.4."
        )
    new_input_file_path = os.path.join(new_dirname, temp_file_name)
    return new_input_file_path



def main(input_file, output_file, owning_company, file_owner):
    input_file_sanitized = _sanitize_path_traversal(input_file)
    schema_object = json.loads(open(input_file_sanitized, 'r').read())
    logger.info("Original schema: %s", schema_object)

    # Make all datetime types with 'logicalType' timestamp-micros to be "timestamp"
    for field in [field for field in schema_object['fields']
                  if field['type'] == 'datetime'
                     and 'logicalType' in field.keys()
                     and field['logicalType'] == 'timestamp-micros'
                  ]:
        field['type'] = 'timestamp'
    logger.info("Transformed schema: %s", schema_object)

    # determine object owner and the entity (company/university) owner works for
    schema_object['owning_company'] = owning_company
    schema_object['file_owner'] = file_owner


    output_file_sanitized = _sanitize_path_traversal(output_file)
    with open(output_file_sanitized, mode="w") as f:
        json.dump(schema_object, f, indent=2)


if __name__ == "__main__":
    logger.info("Starting data transformation...")
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    logger.info("Completed data transformation!")
