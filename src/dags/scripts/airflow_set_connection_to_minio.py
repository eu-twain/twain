import os
from airflow.utils.session import provide_session
from airflow.models import Connection
from airflow.models.crypto import get_fernet
from sqlalchemy import select

MINIO_CONNECTION_NAME = "aws_minio"


@provide_session
def update_connection(conn_name: str, conn_type: str, conn_host: str, conn_username: str, conn_password: str,
                      conn_schema: str | None, conn_port: int, conn_extra: str, session=None):
    fernet = get_fernet()
    if conn_extra:
        conn_extra = fernet.encrypt(bytes(conn_extra, "utf-8")).decode()
    conn_password = fernet.encrypt(bytes(conn_password, "utf-8")).decode()
    conn_name_list = [conn_name]
    connections_found = session.query(Connection) \
        .filter(Connection.conn_id.in_(conn_name_list)) \
        .update(
        {
            Connection.conn_type: conn_type,
            Connection.description: "a description done via code",
            Connection.host: conn_host,
            Connection.login: conn_username,
            Connection.password: conn_password,
            Connection.schema: conn_schema,
            Connection.port: conn_port,
            Connection.extra: conn_extra
        },
        synchronize_session="fetch")
    print(type(connections_found))
    if isinstance(connections_found, int):
        print(connections_found)
        if 1 != connections_found:
            msg = f"An update of connection {conn_name} has failed with return code {connections_found}"
            print(msg)
            raise KeyError(msg)
    else:
        for x in connections_found:
            print(x)
            print(type(x))


@provide_session
def list_connections_fetch_all(session=None, conn_names_list: list = None):
    if not conn_names_list:
        connections_found = session.execute(select(Connection)).fetchall()
    else:
        connections_found = session.execute(
            select(Connection).filter(Connection.conn_id.in_(conn_names_list))).fetchall()
    for x in connections_found:
        print(x)
        print(type(x))
    return connections_found


@provide_session
def list_connections(session=None, conn_names_list: list = None):
    if not conn_names_list:
        connections_found = session.query(Connection)
    else:
        connections_found = session.query(Connection).filter(Connection.conn_id.in_(conn_names_list))
    for x in connections_found:
        print(x)
        print(type(x))
    return connections_found


def get_connection(connection_name: str):
    conn_names_list = [connection_name]
    connection_instance = list_connections(conn_names_list=conn_names_list)
    print(type(connection_instance))
    if not connection_instance:
        msg = f"Connection {connection_name} was not found in DB, no query returned."
        print(msg)
        raise KeyError(msg)
    for a_row in connection_instance:
        return a_row
    msg = f"Connection {connection_name} was not found in DB, empty query result."
    raise KeyError(msg)


def update_minio_connection():
    conn_type = "aws"
    conn_password = os.getenv("MINIO_SERVICE_PASSWORD")
    conn_username = os.getenv("MINIO_SERVICE_USERNAME")
    update_connection(
        conn_name=MINIO_CONNECTION_NAME,
        conn_type=conn_type,
        conn_host="http://twain-minio",
        conn_username=conn_username,
        conn_password=conn_password,
        conn_schema=None,
        conn_port=9000,
        conn_extra='{"endpoint_url": "http://twain-minio:9000"}'
    )


def run():
    list_connections()
    a_list = [MINIO_CONNECTION_NAME]
    minio_connections_found = list_connections(conn_names_list=a_list)
    try:
        minio_connection_found = get_connection(MINIO_CONNECTION_NAME)
        # print(minio_connection_found.as_json())
    except KeyError as ke:
        print(ke.args)
        print(f"An update of connection {MINIO_CONNECTION_NAME} will not commence.")
        return
    update_minio_connection()
    try:
        minio_connection_found = get_connection(MINIO_CONNECTION_NAME)
        print(f"Retrieved {MINIO_CONNECTION_NAME} after update.")
    except KeyError as ke:
        print(ke.args)
        print(f"An update of connection {MINIO_CONNECTION_NAME} has failed.")
        return


if __name__ == "__main__":
    run()

