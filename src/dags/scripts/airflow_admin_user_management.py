#!/opt/bitnami/airflow/venv/bin/python
import subprocess
import os


def create_or_update_admin_user():
    """
    Function adds new Airflow webUI admin account with username and passwords set as env vars.
    It does not delete previously existing admin accounts.
    :return: None
    """
    new_user_name = os.getenv("NEW_USER_NAME", "")
    new_user_password = os.getenv("NEW_USER_PASSWORD", "")
    if not (new_user_name and new_user_password):
        print(
            "Missing environment variables NEW_USER_NAME or NEW_USER_PASSWORD; admin user will not be created/updated.")
        return
    airflow_create_db_connection = "export AIRFLOW__DATABASE__SQL_ALCHEMY_CONN=postgresql+psycopg2://${AIRFLOW_DATABASE_USERNAME}:${AIRFLOW_DATABASE_PASSWORD}@${AIRFLOW_DATABASE_HOST}:${AIRFLOW_DATABASE_PORT_NUMBER}/${AIRFLOW_DATABASE_NAME}; AIRFLOW__CORE__FERNET_KEY=${AIRFLOW_FERNET_KEY}"
    airflow_user_list = "airflow users list"
    airflow_delete_user_command = "airflow users delete -u {new_user_name}"
    airflow_create_user_command = "airflow users create --username {new_user_name} --password {new_user_password} --firstname Peter --lastname Parker --role Admin --email {new_user_name}@superhero.org"

    whole_command = ";".join([
        airflow_create_db_connection,
        airflow_delete_user_command.format(new_user_name=new_user_name),
        airflow_create_user_command.format(new_user_name=new_user_name, new_user_password=new_user_password)
    ])

    proc = subprocess.Popen([whole_command], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                            shell=True)
    out, _ = proc.communicate()
    print(out.decode('utf-8'))


create_or_update_admin_user()
