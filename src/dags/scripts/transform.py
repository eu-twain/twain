#!/opt/bitnami/airflow/venv/bin/python
import json
import logging
import os
import sys
import tempfile

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


def _sanitize_path_traversal(input_file_path: str):
    allowed_tempfile_dirnames = tempfile._candidate_tempdir_list()
    if os.path.dirname(input_file_path) not in allowed_tempfile_dirnames:
        raise KeyError(f"Temp file {input_file_path} not present in allowed directories")
    new_dirname = [x for x in allowed_tempfile_dirnames if x == os.path.dirname(input_file_path)][0]
    temp_file_name = os.path.basename(input_file_path)
    if not os.path.isdir(new_dirname):
        raise KeyError(f"Temp file not in a directory.")
    input_file_path_split = input_file_path.split(os.sep)
    if ".." in input_file_path_split:
        raise KeyError(f"Input file path contains path traversal ('..') instruction, "
                       f"violation of PCI DSS v4.0: 6.2.4.")
    new_input_file_path = os.path.join(new_dirname, temp_file_name)
    return new_input_file_path


def main(input_file, output_file):
    if input_file:
        input_file_sanitized = _sanitize_path_traversal(input_file)
    else:
        raise KeyError("Empty value for input_file")
    if output_file:
        output_file_sanitized = _sanitize_path_traversal(output_file)
    else:
        raise KeyError("Empty value for output_file")
    schema_file = json.loads(open(input_file_sanitized, 'r').read())
    if isinstance(schema_file, dict):
        logger.info("Original schema: %s", schema_file)

    # Make all field types to be "string"
    for field in schema_file['fields']:
        field['type'] = 'string'
    if isinstance(schema_file, dict):
        logger.info("Transformed schema: %s", schema_file)

    with open(output_file_sanitized, mode="w") as f:
        json.dump(schema_file, f, indent=2)


if __name__ == "__main__":
    logger.info("Starting data transformation...")
    main(sys.argv[1], sys.argv[2])
    logger.info("Completed data transformation!")
