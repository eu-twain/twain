"""
The DAG continuously monitors AVSC file gets uploaded to the Temporary Storage.
"""
# workaround for lack of PATH setup
import os
import sys

import pytz

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")


import logging
import pendulum
import sys
from airflow.decorators import dag
from airflow.models import Variable
from src.common.pipelines.etl0_avsc_common_pipeline import etl0_common_taskflow_api

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


BUCKET_DATALAKE: str = Variable.get("bucket_datalake", "datalake")
BUCKET_RESEARCHERS: str = Variable.get("bucket_researchers", "researchers")
BUCKET_QUARANTINE: str = Variable.get("bucket_quarantine", "quarantine")
AVRO_TABLE_PREFIX: str = Variable.get("avro_table_prefix", "avro_")
ENRICH_AVRO_TABLE_PREFIX: str = Variable.get("enrich_avro_table_prefix", "enriched_")
AVSC_W_STRINGS_PREFIX: str = Variable.get("avsc_file_with_all_types_strings_prefix", "str_")

KEY_SENSOR_WILDCARD_AVSC: str = "*.avsc"


@dag(
    dag_id="etl0_private_researchers_avsc",
    description="Move AVSC file(s) to permanent storage and CREATE TABLE accordingly in Trino.",
    schedule="@continuous",
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["MinIO", "Trino", "avsc"]
)
def etl0_taskflow_api() -> None:
    """ ETL 0: move AVSC file(s) uploaded to the temporary location (MinIO bucket)
    to permanent location (Ceph) and create Trino table(s) for it.
    """
    etl0_common_taskflow_api(BUCKET_RESEARCHERS, "etl0_private_researchers_csv")


etl0_taskflow_api()
