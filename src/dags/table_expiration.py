import os
import sys
sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

import logging
import pendulum
from airflow.decorators import dag, task
from airflow.models import Variable
from airflow.utils.trigger_rule import TriggerRule
from airflow.exceptions import AirflowException
from src.common.tasks.table_expiration_tasks import get_trino_tables_with_expiry, get_overdue_tables, \
    split_bronze_table_from_the_rest, handle_expiration_of_standard_tables, handle_expiration_of_bronze_tables

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)

@dag(
    dag_id="twain_data_expiration",
    description="Twain data expiration DAG is responsible for deleting table definitions from Trino "
                "and table data files from MinIO.",
    schedule_interval="0 1 * * *",
    max_active_runs=1,
    start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
    catchup=False,
    tags=["MinIO", "Trino", "data_expiration"]
)
def twain_data_expiration_taskflow_api() -> None:
    """
    Twain data expiration DAG is responsible for deleting table definitions from Trino (and by proxy from hive
    and Openmetadata) and its data files from MinIO
    """
    BRONZE_TABLE_PREFIX = os.getenv('BRONZE_TABLE_PREFIX', default='bronze_')
    TRINO_USER: str = Variable.get("TRINO_USER")
    TRINO_PASSWORD: str = Variable.get("TRINO_PASSWORD")
    trinoHost = Variable.get("TWAIN_TRINO_SERVICE_URL")
    trinoPort = Variable.get("TWAIN_TRINO_SERVICE_PORT")
    BUCKET_RESULTS: str = Variable.get("bucket_results", "ingestion-results")
    aws_conn_id = "aws_minio"

    @task(trigger_rule=TriggerRule.ALL_DONE)
    def mark_run_end_state(**context):
        standard_tables_results = context["ti"].xcom_pull(task_ids="handle_expiration_of_standard_tables")
        standard_tables_that_were_expired = standard_tables_results["standard_tables_that_were_expired"]
        failed_deletions = standard_tables_results["failed_deletions"]
        bronze_tables_results = context["ti"].xcom_pull(task_ids="handle_expiration_of_bronze_tables")
        bronze_tables_that_were_expired = bronze_tables_results["bronze_tables_that_were_expired"]
        bronze_failed_deletions = bronze_tables_results["failed_deletions"]
        consumption_layer_results = bronze_tables_results["consumption_layer_results"]
        standard_tables_that_were_expired.append(bronze_tables_that_were_expired)
        if standard_tables_that_were_expired:
            logger.info(f"Expiration process expired following tables: {standard_tables_that_were_expired}")
        else:
            logger.info(f"Expiration process did not expire any tables.")
        failed_consumption_layer_files = list()
        for bronze_table_to_be_expired, consumption_layer_result in consumption_layer_results.items():
            if consumption_layer_result["bronze_table_data_files_removed_from_consumption"]:
                logger.info(f"For bronze table bronze_table_to_be_expired: {bronze_table_to_be_expired} "
                            f"process expired following files from consumption layer: "
                            f"{consumption_layer_result['bronze_table_data_files_removed_from_consumption']}")
            if consumption_layer_result["bronze_table_data_files_NOT_removed_from_consumption"]:
                failed_consumption_layer_files.append(
                    consumption_layer_result["bronze_table_data_files_NOT_removed_from_consumption"])
        if failed_consumption_layer_files:
            raise AirflowException(f"Failed data expiration from consumption layer")
        if failed_deletions["failed_minio_objects"] or failed_deletions["failed_trino_tables"] or \
                bronze_failed_deletions["failed_minio_objects"] or bronze_failed_deletions["failed_trino_tables"]:
            raise AirflowException(f"Failed data expiration")

    split_op = split_bronze_table_from_the_rest(
        get_overdue_tables(
            get_trino_tables_with_expiry(
                trinoHost=trinoHost, trinoPort=trinoPort, TRINO_USER=TRINO_USER, TRINO_PASSWORD=TRINO_PASSWORD),
            logger),
        logger,
        BRONZE_TABLE_PREFIX
    )
    standard_expiration_op = handle_expiration_of_standard_tables(
        input_dict=split_op, trinoHost=trinoHost, trinoPort=trinoPort,
        TRINO_USER=TRINO_USER, TRINO_PASSWORD=TRINO_PASSWORD, aws_conn_id=aws_conn_id)
    bronze_expiration_op = handle_expiration_of_bronze_tables(
        input_dict=split_op, trinoHost=trinoHost, trinoPort=trinoPort,
        TRINO_USER=TRINO_USER, TRINO_PASSWORD=TRINO_PASSWORD, aws_conn_id=aws_conn_id, bucket_results=BUCKET_RESULTS)
    mark_run_end_state_task = mark_run_end_state()
    standard_expiration_op.set_downstream(mark_run_end_state_task)
    bronze_expiration_op.set_downstream(mark_run_end_state_task)


twain_data_expiration_taskflow_api()
