import datetime
import sys

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

from src.common.utils.statics import DATA_EXPIRATION_DATE_PATTERN
from utils.exceptions import TwainCustomException


def parse_non_static_avro_schema(schema: dict):
    """
    Function checks the correctness of the semi-avro schema provided by the user in non-static ingestions.
    Raises TwainCustomException with description of the error
    :param schema: the semi-avro schema to be checked for common errors
    :return: None
    """
    if 'fields' not in schema.keys():
        details = "Provided avro schema lacks definitions of fields."
        raise TwainCustomException(message="Error in avro schema", details=details)
    if 'skip_header_line_count' not in schema.keys() and 'header_line_count' not in schema.keys():
        details = "Provided avro schema lacks the number of header rows to be skipped," \
                  " called \"skip_header_line_count\" or \"header_line_count\"."
        raise TwainCustomException(message="Error in avro schema", details=details)
    else:
        if 'skip_header_line_count' in schema.keys():
            skip_header_line_count = 'skip_header_line_count'
        else:
            skip_header_line_count = 'header_line_count'
        try:
            int(schema[skip_header_line_count])
        except ValueError as ve:
            details = "Provided avro schema lacks the number of header rows to be skipped," \
                      " field called \"skip_header_line_count\" is not an integer."
            raise TwainCustomException(message="Error in avro schema", details=details)
    if 'csv_separator' not in schema.keys():
        details = "Provided avro schema lacks the column separator."
        raise TwainCustomException(message="Error in avro schema", details=details)
    if 'expiry_date' in schema.keys() and 'data_retention' in schema.keys():
        if schema['expiry_date'] and schema['data_retention']:
            details = "Provided avro schema has both expiry_date and data_retention, please use only one of those."
            raise TwainCustomException(message="Error in avro schema", details=details)
    if 'expiry_date' in schema.keys() and schema['expiry_date']:
        try:
            datetime.datetime.strptime(schema['expiry_date'], DATA_EXPIRATION_DATE_PATTERN)
        except ValueError as ve:
            details = "Provided avro schema expiry_date cannot be parsed into date."
            raise TwainCustomException(message="Error in avro schema", details=details)
    if 'data_retention' in schema.keys() and schema['data_retention']:
        try:
            int(schema['data_retention'])
        except ValueError as ve:
            details = "Provided avro schema data_retention cannot be parsed into integer."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if int(schema['data_retention']) < 1:
            details = "Provided avro schema data_retention cannot be smaller than one day."
            raise TwainCustomException(message="Error in avro schema", details=details)
    for index_i, field in enumerate(schema['fields']):
        if "name" not in field.keys():
            details = f"Provided avro schema lacks a name of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "." in field["name"]:
            details = f"Provided avro schema has an invalid character (dot) a name of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "type" not in field.keys():
            details = f"Provided avro schema lacks a type of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "datetime" == field["type"] and "parsing_patterns" not in field.keys():
            details = f"Provided avro schema lacks the parsing patterns for datetime field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "datetime" == field["type"] and "logicalType" not in field.keys():
            details = f"Provided avro schema lacks the logical type for datetime field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)


def sanitize_filename(file_name: str) -> str:
    output = file_name.replace('-', '_').replace(" ", "_").lower()
    return output
