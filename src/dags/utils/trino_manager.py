import logging
import re
import sys
import warnings
import pandas as pd
import trino.exceptions
from trino.dbapi import connect
from trino.auth import BasicAuthentication
from trino.constants import HTTPS as TRINO_HTTPS

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

from src.common.utils.statics import PROTECTED_TABLES
from utils.exceptions import TwainCustomException


class TrinoManager(object):
    def __init__(self, host, port, username, password, catalog, schema):
        self.log = logging.getLogger(self.__class__.__name__)
        self.location_pattern = re.compile('^[ ]+external_location = \'(.*)\',')
        self.connection = connect(
            host=host,
            port=port,
            user=username,
            auth=BasicAuthentication(username, password),
            catalog=catalog,
            schema=schema,
            http_scheme=TRINO_HTTPS,
            verify=False
        )

    def get_column_count(self, table_list):
        """
        Function returns the count of columns inside the requested tables in Trino;
        row and array columns count as only one column.
        :param table_list: a list of table names
        :return: a dictionary where the key is the table name and the value is its column count
        """
        output = dict()
        cur = self.connection.cursor()
        if len(table_list) > 1:
            table_list_text = ','.join(["'" + x + "'" for x in table_list])
        else:
            table_list_text = '\'' + table_list[0] + '\''
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore", Warning)
            cur.execute(
                f"select table_name, count(1) from hive.information_schema.columns "
                f"where table_name in ({table_list_text}) group by table_name")
        rows = cur.fetchall()
        cur.close()
        for x in rows:
            output[x[0]] = x[1]
        return output

    def get_table_row_count(self, table_list):
        """
        Function returns the row counts of the requested tables in Trino
        :param table_list: a list of table names
        :return: a dictionary where the key is the table name and the value is its row count
        """
        cur = self.connection.cursor()
        output = dict()
        for table_name in table_list:
            with warnings.catch_warnings(record=True):
                warnings.simplefilter("ignore", Warning)
                cur.execute(f"select count(1) from {table_name}")
                rows = cur.fetchall()
            for x in rows:
                output[table_name] = x[0]
        cur.close()
        return output

    def get_tables_partitions(self, table_list: list):
        """
        Function returns the dictionary which for each key (table name from input table list) contains 2 elements,
        first element is the dictionary storing the table partitions and the second is the list of columns that
        are the partitioning columns of the table.
        If table does not exist, the value in the output is empty.
        :param table_list: the list of table names
        :return: the dictionary which key is the table name and the value is a dictionary with partitions details
        """
        cur = self.connection.cursor()
        output = dict()
        for table_name in table_list:
            try:
                with warnings.catch_warnings(record=True):
                    warnings.simplefilter("ignore", Warning)
                    cur.execute(f'select * from "{table_name}$partitions"')
                    rows = cur.fetchall()
                columns = [i[0] for i in cur.description]
                cur.close()
                pd_df = pd.DataFrame.from_records(rows, columns=columns)
                output[table_name] = {
                    'data': pd_df.to_dict(),
                    'columns': columns
                }
            except trino.exceptions.TrinoUserError as tue:
                cur.close()
                if "TABLE_NOT_FOUND" == tue.error_name:
                    output[table_name] = None
                else:
                    raise tue
        return output

    def run_query(self, sql_text: str):
        """
        Function runs the query on Trino
        :param sql_text: the text of the query
        :return:
        """
        cur = self.connection.cursor()
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore", Warning)
            cur.execute(f'select count(1) as returned_row_count from ({sql_text})')
            rows = cur.fetchall()
        for index_i, x in enumerate(rows):
            self.log.debug(f"requested query will return {x[0]} records.")
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore", Warning)
            cur.execute(sql_text)
            rows = cur.fetchall()
        cur.close()
        return rows

    @staticmethod
    def _transform_data_location_to_visible_outside_of_trino(input_location: str):
        """
        Function replaces s3 with s3a in input string if said string starts with s3
        :param input_location: the URI to be converted
        :return:
        """
        search_string = 's3://'
        if input_location.startswith(search_string):
            input_location = 's3a://' + input_location[len(search_string):]
        return input_location

    def get_file_location(self, table_name, transform_file_system: bool = False):
        """
        Function returns the external location of the files used by the table;
        flag transform_file_system allows to transform URI to the one used by spark applications
        :param table_name: the table name
        :param transform_file_system: the boolean flag to set 's3' or 's3a' in the URI
        :return:
        """
        cur = self.connection.cursor()
        with warnings.catch_warnings(record=True):
            warnings.simplefilter("ignore", Warning)
            cur.execute(f"show create table {table_name}")
            rows = cur.fetchall()
        cur.close()
        for z in rows:
            for x in z:
                for y in x.split('\n'):
                    matches = re.search(self.location_pattern, y)
                    if matches:
                        self.log.info(f"Reading will commence from directory {y}")
                        if not transform_file_system:
                            return matches.group(1)
                        else:
                            return self._transform_data_location_to_visible_outside_of_trino(matches.group(1))
        return None

    def drop_table(self, table_name: str):
        """
        Function drops table from Trino; raises TwainCustomException if the table is in the protected tables list.
        Errors thrown by Trino while dropping non-existing tables are silenced.
        :param table_name: the table name
        :return:
        """
        if table_name.lower() in PROTECTED_TABLES:
            raise TwainCustomException(message="Error table deletion",
                                       details=f"Requested deletion of protected table {table_name}")
        cur = self.connection.cursor()
        try:
            cur.execute(f"drop table {table_name}")
        except trino.exceptions.TrinoUserError as tue:
            cur.close()
            if "TABLE_NOT_FOUND" == tue.error_name:
                self.log.warning(f"Table {table_name} was not found in Trino.")
            else:
                raise tue
        cur.close()
        return True
