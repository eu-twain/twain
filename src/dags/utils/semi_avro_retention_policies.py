import datetime
import sys

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

from src.common.utils.statics import TABLE_SUFFIX_PATTERN, DATA_EXPIRATION_DATE_PATTERN, DEFAULT_DATA_RETENTION_IN_DAYS


def get_expiry_date_from_avsc(schema: dict, ingestion_timestamp: str):
    """
    Function retrieves expiration date (expiry_date) from schema or calculates expiration date from ingestion timestamp
    and field data_retention from said schema; return None if neither data_retention nor expiry_date are set.
    :param schema: the dictionary based on parsed semi-avro schema
    :param ingestion_timestamp: the string representation of ingestion timestamp in format
    :return: string representation of calculated data expiration date or None
    """
    if 'expiry_date' in schema.keys() and schema['expiry_date']:
        return schema['expiry_date']
    elif ('data_retention' not in schema.keys()) or not schema['data_retention']:
        calculated_date = datetime.datetime.strptime(ingestion_timestamp, TABLE_SUFFIX_PATTERN) + \
                          datetime.timedelta(days=DEFAULT_DATA_RETENTION_IN_DAYS + 1)
        return calculated_date.strftime(DATA_EXPIRATION_DATE_PATTERN)
    elif 'data_retention' in schema.keys() and schema['data_retention']:
        calculated_date = datetime.datetime.strptime(ingestion_timestamp, TABLE_SUFFIX_PATTERN) + \
                          datetime.timedelta(days=int(schema['data_retention']) + 1)
        return calculated_date.strftime(DATA_EXPIRATION_DATE_PATTERN)
