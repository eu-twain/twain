class TwainCustomException(Exception):
    def __init__(self, message, details):
        super().__init__(message)
        self.details = details


DATE_PARSING_EXCEPTION = 'Date parsing exception'
PARSING_ERROR = "Parsing error"
