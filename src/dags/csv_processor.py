import pandas as pd

from pandas import DataFrame
import json
import re
from typing import IO, Union
from io import BytesIO
import os
from avro_processor import normalize_table_name
from utils.exceptions import TwainCustomException, DATE_PARSING_EXCEPTION

DEFAULT_DATA_SHARING_AGREEMENT = {
    'ENGIE_GREEN_FRANCE': 'DTU;CENER;TUD;TUM;ENGIE_GREEN_FRANCE;ENGIE_LABORELEC;RAMBOLL;RAMBOLL_DANMARK_AS',
    'ENGIE_LABORELEC': 'DTU;CENER;TUD;TUM;ENGIE_GREEN_FRANCE;ENGIE_LABORELEC;RAMBOLL;RAMBOLL_DANMARK_AS',
    'EDF': 'DTU;CENER;TUD;TUM;EDF;RAMBOLL;RAMBOLL_DANMARK_AS',
    'VATT': 'DTU;CENER;TUD;TUM;VATT;RAMBOLL;RAMBOLL_DANMARK_AS'
}


AVRO_TO_PANDAS_TYPE_MAP: dict[str, str] = {
    "boolean": "bool",
    "long": "int64",
    "int": "int64",
    "float": "float64",
    "double": "float64",
    "string": "str",
    "bytes": "str",
    "datetime": "datetime",
}


AVRO_TO_PANDAS_LOGICAL_TYPE_MAP: dict[str, str] = {
    "decimal": "float64",
    "uuid": "str",
    "timestamp-millis": "datetime",
    "timestamp-micros": "datetime",
}


def load_schema(path: str) -> dict:
    # vulnerability checker didn't like this one
    # with open(path) as f:
    #     return json.loads(f.read())

    data = {}
    try:
        f = open(path)
        data = json.loads(f.read())
    finally:
        f.close()
    return data


def pandas_type_from_field(field: dict) -> str:
    if isinstance(field["type"], list):
        for t in field["type"]:
            if isinstance(t, dict):
                return pandas_type_from_field(t)
            trino_type = AVRO_TO_PANDAS_TYPE_MAP.get(t, "null")
            if trino_type != "null":
                break
        if not trino_type:
            raise KeyError

    else:
        trino_type = AVRO_TO_PANDAS_TYPE_MAP[field["type"]]

    if "logicalType" in field:
        trino_type = AVRO_TO_PANDAS_LOGICAL_TYPE_MAP[field["logicalType"]]
    return trino_type


def load_csv_with_schema(file_path: Union[str,IO] , schema: dict, owner: str = "") -> DataFrame:
    """
    function loads csv file to pandas.DataFrame and then performs
    type casting of each column according to the provided avro schema.
    This allows the frame to be later saved into .parquet file
    with types matching the avro and then create table based on
    avro derrived definition + parquet content

    :param str file_path: path to .csv file
    :param dict schema: avro schema
    :return: pandas frame
    :rtype: pandas.DataFrame
    """
    separator = schema["csv_separator"]
    skip_rows = int(schema["skip_header_line_count"])
    frame = pd.read_csv(file_path, sep=separator, skiprows=skip_rows, header=None)

    # cast frame columns to types defined in schema
    for f_column, field in zip(frame.columns, schema["fields"]):
        field_type = pandas_type_from_field(field)

        if field_type == "datetime":
            try:
                frame[f_column] = pd.to_datetime(
                    frame[f_column], format=field["parsing_pattern"]
                )
            except ValueError as ve:
                raise TwainCustomException(DATE_PARSING_EXCEPTION, ve.__str__())
            except re.error as rerr:
                raise TwainCustomException(DATE_PARSING_EXCEPTION, str(rerr))

        else:
            frame[f_column] = frame[f_column].astype(field_type)

    frame.rename(
        columns={
            f_column: field["name"] for f_column, field in
            zip(frame.columns, schema["fields"])
        },
        inplace=True
    )

    sharing_policy = schema.get("ro_access_companies")
    if not sharing_policy:
        sharing_policy = DEFAULT_DATA_SHARING_AGREEMENT.get(owner, owner)
    frame['ro_access_companies'] = sharing_policy
    frame['file_owner'] = owner
    frame['owning_company'] = owner
    return frame


def parquet_files(path: str):
    for dirname, subdirs, filenames in os.walk(path):
        if filenames:
            for fname in filenames:
                yield f"{dirname}/{fname}"


def parquet_data_from_dframe(frame: DataFrame, schema: dict) -> list:
    path = f"/tmp/{normalize_table_name(schema['name'])}"
    frame.to_parquet(
        path=path,
        partition_cols=['owning_company']
    )
    files = list(parquet_files(path))
    print(f"all files generated: {files}")
    return files


def save_frame_to_parquet(frame: DataFrame, path: str) -> str:
    frame.to_parquet(path)
    return path


def build_sql_with_parquet(path: str, column: str) -> str:
    return f"""
    WITH (
        external_location = '{path}',
        format = 'PARQUET',
        partitioned_by = ARRAY['{column}']
    )
    """


if __name__ == "__main__":
    import pandas as pd
    import json

    schema = {
        "type": "record",
        "namespace": "dk.dtu.twain",
        "name": "static_info_la_haute_borne",
        "doc": "Static information about La Haute Borne wind farm",
        "format": "CSV",
        "csv_separator": ";",
        "skip_header_line_count": "1",
        "fields": [
            {
                "name": "wind_turbine_name",
                "type": "string",
                "doc": "Wind turbine name",
            },
            {
                "name": "wind_turbine_long_name",
                "type": "string",
                "doc": "Wind turbine long name",
            },
            {"name": "manufacturer", "type": "string", "doc": "Manufacturer"},
            {"name": "model", "type": "string", "doc": "Model"},
            {"name": "rated_power", "type": "int", "doc": "Rated power (kW)"},
            {"name": "hub_height", "type": "int", "doc": "Hub height (m)"},
            {"name": "rotor_diameter", "type": "int", "doc": "Rotor diameter (m)."},
            {"name": "gps", "type": "string", "doc": "GPS"},
            {"name": "altitude", "type": "int", "doc": "Altitude (m)."},
            {
                "name": "comissioning_date",
                "type": "datetime",
                "doc": "Comissioning date",
                "parsing_pattern": "%Y-%m-%d",
            },
            {"name": "region", "type": "string", "doc": "Region"},
        ],
    }

    frame = load_csv_with_schema("static-information.csv", schema=schema)
    print(frame.dtypes)
    print(frame)
