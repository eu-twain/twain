import json
import os
import re
import math
from collections import Counter
from dataclasses import dataclass
from itertools import groupby
from typing import Optional

import pandas as pd
from pandas.tseries.api import guess_datetime_format
from typing import Callable, Tuple

TEN_MEGABYTE_LIMIT = 10485760

UNIT_PATTERN = re.compile(r"([\<\[\(][^\>\]\)]+[\]\>\)])", re.I | re.M | re.S)
NON_ALPHANUMERIC_PATTERN = re.compile(r"[^\w _-]+")
WHITESPACE_PATTERN = re.compile(r"\s+|_-")
WHITESPACE_PATTERN_FOR_AVRO_NAME = re.compile(r"\s+|-")
QUOTES_CHARACTER = '\"'


NAME_REPLACEMENTS = {
    UNIT_PATTERN: "",
    NON_ALPHANUMERIC_PATTERN: "",
    WHITESPACE_PATTERN: "_",
}
NAME_REPLACEMENTS_FOR_AVRO_NAME = {
    UNIT_PATTERN: "",
    NON_ALPHANUMERIC_PATTERN: "",
    WHITESPACE_PATTERN_FOR_AVRO_NAME: "_",
}

@dataclass
class AvroType:
    t: str
    extra: Optional[str] = None


def make_schema(data_frame: pd.DataFrame) -> dict:
    return {
        "type": "record",
        "namespace": "dk.dtu.twain",
        "name": "",
        "doc": "",
        "format": "CSV",
        "skip_header_line_count": "",
        "csv_separator": "",
        "expiry_date": "",
        "data_retention": 30,
        "fields": make_fields(data_frame),
    }


def sanitize_name(name: str, maintain_letter_case: bool = False, prepare_avro_name: bool = False) -> str:
    new_name = name
    if not prepare_avro_name:
        replacements_list = NAME_REPLACEMENTS
    else:
        replacements_list = NAME_REPLACEMENTS_FOR_AVRO_NAME
    for pattern, replacement in replacements_list.items():
        new_name = pattern.sub(replacement, new_name)
        new_name = new_name.strip()
    if new_name[0].isdigit():
        new_name = "_" + new_name
    if not maintain_letter_case:
        return new_name.lower()
    return new_name


def make_fields(data_frame: pd.DataFrame) -> list[dict]:
    return [detect_type(data_frame[column]) for column in data_frame]


def detect_type(column) -> dict:
    for t in TYPE_ORDER:
        try:
            type_resolved = TYPE_MAP[t](column)
            break
        except TypeError:
            type_resolved = AvroType(t='string')

    field_name = sanitize_name(column.name)
    type_definition = {
        'name': field_name,
        'type': type_resolved.t,
        'doc': column.name,
    }
    if type_resolved.t in ('datetime', 'probably_datetime'):
        type_definition['parsing_pattern'] = type_resolved.extra
        type_definition['parsing_patterns'] = type_resolved.extra
        type_definition['logicalType'] = 'timestamp-micros'
    return type_definition


def all_equal(iterable):
    g = groupby(iterable)
    return next(g, True) and not next(g, False)


def cast_to_datetime(column):
    try:
        formats, are_we_sure = _guess_format(column)
        if formats and are_we_sure:
            fmt = formats[0]
            pd.to_datetime(column, format=fmt)
            return AvroType(t='datetime', extra=fmt)
        elif formats and not are_we_sure:
            for fmt in formats:
                pd.to_datetime(column, format=fmt)
            extra_text = "several patterns match all non-empty records," \
                         " human intervention needed to decide the format: "
            extra_text += ";".join(formats)
            return AvroType(t='probably_datetime', extra=extra_text)
        else:
            raise TypeError('not a datetime')
    except Exception as e:
        raise TypeError("not a datetime")


def cast_to_integer(column):
    try:
        for r in column:
            int(str(r))
        column.astype('int')
        return AvroType(t='int')
    except Exception:
        raise TypeError("not an integer")


def cast_to_double(column):
    try:
        column.astype('double')
        for r in column:
            float(str(r))
        return AvroType(t='double')
    except Exception:
        raise TypeError("not a double")


def cast_to_float(column):
    try:
        column.astype('float')
        for r in column:
            float(str(r))
        return AvroType(t='float')
    except Exception:
        raise TypeError("not a float")


def _guess_format(column) -> tuple[list[str] | None, bool]:
    all_formats = [{guess_datetime_format(r, dayfirst=True), guess_datetime_format(r, dayfirst=False)}
                   for r in column if r and isinstance(r, str)]
    nans_count = len([r for r in column if r and isinstance(r, float) and math.isnan(r)])
    non_null_columns_count = len([r for r in column if r])
    if non_null_columns_count - nans_count != len(all_formats):
        return None, True
    # if all_formats and all_equal(all_formats):
    #     return [list(all_formats[0])[0]], True
    if all_formats:
        counter = Counter([x for xs in all_formats for x in xs])
        most_common_frequency = counter.most_common(1)[0][1]
        if most_common_frequency != len(all_formats):
            # there is no data format that matches all records in the sample
            return None, True
        most_probable_formats = [a_key for a_key, a_value in counter.items() if a_value == most_common_frequency]
        if len(most_probable_formats) == 1:
            return [most_probable_formats[0]], True
        else:
            return most_probable_formats, False
    return None, True


TYPE_ORDER = [
    'datetime',
    'int',
    'double',
    'float',
]


TYPE_MAP: dict[str, Callable] = {
    'datetime': cast_to_datetime,
    'int': cast_to_integer,
    'double': cast_to_double,
    'float': cast_to_float,
}


def find_separator(header: str) -> Tuple[str, int]:
    possible_separators = {",", ";", "|", "\t"}
    counter = Counter((ch for ch in header if ch in possible_separators))
    try:
        return counter.most_common()[0]
    except IndexError:
        return ("", 0)


def parse_line_with_quotes(a_line: str, separator: str) -> str:
    within_quotes = False
    reverted_string_with_no_comas_in_quotes = list()
    for character in a_line[::-1]:
        if QUOTES_CHARACTER == character:
            within_quotes = not within_quotes
        elif separator == character:
            if within_quotes:
                character = " "
        reverted_string_with_no_comas_in_quotes.append(character)
    return "".join(reverted_string_with_no_comas_in_quotes[::-1])


def find_headerline(path: str) -> int:
    with open(path, encoding='utf-8') as f:
        lines = f.readlines()
        separator, count = find_separator(lines[-1])
        if QUOTES_CHARACTER in lines[-1]:
            last_line_sanitized = parse_line_with_quotes(lines[-1], separator=separator)
            separator, count = find_separator(last_line_sanitized)
        if not separator:
            separator = ","
        # make sure it's not just a straight line of commas
        header_pattern = re.compile(
            fr"([^{separator}]+{separator}){{{count}}}"
        )
        for i, line in enumerate(lines):
            if "\"" in line:
                line_without_commas_in_names = parse_line_with_quotes(line, separator=separator)
            else:
                line_without_commas_in_names = line
            if (separator, count) == find_separator(line_without_commas_in_names):
                if header_pattern.match(line_without_commas_in_names):
                    return i
        return 0


def csv_to_avro(
    path: str,
    separator: Optional[str] = None,
    skip_header_line_count: Optional[int] = None
):
    """Generate Avro Schema from sample CSV file.
    The file should be smaller than 10MB
    Header of the file containing the names of columns is expected
    as field names will be derived from the column names.
    The function works best with several well defined rows
    that provide accurate representation of data expected in the
    actual .csv file.
    e.g. The function tries to determine datetime string format
    of the entered data but will struggle on dates like 1/1/25

    :param str path: Path to a .csv file.
    :param str separator: Optional. Character used as separator in csv file.
               If None, the function will try to determine the separator
               based on the provided .csf file's header.
    :param int skip_header_line_count: Optional.
               Number of lines at the beginning of .csv file
               that don't contain data (including header).
               If this param is skipped, the function will try to
               automatically determine header line based on the file content
    """
    filename = os.path.basename(path)
    file_local_os_dirname = os.path.dirname(path)
    if separator is None:
        with open(path, encoding='utf-8') as f:
            data_row = f.readlines()[-1]
            separator, _ = find_separator(data_row)
    if not separator:
        separator = ","
    if skip_header_line_count is None:
        skip_header_line_count = find_headerline(path)

    df = pd.read_csv(path, sep=separator, skiprows=skip_header_line_count, index_col=False)

    schema = make_schema(df)
    filename_no_format = '.'.join(filename.split('.')[:-1])
    name = sanitize_name(filename_no_format, maintain_letter_case=True)
    avro_compliant_name = \
        sanitize_name(filename_no_format, maintain_letter_case=False, prepare_avro_name=True)
    schema['name'] = avro_compliant_name
    schema['skip_header_line_count'] = skip_header_line_count + 1
    schema['csv_separator'] = separator

    schema_string = json.dumps(schema, indent=True)
    output_format = "avsc"
    if "probably_datetime" in [x["type"] for x in schema["fields"]]:
        output_format = "avsc_need_human_intervention_in_datetime"
    output_filename = '.'.join((filename_no_format, output_format))
    print('output_filename', output_filename)
    output_filepath = os.path.join(file_local_os_dirname, output_filename)
    print('output_filepath', output_filepath)
    with open(output_filepath, 'w', encoding='utf-8') as f:
        f.write(schema_string)

    return schema, output_format


__all__ = ["csv_to_avro", TEN_MEGABYTE_LIMIT]
