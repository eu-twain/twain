import logging
import pendulum
import sys

sys.path.append("/opt/bitnami/airflow/dags/git_ps-dags")  # noqa: E402

from collections import defaultdict

from airflow import XComArg
from airflow.decorators import dag, task, task_group
from airflow.models import Variable
from airflow.models.baseoperator import chain
from airflow.providers.amazon.aws.sensors.s3 import S3KeySensor
from airflow.providers.amazon.aws.operators.s3 import (
    S3ListOperator,
    S3CopyObjectOperator,
    S3DeleteObjectsOperator,
)
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.common.sql.operators.sql import SQLExecuteQueryOperator
from airflow.utils.trigger_rule import TriggerRule
from src.common.tasks.static_data_tasks import TrinoSyncOperator, failure_callback, copy_kwargs, ingestion_report, \
    build_sync_partitions, build_create_table


logger = logging.getLogger(__name__)

BUCKET_PUBLIC: str = Variable.get("static_bucket_public", "static-data-ingestion-public")
BUCKET_DATALAKE: str = Variable.get("bucket_datalake", "datalake")
BUCKET_RESULTS: str = Variable.get("static_bucket_results", "static-data-ingestion-results")

KEY_SENSOR_WILDCARD_AVSC: str = "*.avsc"
KEY_SENSOR_WILDCARD_CSV: str = "*.csv"


def create_dag(dag_id, sub_bucket):
    @dag(
        dag_id=dag_id,
        description="Move AVSC file(s) to permanent storage and CREATE TABLE accordingly in Trino.",
        schedule="@continuous",
        max_active_runs=1,
        start_date=pendulum.datetime(2024, 1, 1, tz="UTC"),
        default_args={
            "on_failure_callback": failure_callback,
        },

        catchup=False,
        tags=["MinIO", "Trino", "avsc"],
    )
    def process_static_data_taskflow_api() -> None:

        @task_group
        def copy_files(s3_files: XComArg) -> None:
            S3CopyObjectOperator.partial(
                task_id="copy_files_to_datalake",
                aws_conn_id="aws_minio",
                source_bucket_name=f"{BUCKET_PUBLIC}",
                dest_bucket_name=f"{BUCKET_DATALAKE}",
            ).expand_kwargs(s3_files.map(copy_kwargs))

        @task_group
        def delete_files(s3_files: XComArg) -> None:
            S3DeleteObjectsOperator.partial(
                trigger_rule=TriggerRule.NONE_SKIPPED,
                task_id="delete_files_from_origin",
                aws_conn_id="aws_minio",
                bucket=f"{BUCKET_PUBLIC}",
            ).expand(keys=s3_files)

        # A Sensor: monitor *.avsc file(s) to be uploaded to INGESTION-PUBLIC bucket.
        sensor_avsc = S3KeySensor(
            task_id="s3_key_sensor_avsc",
            aws_conn_id="aws_minio",
            bucket_name=BUCKET_PUBLIC,
            bucket_key=f"{sub_bucket}/{KEY_SENSOR_WILDCARD_AVSC}",
            wildcard_match=True,
        )

        # A Sensor: monitor *.csv file(s) to be uploaded to INGESTION-PUBLIC bucket.
        sensor_csv = S3KeySensor(
            task_id="s3_key_sensor_csv",
            aws_conn_id="aws_minio",
            bucket_name=BUCKET_PUBLIC,
            bucket_key=f"{sub_bucket}/{KEY_SENSOR_WILDCARD_CSV}",
            wildcard_match=True,
        )

        list_avro_files = S3ListOperator(
            # Get Files List and store in the XCom
            task_id="list_avro_files",
            aws_conn_id="aws_minio",
            bucket=BUCKET_PUBLIC,
            prefix=f"{sub_bucket}/{KEY_SENSOR_WILDCARD_AVSC}",
            # the delimiter marks key hierarchy, e.g. if "/" then doesn't traverse subfolders
            delimiter="",
            apply_wildcard=True,
        )

        list_csv_files = S3ListOperator(
            # Get Files List and store in the XCom
            task_id="list_csv_files",
            aws_conn_id="aws_minio",
            bucket=BUCKET_PUBLIC,
            prefix=f"{sub_bucket}/{KEY_SENSOR_WILDCARD_CSV}",
            # the delimiter marks key hierarchy, e.g. if "/" then doesn't traverse subfolders
            delimiter="",
            apply_wildcard=True,
        )

        create_trino_tables_sql = build_create_table(
            list_avro_files.output, list_csv_files.output, sub_bucket
        )
        sync_partitions_sql = build_sync_partitions(list_avro_files.output)

        trino_create_table = TrinoSyncOperator(
            task_id="trino_create_table_parquet",
            conn_id="twain_trino",
            sql=create_trino_tables_sql,
            split_statements=True,
            handler=list,
        )

        trino_sync_partitions = SQLExecuteQueryOperator(
            task_id="trino_sync_partitions",
            conn_id="twain_trino",
            sql=sync_partitions_sql,
            split_statements=True,
            handler=list,
        )

        copy_avro_files_to_datalake = copy_files(list_avro_files.output)
        copy_csv_files_to_datalake = copy_files(list_csv_files.output)
        delete_avro_files_from_origin = delete_files(list_avro_files.output)
        delete_csv_files_from_origin = delete_files(list_csv_files.output)
        copy_avro_files_to_datalake >> delete_avro_files_from_origin
        copy_csv_files_to_datalake >> delete_csv_files_from_origin
        chain(
            create_trino_tables_sql,
            sync_partitions_sql
        )
        report = ingestion_report(list_csv_files.output, list_avro_files.output)

        chain(
            sensor_avsc,
            sensor_csv,
            list_avro_files,
            copy_avro_files_to_datalake,
            list_csv_files,
            copy_csv_files_to_datalake,
            create_trino_tables_sql,
            trino_create_table,
            trino_sync_partitions,
            delete_avro_files_from_origin,
            delete_csv_files_from_origin,
            report
        )

    generated_dag = process_static_data_taskflow_api()
    return generated_dag



CONSORTIUM_MEMBERS = {
    'CENER',
    'DTU',
    'EDF',
    'ENGIE_GREEN_FRANCE',
    'ENGIE_LABORELEC',
    'RAMBOLL',
    'RAMBOLL_DANMARK_AS',
    'TUD',
    'TUM',
    'VATT',
    'public'
}

sub_buckets: list[str] = list(CONSORTIUM_MEMBERS)
for sub_bucket in sub_buckets:
    dag_id = f"static_data_{sub_bucket}"
    globals()[dag_id] = create_dag(dag_id, sub_bucket)
