#!/usr/bin/env python

from utils.exceptions import TwainCustomException


AVRO_TO_TRINO_TYPE_MAP: dict[str, str] = {
    "boolean": "BOOLEAN",
    "long": "BIGINT",
    "int": "INT",
    "float": "REAL",
    "double": "DOUBLE",
    "string": "VARCHAR",
    "bytes": "VARCHAR",
    "datetime": "TIMESTAMP(6)", # TODO: check exact timestamp setup in Trino
}

AVRO_TO_TRINO_LOGICAL_TYPE_MAP: dict[str, str] = {
    "decimal": "DECIMAL",
    "uuid": "UUID",
    "timestamp-millis": "TIMESTAMP(3)",
    "timestamp-micros": "TIMESTAMP(6)",
}


CREATE_TABLE_STATEMENT = """
CREATE TABLE IF NOT EXISTS hive.default.{table_name} (
{columns}
)
"""

COLUMN_STATEMENT = "    {column_name} {column_type}{comment}"
COMMENT_STATEMENT = " COMMENT '{comment_text}'"

STATIC_INFO_TABLE_NAME_PREFIX = 'static_info_'


class UnsupportedType(Exception):
    def __init__(self, field):
        type_kind = "logicalType" if "logicalType" in field else "type"
        message = (
            f"Unsupported {type_kind} type {field['type']} for field {field['name']}"
        )

        self.message = message
        super().__init__(self.message)


def normalize_table_name(input_table_name: str, lower_case: bool = True):
    """
    Function normalizes the static ingestion table name by adding STATIC_INFO_TABLE_NAME_PREFIX if it is
    not present already; additionally it converts the table name to lowercase unless flag lower_case is set to False
    :param input_table_name: the name of the input table
    :param lower_case: the boolean flag which when set to False omits lowering the letters in table name
    :return: normalized table name
    """
    if not input_table_name.lower().startswith(STATIC_INFO_TABLE_NAME_PREFIX):
        input_table_name = STATIC_INFO_TABLE_NAME_PREFIX + input_table_name
    if lower_case:
        input_table_name = input_table_name.lower()
    return input_table_name


def parse_avro_schema(schema: dict):
    """
    Function checks the correctness of the semi-avro schema provided by the user.
    Raises TwainCustomException with description of the error
    :param schema: the semi-avro schema to be checked for common errors
    :return: None
    """
    if 'fields' not in schema.keys():
        details = "Provided avro schema lacks definitions of fields."
        raise TwainCustomException(message="Error in avro schema", details=details)
    if 'skip_header_line_count' not in schema.keys():
        details = "Provided avro schema lacks the number of header rows to be skipped," \
                  " called \"skip_header_line_count\"."
        raise TwainCustomException(message="Error in avro schema", details=details)
    else:
        try:
            int(schema['skip_header_line_count'])
        except ValueError as ve:
            details = "Provided avro schema lacks the number of header rows to be skipped," \
                      " field called \"skip_header_line_count\" is not an integer."
            raise TwainCustomException(message="Error in avro schema", details=details)
    if 'csv_separator' not in schema.keys():
        details = "Provided avro schema lacks the column separator."
        raise TwainCustomException(message="Error in avro schema", details=details)
    for index_i, field in enumerate(schema['fields']):
        if "name" not in field.keys():
            details = f"Provided avro schema lacks a name of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "." in field["name"]:
            details = f"Provided avro schema has an invalid character (dot) a name of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "type" not in field.keys():
            details = f"Provided avro schema lacks a type of field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)
        if "datetime" == field["type"] and "parsing_pattern" not in field.keys():
            details = f"Provided avro schema lacks a parsing pattern for datetime field number {index_i + 1}."
            raise TwainCustomException(message="Error in avro schema", details=details)


def trino_type_from_field(field: dict) -> str:
    if isinstance(field["type"], list):
        for t in field["type"]:
            if isinstance(t, dict):
                return trino_type_from_field(t)
            trino_type = AVRO_TO_TRINO_TYPE_MAP.get(t, None)
            if trino_type and trino_type != "null":
                break
        if not trino_type:
            raise KeyError

    else:
        trino_type = AVRO_TO_TRINO_TYPE_MAP[field["type"]]

    if "logicalType" in field:
        trino_type = AVRO_TO_TRINO_LOGICAL_TYPE_MAP[field["logicalType"]]
    return trino_type


def field_to_typed_column(field: dict) -> str:
    try:
        trino_type = trino_type_from_field(field)
        return COLUMN_STATEMENT.format(
            column_name=field["name"],
            column_type=trino_type,
            comment=(
                COMMENT_STATEMENT.format(comment_text=field["doc"])
                if "doc" in field
                else ""
            ),
        )
    except KeyError as e:
        raise UnsupportedType(field) from e


def field_to_str_column(field: dict) -> str:
    return COLUMN_STATEMENT.format(
        column_name=field["name"],
        column_type="VARCHAR",
        comment=(
            COMMENT_STATEMENT.format(comment_text=field["doc"])
            if "doc" in field
            else ""
        ),
    )


def trino_create_table_statement(schema: dict, only_str=False, table_name=None) -> str:
    """
    Function returns a string containing  'CREATE TABLE'
    statement with corresponding types

    :param dict schema: Twain Avro~ish schema
    :param bool only_str: if set to True, the column
         types in output will be only of varchar types.
         Otherwise the types will correspond to the ones defined in schema
    :param str|None table_name: optional table name to override the default name coming
         from avro schema

    :return: trino SQL string
    :rtype: str

    :raises UnsupportedType: if the type provided in avro is unsupported

    """

    mapper = field_to_str_column if only_str else field_to_typed_column
    columns = [mapper(field) for field in schema["fields"]]
    columns.append("ro_access_companies VARCHAR")
    columns.append("file_owner VARCHAR")
    columns.append("owning_company VARCHAR")
    table_name = table_name or schema['name']
    table_name = normalize_table_name(table_name)

    return CREATE_TABLE_STATEMENT.format(
        table_name=table_name, columns=",\n".join(columns)
    )


if __name__ == "__main__":

    test_avro = {
        "type": "record",
        "namespace": "dk.dtu.twain",
        "name": "static_info_la_haute_borne",
        "doc": "Static information about La Haute Borne wind farm",
        "format": "CSV",
        "csv_separator": ";",
        "skip_header_line_count": "1",
        "fields": [
            {
                "name": "wind_turbine_name",
                "type": "string",
                "doc": "Wind turbine name",
            },
            {
                "name": "wind_turbine_long_name",
                "type": "string",
                "doc": "Wind turbine long name",
            },
            {"name": "manufacturer", "type": "string", "doc": "Manufacturer"},
            {"name": "model", "type": "string", "doc": "Model"},
            {"name": "rated_power", "type": "int", "doc": "Rated power (kW)"},
            {"name": "hub_height", "type": "int", "doc": "Hub height (m)"},
            {"name": "rotor_diameter", "type": "int", "doc": "Rotor diameter (m)."},
            {"name": "gps", "type": "string", "doc": "GPS"},
            {"name": "altitude", "type": "int", "doc": "Altitude (m)."},
            {
                "name": "comissioning_date",
                "type": "datetime",
                "doc": "Comissioning date",
                "parsing_pattern": "%Y-%m-%d",
            },
            {"name": "departament", "type": "string", "doc": "Departament"},
            {"name": "region", "type": "string", "doc": "Region"},
        ],
    }

    # test_avro = {
    #     'type': 'record',
    #     'name': 'namespace',
    #     'fields': [{'name': 'wtur_id',
    #                 'type': 'int',
    #                 'doc': 'a fake turbine  identifier 470 or something'},
    #                {'name': 'twain_name',
    #                 'type': 'string',
    #                 'doc': 'altered name to twain name field description from airfow  minikube'},
    #                {'name': 'surname',
    #                 'type': ['string', 'null'],
    #                 'doc': 'surname of the person'},
    #                {'name': 'location_name',
    #                 'type': ['string', 'null'],
    #                 'doc': 'some comment set in file 075'},
    #                {'name': 'grd_prod_pwr_std',
    #                 'type': ['double', 'null'],
    #                 'doc': 'some comment of the columns for fake_data_ps_047_engie_csv_minikube'},
    #                {'name': 'date',
    #                 'type': [{'type': 'long', 'logicalType': 'timestamp-millis'}, "null"],
    #                 'doc': 'started using datetime, a non-existing type in avro since file 076'},
    #                {'name': 'hcnt_avg_gen1', 'type': ['double', 'null']},
    #                {'name': 'gen_rpm_avg', 'type': ['double', 'null']},
    #                {'name': 'wgen_rotspd_avg', 'type': ['double', 'null']},
    #                {'name': 'location_latitude', 'type': ['double', 'null']},
    #                {'name': 'location_longitude', 'type': ['double', 'null']},
    #                {'name': 'twain_original_file_name', 'type': ['string', 'null']},
    #                {'name': 'twain_data_ro_allowed_companies', 'type': ['string', 'null']},
    #                {'name': 'twain_data_owning_company', 'type': 'string'},
    #                {'name': 'twain_data_owning_member', 'type': 'string'}],
    #     'owner': 'Unknown',
    #     'retention': 90}

    print(trino_create_table_statement(test_avro, only_str=False))
