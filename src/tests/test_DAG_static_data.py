import json
import os
import sys
import logging
import datetime
import pytz
import trino
import pytest
from typing import List
from unittest.mock import Mock, patch, MagicMock
from dateutil.relativedelta import relativedelta
from airflow.exceptions import AirflowException
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "dags"))
sys.modules["src.common.utils.statics"] = MagicMock()

from src.dags.src.common.tasks.static_data_tasks import TrinoSyncOperator, coalesce, failure_callback, \
    copy_kwargs, ingestion_report, _get_expected_ingestion_details, get_table_partitions_as_strings, \
    build_sync_partitions, infer_ingestion_status, build_create_table
from src.dags import utils

LOGGER = logging.getLogger("py_test-openmetadata-automatic-connection-to-twain-trino")
logging.basicConfig(format="%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S",
                    level="INFO", stream=sys.stdout)


class TaskInstanceOverwrite(object):
    def __init__(self, init_key: str = None, init_value: str = None):
        self._dict = dict()
        if init_key:
            self._dict[init_key] = init_value

    def xcom_push(self, key, value):
        if key:
            self._dict[key] = value

    def xcom_pull(self, key):
        return self._dict[key] if key in self._dict.keys() else None

    def xcom_pull(self, key, task_ids: List[str] = None):
        if "ingestion_stage" == key and "ingestion_stage" not in self._dict.keys():
            return [None, None, "trino_create_table_parquet"]
        return self._dict[key] if key in self._dict.keys() else None


class S3FileMockContent(object):
    def __init__(self, string_data: str, key: str, bucket_name: str, replace: bool):
        self._dict = dict()
        if key:
            self._dict[key] = {
                "string_data": string_data,
                "bucket_name": bucket_name
            }

    def append_or_replace(self, string_data: str, key: str, bucket_name: str, replace: bool):
        if not replace:
            if key in self._dict.keys():
                return
        self._dict[key] = {
            "string_data": string_data,
            "bucket_name": bucket_name
        }

    def append_or_replace_bites_data(self, bytes_data: bytes, key: str, bucket_name: str, replace: bool):
        if not replace:
            if key in self._dict.keys():
                return
        self._dict[key] = {
            "string_data": bytes_data.decode("utf-8") if isinstance(bytes_data, bytes) else bytes_data,
            "bucket_name": bucket_name
        }

    def get_best_match_key(self, partial_key: str):
        for x in self._dict.keys():
            if x.startswith(partial_key):
                return x


class MockRead(object):
    @staticmethod
    def read():
        return "something, something"


def mock_remove_function(input_argument):
    if input_argument and "/tmp/trigger_value_exception" == input_argument:
        raise ValueError("Mocked value error")
    return True


class S3FileMockLoadBytesWthMemory(object):
    def __init__(self, init_limit = 2):
        self.limit = init_limit
        self.counter = 0

    def append_or_replace_bites_data(self, bytes_data: bytes, key: str, bucket_name: str, replace: bool):
        self.counter += 1
        if self.counter >= self.limit:
            raise ValueError("Mocked value error")


@patch("airflow.providers.common.sql.operators.sql.SQLExecuteQueryOperator.execute")
def test_trino_sync_operator(mock_sqlexecutequeryoperator_execute):
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tso = TrinoSyncOperator(
        task_id="trino_create_table_parquet",
        conn_id="twain_trino",
        sql="SELECT 1",
        split_statements=True,
        handler=list,)
    mock_sqlexecutequeryoperator_execute.return_value = None
    context = {"ti": tio}
    tso.execute(context)
    assert "trino_create_table" == context["ti"]._dict["ingestion_stage"]
    assert context["ti"]._dict["some_empty_key"] is None
    assert True if context["ti"]._dict["task_finished_successfuly"] is True else False


def test_coalesce():
    a_test_list = [None, "", "text", 123]
    assert "" == coalesce(a_test_list)


def test_coalesce_number():
    a_test_list = [None, None, None, 123]
    assert 123 == coalesce(a_test_list)


def test_failure_callback():
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    context = {"ti": tio}
    failure_callback(context)
    assert True


@patch("src.dags.src.common.tasks.static_data_tasks.BUCKET_PUBLIC", "public_bucket")
@patch("src.dags.src.common.tasks.static_data_tasks.BUCKET_DATALAKE", "datalake")
def test_copy_kwargs():
    a_file_path = "a_directory/a_subdirectry/a_file_name.txt"
    result = copy_kwargs(a_file_path)
    expected_result = {
        "source_bucket_key": a_file_path,
        "dest_bucket_key": "a_file_name.txt"
    }
    assert expected_result == result


fmc = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc.append_or_replace)
def test_ingestion_report_happy_path():
    csv_files = [
        "a_file_001.csv",
        "completelly_different_name.csv"
    ]
    avro_files = [
        "file_001.avsc",
        "file_002.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push(
        "file_pair_ingestion_details",
        {
            "s3://datalake/some_bucket/file_001.avsc": {
                "expected_table_name": "file_001",
                "paired_csv_date_file": "other_bucket/subdir/a_data_file_001.csv",
                "csv_to_parquet_map": {
                    "other_bucket/subdir/a_data_file_001.csv": [
                        "s3://datalake/static_info_some_fake_data_dude_013/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet",
                        "s3://datalake/static_info_some_fake_data_dude_013/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet"
                    ]
                }
            },
            "s3://datalake/some_bucket/file_002.avsc": {
                "expected_table_name": "file_002",
                "paired_csv_date_file": "other_bucket/different_subdir/a_data_file_002.csv",
                "csv_to_parquet_map": {
                    "other_bucket/different_subdir/a_data_file_002.csv": [
                        "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet",
                        "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet"
                    ]
                }
            }
        }
    )
    tio.xcom_push("task_finished_successfuly", True)
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc looks like: {fmc._dict}")
    assert not exception_raised
    assert len(fmc._dict.keys()) == 2

    first_key = fmc.get_best_match_key("success_ingestion_other_bucket_subpath_subdir_subpath_a_data_file_001.")
    assert "static-data-ingestion-results" == fmc._dict[first_key]["bucket_name"]
    assert fmc._dict[first_key]["string_data"].startswith("\n            STATIC INGESTION SUCCESS. Finished at:")
    assert "Generated table: file_001" in fmc._dict[first_key]["string_data"]
    assert "s3://datalake/static_info_some_fake_data_dude_013/owning_company=public/" \
           "59ee61432d2442f99e0bc9157f0ac038-0.parquet\n" \
           "s3://datalake/static_info_some_fake_data_dude_013/owning_company=public/" \
           "3c172511b28e4149accb36df16b34be5-0.parquet\n" \
           in fmc._dict[first_key]["string_data"]

    second_key = fmc.get_best_match_key(
        "success_ingestion_other_bucket_subpath_different_subdir_subpath_a_data_file_002.")
    assert "static-data-ingestion-results" == fmc._dict[second_key]["bucket_name"]
    assert fmc._dict[second_key]["string_data"].startswith("\n            STATIC INGESTION SUCCESS. Finished at:")
    assert "Generated table: file_002" in fmc._dict[second_key]["string_data"]
    assert "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet\n" \
           "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet\n" \
           in fmc._dict[second_key]["string_data"]


fmc_2 = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc_2.append_or_replace)
def test_ingestion_report_simple_error():
    csv_files = [
        "a_file_001.csv",
        "completelly_different_name.csv"
    ]
    avro_files = [
        "datalake/some_bucket/file_001.avsc",
        "datalake/some_bucket/file_002.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push(
        "file_pair_ingestion_details",
        {
            "datalake/some_bucket/file_001.avsc": {
                "csv_to_parquet_map": None,
            },
            "datalake/some_bucket/file_002.avsc": {
                "expected_table_name": "file_002",
                "paired_csv_date_file": "other_bucket/different_subdir/a_data_file_002.csv",
                "csv_to_parquet_map": {
                    "other_bucket/different_subdir/a_data_file_002.csv": [
                        "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet",
                        "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet"
                    ]
                }
            }
        }
    )
    tio.xcom_push("task_finished_successfuly", False)
    tio.xcom_push(
        "incorrect_avsc",
        {
            "datalake/some_bucket/file_001.avsc": "Error due to incorrect json"
        }
    )
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc_2 looks like: {fmc_2._dict}")
    assert exception_raised
    assert len(fmc_2._dict.keys()) == 2

    failed_avro_key = fmc_2.get_best_match_key("failed_ingestion_datalake_subpath_some_bucket_subpath_file_001.avsc")
    assert "static-data-ingestion-results" == fmc_2._dict[failed_avro_key]["bucket_name"]
    assert fmc_2._dict[failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Error details: Error due to incorrect json\n" in fmc_2._dict[failed_avro_key]["string_data"]

    successful_avro_key = fmc_2.get_best_match_key(
        "success_ingestion_other_bucket_subpath_different_subdir_subpath_a_data_file_002.csv")
    assert "static-data-ingestion-results" == fmc_2._dict[successful_avro_key]["bucket_name"]
    assert fmc_2._dict[successful_avro_key]["string_data"].startswith(
        "\n            STATIC INGESTION SUCCESS. Finished at:")
    assert "Generated table: file_002" in fmc_2._dict[successful_avro_key]["string_data"]
    assert "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet\n" \
           "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet\n" \
           in fmc_2._dict[successful_avro_key]["string_data"]


fmc_3 = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc_3.append_or_replace)
def test_ingestion_report_simple_error_unknown_stage():
    csv_files = [
        "a_file_001.csv",
        "completelly_different_name.csv"
    ]
    avro_files = [
        "datalake/some_bucket/file_001.avsc",
        "datalake/some_bucket/file_002.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push(
        "file_pair_ingestion_details",
        {
            "datalake/some_bucket/file_001.avsc": {
                "csv_to_parquet_map": None,
            },
            "datalake/some_bucket/file_002.avsc": {
                "expected_table_name": "file_002",
                "paired_csv_date_file": "other_bucket/different_subdir/a_data_file_002.csv",
                "csv_to_parquet_map": {
                    "other_bucket/different_subdir/a_data_file_002.csv": [
                        "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet",
                        "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet"
                    ]
                }
            }
        }
    )
    tio.xcom_push("task_finished_successfuly", False)
    tio.xcom_push("ingestion_stage", [None, None, None])
    tio.xcom_push(
        "incorrect_avsc",
        {
            "datalake/some_bucket/file_001.avsc": "Error due to incorrect json"
        }
    )
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc_3 looks like: {fmc_3._dict}")
    assert exception_raised
    assert len(fmc_3._dict.keys()) == 2

    failed_avro_key = fmc_3.get_best_match_key("failed_ingestion_datalake_subpath_some_bucket_subpath_file_001.avsc")
    assert "static-data-ingestion-results" == fmc_3._dict[failed_avro_key]["bucket_name"]
    assert fmc_3._dict[failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Error details: Error due to incorrect json\n" in fmc_3._dict[failed_avro_key]["string_data"]
    assert "Ingestion failed on stage: UNKNOWN" in fmc_3._dict[failed_avro_key]["string_data"]

    successful_avro_key = fmc_3.get_best_match_key(
        "success_ingestion_other_bucket_subpath_different_subdir_subpath_a_data_file_002.csv")
    assert "static-data-ingestion-results" == fmc_3._dict[successful_avro_key]["bucket_name"]
    assert fmc_3._dict[successful_avro_key]["string_data"].startswith(
        "\n            STATIC INGESTION SUCCESS. Finished at:")
    assert "Generated table: file_002" in fmc_3._dict[successful_avro_key]["string_data"]
    assert "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet\n" \
           "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet\n" \
           in fmc_3._dict[successful_avro_key]["string_data"]


fmc_4 = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc_4.append_or_replace)
def test_ingestion_report_unknown_error_unknown_stage():
    csv_files = [
        "other_bucket/different_subdir/completelly_different_name.csv",
        "other_bucket/different_subdir/a_data_file_002.csv"
    ]
    avro_files = [
        "datalake/some_bucket/file_001.avsc",
        "datalake/some_bucket/file_002.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push(
        "file_pair_ingestion_details",
        {
            "datalake/some_bucket/file_001.avsc": {
                "expected_table_name": "file_001",
                "paired_csv_date_file": "other_bucket/different_subdir/completelly_different_name.csv",
                "csv_to_parquet_map": None,
            },
            "datalake/some_bucket/file_002.avsc": {
                "expected_table_name": "file_002",
                "paired_csv_date_file": "other_bucket/different_subdir/a_data_file_002.csv",
                "csv_to_parquet_map": {
                    "other_bucket/different_subdir/a_data_file_002.csv": [
                        "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet",
                        "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet"
                    ]
                }
            }
        }
    )
    tio.xcom_push("task_finished_successfuly", False)
    tio.xcom_push("ingestion_stage", [None, None, None])
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc_4 looks like: {fmc_4._dict}")
    assert exception_raised
    assert len(fmc_4._dict.keys()) == 2

    failed_avro_key = fmc_4.get_best_match_key(
        "failed_ingestion_other_bucket_subpath_different_subdir_subpath_completelly_different_name.csv")
    LOGGER.info(f"failed_avro_key: {failed_avro_key}")
    assert "static-data-ingestion-results" == fmc_4._dict[failed_avro_key]["bucket_name"]
    assert fmc_4._dict[failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Ingestion failed on stage: UNKNOWN\n" in fmc_4._dict[failed_avro_key]["string_data"]

    successful_avro_key = fmc_4.get_best_match_key(
        "success_ingestion_other_bucket_subpath_different_subdir_subpath_a_data_file_002.csv")
    assert "static-data-ingestion-results" == fmc_4._dict[successful_avro_key]["bucket_name"]
    assert fmc_4._dict[successful_avro_key]["string_data"].startswith(
        "\n            STATIC INGESTION SUCCESS. Finished at:")
    assert "Generated table: file_002" in fmc_4._dict[successful_avro_key]["string_data"]
    assert "s3://datalake/static_info_file_002/owning_company=public/59ee61432d2442f99e0bc9157f0ac038-0.parquet\n" \
           "s3://datalake/static_info_file_002/owning_company=public/3c172511b28e4149accb36df16b34be5-0.parquet\n" \
           in fmc_4._dict[successful_avro_key]["string_data"]


fmc_5 = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc_5.append_or_replace)
def test_ingestion_report_4_simple_errors_no_success():
    csv_files = [
        "a_file_001.csv",
        "completelly_different_name.csv",
        "details_about_windfarm.csv"
    ]
    avro_files = [
        "datalake/some_bucket/file_001.avsc",
        "datalake/some_bucket/file_002.avsc",
        "datalake/some_bucket/file_003.avsc",
        "a_file_that_is_supposed_to_fail.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push("task_finished_successfuly", False)
    tio.xcom_push(
        "incorrect_avsc",
        {
            "datalake/some_bucket/file_001.avsc": "Error due to incorrect json",
            "datalake/some_bucket/file_002.avsc": "Error due to incorrect date parsing pattern",
            "datalake/some_bucket/file_003.avsc": "Error due to incorrect json"
        }
    )
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc_5 looks like: {fmc_5._dict}")
    assert exception_raised
    assert len(fmc_5._dict.keys()) == 4

    first_failed_avro_key = fmc_5.get_best_match_key(
        "failed_ingestion_datalake_subpath_some_bucket_subpath_file_001.avsc")
    assert "static-data-ingestion-results" == fmc_5._dict[first_failed_avro_key]["bucket_name"]
    assert fmc_5._dict[first_failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Error details: Error due to incorrect json\n" in fmc_5._dict[first_failed_avro_key]["string_data"]

    second_failed_avro_key = fmc_5.get_best_match_key(
        "failed_ingestion_datalake_subpath_some_bucket_subpath_file_002.avsc")
    assert "static-data-ingestion-results" == fmc_5._dict[second_failed_avro_key]["bucket_name"]
    assert fmc_5._dict[second_failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Error due to incorrect date parsing pattern\n" in fmc_5._dict[second_failed_avro_key]["string_data"]

    third_failed_avro_key = fmc_5.get_best_match_key(
        "failed_ingestion_datalake_subpath_some_bucket_subpath_file_003.avsc")
    assert "static-data-ingestion-results" == fmc_5._dict[third_failed_avro_key]["bucket_name"]
    assert fmc_5._dict[third_failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Error details: Error due to incorrect json\n" in fmc_5._dict[third_failed_avro_key]["string_data"]

    fourth_failed_avro_key = fmc_5.get_best_match_key("failed_ingestion_a_file_that_is_supposed_to_fail.avsc")
    assert "static-data-ingestion-results" == fmc_5._dict[fourth_failed_avro_key]["bucket_name"]
    assert fmc_5._dict[fourth_failed_avro_key]["string_data"].startswith("\n            STATIC INGESTION FAILED at ")
    assert "Ingestion failed on stage: trino_create_table_parquet" in fmc_5._dict[fourth_failed_avro_key]["string_data"]


fmc_6 = S3FileMockContent("", None, None, False)


@patch("src.dags.src.common.tasks.static_data_tasks.TABLE_SUFFIX_PATTERN", "%Y%m%d%H%M%S")
@patch.object(S3Hook, "load_string", fmc_6.append_or_replace)
def test_ingestion_report_2_complex_errors_no_success():
    csv_files = [
        "a_file_001.csv"
    ]
    avro_files = [
        "datalake/some_bucket/file_001.avsc",
        "datalake/some_bucket/file_002.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    tio.xcom_push("task_finished_successfuly", False)
    tio.xcom_push(
        "file_pair_ingestion_details",
        {
            "datalake/some_bucket/file_001.avsc": {
                "expected_table_name": "file_001",
                "paired_csv_date_file": "a_file_001.csv",
                "csv_to_parquet_map": {
                    "a_file_001.csv": None
                }
            },
            "datalake/some_bucket/file_002.avsc": {
                "expected_table_name": "file_002",
                "paired_csv_date_file": "a_file_002.csv",
                "csv_to_parquet_map": {
                    "I_do_not_match": None
                }
            }
        }
    )
    exception_raised = False
    try:
        ingestion_report.function(csv_files, avro_files, ti=tio)
    except Exception as exc:
        exception_raised = True
        LOGGER.info(f"exc raised: {type(exc)}: {exc}")
    LOGGER.info(f"After test fmc_6 looks like: {fmc_6._dict}")
    assert exception_raised
    assert len(fmc_6._dict.keys()) == 2

    first_failed_avro_key = fmc_6.get_best_match_key(
        "failed_ingestion_a_file_001.csv")
    assert "static-data-ingestion-results" == fmc_6._dict[first_failed_avro_key]["bucket_name"]
    assert fmc_6._dict[first_failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Ingestion failed on stage: trino_create_table_parquet\n" in\
           fmc_6._dict[first_failed_avro_key]["string_data"]

    second_failed_avro_key = fmc_6.get_best_match_key(
        "failed_ingestion_a_file_002.csv")
    assert "static-data-ingestion-results" == fmc_6._dict[second_failed_avro_key]["bucket_name"]
    assert fmc_6._dict[second_failed_avro_key]["string_data"].startswith("\n        STATIC INGESTION FAILED at ")
    assert "Ingestion failed on stage: trino_create_table_parquet\n" in \
           fmc_6._dict[second_failed_avro_key]["string_data"]


def s3_read_key_overwrite(first, stripped_avro, bucket_name):
    LOGGER.info("s3_read_key_overwrite called")
    if stripped_avro == "happy_path.avsc":
        happy_path_dict = {
            "name": "table_name_001",
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": 1,
            "csv_separator": ","
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "happy_path.second_file.avsc":
        happy_path_dict = {
            "name": "table_name_002",
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": 3,
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "happy_path.third_file.avsc":
        happy_path_dict = {
            "name": "UppeR_TablE_nAme",
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": 3,
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "happy_path.fourth_file.avsc":
        happy_path_dict = {
            "name": "UppeR_TablE_nAme_again",
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                },
                {
                    "name": "datetime",
                    "type": "datetime",
                    "logicalType": "timestamp-micros",
                    "parsing_pattern": "%d/%m/%Y",
                    "doc": "a comment about THE Column"
                }
            ],
            "skip_header_line_count": 3,
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "error_no_name.avsc":
        happy_path_dict = {
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": 3,
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "lack_of_name.avsc":
        happy_path_dict = {
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": 3,
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "error_not.a_json.avsc":
        return "I am not a json! You shall not parse!"
    elif stripped_avro == "error_not.an_integer.avsc":
        happy_path_dict = {
            "name": "table_name_not_an_integer",
            "fields": [
                {
                    "name": "a_sample_field",
                    "type": "string"
                }
            ],
            "skip_header_line_count": "You shall not parse!",
            "csv_separator": ";"
        }
        return json.dumps(happy_path_dict)
    elif stripped_avro == "throw_an_value_error.avsc":
        LOGGER.info(f"s3_read_key_overwrite rasies exception for input {stripped_avro}")
        raise ValueError("Mocked value error")
    return "{'a_key': 'a_value'}"


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test__get_expected_ingestion_details_happy_path():
    csv_files = [
        "a_file_001.csv",
        "a_file_002.csv"
    ]
    avro_files = [
        "datalake/some_bucket/happy_path.avsc",
        "datalake/some_bucket/happy_path.second_file.avsc"
    ]
    expected_result = {
        "datalake/some_bucket/happy_path.avsc": {
            "paired_csv_date_file": "a_file_001.csv",
            "expected_table_name": "static_info_table_name_001",
            "csv_to_parquet_map": {}
        },
        "datalake/some_bucket/happy_path.second_file.avsc": {
            "paired_csv_date_file": "a_file_002.csv",
            "expected_table_name": "static_info_table_name_002",
            "csv_to_parquet_map": {}
        }
    }
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    expected_table_ingestion_details, unparsable_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    LOGGER.info(f"expected_table_ingestion_details: {expected_table_ingestion_details}")
    assert not unparsable_avsc.keys()
    assert len(expected_table_ingestion_details.keys()) == 2
    assert expected_result == expected_table_ingestion_details


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test__get_expected_ingestion_details_happy_path_reverse_csv_order():
    csv_files = [
        "a_file_002.csv",
        "a_file_001.csv"
    ]
    avro_files = [
        "datalake/some_bucket/happy_path.avsc",
        "datalake/some_bucket/happy_path.second_file.avsc"
    ]
    expected_result = {
        "datalake/some_bucket/happy_path.avsc": {
            "paired_csv_date_file": "a_file_002.csv",
            "expected_table_name": "static_info_table_name_001",
            "csv_to_parquet_map": {}
        },
        "datalake/some_bucket/happy_path.second_file.avsc": {
            "paired_csv_date_file": "a_file_001.csv",
            "expected_table_name": "static_info_table_name_002",
            "csv_to_parquet_map": {}
        }
    }
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    expected_table_ingestion_details, unparsable_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    LOGGER.info(f"expected_table_ingestion_details: {expected_table_ingestion_details}")
    assert not unparsable_avsc.keys()
    assert len(expected_table_ingestion_details.keys()) == 2
    assert expected_result == expected_table_ingestion_details


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test__get_expected_ingestion_details_error_no_name():
    csv_files = [
        "a_file_001.csv"
    ]
    avro_files = [
        "datalake/some_bucket/error_no_name.avsc"
    ]
    expected_result = {
        "datalake/some_bucket/error_no_name.avsc":
            "Error parsing file datalake/some_bucket/error_no_name.avsc. Missing parameter 'name' responsible for "
            "containing the target table name."
    }
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    expected_table_ingestion_details, unparsable_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    LOGGER.info(f"expected_table_ingestion_details: {expected_table_ingestion_details}")
    assert not expected_table_ingestion_details.keys()
    assert len(unparsable_avsc.keys()) == 1
    assert expected_result == unparsable_avsc


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test__get_expected_ingestion_details_error_not_a_json():
    csv_files = [
        "a_file_001.csv"
    ]
    avro_files = [
        "datalake/some_bucket/error_not.a_json.avsc"
    ]
    expected_result = {
        "datalake/some_bucket/error_not.a_json.avsc":
            "Error parsing file datalake/some_bucket/error_not.a_json.avsc. Are you sure it is a json?"
    }
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    expected_table_ingestion_details, unparsable_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    LOGGER.info(f"expected_table_ingestion_details: {expected_table_ingestion_details}")
    assert not expected_table_ingestion_details.keys()
    assert len(unparsable_avsc.keys()) == 1
    assert expected_result == unparsable_avsc


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test__get_expected_ingestion_details_error_skip_header_line_not_integer():
    csv_files = [
        "a_file_001.csv"
    ]
    avro_files = [
        "datalake/some_bucket/error_not.an_integer.avsc"
    ]
    expected_result = {
        "datalake/some_bucket/error_not.an_integer.avsc":
            "Provided avro schema lacks the number of header rows to be skipped,"
            " field called \"skip_header_line_count\" is not an integer."
    }
    source_s3 = S3Hook(aws_conn_id="aws_minio")
    expected_table_ingestion_details, unparsable_avsc = \
        _get_expected_ingestion_details(source_s3, avro_files, csv_files)
    LOGGER.info(f"unparsable_avsc: {unparsable_avsc}")
    assert not expected_table_ingestion_details.keys()
    assert len(unparsable_avsc.keys()) == 1
    assert expected_result == unparsable_avsc


def test_get_table_partitions_as_strings_happy_path():
    table_name = "static_info_some_fake_data_dude"
    current_table_partitions = {
        "static_info_some_fake_data_dude": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "university_001"
                }
            },
            "columns": [
                "owning_company"
            ]
        },
        "static_info_table_001": None
    }
    expected_result = [
        "owning_company=company_1",
        "owning_company=company_ABC",
        "owning_company=university_001"
    ]
    partitions_full_strings = get_table_partitions_as_strings(table_name, current_table_partitions)
    assert partitions_full_strings is not None
    assert expected_result == partitions_full_strings


def test_get_table_partitions_as_strings_complex_happy_path():
    table_name = "static_info_some_fake_data_dude"
    current_table_partitions = {
        "static_info_some_fake_data_dude": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "company_ABC",
                    3: "TWAIN_CONSORTIUM"
                },
                "owner": {
                    0: "first_person_from_company_1",
                    1: "first_person_from_company_ABC",
                    2: "second_person_from_company_1",
                    3: "TWAIN_CONSORTIUM_MEMBERS"
                }
            },
            "columns": [
                "owning_company",
                "owner"
            ]
        }
    }
    expected_result = [
        "owning_company=company_1/owner=first_person_from_company_1",
        "owning_company=company_ABC/owner=first_person_from_company_ABC",
        "owning_company=company_ABC/owner=second_person_from_company_1",
        "owning_company=TWAIN_CONSORTIUM/owner=TWAIN_CONSORTIUM_MEMBERS"
    ]
    partitions_full_strings = get_table_partitions_as_strings(table_name, current_table_partitions)
    assert partitions_full_strings is not None
    assert expected_result == partitions_full_strings


def test_get_table_partitions_as_strings_empty_table():
    table_name = "static_info_table_001"
    current_table_partitions = {
        "static_info_some_fake_data_dude": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "university_001"
                }
            },
            "columns": [
                "owning_company"
            ]
        },
        "static_info_table_001": None
    }
    expected_result = []
    partitions_full_strings = get_table_partitions_as_strings(table_name, current_table_partitions)
    assert partitions_full_strings is None


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test_build_sync_partitions_happy_path():
    avsc_files = [
        "happy_path.avsc",
        "happy_path.second_file.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    expected_result = [
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_001', 'DROP')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_001', 'ADD')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_002', 'DROP')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_002', 'ADD')",
    ]
    sql_list = build_sync_partitions.function(avsc_files, ti=tio)
    assert 4 == len(sql_list)
    assert expected_result == sql_list
    assert "build_sync_partitions" == tio.xcom_pull("ingestion_stage")


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
def test_build_sync_partitions_happy_path_3rd_file():
    avsc_files = [
        "happy_path.third_file.avsc",
        "happy_path.second_file.avsc",
        "happy_path.avsc"
    ]
    tio = TaskInstanceOverwrite(init_key="ingestion_stage")
    expected_result = [
        "CALL system.sync_partition_metadata('default', 'static_info_upper_table_name', 'DROP')",
        "CALL system.sync_partition_metadata('default', 'static_info_upper_table_name', 'ADD')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_002', 'DROP')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_002', 'ADD')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_001', 'DROP')",
        "CALL system.sync_partition_metadata('default', 'static_info_table_name_001', 'ADD')",
    ]
    sql_list = build_sync_partitions.function(avsc_files, ti=tio)
    assert 6 == len(sql_list)
    assert expected_result == sql_list
    assert "build_sync_partitions" == tio.xcom_pull("ingestion_stage")


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_happy_path_empty_table(mock_tm_get_tables_partitions):
    table_name = "mock_table"
    parquet_file_path_partition = list()
    mock_tm_get_tables_partitions.return_value = None
    expected_result = (False, list())
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_tm_get_tables_partitions.called


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_happy_path_table_already_exists(mock_tm_get_tables_partitions):
    table_name = "mock_table"
    parquet_file_path_partition = list()
    mock_tm_get_tables_partitions.return_value = {
        "mock_table": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "university_001"
                }
            },
            "columns": [
                "owning_company"
            ]
        }
    }
    expected_result = (False, list())
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_tm_get_tables_partitions.called


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_happy_path_table_already_exists_parquet_exists(mock_tm_get_tables_partitions):
    table_name = "mock_table"
    parquet_file_path_partition = [
        "mock_table/owning_company=university_001/3c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=university_222/4c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=other_company/4c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_tm_get_tables_partitions.return_value = {
        "mock_table": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "university_001",
                    3: "university_222"
                }
            },
            "columns": [
                "owning_company"
            ]
        }
    }
    expected_result = (
        True,
        [
            "mock_table/owning_company=university_001/3c172511b28e4149accb36df16b34be5-0.parquet",
            "mock_table/owning_company=university_222/4c172511b28e4149accb36df16b34be5-0.parquet"
        ]
    )
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_tm_get_tables_partitions.called


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_happy_path_complex_table_already_exists_parquet_exists(mock_tm_get_tables_partitions):
    table_name = "mock_table"
    parquet_file_path_partition = [
        "mock_table/owning_company=university_001/owner=person_1/3c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=university_222/owner=person_2/4c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=other_company/owner=person_3/5c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_tm_get_tables_partitions.return_value = {
        "mock_table": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "company_ABC",
                    3: "university_001",
                    4: "university_222"
                },
                "owner": {
                    0: "person_ABC",
                    1: "person_ABC",
                    2: "person_ABC",
                    3: "person_ABC",
                    4: "person_2"
                }
            },
            "columns": [
                "owning_company",
                "owner"
            ]
        }
    }
    expected_result = (
        True,
        [
            "mock_table/owning_company=university_222/owner=person_2/4c172511b28e4149accb36df16b34be5-0.parquet"
        ]
    )
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_tm_get_tables_partitions.called


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_missing_matches_complex_table_already_exists_parquet_exists(mock_tm_get_tables_partitions):
    table_name = "mock_table"
    parquet_file_path_partition = [
        "mock_table/owning_company=university_001/owner=person_1/3c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=university_222/owner=person_2/4c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=other_company/owner=person_3/5c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_tm_get_tables_partitions.return_value = {
        "mock_table": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "company_ABC",
                    3: "university_001",
                    4: "university_222"
                },
                "owner": {
                    0: "person_ABC",
                    1: "person_ABC",
                    2: "person_ABC",
                    3: "person_ABC",
                    4: "person_3"
                }
            },
            "columns": [
                "owning_company",
                "owner"
            ]
        }
    }
    expected_result = (False, [])
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_tm_get_tables_partitions.called


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.static_data_tasks.get_table_partitions_as_strings")
@patch("utils.trino_manager.TrinoManager.get_tables_partitions")
def test_infer_ingestion_status_happy_path_get_table_partitions_as_strings_overwritten(
        mock_tm_get_tables_partitions,
        mock_get_table_partitions_as_strings
):
    table_name = "mock_table"
    parquet_file_path_partition = [
        "mock_table/owning_company=university_001/3c172511b28e4149accb36df16b34be5-0.parquet",
        "mock_table/owning_company=university_222/4c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_tm_get_tables_partitions.return_value = {
        "mock_table": {
            "data": {
                "owning_company": {
                    0: "company_1",
                    1: "company_ABC",
                    2: "university_001",
                    3: "university_222"
                }
            },
            "columns": [
                "owning_company"
            ]
        }
    }
    mock_get_table_partitions_as_strings.return_value = []
    expected_result = (False, [])
    result = infer_ingestion_status(table_name, parquet_file_path_partition)
    assert expected_result == result
    assert mock_get_table_partitions_as_strings.called
    assert mock_tm_get_tables_partitions.called


fmc_build_create_table_1 = S3FileMockContent("", None, None, False)


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_happy_path(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open
):
    csv_files = [
        "a_file_002.csv",
        "a_file_001.csv"
    ]
    avsc_files = [
        "datalake/some_bucket/happy_path.avsc",
        "datalake/some_bucket/happy_path.second_file.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/static_info_some_fake_data_dude/owning_company=company_C/a8f6807dbd0e46a5b095ed454f7b3f2a-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    expected_result = [
        "\nCREATE TABLE IF NOT EXISTS hive.default.static_info_table_name_001 (\n    a_sample_field VARCHAR,\n"
          "ro_access_companies VARCHAR,\nfile_owner VARCHAR,\nowning_company VARCHAR\n)\n\n\n    "
          "WITH (\n        external_location = 's3a://datalake/static_info_table_name_001/',\n        "
          "format = 'PARQUET',\n        "
          "partitioned_by = ARRAY['owning_company']\n    )\n    ",
        "\nCREATE TABLE IF NOT EXISTS hive.default.static_info_table_name_002 (\n    a_sample_field VARCHAR,\n"
          "ro_access_companies VARCHAR,\nfile_owner VARCHAR,\nowning_company VARCHAR\n)\n\n\n    "
          "WITH (\n        external_location = 's3a://datalake/static_info_table_name_002/',\n        "
          "format = 'PARQUET',\n        partitioned_by = ARRAY['owning_company']\n    )\n    "
    ]
    with patch("os.remove", mock_remove_function):
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    LOGGER.info(f"sql_list: {sql_list}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 2 == len(sql_list)
    assert expected_result == sql_list


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_happy_path_other_tables(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open
):
    csv_files = [
        "a_file_ZZZ.csv",
        "a_file_AAA.csv"
    ]
    avsc_files = [
        "datalake/some_bucket/happy_path.avsc",
        "datalake/some_bucket/happy_path.fourth_file.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/static_info_some_fake_data_dude/owning_company=company_C/a8f6807dbd0e46a5b095ed454f7b3f2a-0.parquet",
        "/tmp/static_info_some_fake_data_dude/owning_company=company_C/b1f6807dbd0e46a5b095ed454f7b3f2a-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    expected_result = [
        "\nCREATE TABLE IF NOT EXISTS hive.default.static_info_table_name_001 (\n    a_sample_field VARCHAR,\n"
          "ro_access_companies VARCHAR,\nfile_owner VARCHAR,\nowning_company VARCHAR\n)\n\n\n    "
          "WITH (\n        external_location = 's3a://datalake/static_info_table_name_001/',\n        "
          "format = 'PARQUET',\n        "
          "partitioned_by = ARRAY['owning_company']\n    )\n    ",
        "\nCREATE TABLE IF NOT EXISTS hive.default.static_info_upper_table_name_again (\n    a_sample_field VARCHAR,\n"
          "    datetime TIMESTAMP(6) COMMENT 'a comment about THE Column',\n"
          "ro_access_companies VARCHAR,\nfile_owner VARCHAR,\nowning_company VARCHAR\n)\n\n\n    "
          "WITH (\n        external_location = 's3a://datalake/static_info_upper_table_name_again/',\n        "
          "format = 'PARQUET',\n        partitioned_by = ARRAY['owning_company']\n    )\n    "
    ]
    with patch("os.remove", mock_remove_function):
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    LOGGER.info(f"sql_list: {sql_list}")
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 2 == len(sql_list)
    assert expected_result == sql_list
    assert "a_file_AAA.csv" == \
           file_pair_ingestion_details["datalake/some_bucket/happy_path.avsc"]["paired_csv_date_file"]
    assert "a_file_ZZZ.csv" == \
           file_pair_ingestion_details["datalake/some_bucket/happy_path.fourth_file.avsc"]["paired_csv_date_file"]


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_one_twain_custom_exception_error(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open
):
    csv_files = [
        "a_file_ZZZ.csv"
    ]
    avsc_files = [
        "datalake/some_bucket/error_no_name.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/static_info_some_fake_data_dude/owning_company=company_C/a8f6807dbd0e46a5b095ed454f7b3f2a-0.parquet",
        "/tmp/static_info_some_fake_data_dude/owning_company=company_C/b1f6807dbd0e46a5b095ed454f7b3f2a-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    expected_result = []
    sql_list = []
    with patch("os.remove", mock_remove_function), pytest.raises(Exception) as e_info:
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    if e_info:
        LOGGER.warning(f"Exception thrown of type : {type(e_info)}; content: {e_info}, "
                       f"type function result: {e_info.type}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 0 == len(sql_list)
    assert 0 == len(file_pair_ingestion_details)
    assert expected_result == sql_list
    assert "<class 'utils.exceptions.TwainCustomException'>" == str(e_info.type)
    assert "Parsing error" in str(e_info.value)


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.infer_ingestion_status")
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_first_file_ok_second_throws_twain_custom_exception_error(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open,
        mock_infer_ingestion_status
):
    """
    Test checks if a specific scenario of ingesting 2 file pairs works well:
       - the first pair of files is OK and places parquet files in already existing table;
       - the second pair of files has an error in avsc file.
    As result we want to see that the first file was ingested and the second is not, the process should throw
    a TwainCustomException
    :param mock_csv_processor_parquet_data_from_dframe:
    :param mock_csv_processor_load_csv_with_schema:
    :param mock_embedded_open:
    :param mock_infer_ingestion_status:
    :return:
    """
    csv_files = [
        "a_file_ZZZ.csv",
        "a_file_AAA.csv"
    ]
    avsc_files = [
        "datalake/2nd_bucket/lack_of_name.avsc",
        "datalake/1st_bucket/happy_path.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
        "/tmp/table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    mock_infer_ingestion_status.return_value = (
        True,
        [
            "static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
            "static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
        ]
    )
    expected_result = []
    expected_file_pair_ingestion_details = {
        "datalake/1st_bucket/happy_path.avsc": {
            "paired_csv_date_file": "a_file_AAA.csv",
            "expected_table_name": "static_info_table_name_001",
            "csv_to_parquet_map":  {
                "a_file_AAA.csv": [
                    "static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
                    "static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
                ]
            }
        }
    }
    expected_incorrect_avsc = {
        "datalake/2nd_bucket/lack_of_name.avsc":
            "Error parsing file datalake/2nd_bucket/lack_of_name.avsc. "
            "Missing parameter 'name' responsible for containing the target table name."
    }
    sql_list = []
    with patch("os.remove", mock_remove_function), pytest.raises(Exception) as e_info:
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    incorrect_avsc = tio.xcom_pull("incorrect_avsc")
    LOGGER.info(f"incorrect_avsc: {incorrect_avsc}")
    if e_info:
        LOGGER.warning(f"Exception thrown of type : {type(e_info)}; content: {e_info}, "
                       f"type function result: {e_info.type}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 0 == len(sql_list)
    assert 1 == len(file_pair_ingestion_details)
    assert 1 == len(incorrect_avsc)
    assert expected_result == sql_list
    assert expected_file_pair_ingestion_details == file_pair_ingestion_details
    assert expected_incorrect_avsc == incorrect_avsc
    assert "<class 'utils.exceptions.TwainCustomException'>" == str(e_info.type)
    assert "Parsing error" in str(e_info.value)


@patch.dict("os.environ", AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict("os.environ", AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict("os.environ", AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.infer_ingestion_status")
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_first_file_throws_twain_custom_exception_error_second_ok(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open,
        mock_infer_ingestion_status
):
    """
    Test checks if a specific scenario of ingesting 2 file pairs works well:
       - the first pair of files has an error in avsc file.
       - the second pair of files is OK and places parquet files in already existing table;
    As result we want to see that no file was ingested properly, the process should throw a TwainCustomException
    :param mock_csv_processor_parquet_data_from_dframe:
    :param mock_csv_processor_load_csv_with_schema:
    :param mock_embedded_open:
    :param mock_infer_ingestion_status:
    :return:
    """
    csv_files = [
        "a_file_ZZZ.csv",
        "a_file_AAA.csv"
    ]
    avsc_files = [
        "datalake/1st_bucket/lack_of_name.avsc",
        "datalake/2nd_bucket/happy_path.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
        "/tmp/table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    mock_infer_ingestion_status.return_value = (
        True,
        [
            "static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
            "static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
        ]
    )
    expected_result = []
    expected_file_pair_ingestion_details = {
        "datalake/2nd_bucket/happy_path.avsc": {
            "csv_to_parquet_map": {},
            "expected_table_name": "static_info_table_name_001",
            "paired_csv_date_file": "a_file_ZZZ.csv"
        }
    }
    expected_incorrect_avsc = {
        "datalake/1st_bucket/lack_of_name.avsc":
            "Error parsing file datalake/1st_bucket/lack_of_name.avsc. "
            "Missing parameter 'name' responsible for containing the target table name."
    }
    sql_list = []
    with patch("os.remove", mock_remove_function), pytest.raises(Exception) as e_info:
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    incorrect_avsc = tio.xcom_pull("incorrect_avsc")
    LOGGER.info(f"incorrect_avsc: {incorrect_avsc}")
    if e_info:
        LOGGER.warning(f"Exception thrown of type : {type(e_info)}; content: {e_info}, "
                       f"type function result: {e_info.type}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 0 == len(sql_list)
    assert 1 == len(file_pair_ingestion_details)
    assert 1 == len(incorrect_avsc)
    assert expected_result == sql_list
    assert expected_file_pair_ingestion_details == file_pair_ingestion_details
    assert expected_incorrect_avsc == incorrect_avsc
    assert "<class 'utils.exceptions.TwainCustomException'>" == str(e_info.type)
    assert "Parsing error" in str(e_info.value)


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", fmc_build_create_table_1.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_random_exception(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open
):
    csv_files = [
        "a_file_002.csv",
        "a_file_001.csv"
    ]
    avsc_files = [
        "datalake/some_bucket/happy_path.avsc",
        "datalake/some_bucket/happy_path.second_file.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/trigger_value_exception"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    expected_result = []
    expected_file_pair_ingestion_details = {
        "datalake/some_bucket/happy_path.avsc": {
            "csv_to_parquet_map": {},
            "expected_table_name": "static_info_table_name_001",
            "paired_csv_date_file": "a_file_001.csv"
        },
        "datalake/some_bucket/happy_path.second_file.avsc": {
            "csv_to_parquet_map": {},
            "expected_table_name": "static_info_table_name_002",
            "paired_csv_date_file": "a_file_002.csv"
        }
    }
    sql_list = []
    with patch("os.remove", mock_remove_function), pytest.raises(ValueError) as e_info:
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    LOGGER.info(f"sql_list: {sql_list}")
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 0 == len(sql_list)
    assert 2 == len(file_pair_ingestion_details)
    assert expected_result == sql_list
    assert expected_file_pair_ingestion_details == file_pair_ingestion_details
    assert "Mocked value error" == str(e_info.value)


value_error_thrower = S3FileMockLoadBytesWthMemory(3)


@patch.object(S3Hook, "read_key", s3_read_key_overwrite)
@patch.object(S3Hook, "load_bytes", value_error_thrower.append_or_replace_bites_data)
@patch("src.dags.src.common.tasks.static_data_tasks.infer_ingestion_status")
@patch("src.dags.src.common.tasks.static_data_tasks.open")
@patch("src.dags.src.common.tasks.static_data_tasks.load_csv_with_schema")
@patch("src.dags.src.common.tasks.static_data_tasks.parquet_data_from_dframe")
def test_build_create_table_random_exception_while_processing_second_file(
        mock_csv_processor_parquet_data_from_dframe,
        mock_csv_processor_load_csv_with_schema,
        mock_embedded_open,
        mock_infer_ingestion_status
):
    csv_files = [
        "a_file_002.csv",
        "a_file_001.csv"
    ]
    avsc_files = [
        "datalake/1st_dir/happy_path.avsc",
        "datalake/2nd_dir/happy_path.avsc"
    ]
    sub_bucket = "company_A"
    tio = TaskInstanceOverwrite(init_key="some_empty_key")
    mock_csv_processor_parquet_data_from_dframe.return_value = [
        "/tmp/static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
        "/tmp/static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
    ]
    mock_csv_processor_load_csv_with_schema.return_value = None
    mock_embedded_open.return_value = MockRead()
    mock_infer_ingestion_status.return_value = (
        True,
        [
            "static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
            "static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
        ]
    )

    expected_result = []
    expected_file_pair_ingestion_details = {
        "datalake/1st_dir/happy_path.avsc": {
            "csv_to_parquet_map": {
                "a_file_001.csv": [
                    "static_info_table_name_001/owning_company=university_222/a1c172511b28e4149accb36df16b34be5-0.parquet",
                    "static_info_table_name_001/owning_company=university_222/a2c172511b28e4149accb36df16b34be5-0.parquet"
                ]
            },
            "expected_table_name": "static_info_table_name_001",
            "paired_csv_date_file": "a_file_001.csv"
        },
        "datalake/2nd_dir/happy_path.avsc": {
            "csv_to_parquet_map": {},
            "expected_table_name": "static_info_table_name_001",
            "paired_csv_date_file": "a_file_002.csv"
        }
    }
    sql_list = []
    with patch("os.remove", mock_remove_function), pytest.raises(ValueError) as e_info:
        sql_list = build_create_table.function(avsc_files, csv_files, sub_bucket, ti=tio)
    LOGGER.info(f"sql_list: {sql_list}")
    file_pair_ingestion_details = tio.xcom_pull("file_pair_ingestion_details")
    LOGGER.info(f"file_pair_ingestion_details: {file_pair_ingestion_details}")
    assert "build_create_table" == tio.xcom_pull("ingestion_stage")
    assert 0 == len(sql_list)
    assert 2 == len(file_pair_ingestion_details)
    assert expected_result == sql_list
    assert expected_file_pair_ingestion_details == file_pair_ingestion_details
    assert "Mocked value error" == str(e_info.value)
