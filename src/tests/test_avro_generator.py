import os
import re
import sys
import logging
from unittest.mock import Mock, patch

import pandas as pd
from pytest import fixture, raises

from ..avro_generator import (
    AvroType,
    cast_to_datetime,
    csv_to_avro,
    detect_type,
    find_headerline,
    find_separator,
    sanitize_name,
    cast_to_float,
    _guess_format,
    all_equal
)

LOGGER = logging.getLogger("py_test_avro_generator")
logging.basicConfig(format="%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S",
                    level="INFO", stream=sys.stdout)


@fixture
def current_dir():
    return os.path.dirname(os.path.abspath(__file__))


@fixture(scope="function")
def df_datetimes(current_dir):
    filename = os.path.join(current_dir, "data", "datetimes.csv")

    return pd.read_csv(filename, sep=";")


@fixture
def df_datetimes_path(current_dir):
    return os.path.join(current_dir, "data", "datetimes.csv")


@fixture(scope="function")
def df_types(current_dir):
    filename = os.path.join(current_dir, "data", "types.csv")
    return pd.read_csv(filename, sep=";")


@fixture(scope="function")
def df_types_wrong_values(current_dir):
    filename = os.path.join(current_dir, "data", "types.wrong_values.csv")
    return pd.read_csv(filename, sep=";")


@fixture
def df_separator(current_dir):
    return os.path.join(current_dir, "data", "separator.csv")


@fixture
def df_no_separator(current_dir):
    return os.path.join(current_dir, "data", "no_separator.csv")


@fixture
def df_separator_with_quotes(current_dir):
    return os.path.join(current_dir, "data", "separator_with_quotes.csv")


@fixture
def df_separator_without_quotes_in_last_line(current_dir):
    return os.path.join(current_dir, "data", "separator_without_quotes_in_last_line.csv")


class MatchAlwaysFalse(object):
    def match(self, input_string):
        return False


def compile_mock(input_arg):
    return MatchAlwaysFalse()


def test_sanitize_name():
    names = [
        "Quick Brown (garbage text) Fox  Jumps-Over the. Lazy Dog",
        "  Quick [Brown]Brown fox jumps over the    Lazy dog",
        "quick [<(>)] brown fox jumps over the lazy DOG",
    ]

    reference_name = "quick_brown_fox_jumps_over_the_lazy_dog"
    for name in names:
        transformed = sanitize_name(name, prepare_avro_name=True)
        assert transformed == reference_name


def test_sanitize_name_file_name():
    names = [
        "Quick Brown (garbage text) Fox- Jumps-Over the. Lazy Dog",
        "  Quick [Brown]Brown fox jumps over the    Lazy dog",
        "quick [<(>)] brown fox jumps over the lazy DOG",
        "     1st Panzer-Division    "
    ]

    reference_names = [
        "quick_brown_fox-_jumps-over_the_lazy_dog",
        "quick_brown_fox_jumps_over_the_lazy_dog",
        "quick_brown_fox_jumps_over_the_lazy_dog",
        "_1st_panzer-division"
    ]
    transformed = [sanitize_name(name, prepare_avro_name=False) for name in names]
    assert transformed == reference_names


def test_datetime_cast(df_datetimes):
    assert cast_to_datetime(df_datetimes["col1"]) == AvroType(
        t="datetime", extra="%Y-%m-%d"
    )

    assert cast_to_datetime(df_datetimes["col2"]) == AvroType(
        t="datetime", extra="%m/%d/%Y %H:%M"
    )

    assert cast_to_datetime(df_datetimes["col3"]) == AvroType(
        t="datetime", extra="%Y-%m-%d %H:%M:%S"
    )

    assert cast_to_datetime(df_datetimes["col4"]) in (
        AvroType(
            t="probably_datetime",
            extra="several patterns match all non-empty records, "
                  "human intervention needed to decide the format: %d/%m/%Y;%m/%d/%Y"
        ),
        AvroType(
            t="probably_datetime",
            extra="several patterns match all non-empty records, "
                  "human intervention needed to decide the format: %m/%d/%Y;%d/%m/%Y"
        )
    )

    with raises(TypeError) as e_info:
        cast_to_datetime(df_datetimes["col5"])
    assert "not a datetime" in str(e_info.value)


def test_detect_type(df_types):
    assert detect_type(df_types["int"]) == {"doc": "int", "name": "int", "type": "int"}

    assert detect_type(df_types["double"]) == {
        "doc": "double",
        "name": "double",
        "type": "double",
    }

    assert detect_type(df_types["datetime"]) == {
        "doc": "datetime",
        "name": "datetime",
        "type": "datetime",
        "parsing_pattern": "%Y-%m-%d %H:%M:%S",
        "parsing_patterns": "%Y-%m-%d %H:%M:%S",
        "logicalType": "timestamp-micros"
    }


def test_cast_to_float(df_types):
    assert AvroType(t='float') == cast_to_float(df_types['float'])


def test_cast_to_float_wrong_type(df_types_wrong_values):
    with raises(TypeError) as e_info:
        cast_to_float(df_types_wrong_values["float"])
    assert "not a float" in str(e_info.value)


def test_find_separator():
    assert find_separator(
        "a;b;c;123.4;2012/10,23;223;392;some test text"
    ) == (";", 7)


def test_find_headerline(df_separator):
    assert find_headerline(df_separator) == 5


def test_find_headerline_with_quotes(df_separator_with_quotes):
    assert find_headerline(df_separator_with_quotes) == 9


@patch.object(re, "compile", compile_mock)
def test_find_headerline_force_no_header(df_separator):
    assert find_headerline(df_separator) == 0


@patch("src.avro_generator.find_separator")
def test_find_headerline_no_separator(mock_find_separator, df_separator):
    mock_find_separator.return_value = None, 0
    assert 0 == find_headerline(df_separator)
    assert mock_find_separator.called


def test_guess_format():
    assert (None, True) == _guess_format([])


def test_csv_to_avro(df_separator):
    basename_mock = Mock()
    basename_mock.return_value = df_separator
    with patch("os.path.basename", basename_mock):
        data = csv_to_avro(df_separator)
        assert data["skip_header_line_count"] == 6


def test_csv_to_avro_too_big_file(df_separator):
    basename_mock = Mock()
    basename_mock.return_value = 100 * 1024 * 1024
    with patch("os.path.getsize", basename_mock), raises(Exception) as e_info:
        csv_to_avro(df_separator)
        assert "File size exceeds 10MB!" in str(e_info.value)
        assert basename_mock.called


def test_csv_to_avro_no_separator(df_no_separator):
    basename_mock = Mock()
    basename_mock.return_value = df_no_separator
    with patch("os.path.basename", basename_mock):
        data = csv_to_avro(df_no_separator)
        assert data["name"].endswith("no_separator")
        data["name"] = "no_separator"
        assert {
            "type": "record",
             "namespace": "dk.dtu.twain",
             "name": "no_separator",
             "doc": "",
             "format": "CSV",
             "skip_header_line_count": 1,
             "csv_separator": ",",
             "expiry_date": "",
             "data_retention": 30,
             "fields": [
              {
               "name": "comment_line_1_name",
               "type": "string",
               "doc": "# comment line 1 name"
              }
             ]
            } == data


def test_csv_to_avro_ambiguous_date(df_datetimes_path):
    basename_mock = Mock()
    basename_mock.return_value = df_datetimes_path
    with patch("os.path.basename", basename_mock):
        data = csv_to_avro(df_datetimes_path)
    assert data["csv_separator"] == ";"
    assert [
               {
                   "name": "col1",
                   "type": "datetime",
                   "doc": "col1",
                   "logicalType": "timestamp-micros",
                   "parsing_pattern": "%Y-%m-%d",
                   "parsing_patterns": "%Y-%m-%d"
               },
               {
                   "name": "col2",
                   "type": "datetime",
                   "doc": "col2",
                   "logicalType": "timestamp-micros",
                   "parsing_pattern": "%m/%d/%Y %H:%M",
                   "parsing_patterns": "%m/%d/%Y %H:%M"
               },
               {
                   "name": "col3",
                   "type": "datetime",
                   "doc": "col3",
                   "logicalType": "timestamp-micros",
                   "parsing_pattern": "%Y-%m-%d %H:%M:%S",
                   "parsing_patterns": "%Y-%m-%d %H:%M:%S"
               },
               {
                   'doc': 'col5',
                   "name": "col5",
                   "type": "string"
               }
         ] == data["fields"][:3] + [data["fields"][-1]]
    assert data["fields"][3] in [
        {
            "name": "col4",
            "type": "probably_datetime",
            "doc": "col4",
            "logicalType": "timestamp-micros",
            "parsing_pattern": "several patterns match all non-empty records, human intervention "
                               "needed to decide the format: %d/%m/%Y;%m/%d/%Y",
            "parsing_patterns": "several patterns match all non-empty records, human intervention "
                                "needed to decide the format: %d/%m/%Y;%m/%d/%Y"
        },
        {
            "name": "col4",
            "type": "probably_datetime",
            "doc": "col4",
            "logicalType": "timestamp-micros",
            "parsing_pattern": "several patterns match all non-empty records, human intervention "
                               "needed to decide the format: %m/%d/%Y;%d/%m/%Y",
            "parsing_patterns": "several patterns match all non-empty records, human intervention "
                                "needed to decide the format: %m/%d/%Y;%d/%m/%Y"
        }
    ]


def test_csv_to_avro_with_quotes(df_separator_with_quotes):
    basename_mock = Mock()
    basename_mock.return_value = df_separator_with_quotes
    with patch("os.path.basename", basename_mock):
        schema = csv_to_avro(df_separator_with_quotes)
        assert schema["skip_header_line_count"] == 10
        assert len(schema["fields"]) == 29
        assert {
            "date_and_time": "datetime",
            "some_text_i_hope_my_separatorwill_work": "string",
            "wind_speed_standard_deviation": "double",
            "wind_speed_minimum": "double",
            "wind_speed_maximum": "double",
            "wind_direction": "double",
            "nacelle_position": "int",
            "wind_direction_standard_deviation": "double",
            "wind_direction_minimum": "double",
            "wind_direction_maximum": "double",
            "nacelle_position_standard_deviation": "int",
            "nacelle_position_minimum": "int",
            "nacelle_position_maximum": "int",
            "vane_position_12": "double",
            "vane_position_12_max": "double",
            "vane_position_12_min": "double",
            "vane_position_12_stddev": "double",
            "nacelle_ambient_temperature": "double",
            "nacelle_ambient_temperature_max": "double",
            "nacelle_ambient_temperature_min": "double",
            "nacelle_ambient_temperature_stddev": "double",
            "rotor_speed": "int",
            "rotor_speed_max": "int",
            "rotor_speed_min": "int",
            "rotor_speed_standard_deviation": "int",
            "apparent_power": "int",
            "apparent_power_max": "int",
            "apparent_power_min": "double",
            "apparent_power_stddev": "int"
        } == {x['name']: x['type'] for x in schema["fields"]}


def test_csv_to_avro_without_quotes(df_separator_without_quotes_in_last_line):
    basename_mock = Mock()
    basename_mock.return_value = df_separator_without_quotes_in_last_line
    with patch("os.path.basename", basename_mock):
        schema = csv_to_avro(df_separator_without_quotes_in_last_line)
        assert schema["skip_header_line_count"] == 10
        assert len(schema["fields"]) == 29
        assert {
            "date_and_time": "datetime",
            "some_text_i_hope": "string",
            "wind_speed_standard_deviation": "double",
            "wind_speed_minimum": "string",
            "wind_speed_maximum": "double",
            "wind_direction": "double",
            "nacelle_position": "int",
            "wind_direction_standard_deviation": "double",
            "wind_direction_minimum": "double",
            "wind_direction_maximum": "double",
            "nacelle_position_standard_deviation": "int",
            "nacelle_position_minimum": "int",
            "nacelle_position_maximum": "int",
            "vane_position_12": "double",
            "vane_position_12_max": "double",
            "vane_position_12_min": "double",
            "vane_position_12_stddev": "double",
            "nacelle_ambient_temperature": "double",
            "nacelle_ambient_temperature_max": "double",
            "nacelle_ambient_temperature_min": "double",
            "nacelle_ambient_temperature_stddev": "double",
            "rotor_speed": "int",
            "rotor_speed_max": "int",
            "rotor_speed_min": "int",
            "rotor_speed_standard_deviation": "int",
            "apparent_power": "int",
            "apparent_power_max": "int",
            "apparent_power_min": "double",
            "apparent_power_stddev": "int"
        } == {x['name']: x['type'] for x in schema["fields"]}


def test_all_equal():
    assert all_equal(['a', 'a', 'a'])
    assert not all_equal(['A', 'a', 'a'])
    assert not all_equal([4, 'a', 7.0])
    assert not all_equal([4, '4', 4.0])
