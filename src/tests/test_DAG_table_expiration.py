import os
import sys
import logging
import datetime
import pytz
import trino
from unittest.mock import Mock, patch, MagicMock
from dateutil.relativedelta import relativedelta
from airflow.exceptions import AirflowException
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'dags'))
sys.modules["src.common.utils.statics"] = MagicMock()
from utils.trino_manager import TrinoManager

@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
def import_table_expiration_dags_with_mock():
    from src.dags.src.common.tasks.table_expiration_tasks import get_trino_tables_with_expiry


import_table_expiration_dags_with_mock()

LOGGER = logging.getLogger('py_test-openmetadata-automatic-connection-to-twain-trino')
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


trino_mock_arguments = {
    "trinoHost": "mock",
    "trinoPort": 1,
    "TRINO_USER": "a_user",
    "TRINO_PASSWORD": "a_password"
}


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("utils.trino_manager.TrinoManager.run_query")
def test_get_trino_tables_with_expiry(mock_tm_run_query):
    from src.dags.src.common.tasks.table_expiration_tasks import get_trino_tables_with_expiry
    mock_tm_run_query.return_value = [
        ["common_data_model", '{"expiry_date": "2023-12-16"}'],
        ["bronze_table_001", '{"expiry_date": "2023-12-16"}'],
        ["bronze_table_002", '{"expiry_date"}'],
        ["comMon_datA_model", '{"expiry_date": "2023-12-16"}'],
    ]
    with patch('src.dags.src.common.utils.statics.PROTECTED_TABLES', ['common_data_model']) as mocked_mf:
        result = get_trino_tables_with_expiry.function(**trino_mock_arguments)
    expected_tables_expiries = {
        "bronze_table_001": {
            "expiry_date": "2023-12-16"
        }
    }
    LOGGER.info(f"called mock_request_get: {mock_tm_run_query.called}")
    assert mock_tm_run_query.called
    assert expected_tables_expiries == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.DATA_EXPIRATION_DATE_PATTERN", "%Y-%m-%d")
def test_get_overdue_tables():
    from src.dags.src.common.tasks.table_expiration_tasks import get_overdue_tables
    current_day = datetime.datetime.now(tz=pytz.UTC)
    next_day = datetime.datetime.now(tz=pytz.UTC) + datetime.timedelta(days=1)
    last_year = datetime.datetime.now(tz=pytz.UTC) - relativedelta(years=1)
    input_dict = {
        "table_A": {
            "comment_text": "a text",
            "expiry_date": current_day.strftime("%Y-%m-%d")
        },
        "table_B": {
            "comment_text": "a text",
            "expiry_date": next_day.strftime("%Y-%m-%d")
        },
        "table_C": {
            "comment_text": "a text again",
            "other_key": "Some other value",
            "expiry_date": last_year.strftime("%Y-%m-%d")
        },
        "table_D": {
            "expiry_date": "Wrong text"
        },
        "table_E": {
            "expiry_date": last_year.strftime("%m-%d-%YT%H-%M-%S")
        },
        "table_F": {
            "comment_text": "a text",
            "expiry_date": last_year.strftime("%Y-%m-%d")
        },
    }
    output = get_overdue_tables.function(input_dict, LOGGER)
    expected_output = {
        "table_A": current_day.strftime("%Y-%m-%d"),
        "table_C": last_year.strftime("%Y-%m-%d"),
        "table_F": last_year.strftime("%Y-%m-%d")
    }
    assert expected_output == output


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.DATA_EXPIRATION_DATE_PATTERN", "%Y-%m-%d")
def test_split_bronze_table_from_the_rest():
    from src.dags.src.common.tasks.table_expiration_tasks import split_bronze_table_from_the_rest
    input_dict = {
        "table_A": "2021-12-10",
        "bronze_table_C": "3030-01-01",
        "table_F": "",
        "bronze_fake_data_ABC_schema": "3030-01-01",
    }
    output = split_bronze_table_from_the_rest.function(input_dict, LOGGER, 'bronze_')
    expected_output = {
        "standard_tables_to_be_expired": {
            "table_A": "2021-12-10",
            "table_F": "",
            "bronze_fake_data_ABC_schema": "3030-01-01"
        },
        "bronze_tables_to_be_expired": {
            "bronze_table_C": "3030-01-01"
        }
    }
    assert expected_output == output


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("utils.trino_manager.TrinoManager.drop_table")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_standard_tables(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_standard_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_abc": "2023-12-16"
        }
    }
    trino_mock_arguments_std_tables = trino_mock_arguments
    trino_mock_arguments_std_tables["input_dict"] = input_dict
    trino_mock_arguments_std_tables["aws_conn_id"] = aws_conn_id
    result = handle_expiration_of_standard_tables.function(**trino_mock_arguments_std_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": [],
            "failed_minio_objects": []
        },
        "standard_tables_that_were_expired": ["table_abc"]
    }
    LOGGER.info(f"called mock_request_get: {mock_tm_get_file_location.called}")
    assert mock_tm_get_file_location.called
    assert expected_tables_expiries == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES",
       ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects",
       side_effect=AirflowException(f"Errors when deleting table"))
@patch("utils.trino_manager.TrinoManager.drop_table")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_standard_tables_airflow_exception(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_standard_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_abc": "2023-12-16"
        }
    }
    trino_mock_arguments_std_tables = trino_mock_arguments
    trino_mock_arguments_std_tables["input_dict"] = input_dict
    trino_mock_arguments_std_tables["aws_conn_id"] = aws_conn_id
    result = handle_expiration_of_standard_tables.function(**trino_mock_arguments_std_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": ["table_abc"],
            "failed_minio_objects": []
        },
        "standard_tables_that_were_expired": []
    }
    LOGGER.info(f"called mock_request_get: {mock_tm_get_file_location.called}")
    assert mock_tm_get_file_location.called
    assert expected_tables_expiries == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES",
       ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("utils.trino_manager.TrinoManager.drop_table",
       side_effect=trino.exceptions.TrinoUserError({f"Errors when deleting table": None}))
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_standard_tables_trino_error(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_standard_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_abc": "2023-12-16"
        }
    }
    trino_mock_arguments_std_tables = trino_mock_arguments
    trino_mock_arguments_std_tables["input_dict"] = input_dict
    trino_mock_arguments_std_tables["aws_conn_id"] = aws_conn_id
    result = handle_expiration_of_standard_tables.function(**trino_mock_arguments_std_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": [],
            "failed_minio_objects": [[
                "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
                "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
            ]]
        },
        "standard_tables_that_were_expired": []
    }
    LOGGER.info(f"called mock_request_get: {mock_tm_get_file_location.called}")
    assert mock_tm_get_file_location.called
    assert expected_tables_expiries == result


def test_sanitize_ingestion_report_content():
    from src.dags.src.common.tasks.table_expiration_tasks import sanitize_ingestion_report_content
    result = sanitize_ingestion_report_content("")
    expected_result = [""]
    assert expected_result == result


def test_sanitize_ingestion_report_content_wildcard():
    from src.dags.src.common.tasks.table_expiration_tasks import sanitize_ingestion_report_content
    result = sanitize_ingestion_report_content("dddde,defw*,dewedwe")
    assert result is None


def test_sanitize_ingestion_report_content_standard_usage():
    from src.dags.src.common.tasks.table_expiration_tasks import sanitize_ingestion_report_content
    result = sanitize_ingestion_report_content(
        "s3://datalake/output/bronze_layer/,s3://datalake/output/consumption_layer/")
    expected_result = ["s3://datalake/output/bronze_layer/", "s3://datalake/output/consumption_layer/"]
    assert expected_result == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.check_for_wildcard_key")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.read_key")
@patch("utils.trino_manager.TrinoManager.run_query")
def test_expire_data_from_consumption_layer_happy_path(
        mock_tm_run_query,
        mock_s3hook_read_key,
        mock_s3hook_check_for_wildcard_key,
        mock_s3hook_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import expire_data_from_consumption_layer
    mock_tm_run_query.return_value = [
        ["table_001_original_file.csv.20241022122704", "a company", "a user", 1000]
    ]
    mock_s3hook_read_key.return_value = "s3://datalake/output/consumption_layer/"
    mock_s3hook_check_for_wildcard_key.return_value = True
    mock_s3hook_delete_objects.return_value = True
    tm = TrinoManager(
        host=trino_mock_arguments["trinoHost"],
        port=trino_mock_arguments["trinoPort"],
        username=trino_mock_arguments["TRINO_USER"],
        password=trino_mock_arguments["TRINO_PASSWORD"],
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    result = expire_data_from_consumption_layer(tm, s3_hook, "bronze_table_001", "bucket_with_results_name")
    expected_result = {
        "bronze_table_data_files_removed_from_consumption": [
            "table_001_original_file.csv.20241022122704"
        ],
        "bronze_table_data_files_NOT_removed_from_consumption": list()
    }
    assert mock_s3hook_delete_objects.called
    assert expected_result == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.check_for_wildcard_key")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.read_key")
@patch("utils.trino_manager.TrinoManager.run_query")
def test_expire_data_from_consumption_layer_ingestion_report_does_not_exists(
        mock_tm_run_query,
        mock_s3hook_read_key,
        mock_s3hook_check_for_wildcard_key,
        mock_s3hook_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import expire_data_from_consumption_layer
    mock_tm_run_query.return_value = [
        ["table_001_original_file.csv.20241022122704", "a company", "a user", 1000]
    ]
    mock_s3hook_read_key.return_value = "s3://datalake/output/consumption_layer/"
    mock_s3hook_check_for_wildcard_key.return_value = False
    mock_s3hook_delete_objects.return_value = True
    tm = TrinoManager(
        host=trino_mock_arguments["trinoHost"],
        port=trino_mock_arguments["trinoPort"],
        username=trino_mock_arguments["TRINO_USER"],
        password=trino_mock_arguments["TRINO_PASSWORD"],
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    result = expire_data_from_consumption_layer(tm, s3_hook, "bronze_table_001", "bucket_with_results_name")
    expected_result = {
        "bronze_table_data_files_removed_from_consumption": [],
        "bronze_table_data_files_NOT_removed_from_consumption": [
            "table_001_original_file.csv.20241022122704"
        ]
    }
    assert expected_result == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.check_for_wildcard_key")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.read_key")
@patch("utils.trino_manager.TrinoManager.run_query")
def test_expire_data_from_consumption_layer_unsanitary_ingestion_report(
        mock_tm_run_query,
        mock_s3hook_read_key,
        mock_s3hook_check_for_wildcard_key,
        mock_s3hook_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import expire_data_from_consumption_layer
    mock_tm_run_query.return_value = [
        ["table_001_original_file.csv.20241022122705", "a company", "a user", 1000]
    ]
    mock_s3hook_read_key.return_value = \
        "s3://datalake/output/consumption_layer/bronze_table_001/*,s3://datalake/output/consumption_layer/bron" \
        "ze_table_001/some_random_parquet.parquet"
    mock_s3hook_check_for_wildcard_key.return_value = True
    mock_s3hook_delete_objects.return_value = True
    tm = TrinoManager(
        host=trino_mock_arguments["trinoHost"],
        port=trino_mock_arguments["trinoPort"],
        username=trino_mock_arguments["TRINO_USER"],
        password=trino_mock_arguments["TRINO_PASSWORD"],
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    result = expire_data_from_consumption_layer(tm, s3_hook, "bronze_table_001", "bucket_with_results_name")
    expected_result = {
        "bronze_table_data_files_removed_from_consumption": [],
        "bronze_table_data_files_NOT_removed_from_consumption": [
            "table_001_original_file.csv.20241022122705"
        ]
    }
    assert mock_s3hook_read_key.called
    assert expected_result == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES", ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects",
       side_effect=AirflowException(f"Errors when deleting table"))
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.check_for_wildcard_key")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.read_key")
@patch("utils.trino_manager.TrinoManager.run_query")
def test_expire_data_from_consumption_layer_s3_delete_failed(
        mock_tm_run_query,
        mock_s3hook_read_key,
        mock_s3hook_check_for_wildcard_key,
        mock_s3hook_delete_objects
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import expire_data_from_consumption_layer
    mock_tm_run_query.return_value = [
        ["table_002_original_file.csv.20241022122706", "a company", "a user", 1000]
    ]
    mock_s3hook_read_key.return_value = \
        "s3://datalake/output/consumption_layer/bronze_table_001/bronze_table/schema.acsv," \
        "s3://datalake/output/consumption_layer/bronze_table_001/some_random_parquet.parquet"
    mock_s3hook_check_for_wildcard_key.return_value = True
    mock_s3hook_delete_objects.return_value = True
    tm = TrinoManager(
        host=trino_mock_arguments["trinoHost"],
        port=trino_mock_arguments["trinoPort"],
        username=trino_mock_arguments["TRINO_USER"],
        password=trino_mock_arguments["TRINO_PASSWORD"],
        catalog='hive',
        schema='default'
    )
    s3_hook = S3Hook(aws_conn_id=aws_conn_id, verify=False)
    result = expire_data_from_consumption_layer(tm, s3_hook, "bronze_table_002", "bucket_with_results_name")
    expected_result = {
        "bronze_table_data_files_removed_from_consumption": [],
        "bronze_table_data_files_NOT_removed_from_consumption": [
            "table_002_original_file.csv.20241022122706"
        ]
    }
    assert mock_s3hook_delete_objects.called
    assert expected_result == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES",
       ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.expire_data_from_consumption_layer")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("utils.trino_manager.TrinoManager.drop_table")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_bronze_tables_happy_path(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects,
        mock_expire_data_from_consumption_layer
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_bronze_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    mock_expire_data_from_consumption_layer.return_value = {
        "bronze_table_data_files_removed_from_consumption":
            ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
        "bronze_table_data_files_NOT_removed_from_consumption": []
    }
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_abc": "2023-12-16"
        },
        "bronze_tables_to_be_expired": {
            "table_def": "2023-12-01"
        }
    }
    trino_mock_arguments_bronze_tables = trino_mock_arguments
    trino_mock_arguments_bronze_tables["input_dict"] = input_dict
    trino_mock_arguments_bronze_tables["aws_conn_id"] = aws_conn_id
    trino_mock_arguments_bronze_tables["bucket_results"] = "bucket_with_results_name"
    result = handle_expiration_of_bronze_tables.function(**trino_mock_arguments_bronze_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": [],
            "failed_minio_objects": []
        },
        "bronze_tables_that_were_expired": ["table_def"],
        "consumption_layer_results": {
            "table_def": {
                "bronze_table_data_files_removed_from_consumption":
                    ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
                "bronze_table_data_files_NOT_removed_from_consumption": []
            }
        }
    }
    assert mock_tm_get_file_location.called
    assert mock_expire_data_from_consumption_layer.called
    assert mock_s3_delete_objects.called
    assert expected_tables_expiries == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES",
       ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.expire_data_from_consumption_layer")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects",
       side_effect=AirflowException(f"Errors when deleting table"))
@patch("utils.trino_manager.TrinoManager.drop_table")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_bronze_tables_s3_delete_failed(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects,
        mock_expire_data_from_consumption_layer
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_bronze_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    mock_expire_data_from_consumption_layer.return_value = {
        "bronze_table_data_files_removed_from_consumption":
            ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
        "bronze_table_data_files_NOT_removed_from_consumption": []
    }
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_abc": "2023-12-16"
        },
        "bronze_tables_to_be_expired": {
            "table_def": "2023-12-01"
        }
    }
    trino_mock_arguments_bronze_tables = trino_mock_arguments
    trino_mock_arguments_bronze_tables["input_dict"] = input_dict
    trino_mock_arguments_bronze_tables["aws_conn_id"] = aws_conn_id
    trino_mock_arguments_bronze_tables["bucket_results"] = "bucket_with_results_name"
    result = handle_expiration_of_bronze_tables.function(**trino_mock_arguments_bronze_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": ["table_def"],
            "failed_minio_objects": []
        },
        "bronze_tables_that_were_expired": [],
        "consumption_layer_results": {
            "table_def": {
                "bronze_table_data_files_removed_from_consumption":
                    ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
                "bronze_table_data_files_NOT_removed_from_consumption": []
            }
        }
    }
    assert mock_tm_get_file_location.called
    assert mock_expire_data_from_consumption_layer.called
    assert mock_s3_delete_objects.called
    assert expected_tables_expiries == result


@patch.dict('os.environ', AIRFLOW_VAR_TRINO_USER="a_mock_trino_user")
@patch.dict('os.environ', AIRFLOW_VAR_TRINO_PASSWORD="a_mock_trino_password")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_URL="a_mock_trino_service_url")
@patch.dict('os.environ', AIRFLOW_VAR_TWAIN_TRINO_SERVICE_PORT="a_mock_trino_service_port")
@patch("src.dags.src.common.tasks.table_expiration_tasks.PROTECTED_TABLES",
       ['common_data_model', 'common_data_model_schema'])
@patch("trino.dbapi.connect", None)
@patch("src.dags.src.common.tasks.table_expiration_tasks.expire_data_from_consumption_layer")
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.delete_objects")
@patch("utils.trino_manager.TrinoManager.drop_table",
       side_effect=trino.exceptions.TrinoUserError({f"Errors when deleting table": None}))
@patch("src.dags.src.common.tasks.table_expiration_tasks.S3Hook.list_keys")
@patch("utils.trino_manager.TrinoManager.get_file_location")
def test_handle_expiration_of_bronze_tables_trino_error(
        mock_tm_get_file_location,
        mock_s3_list_keys,
        mock_tm_drop_table,
        mock_s3_delete_objects,
        mock_expire_data_from_consumption_layer
):
    aws_conn_id = "aws_minio"
    from src.dags.src.common.tasks.table_expiration_tasks import handle_expiration_of_bronze_tables
    mock_tm_get_file_location.return_value = \
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810"
    mock_s3_list_keys.return_value = [
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
        "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
    ]
    mock_tm_drop_table.return_value = True
    mock_s3_delete_objects.return_value = None
    mock_expire_data_from_consumption_layer.return_value = {
        "bronze_table_data_files_removed_from_consumption":
            ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
        "bronze_table_data_files_NOT_removed_from_consumption": []
    }
    input_dict = {
        "standard_tables_to_be_expired": {
            "table_xyz": "2023-12-16"
        },
        "bronze_tables_to_be_expired": {
            "table_zyx": "2023-12-01"
        }
    }
    trino_mock_arguments_bronze_tables = trino_mock_arguments
    trino_mock_arguments_bronze_tables["input_dict"] = input_dict
    trino_mock_arguments_bronze_tables["aws_conn_id"] = aws_conn_id
    trino_mock_arguments_bronze_tables["bucket_results"] = "bucket_with_results_name"
    result = handle_expiration_of_bronze_tables.function(**trino_mock_arguments_bronze_tables)
    expected_tables_expiries = {
        "failed_deletions": {
            "failed_trino_tables": [],
            "failed_minio_objects": [
                [
                    "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/schema.avsc",
                    "s3://datalake/output/bronze_layer/bronze_fake_data_ps_143_test_non_static_1_no_csv_2_ok_but_fail_pod_2_outof_03_20241025100810/some_random_parquet.parquet"
                ]
            ]
        },
        "bronze_tables_that_were_expired": [],
        "consumption_layer_results": {
            "table_zyx": {
                "bronze_table_data_files_removed_from_consumption":
                    ["fake_data_ps_137_test_non_static_one_OK__second_wrONG_1_outof_02.csv.20241022122704"],
                "bronze_table_data_files_NOT_removed_from_consumption": []
            }
        }
    }
    assert mock_tm_get_file_location.called
    assert mock_expire_data_from_consumption_layer.called
    assert mock_tm_drop_table.called
    assert expected_tables_expiries == result
