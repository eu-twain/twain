import os
import sys
import logging
from unittest.mock import Mock, patch

from requests.exceptions import HTTPError
from http import HTTPStatus

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'openmetadata-automatic-connection-to-twain-trino'))

from omd_create_connection_job import _redact_passwords, _handle_omd_api, DATABASE_SERVICES_API_ENDPOINT, \
    OMDServerConnectionException, get_omd_twain_trino_service, SERVICE_CREATE_NEW, SERVICE_CREATED_MANUALLY, \
    AUTOMATIC_INGESTION_DB_SERVICE_NAME, SERVICE_ALREADY_EXISTS, INGESTION_PIPELINE_API_ENDPOINT, \
    _create_or_update_db_service, _get_current_ingestion_pipeline, _create_or_get_ingestion_pipeline, \
    _deploy_ingestion_pipeline


LOGGER = logging.getLogger('py_test-openmetadata-automatic-connection-to-twain-trino')
logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level="INFO", stream=sys.stdout)


def _mock_api_response(
        status=200,
        json_data=None,
        raise_for_status=None):
    mock_resp = Mock()
    mock_resp.raise_for_status = Mock()
    if raise_for_status:
        mock_resp.raise_for_status.side_effect = raise_for_status
    mock_resp.status_code = status
    if json_data:
        mock_resp.json = Mock(
            return_value=json_data
        )
    return mock_resp


class FakeResponseClass(object):
    def __init__(self, http_status, **kwargs):
        self.status_code = http_status

    def raise_for_status(self):
        raise HTTPError(HTTPStatus.NOT_FOUND)


def test_redact_passwords():
    json_example = {
        "name": "Billy",
        "password": "supersecret",
        "internal_password": "even_more_secret"
    }

    reference_name = {
        "name": "Billy",
        "password": "redacted",
        "internal_password": "redacted"
    }
    transformed = _redact_passwords(json_example)
    assert transformed == reference_name


def test_redact_passwords_null():
    test_cases = {
        "1": {
            "input": None,
            "output": None
        },
        "2": {
            "input": "a string",
            "output": "a string"
        },
        "3": {
            "input": {
                "data": [
                    {
                        "name": "Billy",
                        "password": "supersecret",
                        "internal_password": "even_more_secret"
                    },
                    {
                        "name": "Amy",
                        "password": "supersecret",
                        "internal_password": "even_more_secret",
                        "auth_details": {
                            "auth_method": "JWTToken",
                            "JWTToken": "very_secret_lengthily_text"
                        }
                    }
                ]
            },
            "output": {
                "data": [
                    {
                        "name": "Billy",
                        "password": "redacted",
                        "internal_password": "redacted"
                    },
                    {
                        "name": "Amy",
                        "password": "redacted",
                        "internal_password": "redacted",
                        "auth_details": {
                            "auth_method": "JWTToken",
                            "JWTToken": "redacted"
                        }
                    }
                ]
            }
        }
    }
    for test_cases_number, test_case_case in test_cases.items():
        transformed = _redact_passwords(test_case_case["input"])
        assert transformed == test_case_case["output"]


def test__redact_passwords_embedded():
    json_example = {
        "name": "Billy",
        "config": {
            "password": "supersecret",
            "internal_password": "even_more_secret",
            "host": "www.somepage.com",
            "auth_details": {
                "auth_method": "JWTToken",
                "JWTToken": "very_secret_lengthily_text"
            }
        }
    }

    reference_name = {
        "name": "Billy",
        "config": {
            "password": "redacted",
            "internal_password": "redacted",
            "host": "www.somepage.com",
            "auth_details": {
                "auth_method": "JWTToken",
                "JWTToken": "redacted"
            }
        }
    }
    transformed = _redact_passwords(json_example)
    assert transformed == reference_name


@patch('omd_create_connection_job.requests.request')
def test__handle_omd_api_successful_api_call(mock_request_get):
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER, url="http://fake", bot_token="fake_token", endpoint_url=DATABASE_SERVICES_API_ENDPOINT)
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception is None


@patch('omd_create_connection_job.requests.request',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__handle_omd_api_unauthorized_error(mock_request_get):
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER, url="http://fake", bot_token="fake_token", endpoint_url=DATABASE_SERVICES_API_ENDPOINT)
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, OMDServerConnectionException) and \
           "Invalid Openmetadata authorization token." == returned_exception.args[0]


@patch('omd_create_connection_job.requests.request',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.NOT_FOUND)))
def test__handle_omd_api_not_found_error(mock_request_get):
    url = "http://fake"
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER, url=url, bot_token="fake_token", endpoint_url=DATABASE_SERVICES_API_ENDPOINT)
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert mock_request_get.callled and returned_exception \
           and isinstance(returned_exception, OMDServerConnectionException) and \
           f"Could not connect to Openmetadata server at {url}" == returned_exception.args[0]


@patch('omd_create_connection_job.requests.request')
def test__handle_omd_api_raise_for_status_404(mock_request_get):
    LOGGER.info(f"called mock_request_get: {mock_request_get.called}")
    mock_request_get.return_value = _mock_api_response(
        status=HTTPStatus.NOT_FOUND,
        raise_for_status=HTTPError(
            "Twain Openmetadata is down",
            response=FakeResponseClass(HTTPStatus.NOT_FOUND)
        )
    )
    url = "http://fake"
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER,
            url=url,
            bot_token="fake_token",
            endpoint_url=DATABASE_SERVICES_API_ENDPOINT,
            request_custom_headers={'key': 'value'},
            request_data={'key': 'value'}
        )
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception is not None \
           and isinstance(returned_exception, OMDServerConnectionException) and \
           f"Could not connect to Openmetadata server at {url}" == returned_exception.args[0]


@patch('omd_create_connection_job.requests.request')
def test__handle_omd_api_raise_for_status_400(mock_request_get):
    LOGGER.info(f"called mock_request_get: {mock_request_get.called}")
    mock_request_get.return_value = _mock_api_response(
        status=HTTPStatus.BAD_REQUEST,
        raise_for_status=HTTPError(
            "Twain Openmetadata is down",
            response=FakeResponseClass(HTTPStatus.BAD_REQUEST)
        )
    )
    url = "http://fake"
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER,
            url=url,
            bot_token="fake_token",
            endpoint_url=DATABASE_SERVICES_API_ENDPOINT,
            request_custom_headers={'key': 'value'},
            request_data={'key': 'value'}
        )
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception is not None \
           and isinstance(returned_exception, HTTPError)


@patch('omd_create_connection_job.requests.request',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__handle_omd_api_post_with_request_data(mock_request_get):
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER, method="POST", request_data={'key': 'value'},
            url="http://fake", bot_token="fake_token",
            endpoint_url=INGESTION_PIPELINE_API_ENDPOINT)
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, OMDServerConnectionException) and \
           "Invalid Openmetadata authorization token." == returned_exception.args[0]


@patch('omd_create_connection_job.requests.request',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__handle_omd_api_get_with_request_data(mock_request_get):
    returned_exception = None
    try:
        response = _handle_omd_api(
            LOGGER, method="GET", request_data={'key': 'value'},
            url="http://fake", bot_token="fake_token",
            endpoint_url=INGESTION_PIPELINE_API_ENDPOINT)
        LOGGER.info(f"Response: {response}")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, OMDServerConnectionException) and \
           "Invalid Openmetadata authorization token." == returned_exception.args[0]


@patch('omd_create_connection_job._handle_omd_api')
def test_get_omd_twain_trino_service_no_ingestion_before(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "some_password": "admin1234"
        }
    )
    result = get_omd_twain_trino_service(LOGGER, url=url, bot_token="fake_token",)
    assert (SERVICE_CREATE_NEW, None, None) == result


@patch('omd_create_connection_job._handle_omd_api')
def test_get_omd_twain_trino_service_ingestion_done_manually_before(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "some_password": "very_secret",
            "data": [
                {
                    "id": "44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "name": "twain-trino-data-warehouse",
                    "fullyQualifiedName": "twain-trino-data-warehouse",
                    "serviceType": "Trino",
                    "description": "twain-trino-data-warehouse",
                    "connection": None,
                    "version": 0.1,
                    "updatedAt": 1732179051641,
                    "updatedBy": "twain_openmetadata_admin_user",
                    "href": "http://twain-openmetadata:8585/api/v1/services/databaseServices/44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "deleted": False
                }
            ],
            "paging": {
                "total": 1
            }
        }
    )
    result = get_omd_twain_trino_service(LOGGER, url=url, bot_token="fake_token",)
    assert (SERVICE_CREATED_MANUALLY, "44553817-4a1a-4dc5-9a11-93f3b63f1f56", "twain_openmetadata_admin_user") == result


@patch('omd_create_connection_job._handle_omd_api')
def test_get_omd_twain_trino_service_ingestion_done_automatically_before(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "some_password": "very_secret",
            "data": [
                {
                    "id": "44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "name": "twain-trino-data-warehouse",
                    "fullyQualifiedName": "twain-trino-data-warehouse",
                    "serviceType": "Trino",
                    "description": "twain-trino-data-warehouse",
                    "connection": None,
                    "version": 0.1,
                    "updatedAt": 1732179051641,
                    "updatedBy": "ingestion-bot",
                    "href": "http://twain-openmetadata:8585/api/v1/services/databaseServices/44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "deleted": False
                }
            ],
            "paging": {
                "total": 1
            }
        }
    )
    result = get_omd_twain_trino_service(LOGGER, url=url, bot_token="fake_token",)
    assert (SERVICE_ALREADY_EXISTS, "44553817-4a1a-4dc5-9a11-93f3b63f1f56", "ingestion-bot") == result


@patch('omd_create_connection_job._handle_omd_api')
def test_get_omd_twain_trino_service_ingestion_not_done_before_but_other_done(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "some_password": "very_secret",
            "data": [
                {
                    "id": "44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "name": "twain-trino-data-name-does-not-match",
                    "fullyQualifiedName": "twain-trino-data-name-does-not-match",
                    "serviceType": "Trino",
                    "description": "twain-trino-data-name-does-not-match",
                    "connection": None,
                    "version": 0.1,
                    "updatedAt": 1732179051641,
                    "updatedBy": "ingestion-bot",
                    "href": "http://twain-openmetadata:8585/api/v1/services/databaseServices/44553817-4a1a-4dc5-9a11-93f3b63f1f56",
                    "deleted": False
                }
            ],
            "paging": {
                "total": 1
            }
        }
    )
    result = get_omd_twain_trino_service(LOGGER, url=url, bot_token="fake_token")
    assert (SERVICE_CREATE_NEW, None, None) == result


@patch('omd_create_connection_job._handle_omd_api')
def test__create_or_update_db_service(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "fullyQualifiedName": "fullyQualifiedName",
            "password_secret": "secret_password_value"
        }
    )
    result = _create_or_update_db_service(LOGGER, url=url, bot_token="fake_token")
    assert ("an id", "fullyQualifiedName") == result


@patch('omd_create_connection_job._handle_omd_api')
def test__get_current_ingestion_pipeline(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "data": [
                {
                    "id": "first_id",
                    "jwttoken": "some token"
                },
                {
                    "id": "second",
                    "jwttoken": "some token"
                }
            ]
        }
    )
    result = _get_current_ingestion_pipeline(LOGGER, url, db_service_fqn="fake_fqn", bot_token="fake_token")
    assert "first_id" == result


@patch('omd_create_connection_job._handle_omd_api',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__get_current_ingestion_pipeline_error(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "data": [
                {
                    "id": "first_id",
                    "jwttoken": "some token"
                },
                {
                    "id": "second",
                    "jwttoken": "some token"
                }
            ]
        }
    )
    try:
        result = _get_current_ingestion_pipeline(LOGGER, url, db_service_fqn="fake_fqn", bot_token="fake_token")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, HTTPError)


@patch('omd_create_connection_job._handle_omd_api')
def test__create_or_get_ingestion_pipeline(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "username": "to_be_overwritten"
        }
    )
    ingestion_pipeline_id = _create_or_get_ingestion_pipeline(
        LOGGER, url, db_service_id="fake", db_service_fqn="fake", bot_token="fake token")
    assert "an id" == ingestion_pipeline_id


@patch('omd_create_connection_job._handle_omd_api',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__create_or_get_ingestion_pipeline_unauthorized_error(mock_request__handle_omd_api):
    url = "http://fake"
    returned_exception = None
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "username": "to_be_overwritten"
        }
    )
    try:
        ingestion_pipeline_id = _create_or_get_ingestion_pipeline(
            LOGGER, url, db_service_id="fake", db_service_fqn="fake", bot_token="fake token")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, HTTPError)


@patch('omd_create_connection_job._handle_omd_api',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.CONFLICT)))
@patch('omd_create_connection_job._get_current_ingestion_pipeline')
def test__create_or_get_ingestion_pipeline_conflict_error(
        mock_request__get_current_ingestion_pipeline,
        mock_request__handle_omd_api):
    url = "http://fake"
    returned_exception = None
    ingestion_pipeline_id = None
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "username": "to_be_overwritten"
        }
    )
    mock_request__get_current_ingestion_pipeline.return_value = 1
    try:
        ingestion_pipeline_id = _create_or_get_ingestion_pipeline(
            LOGGER, url, db_service_id="fake", db_service_fqn="fake", bot_token="fake token")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception is None and ingestion_pipeline_id and 1 == ingestion_pipeline_id


@patch('omd_create_connection_job._handle_omd_api')
def test__deploy_ingestion_pipeline(mock_request__handle_omd_api):
    url = "http://fake"
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "username": "to_be_overwritten",
            "code": "200"
        }
    )
    result = _deploy_ingestion_pipeline(
        LOGGER, url, ingestion_pipeline_id="fake", bot_token="fake_token")
    assert "200" == result


@patch('omd_create_connection_job._handle_omd_api',
       side_effect=HTTPError(response=FakeResponseClass(HTTPStatus.UNAUTHORIZED)))
def test__deploy_ingestion_pipeline_error(mock_request__handle_omd_api):
    url = "http://fake"
    returned_exception = None
    mock_request__handle_omd_api.return_value = _mock_api_response(
        status=HTTPStatus.OK,
        json_data={
            "id": "an id",
            "username": "to_be_overwritten",
            "code": "200"
        }
    )
    try:
        result = _deploy_ingestion_pipeline(
            LOGGER, url, ingestion_pipeline_id="fake", bot_token="fake_token")
    except Exception as e:
        returned_exception = e
        LOGGER.info(f"Exception {type(e)} caught.")
    assert returned_exception and isinstance(returned_exception, HTTPError)


"""
{
            "some_password": "admin1234",
            "data": [
                {
                    "id": "some_id",
                    "name": "OpenMetadata_dataInsight",
                    "displayName": "OpenMetadata_dataInsight",
                    "description": "OpenMetadata DataInsight Pipeline",
                    "pipelineType": "dataInsight",
                    "fullyQualifiedName": "OpenMetadata.OpenMetadata_dataInsight",
                    "sourceConfig": {
                        "config": {
                            "type": "MetadataToElasticSearch",
                            "useSSL": False,
                            "timeout": 30,
                            "batchSize": 1000,
                            "verifyCerts": False,
                            "recreateIndex": True,
                            "useAwsCredentials": False,
                            "searchIndexMappingLanguage": "EN"
                        }
                    },
                    "openMetadataServerConnection": {
                        "clusterName": "openmetadata",
                        "type": "OpenMetadata",
                        "hostPort": "http://twain-openmetadata:8585/api",
                        "authProvider": "openmetadata",
                        "verifySSL": "no-ssl",
                        "securityConfig": {
                            "jwtToken": "some_token"
                        },
                        "secretsManagerProvider": "db",
                        "secretsManagerLoader": "noop",
                        "apiVersion": "v1",
                        "includeTopics": True,
                        "includeTables": True,
                        "includeDashboards": True,
                        "includePipelines": True,
                        "includeMlModels": True,
                        "includeUsers": True,
                        "includeTeams": True,
                        "includeGlossaryTerms": True,
                        "includeTags": True,
                        "includePolicy": True,
                        "includeMessagingServices": True,
                        "enableVersionValidation": True,
                        "includeDatabaseServices": True,
                        "includePipelineServices": True,
                        "limitRecords": 1000,
                        "forceEntityOverwriting": False,
                        "storeServiceConnection": True,
                        "supportsDataInsightExtraction": True,
                        "supportsElasticSearchReindexingExtraction": True
                    },
                    "airflowConfig": {
                        "pausePipeline": False,
                        "concurrency": 1,
                        "pipelineTimezone": "UTC",
                        "retries": 0,
                        "retryDelay": 300,
                        "pipelineCatchup": False,
                        "scheduleInterval": "0 0 1/1 * *",
                        "maxActiveRuns": 1,
                        "workflowDefaultView": "tree",
                        "workflowDefaultViewOrientation": "LR"
                    },
                    "service": {
                        "id": "c5eedc2c-19d1-4018-8d6d-e2c1a6abb5a0",
                        "type": "metadataService",
                        "name": "OpenMetadata",
                        "fullyQualifiedName": "OpenMetadata",
                        "description": "Service Used for creating OpenMetadata Ingestion Pipelines.",
                        "displayName": "OpenMetadata Service",
                        "deleted": False,
                        "href": "http://twain-openmetadata:8585/api/v1/services/databaseServices/c5eedc2c-19d1-4018-8d6d-e2c1a6abb5a0"
                    },
                    "loggerLevel": "INFO",
                    "deployed": False,
                    "enabled": True,
                    "href": "http://twain-openmetadata:8585/api/v1/services/ingestionPipelines/ee75ce4d-2034-4606-803a-6391d3fa9ead",
                    "version": 0.1,
                    "updatedAt": 1732177880875,
                    "updatedBy": "admin",
                    "deleted": False,
                    "provider": "user"
                },
                {
                    "id": "277dd6bd-915b-469d-9cc5-acfe9cba783f",
                    "name": AUTOMATIC_INGESTION_DB_SERVICE_NAME,
                    "displayName": "trino_metadata_cFjZmNAG",
                    "pipelineType": "metadata",
                    "fullyQualifiedName": "twain-trino-data-warehouse_created_via_API.trino_metadata_cFjZmNAG",
                    "sourceConfig": {
                        "config": {
                            "type": "DatabaseMetadata",
                            "includeTags": False,
                            "includeViews": False,
                            "markDeletedTables": True,
                            "tableFilterPattern": {
                                "excludes": [
                                    "^avro_.*",
                                    ".*_schema$"
                                ],
                                "includes": []
                            }
                        }
                    },
                    "openMetadataServerConnection": {
                        "clusterName": "openmetadata",
                        "type": "OpenMetadata",
                        "hostPort": "http://twain-openmetadata:8585/api",
                        "authProvider": "openmetadata",
                        "verifySSL": "no-ssl",
                        "securityConfig": {
                            "jwtToken": "some_other_token"
                        },
                        "secretsManagerProvider": "db",
                        "secretsManagerLoader": "noop",
                        "apiVersion": "v1",
                        "includeTopics": True,
                        "includeTables": True,
                        "includeDashboards": True,
                        "includePipelines": True,
                        "includeMlModels": True,
                        "includeUsers": True,
                        "includeTeams": True,
                        "includeGlossaryTerms": True,
                        "includeTags": True,
                        "includePolicy": True,
                        "includeMessagingServices": True,
                        "enableVersionValidation": True,
                        "includeDatabaseServices": True,
                        "includePipelineServices": True,
                        "limitRecords": 1000,
                        "forceEntityOverwriting": False,
                        "storeServiceConnection": True,
                        "supportsDataInsightExtraction": True,
                        "supportsElasticSearchReindexingExtraction": True
                    },
                    "airflowConfig": {
                        "pausePipeline": False,
                        "concurrency": 1,
                        "pipelineTimezone": "UTC",
                        "retries": 0,
                        "retryDelay": 300,
                        "pipelineCatchup": False,
                        "scheduleInterval": "0 * * * *",
                        "maxActiveRuns": 1,
                        "workflowDefaultView": "tree",
                        "workflowDefaultViewOrientation": "LR"
                    },
                    "service": {
                        "id": "b8c8fa4c-461d-4541-b00e-b966cf2aa9a2",
                        "type": "databaseService",
                        "name": "twain-trino-data-warehouse_created_via_API",
                        "fullyQualifiedName": "twain-trino-data-warehouse_created_via_API",
                        "deleted": False,
                        "href": "http://twain-openmetadata:8585/api/v1/services/databaseServices/b8c8fa4c-461d-4541-b00e-b966cf2aa9a2"
                    },
                    "loggerLevel": "INFO",
                    "deployed": True,
                    "enabled": True,
                    "href": "http://twain-openmetadata:8585/api/v1/services/ingestionPipelines/277dd6bd-915b-469d-9cc5-acfe9cba783f",
                    "version": 0.2,
                    "updatedAt": 1732184405174,
                    "updatedBy": "ingestion-bot",
                    "changeDescription": {
                        "fieldsAdded": [],
                        "fieldsUpdated": [
                            {
                                "name": "deployed",
                                "oldValue": False,
                                "newValue": True
                            }
                        ],
                        "fieldsDeleted": [],
                        "previousVersion": 0.1
                    },
                    "deleted": False,
                    "provider": "user"
                }
            ],
            "paging": {
                "total": 2
            }
        }
"""