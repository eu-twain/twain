import json
import requests
import backoff
import psycopg2
import datetime
import logging
import os
import sys
import pytz
from requests.exceptions import HTTPError
from http import HTTPStatus
from cryptography.fernet import Fernet, MultiFernet


POSTGRES_MAX_RETRY_TIME = datetime.timedelta(minutes=10).seconds
AUTOMATIC_INGESTION_DB_SERVICE_NAME = 'twain-trino-data-warehouse'
AUTOMATIC_INGESTION_INGESTION_PIPELINE_NAME = "twain-trino-data-warehouse-ingestion"
DATABASE_SERVICES_API_ENDPOINT = "/v1/services/databaseServices"
INGESTION_PIPELINE_API_ENDPOINT = "/v1/services/ingestionPipelines"
INGESTION_PIPELINE_DEPLOYMENT_API_ENDPOINT = "/v1/services/ingestionPipelines/deploy/{id}"
SERVICE_CREATE_NEW = 'SERVICE_CREATE_NEW'
SERVICE_ALREADY_EXISTS = 'SERVICE_ALREADY_EXISTS'
SERVICE_CREATED_MANUALLY = 'SERVICE_CREATED_MANUALLY'


class OMDBotNotReadyException(Exception):
    pass


class OMDServerConnectionException(Exception):
    pass


class OMDIngestionPipelineDeploymentException(Exception):
    pass


def _redact_passwords(input_dict: dict = None):
    """
    Function redacts the password, users and jwttokens from the input dictionary replacing them with word 'redacted'.
    :param input_dict: an input dictionary or a string
    :return: a dictionary with secretive details redacted or an unaltered input string
    """
    if not input_dict:
        return None
    result_dict = dict()
    if isinstance(input_dict, str):
        return input_dict
    for key, value in input_dict.items():
        if 'password' in key or 'username' in key or 'jwttoken' in key.lower():
            result_dict[key] = 'redacted'
        elif isinstance(value, dict):
            result_dict[key] = _redact_passwords(value)
        elif isinstance(value, list):
            elements = [_redact_passwords(x) for x in value]
            result_dict[key] = elements
        else:
            result_dict[key] = value
    return result_dict


def _decrypt_bot_token(log_handle, encrypted_bot_token: str):
    fernet_key = os.getenv("FERNET_KEY")
    _fernet = MultiFernet(
        [Fernet(fernet_part.encode("utf-8")) for fernet_part in fernet_key.split(",")]
    )
    _fernet.is_encrypted = True
    if not encrypted_bot_token.startswith("fernet:"):
        return encrypted_bot_token

    a_token_with_fernet_split = encrypted_bot_token.split(":")
    timestamp, data = Fernet._get_unverified_token_data(a_token_with_fernet_split[1])
    decrypted_bot_token = _fernet._fernets[0]._decrypt_data(data, timestamp, None).decode("utf-8")
    return decrypted_bot_token


@backoff.on_exception(
    backoff.expo,
    OMDBotNotReadyException,
    max_time=POSTGRES_MAX_RETRY_TIME,
    jitter=None
)
def get_bot_token(log_handle, postgres_host: str, postgres_port: int,
                  postgres_db: str, postgres_omd_username: str,
                  postgres_omd_password: str):
    """
    Function retrieving the ingestion-bots JWT token from PostgresDB; tries to retrieve the password using exponential
    backoff; if it fails or the returned ingestion-bot details cannot be parsed it throws OMDBotNotReadyException.
    :param log_handle: an instance of a logger;
    :param postgres_host: the hostname of the server hosting Postgres db;
    :param postgres_port: the port of the server hosting Postgres DC
    :param postgres_db: the name of the Postgres db
    :param postgres_omd_username: the username credential to login into Postgres db
    :param postgres_omd_password: the password credential to login into Postgres db
    :return:
    """
    connection = psycopg2.connect(
        host=postgres_host,
        port=postgres_port,
        database=postgres_db,
        user=postgres_omd_username,
        password=postgres_omd_password)
    cursor = connection.cursor()
    rendered_query = f"select json from user_entity where name = 'ingestion-bot';"
    log_handle.info(f"Prepared sql query: \"{rendered_query}\".")
    cursor.execute(rendered_query)
    records = cursor.fetchall()
    if not records:
        if connection:
            connection.close()
        raise OMDBotNotReadyException
    bot_json = records[0][0]
    if connection:
        connection.close()
    log_handle.info(f"type of bot_json: {type(bot_json)}")
    if isinstance(bot_json, str):
        bot_json = json.loads(bot_json)
    try:
        bot_token = bot_json['authenticationMechanism']['config']['JWTToken']
    except KeyError as ke:
        raise OMDBotNotReadyException("ingestion-bot exists, but its json representation has an unexpected structure")
    bot_token = _decrypt_bot_token(log_handle, bot_token)
    return bot_token


def _handle_omd_api(log_handle, url: str, bot_token: str, endpoint_url: str,
                    method: str = "GET", request_custom_headers: dict = None, request_data: dict = None):
    """
    Function managing the API connection to Openmetadata server; raises OMDServerConnectionException when server
    is not available or the authentication is not valid.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param bot_token: the authentication token into target OMD
    :param endpoint_url: the API endpoint to be targeted by the request
    :param method: the HTTP method to be used
    :param request_custom_headers: the custom headers to be added to the request
    :param request_data: the data to be passed to API; it the method is set to "GET", the request_data is dumped
    as request parameters; it the method is set to "POST", the contents are funneled as request data
    :return: the full, unaltered API response
    """
    base_headers = {"Authorization": f"Bearer {bot_token}"}
    if request_custom_headers:
        base_headers.update(request_custom_headers)
    if (DATABASE_SERVICES_API_ENDPOINT == endpoint_url) and request_data:
        request_data = json.dumps(request_data)

    request_params = None
    if (INGESTION_PIPELINE_API_ENDPOINT == endpoint_url) and request_data:
        if "POST" == method.upper():
            request_data = json.dumps(request_data)
        elif "GET" == method.upper():
            request_params = request_data
    try:
        response = requests.request(
            method,
            f"{url}{endpoint_url}",
            headers=base_headers,
            data=request_data,
            params=request_params
        )
        response.raise_for_status()
    except HTTPError as exc:
        code = exc.response.status_code
        log_handle.error(f"Error receiving Openmetadatas API: {code}, endpoint: {endpoint_url}.")
        if code == HTTPStatus.NOT_FOUND:
            raise OMDServerConnectionException(f"Could not connect to Openmetadata server at {url}")
        if code == HTTPStatus.UNAUTHORIZED:
            raise OMDServerConnectionException(f"Invalid Openmetadata authorization token.")
        raise exc
    return response


def get_omd_twain_trino_service(log_handle, url: str, bot_token: str):
    """
    Function retrieves current state of ingestion setup on OMD server; function calls OMD API endpoint to retrieve
    already existing database services, then filters it by the name that has to perfectly match constant set in
    AUTOMATIC_INGESTION_DB_SERVICE_NAME;
    if no such database service exists, the function returns 3 element tuple where first element is the command
    to create a new instance of database service with the name from constant
    AUTOMATIC_INGESTION_DB_SERVICE_NAME and the rest of the elements are Nones;
    if the database service with name set in constant AUTOMATIC_INGESTION_DB_SERVICE_NAME already exists, then function
    checks who has created it; if the creator is not 'ingestion bot', then function returns a 3 element tuple
    where the first element is the command SERVICE_CREATED_MANUALLY, the second element is the id of the service
    and the third is the name of the user who has set this ingestion up;
    it the creator of already existing database service with name set in constant AUTOMATIC_INGESTION_DB_SERVICE_NAME
    matches name 'ingestion-bot', then the function returns 3 element tuple where the first element is the command
    SERVICE_ALREADY_EXISTS, the send is the id of the service and the third is the name of the creator.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param bot_token: the authentication token into target OMD
    :return: a tuple there first element is the command to be executed by downstream function,
             the second element is the nullable id of the database service that matches search criteria,
             the third element is nullable name of the user that created already existing database service
    """
    response = _handle_omd_api(log_handle, url, bot_token, DATABASE_SERVICES_API_ENDPOINT)
    log_handle.info(f"Retrieved from {DATABASE_SERVICES_API_ENDPOINT}: "
                    f"{json.dumps(_redact_passwords(response.json()), indent=2, ensure_ascii=False)}")
    if ('data' not in response.json()) or not response.json()['data']:
        return SERVICE_CREATE_NEW, None, None
    for service_instance in response.json()['data']:
        if AUTOMATIC_INGESTION_DB_SERVICE_NAME == service_instance['name']:
            if 'updatedBy' in service_instance.keys() and 'ingestion-bot' == service_instance['updatedBy']:
                return SERVICE_ALREADY_EXISTS, service_instance['id'], service_instance['updatedBy']
            else:
                return SERVICE_CREATED_MANUALLY, service_instance['id'], \
                       None if "updatedBy" not in service_instance.keys() else service_instance['updatedBy']
    return SERVICE_CREATE_NEW, None, None


def _create_or_update_db_service(log_handle, url: str, bot_token: str):
    """
    Function calls Openmetadata API endpoint to create a new definition of database service or update the already
    existing database service with the name matching the constant AUTOMATIC_INGESTION_DB_SERVICE_NAME.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param bot_token: the authentication token into target OMD
    :return: a 2 element tuple where first element is the id of the created or updated database service and the second
             element is the FQN (fully qualified name) of said service.
    """
    trino_admin_username = os.getenv("AIRFLOW_TRINO_USER")
    trino_admin_password = os.getenv("AIRFLOW_TRINO_PASSWORD")
    create_or_update_database_services_data = {
        "name": AUTOMATIC_INGESTION_DB_SERVICE_NAME,
        "serviceType": "Trino",
        "connection": {
            "config": {
                "type": "Trino",
                "scheme": "trino",
                "username": f"{trino_admin_username}",
                "authType": {
                    "password": f"{trino_admin_password}",
                },
                "hostPort": "twain-trino:8443",
                "catalog": "hive",
                "databaseSchema": "default",
                "connectionOptions": {
                    "source": "twain_metadata_airflow",
                    "verify": "false"
                },
                "supportsMetadataExtraction": True,
                "supportsUsageExtraction": True,
                "supportsLineageExtraction": True,
                "supportsDBTExtraction": True,
                "supportsProfiler": True,
                "supportsDatabase": True,
                "supportsQueryComment": True
            }
        }
    }
    response = _handle_omd_api(log_handle, url, bot_token, DATABASE_SERVICES_API_ENDPOINT,
                               method="PUT", request_custom_headers={'Content-Type': 'application/json'},
                               request_data=create_or_update_database_services_data)
    log_handle.info(f"Retrieved from database services API: "
                    f"{json.dumps(_redact_passwords(response.json()), indent=2, ensure_ascii=False)}")
    return response.json()['id'], response.json()['fullyQualifiedName']


def _get_current_ingestion_pipeline(log_handle, url: str, db_service_fqn: str, bot_token: str):
    """
    Functions calls Openmetadata API endpoint in order to return the ID of the first ingestion pipeline that
    belongs to database service identified by its fully qualified name.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param db_service_fqn: the fully qualified name of the database service for which
           the ingestion pipelines have to be listed
    :param bot_token: the authentication token into target OMD
    :return: the ID of the first already existing ingestion pipeline
    """
    log_handle.info("Getting currently existing ingestion pipeline.")
    get_ingestion_pipeline_data = {
        "service": db_service_fqn,
        "pipelineType": "metadata"
    }
    try:
        response = _handle_omd_api(log_handle, url, bot_token, INGESTION_PIPELINE_API_ENDPOINT,
                                   method="GET", request_custom_headers={'Content-Type': 'application/json'},
                                   request_data=get_ingestion_pipeline_data)
        log_handle.info(f"Retrieved currently existing ingestion pipelines via API: "
                        f"{json.dumps(_redact_passwords(response.json()), indent=2, ensure_ascii=False)}")
    except HTTPError as http_error:
        code = http_error.response.status_code
        log_handle.error(f"Error receiving Openmetadatas ingestion pipelines: {code}")
        raise http_error
    return response.json()["data"][0]["id"]


def _create_or_get_ingestion_pipeline(log_handle, url: str, db_service_id: str, db_service_fqn: str, bot_token: str):
    """
    Function creates a new instance of ingestion pipeline for already existing database service identified by
    its ID and fully qualified name. The new ingestion pipeline will have the name specified in constant
    AUTOMATIC_INGESTION_INGESTION_PIPELINE_NAME.
    If ingestion pipeline with matching name already exists for the database service, the function calls
    _get_current_ingestion_pipeline to retrieve its id.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param db_service_id: the ID of the database service for which the ingestion pipelines have to be listed
    :param db_service_fqn: the fully qualified name of the database service for which
           the ingestion pipelines have to be listed
    :param bot_token: the authentication token into target OMD
    :return: the ID of created ingestion pipeline or the ID of the first already existing ingestion pipeline
    """
    create_ingestion_pipeline_data = {
        "airflowConfig": {
            "scheduleInterval": "0 * * * *"
        },
        "loggerLevel": "INFO",
        "name": AUTOMATIC_INGESTION_INGESTION_PIPELINE_NAME,
        "displayName": AUTOMATIC_INGESTION_INGESTION_PIPELINE_NAME,
        "pipelineType": "metadata",
        "service": {
            "id": db_service_id,
            "type": "databaseService"
        },
        "sourceConfig": {
            "config": {
                "includeViews": False,
                "includeTags": False,
                "markDeletedTables": True,
                "tableFilterPattern": {"excludes": ["^avro_.*", ".*_schema$"], "includes": []},
                "type": "DatabaseMetadata"
            }
        }
    }
    try:
        response = _handle_omd_api(log_handle, url, bot_token, INGESTION_PIPELINE_API_ENDPOINT,
                                   method="POST", request_custom_headers={'Content-Type': 'application/json'},
                                   request_data=create_ingestion_pipeline_data)
        log_handle.info(f"Retrieved from ingestion pipelines API: "
                        f"{json.dumps(_redact_passwords(response.json()), indent=2, ensure_ascii=False)}")
    except HTTPError as http_error:
        code = http_error.response.status_code
        if code == HTTPStatus.CONFLICT:
            log_handle.error(f"Error setting Openmetadatas ingestion pipeline (ip): {code},"
                             f" it means ip already exists.")
            return _get_current_ingestion_pipeline(log_handle, url, db_service_fqn, bot_token)
        else:
            log_handle.error(f"Error receiving Openmetadatas ingestion pipelines: {code}")
            raise http_error
    return response.json()["id"]


def _deploy_ingestion_pipeline(log_handle, url: str, ingestion_pipeline_id: str, bot_token: str):
    """
    Function calls Openmetadata API endpoint that triggers deployment of the ingestion pipelines into target scheduler.
    :param log_handle: an instance of a logger;
    :param url: the url of target OMD
    :param ingestion_pipeline_id: the ID of the ingestion pipeline to be deployed on the target scheduler
    :param bot_token: the authentication token into target OMD
    :return: the HTTP code of the response
    """
    try:
        response = _handle_omd_api(
            log_handle,
            url,
            bot_token,
            INGESTION_PIPELINE_DEPLOYMENT_API_ENDPOINT.format(id=ingestion_pipeline_id),
            method="POST",
            request_custom_headers={'Content-Type': 'application/json'})
        log_handle.info(f"Retrieved from ingestion pipeline deployment API: "
                        f"{json.dumps(_redact_passwords(response.json()), indent=2, ensure_ascii=False)}")
    except HTTPError as http_error:
        code = http_error.response.status_code
        log_handle.error(f"Error receiving Openmetadatas ingestion pipelines: {code}")
        raise http_error
    return response.json()["code"]


def run(log):
    """
    Main function f the job setting the connection between Twains Openmetadata and Twains Trino via calling a series of
    Openmetadata API endpoints.
    :param log:
    :return:
    """
    version = '1.6.1'
    log.info("Started to create the connection and ingestion jobs between twain-openmetadata and twain-trino")
    log.info(f"Version: {version}")
    postgres_host = os.getenv("OMD_POSTGRES_DB_HOST")
    postgres_port = int(os.getenv("OMD_POSTGRES_DB_PORT"))
    postgres_db = os.getenv("OMD_POSTGRES_DB_NAME")
    postgres_omd_username = os.getenv("OMD_POSTGRES_USER")
    postgres_omd_password = os.getenv("OMD_POSTGRES_PASSWORD")
    omd_server_api_url = os.getenv("OMD_SERVER_API_URL")
    try:
        bot_token = get_bot_token(log, postgres_host, postgres_port, postgres_db, postgres_omd_username, postgres_omd_password)
    except OMDBotNotReadyException as omd_error:
        log.error(f"Could not retrieve ingestion bot token, error: {str(omd_error)}")
        raise omd_error
    log.info(f"Creating or updating database service {AUTOMATIC_INGESTION_DB_SERVICE_NAME}")
    try:
        command, omd_twain_trino_service_id, current_omd_twain_trino_service_owner = \
            get_omd_twain_trino_service(log, omd_server_api_url, bot_token)
    except OMDServerConnectionException as omd_server_error:
        log.error(f"OMD Server under address {omd_server_api_url} returned error: {str(OMDServerConnectionException)}")
        raise omd_server_error
    log.info(f"Returned command {command} for trino database service id \"{omd_twain_trino_service_id}\"")
    if SERVICE_CREATED_MANUALLY == command:
        log.info(f"As the database service was already created manually by admin, "
                 f"{current_omd_twain_trino_service_owner} it will not be updated by this automatic process.")
        return
    try:
        db_service_id, db_service_fqn = _create_or_update_db_service(log, omd_server_api_url, bot_token)
    except OMDServerConnectionException as omd_server_error:
        log.error(f"OMD Server under address {omd_server_api_url} returned error: {str(OMDServerConnectionException)}")
        raise omd_server_error
    log.info(f"Created or Updated service with ID: {db_service_id} and fully qualified name {db_service_fqn}.")

    log.info(f"Creating or getting already existing ingestion pipeline {AUTOMATIC_INGESTION_INGESTION_PIPELINE_NAME}"
             f"for service {db_service_fqn}.")
    try:
        ingestion_pipeline_id = _create_or_get_ingestion_pipeline(
            log, omd_server_api_url, db_service_id, db_service_fqn, bot_token)
    except OMDServerConnectionException as omd_server_error:
        log.error(f"OMD Server under address {omd_server_api_url} returned error: {str(OMDServerConnectionException)}")
        raise omd_server_error
    log.info(f"Created or retrieved previously existing ingestion pipeline with ID: {ingestion_pipeline_id}")

    log.info(f"Deploying existing ingestion pipeline {ingestion_pipeline_id}")
    try:
        response_code = _deploy_ingestion_pipeline(
            log, omd_server_api_url, ingestion_pipeline_id, bot_token)
    except OMDServerConnectionException as omd_server_error:
        log.error(f"OMD Server under address {omd_server_api_url} returned error: {str(OMDServerConnectionException)}")
        raise omd_server_error
    if HTTPStatus.OK != response_code:
        error_msg = f"Deployment of ingestion pipeline {ingestion_pipeline_id} failed."
        log.error(error_msg)
        raise OMDIngestionPipelineDeploymentException(error_msg)
    log.info(f"Deployed workflow: {ingestion_pipeline_id}")

    log.info("Automatic setup finished.")


if __name__ == "__main__":
    logger = logging.getLogger('openmetadata-automatic-connection-to-twain-trino')
    logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level="INFO", stream=sys.stdout)
    start_timestamp = datetime.datetime.now(tz=pytz.UTC)
    run(logger)
