#!/usr/bin/env python
"""
Call with no arguments to get all articles owned by the token-holder.

Then call with ARTICLE_ID, which is the numerical value at the end of the URL, and
FILE_PATH to the file to upload.
"""

import hashlib
import json
import os

import requests
from requests.exceptions import HTTPError


BASE_URL = 'https://api.figshare.com/v2/{endpoint}'
# Get the article ID from initial call to get all articles owned by token-holder
ARTICLE_ID:int = 0000000
# Get the token from https://data.dtu.dk/account/applications after logging in
TOKEN:str = 'token'
CHUNK_SIZE = 1048576 # Used for calculating MD5sum only


def raw_issue_request(method, url, data=None, binary=False):
    headers = {'Authorization': 'token ' + TOKEN}
    if data is not None and not binary:
        data = json.dumps(data)
    response = requests.request(method, url, headers=headers, data=data)
    try:
        response.raise_for_status()
        try:
            data = json.loads(response.content)
        except ValueError:
            data = response.content
    except HTTPError as error:
        print('Caught an HTTPError: {}'.format(error.message))
        print('Body:\n', response.content)
        raise

    return data


def issue_request(method, endpoint, *args, **kwargs):
    return raw_issue_request(method, BASE_URL.format(endpoint=endpoint), *args, **kwargs)


def list_articles():
    result = issue_request('GET', 'account/articles')
    print('Listing current articles:')
    if result:
        for item in result:
            print(u'  {url} - {title}'.format(**item))
    else:
        print('  No articles.')
    print()

def list_files_of_article(article_id):
    result = issue_request('GET', 'account/articles/{}/files'.format(article_id))
    print('Listing files for article {}:'.format(article_id))
    if result:
        for item in result:
            print('  {id} - {name}: {download_url}'.format(**item))
    else:
        print('  No files.')

    print()


def get_file_check_data(file_name):
    with open(file_name, 'rb') as fin:
        md5 = hashlib.md5()
        size = 0
        data = fin.read(CHUNK_SIZE)
        while data:
            size += len(data)
            md5.update(data)
            data = fin.read(CHUNK_SIZE)
        return md5.hexdigest(), size


def initiate_new_upload(article_id, file_name):
    endpoint = 'account/articles/{}/files'
    endpoint = endpoint.format(article_id)

    md5, size = get_file_check_data(file_name)
    data = {'name': os.path.basename(file_name),
            'md5': md5,
            'size': size}

    result = issue_request('POST', endpoint, data=data)
    print('Initiated file upload:', result['location'], size, " bytes", '\n')

    result = raw_issue_request('GET', result['location'])

    return result


def complete_upload(article_id, file_id):
    issue_request('POST', 'account/articles/{}/files/{}'.format(article_id, file_id))


def upload_parts(file_info):
    url = '{upload_url}'.format(**file_info)
    result = raw_issue_request('GET', url)

    nparts = len(result["parts"])
    print(f'Uploading {nparts} parts:')
    with open(FILE_PATH, 'rb') as fin:
        for part in result['parts']:
            upload_part(file_info, fin, part)
    print()


def upload_part(file_info, stream, part):
    udata = file_info.copy()
    udata.update(part)
    url = '{upload_url}/{partNo}'.format(**udata)

    stream.seek(part['startOffset'])
    data = stream.read(part['endOffset'] - part['startOffset'] + 1)

    raw_issue_request('PUT', url, data=data, binary=True)
    print('  Uploaded part {partNo} from {startOffset:,} to {endOffset:,}'.format(**part))


def main():
    # Then we upload the file to requested article.
    file_info = initiate_new_upload(ARTICLE_ID, FILE_PATH)

    # Until here we used the figshare API; following lines use the figshare upload service API.
    upload_parts(file_info)

    # We return to the figshare API to complete the file upload process.
    complete_upload(ARTICLE_ID, file_info['id'])

    #list_files_of_article(ARTICLE_ID)

if __name__ == '__main__':
    import sys
    if len(sys.argv) == 1:
        list_articles()
    else:
        ARTICLE_ID = sys.argv[1]
        FILE_PATH = sys.argv[2]
        main()
