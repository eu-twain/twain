from pyspark.sql.types import TimestampType


FAKE_SSP_CSV_DATA_SAMPLE = {
    'locality': {
        'latitude': 'location_latitude',
        'longitude': 'location_longitude'
    },
    'some_embedded_field': {
        'latitude': 'location_latitude',
        'event_timestamp': {
            'search_for_column': 'event_timestamp',
            'parsing_patterns': '%d/%m/%Y;%Y/%m/%d;%Y-%m-%d %H:%M:%S.%f;%Y-%m-%d %H:%M:%S'
        }
    },
    'time_series_data': {
        'wind_speed': 'szybkość wiatru'
    },
    'time_series_data.wind_direction': {
        'wind_direction_x': 'it_blows_from_x',
        'wind_direction_y': 'it_blows_from_y'
    },
    'wind_direction': 'kierunek wiatru',
    'wind_direction_x': 'kierunek wiatru',
    'event_timestamp': {
        'search_for_column': 'event_timestamp',
        'parsing_patterns': '%d/%m/%Y;%Y/%m/%d;%Y-%m-%d %H:%M:%S.%f;%Y-%m-%d %H:%M:%S'
    },
    'name_with_dots': 'name.with.dots'
}

COMMON_DATA_SCHEMA_MAPPING = {
    'FAKE_SSP_CSV_DATA_SAMPLE': FAKE_SSP_CSV_DATA_SAMPLE
}


class DataMapping(object):
    def __init__(self, mapping_name):
        self.mapping = COMMON_DATA_SCHEMA_MAPPING[mapping_name]

    def _get_mapping(self, expected_column_name: str):
        if expected_column_name in self.mapping.keys():
            return self.mapping[expected_column_name]
        else:
            return None

    def get_mapping(self, expected_column_name: str, expected_column_type=None):
        if not expected_column_type or not isinstance(expected_column_type, TimestampType):
            return self._get_mapping(expected_column_name), None
        elif isinstance(expected_column_type, TimestampType):
            timestamp_config = self._get_mapping(expected_column_name)
            return timestamp_config['search_for_column'], timestamp_config['parsing_patterns']
        else:
            raise NotImplementedError(f'Option not implemented')

