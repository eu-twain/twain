from pyspark.sql.types import Row, StructField, DataType, StructType, StringType, DoubleType, FloatType, \
    ArrayType, StructType, TimestampType


EXPECTED_SCHEMA = StructType([
    StructField('name', StringType(), nullable=False),
    StructField('surname', StringType(), nullable=False),
    StructField('event_timestamp', TimestampType(), nullable=True),
    StructField('locality', StructType([
        StructField('latitude', DoubleType(), nullable=False),
        StructField('longitude', DoubleType(), nullable=False),
        StructField('some_embedded_field', StructType([
            StructField('latitude', DoubleType(), nullable=True),
            StructField('event_timestamp', TimestampType(), nullable=True)
        ]), nullable=True)
    ]), nullable=True),
    StructField('time_series_data', ArrayType(
        StructType([
            StructField('x', DoubleType(), nullable=True),
            StructField('y', DoubleType(), nullable=True),
            StructField('wind_speed', DoubleType(), nullable=False),
            StructField('wind_direction', StructType([
                StructField('wind_direction_x', DoubleType(), nullable=True),
                StructField('wind_direction_y', DoubleType(), nullable=True)
            ]), nullable=True)
        ]),
        containsNull=True
    ), nullable=True),
    StructField('wind_direction', StringType(), nullable=True),
    StructField('wind_direction_x', StringType(), nullable=True),
    StructField('name_with_dots', StringType(), nullable=True)

    , StructField('twain_original_file_name', StringType(), nullable=True)
    , StructField('twain_data_ro_allowed_companies', StringType(), nullable=True)
    , StructField('twain_data_owning_company', StringType(), nullable=False)
    , StructField('twain_data_owning_member', StringType(), nullable=False)
])
