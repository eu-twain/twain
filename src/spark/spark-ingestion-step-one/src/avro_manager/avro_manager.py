import logging
import json
import os.path
from pyspark.sql.session import SparkSession
import avro.schema as avro_schema
from .src.converters import FlatFieldConverter, RePatterns, GenericAVSCBuilder
from pyspark.sql.dataframe import DataFrame as pyspark_df
from pyspark.sql.types import StructType, StructField


class AvroManager(object):
    def __init__(self, source_name: str, file_name: str, file_owner: str):
        self.log = logging.getLogger(self.__class__.__name__)
        self.patterns = RePatterns()
        self.source_name = source_name
        self.file_name = file_name
        self.file_owner = file_owner
        self.output_avro_schema_path = f'output/common_data_schema/schemas/{self.source_name}/{self.file_name}.avsc'
        self._create_dir_if_not_exists(os.path.dirname(self.output_avro_schema_path))

    def _create_dir_if_not_exists(self, input_dir):
        if os.path.exists(input_dir):
            return
        try:
            os.mkdir(input_dir)
        except FileNotFoundError as fnf:
            self._create_dir_if_not_exists(os.path.dirname(input_dir))
            os.mkdir(input_dir)

    def _store_additional_info_in_schema(self, input_string: str, avro_schema_dict_for_df: StructType):
        avro_input = json.loads(input_string)
        avro_fields = dict()
        for x in avro_schema_dict_for_df.fields:
            avro_fields[x.name] = x
        self.log.info(f"avro_fields: {avro_fields}")
        for field in avro_input['fields']:
            if field['name'] in avro_fields.keys():
                avro_field = avro_fields[field['name']]  # StructField
                if avro_field.metadata and 'description' in avro_field.metadata:
                    field['doc'] = avro_field.metadata['description']

        avro_input['owner'] = str(self.file_owner)
        avro_input['retention'] = 90
        self.log.info(f"After adding comments to avro_input: {avro_input}")
        return json.dumps(avro_input)

    def compile_schema_from_spark_df(
            self,
            spark_session_object: SparkSession,
            df: pyspark_df,
            avro_schema_dict_for_df: dict,
            check_correctness=True):
        converted_avro = spark_session_object.sparkContext._jvm.org.apache.spark.sql.avro.SchemaConverters.\
            toAvroType(df._jdf.schema(), False, 'namespace', None).toString()
        self.log.info(f'converted_avro type: {type(converted_avro)}; contents: {converted_avro}')
        self.log.info(f"original avro_schema_dict_for_df: {type(avro_schema_dict_for_df)}; {avro_schema_dict_for_df}")
        converted_avro = self._store_additional_info_in_schema(converted_avro, avro_schema_dict_for_df)
        with open(self.output_avro_schema_path, 'w+', encoding='utf8') as output_file:
            output_file.write(json.dumps(json.loads(converted_avro), indent=2, ensure_ascii=False))
        if check_correctness:
            self._check_avro_schema_validity()
        return self.output_avro_schema_path, json.loads(converted_avro)

    def compile_schema_from_spark_df_old(self, df: pyspark_df, check_correctness=True):
        output = GenericAVSCBuilder(self.source_name, self.file_name).build()
        df.limit(1).write.format('avro').mode('overwrite').save('s3a://datalake/small_avro_to_check_schema')
        for column_object in df.schema.fields:
            output['fields'].append(
                FlatFieldConverter(self.patterns, column_object.name, column_object.dataType).build())
        with open(self.output_avro_schema_path, 'w+', encoding='utf8') as output_file:
            output_file.write(json.dumps(output, indent=2, ensure_ascii=False))
        if check_correctness:
            self._check_avro_schema_validity()
        return self.output_avro_schema_path

    def _check_avro_schema_validity(self):
        try:
            with open(self.output_avro_schema_path, "r") as avro_schema_file:
                schema = avro_schema.parse(avro_schema_file.read())
        except Exception as e:
            raise e
        self.log.info('Avro schema written correctly.')
