#!/usr/bin/env python

import json

# AVRO_TO_TRINO_TYPE_MAP: dict[str, str] = {
#     "boolean": "BOOLEAN",
#     "long": "BIGINT",
#     "int": "INT",
#     "float": "REAL",
#     "double": "DOUBLE",
#     "string": "VARCHAR",
#     "bytes": "VARCHAR",
#     "date": "TIMESTAMP(6)",
# }
#
# LOGICAL_TYPES_MAP: dict[str, str] = {
#     "decimal": "DECIMAL",
#     "uuid": "UUID",
#     "timestamp-millis": "TIMESTAMP(3)",
#     "timestamp-micros": "TIMESTAMP(6)",
# }

AVRO_TO_TRINO_TYPE_MAP = {
    "boolean": "BOOLEAN",
    "long": "BIGINT",
    "int": "INT",
    "float": "REAL",
    "double": "DOUBLE",
    "string": "VARCHAR",
    "bytes": "VARCHAR",
    "date": "TIMESTAMP(6)",
}

LOGICAL_TYPES_MAP = {
    "decimal": "DECIMAL",
    "uuid": "UUID",
    "timestamp-millis": "TIMESTAMP(3)",
    "timestamp-micros": "TIMESTAMP(6)",
}


CREATE_TABLE_STATEMENT = """
CREATE TABLE {table_name} (
{columns}
)
{table_comment}
"""

COLUMN_STATEMENT = "    {column_name} {column_type} {comment}"

COMMENT_STATEMENT = "COMMENT '{comment_text}'"


class UnsupportedType(Exception):
    def __init__(self, field):
        type_kind = "logicalType" if "logicalType" in field else "type"
        message = f"Unsupported {type_kind} type {field['type']} for field {field['name']}"

        self.message = message
        super().__init__(self.message)


def trino_type_from_field(field: dict) -> str:
    field_type = field["type"]
    if isinstance(field_type, list):
        for x in field_type:
            if isinstance(x, str):
                if x.lower() == 'null':
                    continue
                else:
                    field_type = x
                    break
            elif isinstance(x, dict):
                if "logicalType" in x.keys():
                    field['logicalType'] = x['logicalType']
                    field_type = x['type']
                    break
    elif isinstance(field_type, dict):
        if "logicalType" in field_type.keys():
            field['logicalType'] = field_type['logicalType']
            field_type = field_type['type']

    trino_type = AVRO_TO_TRINO_TYPE_MAP[field_type]
    if "logicalType" in field:
        trino_type = LOGICAL_TYPES_MAP[field["logicalType"]]
    return trino_type


def field_to_typed_column(field: dict) -> str:
    try:
        trino_type = trino_type_from_field(field)
        return COLUMN_STATEMENT.format(
            column_name=field['name'],
            column_type=trino_type,
            comment=COMMENT_STATEMENT.format(comment_text=field["doc"]) if "doc" in field else ""
        )
    except KeyError as e:
        raise UnsupportedType(field) from e


def field_to_str_column(field: dict) -> str:
    return COLUMN_STATEMENT.format(
        column_name=field['name'],
        column_type='VARCHAR',
        comment=COMMENT_STATEMENT.format(comment_text=field["doc"]) if "doc" in field else ""
    )


def trino_create_table_statement(
        schema: dict,
        only_str=False,
        overwrite_table_name: str = None,
        table_expiry_date_str: str = None) -> str:
    """
    Function returns a string containing  'CREATE TABLE'
    statement with corresponding types

    :param dict schema: Twain Avro~ish schema
    :param bool only_str: if set to True, the column
         types in output will be only of varchar types.
         Otherwise the types will correspond to the ones defined in schema
    :param str overwrite_table_name: An overwrite for table name
    :param str table_expiry_date_str: a string representation of table expiry date, can be None

    :return: trino SQL string
    :rtype: str

    :raises UnsupportedType: if the type provided in avro is unsupported

    """

    mapper = field_to_str_column if only_str else field_to_typed_column
    columns = [
        mapper(field)
        for field in schema['fields']
    ]
    table_comment = ""
    if table_expiry_date_str:
        table_comment = "COMMENT '" + json.dumps({'expiry_date': table_expiry_date_str}) + "'"

    return CREATE_TABLE_STATEMENT.format(
        table_name=schema["name"] if not overwrite_table_name else overwrite_table_name,
        columns=",\n".join(columns),
        table_comment=table_comment
    )


if __name__ == "__main__":
    test_avro = {
      "type": "record",
      "namespace": "dk.dtu.twain",
      "name": "static_info_la_haute_borne",
      "doc": "Static information about La Haute Borne wind farm",
      "format": "CSV",
      "csv_separator": ";",
      "skip_header_line_count": "1",
      "fields": [
        {"name": "commissioning_date", "type": "date", "doc": "Commission date", "parsing_pattern": "%Y-%m-%d"},
        {"name": "rated_power", "type": "int", "doc": "Rated power (kW)."},
        {"name": "rotor_diameter", "type": "int", "doc": "Rotor diameter (m)."},
        # { "name": "broken_field", "type": "unsupported_"},  # this should raise UnsupportedType exception
        {"name": "rotor_diameter_nullable", "type":  ["null", "double"], "doc": "Rotor diameter (m)."},
        {
            "name": "technically_correct_but_no_db_can_handle_type_alternatives",
            "type": ["null", "string", "Foo"],
            "doc": "Rotor diameter (m)."
        },
      ]
    }

    print(trino_create_table_statement(test_avro, only_str=False))

    test_avro_2 = {
       "type": "record",
       "name": "namespace",
       "fields": [
         {
           "name": "wtur_id",
           "type": "int",
           "doc": "a fake turbine  identifier 470 or something"
         },
         {
           "name": "twain_name",
           "type": "string",
           "doc": "altered name to twain name field description from airfow  minikube"
         },
         {
           "name": "surname",
           "type": [
             "string",
             "null"
           ],
           "doc": "surname of the person"
         },
         {
           "name": "location_name",
           "type": [
             "string",
             "null"
           ],
           "doc": "some comment set in file 075"
         },
         {
           "name": "grd_prod_pwr_std",
           "type": [
             "double",
             "null"
           ],
           "doc": "some comment of the columns for fake_data_ps_047_engie_csv_minikube"
         },
         {
           "name": "date",
           "type": [
             {
               "type": "long",
               "logicalType": "timestamp-micros"
             },
             "null"
           ],
           "doc": "started using datetime, a non-existing type in avro since file 076"
         },
         {
           "name": "date_nulls_first",
           "type": [
             "null",
             {
               "type": "long",
               "logicalType": "timestamp-micros"
             }
           ],
           "doc": "started using datetime, a non-existing type in avro since file 076"
         },
         {
           "name": "hcnt_avg_gen1",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "gen_rpm_avg",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "wgen_rotspd_avg",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "location_latitude",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "location_longitude",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "twain_original_file_name",
           "type": [
             "string",
             "null"
           ]
         },
         {
           "name": "twain_data_ro_allowed_companies",
           "type": [
             "string",
             "null"
           ]
         },
         {
           "name": "twain_data_owning_company",
           "type": "string"
         },
         {
           "name": "twain_data_owning_member",
           "type": "string"
         }
       ],
       "owner": "Unknown",
       "retention": 90
     }
    print(trino_create_table_statement(test_avro_2, only_str=False, overwrite_table_name='other_table_name'))
