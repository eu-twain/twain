from pyspark.sql.types import *


PYSPARK_TO_AVRO_TYPES = {
    BooleanType: 'boolean',
    IntegerType: 'int',
    LongType: 'long',
    FloatType: 'float',
    DoubleType: 'double',
    StringType: 'string',
    BinaryType: 'bytes',
    StructType: 'record',
    ArrayType: 'array',
    MapType: 'map'
}

AVRO_TO_PYSPARK_TYPES = {
    'boolean': BooleanType,
    'int': IntegerType,
    'long': LongType,
    'float': FloatType,
    'float64': FloatType,
    'double': DoubleType,
    'string': StringType,
    'bytes': BinaryType,
    'timestamp': TimestampType
}
