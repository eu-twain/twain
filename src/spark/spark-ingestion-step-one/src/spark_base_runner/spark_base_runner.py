import logging
import os
import json
from pyspark.sql import SparkSession


from ..trino_manager.trino_manager import TrinoManager
from ..config.mappings.test_schema_mappings import DataMapping
from ..minio_uploader.minio_uploader import MinioManager
from ..avro_manager.avro_manager import AvroManager

s3endPointLoc = os.getenv('MINIO_ENDPOINT_AND_PORT')      # 'http://172.29.112.1:9100'
s3accessKeyAws = os.getenv('MINIO_USER')
s3secretKeyAws = os.getenv('MINIO_PASSWORD')
trinoHost = os.getenv('TRINO_ENDPOINT_AND_PORT')          # 'https://172.29.112.1:10022'
trino_user = os.getenv('TRINO_USER')
trino_password = os.getenv('TRINO_PASSWORD')
TRINO_DEFAULT_SCHEMA = 'default'
spark_memory_allocation = int(os.getenv('ESTIMATED_MEMORY_ALLOCATION', 2)) - 1  # leaving 1GB for OS, python, etc.
COLUMN_TO_BE_REMOVED_SUFFIX = '__twain_tbr'
ERROR_RECORDS_LIMIT = 10


class SparkBaseRunner(object):
    def __init__(self):
        self.log = logging.getLogger(self.__class__.__name__)
        self.input_data_file = os.getenv('INPUT_DATA_FILE')
        self.file_owner = self._infer_file_owner()
        self.input_data_file_name = self._infer_file_name()
        self.mapping = DataMapping('FAKE_SSP_CSV_DATA_SAMPLE')
        self.bucket_name = "datalake"

        # adding some jars directly in Dockerfile into /spark-ingestion/src/java/jars:
        self.spark = SparkSession.builder.appName("Twain_ETL_step_one"). \
            config('spark.jars', '/spark-ingestion/src/java/jars/io.trino_trino-jdbc-436.jar,'
                                 '/spark-ingestion/src/java/jars/org.apache.hadoop_hadoop-aws-3.2.4.jar,'
                                 '/spark-ingestion/src/java/jars/org.apache.spark_spark-avro_2.12-3.5.0.jar,'
                                 '/spark-ingestion/src/java/jars/com.amazonaws_aws-java-sdk-bundle-1.11.901.jar,'
                                 '/spark-ingestion/src/java/jars/trino-jdbc-437.jar'
                   ).\
            config('spark.jars.packages', 'software.amazon.awssdk:s3:2.22.13'). \
            config('spark.hadoop.fs.s3a.aws.credentials.provider',
                   'org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider'). \
            config("spark.hadoop.fs.s3a.endpoint", s3endPointLoc). \
            config("spark.hadoop.fs.s3a.access.key", s3accessKeyAws). \
            config("spark.hadoop.fs.s3a.secret.key", s3secretKeyAws). \
            config("spark.hadoop.fs.s3a.path.style.access", "true"). \
            config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem"). \
            config("spark.hadoop.fs.s3a.connection.ssl.enabled", "false"). \
            config("spark.hadoop.fs.s3a.committer.magic.enabled", "true"). \
            config("spark.hadoop.fs.s3a.committer.name", "magic"). \
            config("spark.hadoop.fs.s3a.fast.upload.buffer", "bytebuffer"). \
            config("spark.hadoop.fs.s3a.buffer.dir", "/tmp/s3buffer"). \
            config("spark.sql.session.timeZone", "UTC"). \
            config("spark.sql.legacy.timeParserPolicy", "CORRECTED"). \
            config("spark.driver.memory", f"{spark_memory_allocation}g"). \
            getOrCreate()

        self.spark.sparkContext.setLogLevel('INFO')
        self.mm = MinioManager(s3endPointLoc.split('/')[-1])
        self.tm = TrinoManager(host=':'.join(trinoHost.split(':')[:-1]), port=trinoHost.split(':')[-1],
                               username=trino_user,
                               password=trino_password,
                               catalog='hive',
                               schema=TRINO_DEFAULT_SCHEMA)
        self.am = AvroManager(source_name='FAKE_DATA',
                              file_name=os.path.basename(self.input_data_file),
                              file_owner=self.file_owner)

    def _infer_file_owner(self):
        return os.path.basename(os.path.dirname(self.input_data_file))

    def _infer_file_name(self):
        return os.path.basename(self.input_data_file)

    def set_static_xcom(self):
        static_output = {
            'key1': 'value 1',
            'key2': 'value 2',
            'complex_key': {
                'subkey1': 1.0,
                'subkey2': None
            }
        }
        try:
            os.mkdir('/airflow/xcom/')
        except FileExistsError as fee:
            self.log.info('directory /airflow/xcom/ already exists, skipping creation.')
        with open('/airflow/xcom/return.json', 'w+', encoding='UTF-8') as xcom_file:
            xcom_file.write(json.dumps(static_output))
        self.log.info("Written file /airflow/xcom/return.json")
