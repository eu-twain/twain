import numpy as np
import pandas

NUMPY_TO_AVRO_TYPES = {
    np.int8: 'int',
    np.int16: 'int',
    np.int32: 'int',
    np.uint8: {'type': 'int', 'unsigned': True},
    np.uint16: {'type': 'int', 'unsigned': True},
    np.uint32: {'type': 'int', 'unsigned': True},
    np.int64: 'long',
    np.uint64: {'type': 'long', 'unsigned': True},
    np.dtype('O'): 'string',
    np.unicode_: 'string',
    np.float32: 'float',
    np.float64: 'double',
    np.datetime64: {'type': 'long', 'logicalType': 'timestamp-micros'},
    pandas.Timestamp: {'type': 'long', 'logicalType': 'timestamp-micros'},
    'float64': 'double',
    np.dtype('float64'): 'double',
    np.dtype('int64'): 'int',
    np.dtype('datetime64'): {'type': 'long', 'logicalType': 'timestamp-micros'},
    'datetime64[ns]': {'type': 'long', 'logicalType': 'timestamp-micros'},
    np.dtype('<M8[ns]'): {'type': 'long', 'logicalType': 'timestamp-micros'},
}
