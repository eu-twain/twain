class TwainCustomException(Exception):
    def __init__(self, message, details):
        super().__init__(message)
        self.details = details


INCORRECT_DATETIME_DESCRIPTION = 'Datetime pattern specified in the avro file doesn\'t ' \
                                 'match date-time format in the csv file. Please check examples below for details.'
INCORRECT_TYPE_DESCRIPTION = 'Failed to cast column {column_name} to {type}.' \
                             ' Casting would result in NULL values'
CASTED_VALUE_SUFFIX = "__casted_value"
