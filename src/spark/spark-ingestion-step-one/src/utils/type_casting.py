import datetime
from pyspark.sql.types import DataType, StringType, DoubleType, IntegerType, \
    FloatType, TimestampType, _make_type_verifier as type_verifier


def _parse_timestamp(input_value: str, accepted_patterns: str):
    if not accepted_patterns:
        return None
    for x in accepted_patterns.split(";"):
        try:
            output_value = datetime.datetime.strptime(input_value, x)
        except ValueError as ve:
            continue
        if output_value:
            return output_value
    return None


def type_caster(input_value, expected_schema_field_type: DataType, timestamp_patterns: str = None):
    if isinstance(input_value, str) and not input_value:
        output_value = None
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, StringType):
        output_value = input_value
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, FloatType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, DoubleType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, IntegerType):
        output_value = int(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, TimestampType):
        output_value = _parse_timestamp(input_value, timestamp_patterns)
    elif isinstance(input_value, str):
        output_value = float(input_value)
    else:
        output_value = input_value
    try:
        type_verifier(expected_schema_field_type)(output_value)
    except Exception as e:
        raise e
    return output_value
