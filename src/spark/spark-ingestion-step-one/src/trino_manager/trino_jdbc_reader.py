from pyspark.sql import SQLContext, Row
from pyspark.sql.readwriter import DataFrameReader
from pyspark.sql.dataframe import DataFrame


class TrinoJDBCReader(object):
    def __init__(self, spark, trino_manager, trino_host, trino_schema):
        self.spark = spark
        self.tm = trino_manager
        self.trino_host = trino_host
        self.trino_schema = trino_schema
        self.connection: DataFrameReader = self._prepare_connection()

    def _prepare_connection(self):
        sqlCont = SQLContext(self.spark)
        return sqlCont.read.format("jdbc"). \
            option("driver", "io.trino.jdbc.TrinoDriver"). \
            option("url", f"jdbc:trino://{self.trino_host.split('/')[-1]}/hive/{self.trino_schema}"). \
            option("user", self.tm.connection.auth._username). \
            option("password", self.tm.connection.auth._password). \
            option("SSL", True). \
            option("SSLVerification", "NONE"). \
            option("preferTimestampNTZ", False)

    def run_query(self, query: str) -> DataFrame:
        return self.connection.option("query", query).load()
