import datetime
import json
import logging
import os
import re
import sys
from string import Template
from typing import Dict, List

import pytz
from py4j.protocol import Py4JJavaError
from pyspark.sql.dataframe import DataFrame as spark_df
from pyspark.sql.functions import coalesce, col, lit, try_to_timestamp, when
from pyspark.sql.types import (
    DataType,
    DecimalType,
    DoubleType,
    FloatType,
    IntegerType,
    LongType,
    NumericType,
    Row,
    StringType,
    StructField,
    StructType,
    TimestampType,
    _parse_datatype_json_string,
)

from src.avro_manager.src.converters import guess_pyspark_type
from src.config.mappings.data_sharing_default_mapping import (
    DEFAULT_DATA_SHARING_AGREEMENT,
)
from src.config.technical_columns import *
from src.spark_base_runner.spark_base_runner import (
    COLUMN_TO_BE_REMOVED_SUFFIX,
    ERROR_RECORDS_LIMIT,
    TRINO_DEFAULT_SCHEMA,
    SparkBaseRunner,
    trinoHost,
)
from src.trino_manager.trino_jdbc_reader import TrinoJDBCReader

# from pyspark.sql.window import Window
from src.utils.exceptions import (
    CASTED_VALUE_SUFFIX,
    INCORRECT_DATETIME_DESCRIPTION,
    INCORRECT_TYPE_DESCRIPTION,
    TwainCustomException,
)

DATA_CONVERSION_ERROR_PATTERN = re.compile(
    r"INVALID_FORMAT\.MISMATCH_INPUT\]\s+The\sformat\sis\sinvalid:\s(\w+)\.\s+The\sinput\s\"([^\"]+)\"\s(.*?)\sdoes\snot\smatch\sthe\sformat",
    re.I | re.M | re.S,
)

FROM_CSV_TO_BRONZE = "from_csv_to_bronze"
BRONZE_LAYER_NAME = "bronze_layer"
PYTHON_TO_JAVA_MAPPING = {
    "YT": "yyyy'T'",
    "Y": "yyyy",
    "m": "M",
    "dT": "d'T'",
    "d": "d",
    "H": "H",
    "M": "m",
    "S": "s",
    "f": "SSSSSS",
    "z": "XXX",
}


class TypeCaster(object):
    def cast_column_to_type(
        self, spark_dataframe: spark_df, column_name: str, desired_type: DataType
    ):
        return spark_dataframe.withColumn(
            column_name, col(column_name).cast(desired_type)
        )

    def parse_timestamp(
        self, spark_dataframe: spark_df, column_name: str, applicable_patterns: str
    ):
        # preserve the original column value to check later if it was parsed
        original_column_name_altered = column_name + COLUMN_TO_BE_REMOVED_SUFFIX
        spark_dataframe = spark_dataframe.withColumn(
            original_column_name_altered, col(column_name)
        )
        new_column_names = list()
        java_formats = list()
        applicable_patterns = applicable_patterns.split(";")
        for index_i, x in enumerate(applicable_patterns):
            new_column_name = column_name + "_" + str(index_i)
            new_column_names.append(new_column_name)
            java_format = Template(x.replace("%", "$")).substitute(
                **PYTHON_TO_JAVA_MAPPING
            )
            java_formats.append(java_format)
            print(f"java_format for column {new_column_name}: {java_format}")
            spark_dataframe = spark_dataframe.withColumn(
                new_column_name, try_to_timestamp(col(column_name), lit(java_format))
            )
        return spark_dataframe.withColumn(
            column_name, coalesce(*[col(xyz) for xyz in new_column_names])
        ), ";".join(java_formats)


def add_technical_columns(
    input_spark_dataframe: spark_df,
    owning_company: str,
    owning_member: str,
    ro_companies: str,
):
    """
    Function enriches the input spark dataframe with a set of Twain technical columns.
    It overwrites the values of columns present in the input spark dataframe before calling the function.

    :param input_spark_dataframe: input spark dataframe
    :param owning_company: the acronym of the company owning the data
    :param owning_member: the username of the user owning the data
    :param ro_companies: the semicolon separated string of company acronyms that are allowed to browse the data
                         from consumption layer
    :return: enriched python dictionary
    """
    # file name is taken directly from trino metadata "$path" and not added here
    # the order of columns additions matters as partitions must be the last columns of the table
    input_spark_dataframe = (
        input_spark_dataframe.withColumn(
            TWAIN_DATA_RO_ALLOWED_COMPANIES, lit(ro_companies)
        )
        .withColumn(TWAIN_DATA_OWNING_COMPANY, lit(owning_company))
        .withColumn(TWAIN_DATA_OWNING_MEMBER, lit(owning_member))
    )
    return input_spark_dataframe


def create_dir_if_not_exists(input_dir):
    if os.path.exists(input_dir):
        return
    try:
        os.mkdir(input_dir)
    except FileNotFoundError as fnf:
        create_dir_if_not_exists(os.path.dirname(input_dir))
        os.mkdir(input_dir)


class SparkETL1Runner(SparkBaseRunner):
    """
    Class representing the step 1 in ETL process for Twain project.
    The goal is to move data from raw layer (where it is stored in a dummy string type) to bronze layer where
    data has correct types set up by the pseudo-avro schema.
    """

    def __init__(self):
        super().__init__()
        self.log = logging.getLogger(self.__class__.__name__)
        self.version = "2.2.0"
        self.dead_letters_bucket_name = "ingestion-results"
        self.trino_jdbc_reader = TrinoJDBCReader(
            self.spark, self.tm, trinoHost, TRINO_DEFAULT_SCHEMA
        )
        self.target_table_name = None
        self.tc = TypeCaster()
        config_text = os.getenv("INPUT_DATA_CONFIG").replace("'", '"')
        if not config_text:
            self.log.error(f"Could not find argument INPUT_DATA_CONFIG")
            raise ValueError("Could not find argument INPUT_DATA_CONFIG")
        try:
            self.input_data_config = json.loads(config_text)
        except Exception as e:
            self.log.error(f"Could not parse argument {config_text}")
            raise e
        self.csv_table_prefix = os.getenv("CSV_TABLE_PREFIX", default="csv_")
        self.avro_table_prefix = os.getenv("AVRO_TABLE_PREFIX", default="avro_")
        self.bronze_table_prefix = os.getenv("BRONZE_TABLE_PREFIX", default="bronze_")
        self.incorrect_tables = list()
        self.correct_tables = list()

    def _infer_file_owner(self):
        """
        Function not relevant for SparkETL1Runner
        :return:
        """
        return "Unknown"

    def set_target_table_name(self, table_name: str):
        self.target_table_name = table_name

    def _alter_table_name(self, input_name: str, mode: str):
        """
        Function responsible for altering the target table name between data layers.
        |  Raw data layer |  ->  |   Bronze data layer  |
        |   avro_, csv_   |      |       bronze_        |
        :param input_name:
        :param mode:
        :return:
        """
        if mode == FROM_CSV_TO_BRONZE:
            if input_name.startswith(self.csv_table_prefix):
                input_name = input_name[len(self.csv_table_prefix) :]
            return self.bronze_table_prefix + input_name
        else:
            raise NotImplementedError(
                f"Table name transformation mode {mode} not implemented."
            )

    @staticmethod
    def _treat_empty_strings_as_nulls(input_df: spark_df):
        for column_name in input_df.columns:
            input_df = input_df.withColumn(
                column_name,
                when(col(column_name) == "", None).otherwise(col(column_name)),
            )
        return input_df

    def _store_dead_letter(self, file_name: str, file_contents: str):
        """
        Function stores contents of a dead letter in MinIO's bucket as a text file.
        :param file_name: the base name of the file to be stored in MinIO's bucket.
        :param file_contents: the string representation of the contents of the dead letter file
        :return:
        """
        output_file_full_path = f"failed_ingestion_{file_name}.txt"
        local_file_path = f"output/{output_file_full_path}"
        with open(local_file_path, "w+", encoding="utf8") as output_file:
            output_file.write(file_contents)
        self.mm.put_object(
            self.dead_letters_bucket_name, output_file_full_path, local_file_path
        )

    def write_files_directly_on_s3(
        self,
        data_layer: str,
        table_name: str,
        input_df: spark_df,
        owning_company_name: str,
        owning_member: str,
        avro_schema_dict_for_df,
    ):
        """
        Function writes avro schema and parquet data files in proper places in s3 permanent storage.
        :param data_layer: the name of the data layer the file has to be stored at
        :param table_name: the name of the table
        :param input_df: the spark dataframe holding the data
        :param owning_company_name: the name of the company that owns the data
        :param owning_member: the username of the user owning the data
        :param avro_schema_dict_for_df: original schema of df, needed to pass description of fields
        :return: Tuple with first element being the s3a path where data was written and
                 the second element is the path within the container where te avro schema was generated to
        """
        file_name = os.path.basename(self.input_data_file).split(".")[-2]
        local_path = f"output/{data_layer}/{table_name}"
        local_path_avsc = f"output/{data_layer}/{table_name}/schemas/{file_name}.avsc"
        write_path_base = f"s3a://{self.bucket_name}/{local_path}"
        write_path = "/".join(
            [
                f"{write_path_base}",
                f"{TWAIN_DATA_OWNING_COMPANY}={owning_company_name}",
                f"{TWAIN_DATA_OWNING_MEMBER}={owning_member}",
            ]
        )
        output_avro_schema_path, compiled_avro_schema = (
            self.am.compile_schema_from_spark_df(
                self.spark, input_df, avro_schema_dict_for_df
            )
        )
        self.mm.put_object(self.bucket_name, local_path_avsc, output_avro_schema_path)
        input_df = self._drop_partition_columns(input_df)
        input_df.write.option("maxRecordsPerFile", 200000).parquet(
            path=write_path, mode="append"
        )
        self.log.info(f"Wrote file to {write_path}")
        return write_path_base, local_path_avsc, compiled_avro_schema

    @staticmethod
    def _drop_partition_columns(input_df: spark_df):
        return input_df.drop(*TWAIN_PARTITION_COLUMNS)

    def read_table_location(self, table_name: str):
        return self.tm.get_file_location(
            table_name=table_name, transform_file_system=False
        )

    def _procure_dead_letter(
        self, error_count: int, input_list: list[Row], datetime_fields_dict: dict
    ):
        """
        Function procures the contents of a dead letter informing user about the errors found during transformation
        of datetime columns from the original strings values from the incoming file.

        :param error_count: the number of erroneous row in the dataframe
        :param input_list: a list of Rows returned by filtering and limiting dataframe
        :param datetime_fields_dict: a dictionary which keys are names of fields that were supposed
                                     to be a timestamps in strongly typed frame and the values are a dictionary of
                                     python and java string parsing patterns
        :return: a tuple of the original file name, string content of the dead letter and same content in form
                 of a Python dictionary
        """
        results = [x.asDict() for x in input_list]
        file_name = results[0][TWAIN_ORIGINAL_FILE_NAME]
        result_only_errors = list()
        for result in results:
            error_representation = dict()
            for column_name in datetime_fields_dict.keys():
                if (
                    result[column_name + CASTED_VALUE_SUFFIX] is None
                    and result[column_name] is not None
                ):
                    error_representation[column_name] = result[column_name]
            result_only_errors.append(error_representation)
        error_rows_count_txt = (
            f"Process found {error_count} erroneous rows. "
            f"Showing sample {min(ERROR_RECORDS_LIMIT, error_count)} rows."
        )
        error_summary = dict()
        error_summary["error_description"] = INCORRECT_DATETIME_DESCRIPTION
        error_summary["error_rows_count"] = error_rows_count_txt
        error_summary["provided_column_setup"] = datetime_fields_dict
        error_summary["error_examples"] = result_only_errors
        self.log.info(error_rows_count_txt)
        dead_letter_file_content = json.dumps(
            error_summary, ensure_ascii=False, indent=4
        )
        return file_name, dead_letter_file_content, error_summary

    def _determine_datetime_cast_correctness(
        self, input_spark_dataframe: spark_df, datetime_fields_dict: dict
    ):
        """
        Function determines of the transformation of timestamp fields from strings occurred.
        If even one record was not-null in the input file and after date string transformation it
        is null, then function throws TwainCustomException.
        Function is resource heavy so do not use it for entire dataframes.
        :param input_spark_dataframe: a Spark dataframe that underwent transformation to strongly typed frame
        :param datetime_fields_dict: a dictionary which keys are names of fields that were supposed
                                     to be a timestamps in strongly typed frame and the values are a dictionary of
                                     python and java string parsing patterns
        :return: None, throws TwainCustomException
        """
        self.log.info(
            "Checking the correctness of datetime casting; proceeding with compiling a text filter"
        )
        filter_texts = list()
        for datetime_column_name in datetime_fields_dict.keys():
            filter_texts.append(
                f"({datetime_column_name} is null and "
                f"{datetime_column_name + COLUMN_TO_BE_REMOVED_SUFFIX} is not null)"
            )
        filter_text = " OR ".join(filter_texts)
        self.log.info(f"Applying filter {filter_text}")
        self.log.info(
            f"Limiting the results to {ERROR_RECORDS_LIMIT} records to not overload the worker."
        )

        """
        to count error records either use unbounded windows (which warns about performance degradation) or
        perform an action of counting the records in the dataframe, collecting result and add it back to
        limited table; went with option 2 as even though it performs 2 spark actions we avoid memory spill.
        """
        # window_spec = Window.rowsBetween(Window.unboundedPreceding, Window.unboundedFollowing)
        # result = input_spark_dataframe.where(filter_text).select([
        #     datetime_column_name + COLUMN_TO_BE_REMOVED_SUFFIX for datetime_column_name in datetime_fields_list]). \
        #     withColumn("error_rows_count",
        #                count(col(datetime_fields_list[0] + COLUMN_TO_BE_REMOVED_SUFFIX)).over(window_spec)). \
        #     limit(ERROR_RECORDS_LIMIT).collect()

        error_count = input_spark_dataframe.where(filter_text).count()
        if error_count == 0:
            self.log.info("No unparsed strings found. Proceeding with ingestion.")
            return

        selected_columns = [
            col(datetime_column_name + COLUMN_TO_BE_REMOVED_SUFFIX).alias(
                datetime_column_name
            )
            for datetime_column_name in datetime_fields_dict.keys()
        ]
        selected_columns.extend(
            [
                col(datetime_column_name).alias(
                    datetime_column_name + CASTED_VALUE_SUFFIX
                )
                for datetime_column_name in datetime_fields_dict.keys()
            ]
        )
        selected_columns.append(col(TWAIN_ORIGINAL_FILE_NAME))
        result = (
            input_spark_dataframe.where(filter_text)
            .select(selected_columns)
            .limit(ERROR_RECORDS_LIMIT)
            .collect()
        )
        if result:
            file_name, dead_letter_file_content, error_summary = (
                self._procure_dead_letter(error_count, result, datetime_fields_dict)
            )
            self.log.info(dead_letter_file_content)
            self._store_dead_letter(
                file_name=file_name, file_contents=dead_letter_file_content
            )
            raise TwainCustomException(
                message="Failed to cast at least one string into a timestamp",
                details=error_summary,
            )

    def _get_file_name_from_df(self, input_spark_dataframe: spark_df):
        selected_columns = [col(TWAIN_ORIGINAL_FILE_NAME)]
        result = (
            input_spark_dataframe
                .select(selected_columns)
                .limit(1)
                .collect()
        )
        results = [x.asDict() for x in result]
        file_name = results[0][TWAIN_ORIGINAL_FILE_NAME]
        self.log.info(f"The table {self.target_table_name} belongs to file {file_name}.")
        return file_name

    def transform_data_types(
        self,
        input_spark_dataframe: spark_df,
        avro_schema_dict: Dict,
        avro_schema_dict_for_df: StructType,
        owning_company: str,
        owning_member: str,
        ro_companies: str,
    ):
        self.log.info("Transforming data types one by one")
        datetime_fields_dict = dict()
        for avro_field in avro_schema_dict["fields"]:
            retrieved_patterns = None
            column_name = avro_field["name"]
            if "parsing_patterns" in avro_field.keys():
                retrieved_patterns = avro_field["parsing_patterns"]
            expected_data_type = guess_pyspark_type(avro_field["type"])
            self.log.info(
                f"Casting column {column_name} to type {expected_data_type}; {avro_field}"
            )
            if not isinstance(expected_data_type, TimestampType):
                self.log.info(f"Commencing cast to type {expected_data_type}")
                column_nulls = input_spark_dataframe.filter(
                    f"{column_name} is NULL"
                ).count()

                input_spark_dataframe = self.tc.cast_column_to_type(
                    input_spark_dataframe, column_name, expected_data_type
                )
                column_nulls_post_conversion = input_spark_dataframe.filter(
                    f"{column_name} is NULL"
                ).count()
                self.log.info(f"Column {column_name} has {column_nulls} nulls before conversion, "
                              f"{column_nulls_post_conversion} after conversion.")
                # if number of NULL values increased in column
                # the conversion to type has failed somewhere
                if column_nulls_post_conversion > column_nulls:
                    error_rows_count = column_nulls_post_conversion - column_nulls
                    error_summary = dict()
                    error_summary["error_description"] = INCORRECT_TYPE_DESCRIPTION.format(
                        column_name=column_name,
                        type=avro_field["type"]
                    )
                    error_summary["error_rows_count"] = error_rows_count
                    dead_letter_file_content = json.dumps(
                        error_summary, ensure_ascii=False, indent=4
                    )
                    file_name = self._get_file_name_from_df(input_spark_dataframe)
                    self._store_dead_letter(file_name=file_name, file_contents=dead_letter_file_content)
                    raise TwainCustomException(
                        message=(

                        ),
                        details={
                            "number_of_failed_rows": error_rows_count
                        },
                    )

            else:
                input_spark_dataframe, java_pattern = self.tc.parse_timestamp(
                    input_spark_dataframe, column_name, retrieved_patterns
                )
                datetime_fields_dict[column_name] = {
                    "python_timestamp_patterns": retrieved_patterns,
                    "java_timestamp_patterns": java_pattern,
                }

        # persist table in memory so the checking of correct parsers did not trigger the read from Trino again
        try:
            input_spark_dataframe.persist()
        except Exception as e:
            if isinstance(e, Py4JJavaError):
                match = DATA_CONVERSION_ERROR_PATTERN.search(
                    e.java_exception.toString()
                )
                if match:
                    details = {
                        "format_used": match.groups()[0],
                        "data": match.groups()[2],
                    }
                else:
                    details = {"exception_string": e.java_exception.toString()}
                raise TwainCustomException(
                    message="Unable to convert string to number",
                    details=details,
                )

            else:
                raise

        if datetime_fields_dict:
            self._determine_datetime_cast_correctness(
                input_spark_dataframe, datetime_fields_dict
            )
        # selecting relevant columns
        input_spark_dataframe = input_spark_dataframe.select(
            *[
                col(xyz)
                for xyz in avro_schema_dict_for_df.names
                if xyz not in TWAIN_TECHNICAL_COLUMNS
            ],
            col(TWAIN_ORIGINAL_FILE_NAME),
        )
        return add_technical_columns(
            input_spark_dataframe,
            owning_company=owning_company,
            owning_member=owning_member,
            ro_companies=ro_companies,
        )

    def process_one_raw_table(
        self,
        table_name: str,
        table_expiry_date: str,
        avro_schema_dict: Dict,
        avro_schema_dict_for_df: StructType,
    ):
        """
        Function responsible for processing one raw table into one bronze layer table.
        Raw layer table is accessed via jdbc connection to trino, data types transformation occur according to the
        avro schema associated with the raw data table; after transformations the data is brought back from
        python workers into spark dataframe using input parameter avro_schema_dict_for_df which contains the schema
        of bronze layer table in pyspark format (as a StructType).
        The result of transformations is written as a separate table into a BRONZE_LAYER_NAME with
        2 partition columns for the company owning the data and its employee uploading said data into Twain.

        :param table_name: the name of the table in raw data layer;
        :param table_expiry_date: a string representation of expiry date, can be null
        :param avro_schema_dict: a dictionary containing a psaudo-avro schema; pseudo-avro schema means avro schema
                                 enriched with Twain only data type (timestamp) and Twain technical columns
        :param avro_schema_dict_for_df: a pyspark data schema of the bronze layer table;
        :return:
        """
        self.set_target_table_name(
            self._alter_table_name(table_name, FROM_CSV_TO_BRONZE)
        )

        query = (
            f"select a.*, "
            f'substr("$path", -1*position(\'/\' in reverse("$path")) + 1) as twain_original_file_name '
            f"from {table_name} a"
        )
        input_dataframe = self.trino_jdbc_reader.run_query(query)
        input_dataframe = self._treat_empty_strings_as_nulls(input_dataframe)
        self.log.info(
            f"There are {input_dataframe.count()} records in the raw table {table_name}."
        )
        self.log.info(
            f"Raw data schema: {_parse_datatype_json_string(input_dataframe._jdf.schema().json())}"
        )
        # self.log.info(f"Raw data schema: {df._jdf.schema().json()}")
        # self.log.info(f"Raw data schema: {input_dataframe.schema}")
        # self.log.info(f"sample data from input_dataframe:")
        # self.log.info(f"{input_dataframe.show(5)}")
        self.log.info(
            f"avro_schema_dict_for_df.jsonValues: {avro_schema_dict_for_df.jsonValue()}"
        )

        owning_company = avro_schema_dict["owning_company"]
        owning_member = avro_schema_dict["owning_member"]
        ro_companies = avro_schema_dict["ro_access_companies"]

        # transformed_df = input_dataframe.rdd.map(
        #     lambda x: transformations(x, avro_schema_dict)).\
        #     map(lambda x: add_technical_columns(x,  # original_file_name=input_data_file_name,
        #                                         owning_company=owning_company,
        #                                         owning_member=owning_member,
        #                                         ro_companies=ro_companies)).\
        #     toDF(schema=avro_schema_dict_for_df).persist()
        transformed_df = self.transform_data_types(
            input_dataframe,
            avro_schema_dict,
            avro_schema_dict_for_df,
            owning_company,
            owning_member,
            ro_companies,
        ).persist()
        self.log.info(
            f"There are {transformed_df.count()} records in the transformed raw table."
        )
        self.log.info(
            f"Transformed Raw data schema: {_parse_datatype_json_string(transformed_df._jdf.schema().json())}"
        )
        # self.log.info(f"{transformed_df.schema}")
        # self.log.info(transformed_df.show())
        self.log.info(f"Commencing writing table {self.target_table_name}.")
        remote_parquet_file_path, remote_avro_schema_path, compiled_avro_schema = (
            self.write_files_directly_on_s3(
                BRONZE_LAYER_NAME,
                self.target_table_name,
                transformed_df,
                owning_company,
                owning_member,
                avro_schema_dict_for_df,
            )
        )
        self.tm.create_table_and_schema(
            self.target_table_name,
            table_expiry_date,
            remote_parquet_file_path,
            remote_avro_schema_path,
            self.bucket_name,
            avro_schema_dict=compiled_avro_schema,
        )

    def _get_and_parse_avro_schema(self, avro_schema_location: str):
        """
        Function reads the pseudo-avro schema file from s3 buckets and loads it into memory as python dictionary.
        :param avro_schema_location: the s3 path on raw data layer storage to the pseudo-avro schema prepared
                                     in step 0 of the Twain ETL
        :return:
        """
        if avro_schema_location.startswith("s3a://"):
            avro_schema_location = avro_schema_location[len("s3a://") :]
        avro_bucket = avro_schema_location.split("/")[0]
        avro_path = "/".join(avro_schema_location.split("/")[1:])
        avro_file_name = avro_schema_location.split("/")[-1]
        avro_file_local_location = os.path.join(os.getcwd(), avro_file_name)
        self.log.info(f"Reading file from bucket {avro_bucket}, path: {avro_path}.")
        self.mm.client.fget_object(avro_bucket, avro_path, avro_file_local_location)
        return json.load(open(avro_file_local_location))

    def _conform_avro_schema(self, input_schema: Dict):
        """
        Function conform the psudo-avro schema created in the step 0 of Twain ETL by ensuring existence of
        Twain technical fields; additionally it casts pseudo-avro schema into pyspark StructType.

        :param input_schema: a dictionary representation of pseudo-avro schema created in step 0 of ETL process
        :return: a Tuple with first element being the enriched and normalized pseudo-avro schema and
                 the second element is the pyspark StructType representation of the avro schema
        """
        if "owning_company" not in input_schema.keys():
            raise ValueError("Input avro schema lacks special field owning_company.")
        owning_company = input_schema["owning_company"]
        if "ro_access_companies" not in input_schema.keys():
            if owning_company not in DEFAULT_DATA_SHARING_AGREEMENT.keys():
                input_schema["ro_access_companies"] = owning_company
            else:
                input_schema["ro_access_companies"] = DEFAULT_DATA_SHARING_AGREEMENT[
                    owning_company
                ]
        if "file_owner" not in input_schema.keys():
            raise ValueError("Input avro schema lacks special field file_owner.")
        input_schema["owning_member"] = input_schema["file_owner"]

        input_schema_for_df = self._conform_avro_schema_fields(input_schema["fields"])
        return input_schema, input_schema_for_df

    @staticmethod
    def append_technical_fields_to_pyspark_schema(input_field_struct: StructType):
        """
        Function adds Twain technical fields into auto-generated pyspark schema in form of a StructType
        :param input_field_struct: an input StructType representing raw table schema
        :return:
        """
        for twain_technical_column_name in TWAIN_TECHNICAL_COLUMNS:
            input_field_struct.add(
                StructField(
                    name=twain_technical_column_name,
                    dataType=StringType(),
                    nullable=(
                        True
                        if twain_technical_column_name not in TWAIN_PARTITION_COLUMNS
                        else False
                    ),
                    metadata={"doc": "Twain technical field."},
                )
            )
        return input_field_struct

    @staticmethod
    def _conform_avro_schema_fields(input_avro_fields: List):
        """
        Function translates pseudo-avro schema into pyspark data schema in a form of a StructType object.
        :param input_avro_fields: a list of fields present in pseudo-avro schema
        :return: pyspark data schema in a form of a StructType object
        """
        output = StructType()
        for avro_field in input_avro_fields:
            if "nullable" in avro_field.keys():
                nullable = avro_field["nullable"]
            else:
                nullable = True
            if "doc" in avro_field.keys():
                metadata = {"description": avro_field["doc"]}
            else:
                metadata = None
            output.add(
                StructField(
                    name=avro_field["name"],
                    dataType=guess_pyspark_type(avro_field["type"]),
                    nullable=nullable,
                    metadata=metadata,
                )
            )
        output = SparkETL1Runner.append_technical_fields_to_pyspark_schema(output)
        return output

    def set_static_xcom(self, xcom_contents: dict):
        local_mode = json.loads((os.getenv("LOCAL_MODE", "False").lower()))
        if not local_mode:
            xcom_file_location = os.path.join("/", "airflow", "xcom")
        else:
            xcom_file_location = os.path.join(os.getcwd(), "output", "airflow", "xcom")
        try:
            os.mkdir(xcom_file_location)
        except FileExistsError as fee:
            self.log.info(
                f"directory {xcom_file_location} already exists, skipping creation."
            )
        except FileNotFoundError as fnf:
            create_dir_if_not_exists(xcom_file_location)
        xcom_file_path = os.path.join(xcom_file_location, "return.json")
        with open(xcom_file_path, "w+", encoding="UTF-8") as xcom_file:
            xcom_file.write(json.dumps(xcom_contents))
        self.log.info(f"Written file {xcom_file_path}")

    def _interpret_results(self):
        xcom_contents = dict()
        if self.incorrect_tables:
            xcom_contents["incorrect_tables"] = self.incorrect_tables
        if self.correct_tables:
            xcom_contents["correct_tables"] = self.correct_tables
        if xcom_contents.keys():
            self.set_static_xcom(xcom_contents)

    def run(self):
        """
        Main function of the step 1 of the Twain ingestion process. The processing occurs consecutively
        for each item in the configuration. Said configuration is the python dictionary which keys are the locations
        of pseudo-avro schemas in s3 raw layer storage and the values are the list of tables which are supposed to be
        transformed into the structures described in the pseudo-avro schema.
        :return:
        """
        self.log.info(f"Running ETL step 1 version {self.version}.")
        for (
            avro_schema_location,
            avro_applicable_table_configs_list,
        ) in self.input_data_config.items():
            avro_schema_dict = self._get_and_parse_avro_schema(avro_schema_location)
            self.log.info(f"avro_schema_dict: {avro_schema_dict}")
            avro_schema_dict, avro_schema_for_df = self._conform_avro_schema(
                avro_schema_dict
            )
            for table_config in avro_applicable_table_configs_list:
                table_name = table_config["csv_table_name"]
                table_expiry_date = table_config.get("csv_table_expiry_date", None)
                try:
                    self.process_one_raw_table(
                        table_name, table_expiry_date, avro_schema_dict, avro_schema_for_df
                    )
                    self.correct_tables.append(self.target_table_name)
                except Exception as e:
                    self.log.error(e)
                    self.incorrect_tables.append(self.target_table_name)
        self._interpret_results()


if __name__ == "__main__":
    logger = logging.getLogger("etl_step_1_raw2bronze")
    logging.basicConfig(
        format="%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        level="INFO",
        stream=sys.stdout,
    )
    start_timestamp = datetime.datetime.now(tz=pytz.UTC)
    try:
        runner = SparkETL1Runner()
        runner.run()
        runner.log.info(
            f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}."
        )
    except Exception as e:
        raise e
    finally:
        if "runner" in globals():
            runner.log.info(
                f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}."
            )
            runner.tm.close()
        else:
            logger.info(
                f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}."
            )
