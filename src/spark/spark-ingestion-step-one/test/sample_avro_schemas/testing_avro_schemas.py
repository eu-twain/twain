TEST_AVRO_SCHEMA_01 = {
    "namespace": "SSP",
    "type": "record",
    "name": "some_fake_data_dude",
    "fields": [
        {
            "name": "name",
            "type": "string"
        }
    ],
    "owning_company": "SSP",
    "file_owner": "some_ssp_guy"
}

TEST_AVRO_SCHEMA_02 = {
    "namespace": "SSP",
    "type": "record",
    "name": "some_fake_data_dude",
    "fields": [
        {
            "name": "name",
            "type": "string"
        },
        {
            "name": "event_timestamp",
            "type": "timestamp",
            "parsing_patterns": "%d/%m/%Y;%Y/%m/%d;%Y-%m-%d %H:%M:%S.%f;%Y-%m-%d %H:%M:%S",
            "doc": "Field represents the event timestamp"
        },
        {
            "name": "widn_direction",
            "type": "double"
        }
    ],
    "owning_company": "SSP",
    "file_owner": "some_ssp_guy"
}
