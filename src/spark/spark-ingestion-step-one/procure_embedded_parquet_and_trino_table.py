import logging
import os
import sys
import datetime
import pytz
import copy
from typing import Dict
from src.spark_base_runner.spark_base_runner import SparkBaseRunner
from pyspark.sql.types import Row, DataType, StringType, DoubleType, FloatType, TimestampType, \
    ArrayType, StructType, _make_type_verifier as type_verifier, AtomicType
from pyspark.sql.dataframe import DataFrame as spark_df
from fastparquet import write as fp_write
from src.config.common_data_schema import EXPECTED_SCHEMA
from src.config.mappings.test_schema_mappings import DataMapping
from src.config.mappings.data_sharing_default_mapping import DEFAULT_DATA_SHARING_AGREEMENT
from src.config.technical_columns import *


def _parse_timestamp(input_value: str, accepted_patterns: str):
    if not accepted_patterns:
        return None
    for x in accepted_patterns.split(";"):
        try:
            output_value = datetime.datetime.strptime(input_value, x)
        except ValueError as ve:
            continue
        if output_value:
            return output_value
    return None


def _type_caster(input_value, expected_schema_field_type: DataType, timestamp_patterns: str = None):
    if isinstance(input_value, str) and not input_value:
        output_value = None
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, StringType):
        output_value = input_value
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, FloatType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, DoubleType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, TimestampType):
        output_value = _parse_timestamp(input_value, timestamp_patterns)
    elif isinstance(input_value, str):
        output_value = float(input_value)
    else:
        output_value = input_value
    try:
        type_verifier(expected_schema_field_type)(output_value)
    except Exception as e:
        raise e
    return output_value


def transformations(input_row: Row, expected_schema, mapping: DataMapping,
                    field_name_mapping_overwrite: Dict = None, parent_column_path: str = None):
    output = {}
    input_dict = input_row.asDict()
    for x in expected_schema.fields:
        output[x.name] = None
        if isinstance(x.dataType, AtomicType):  # or isinstance(x.dataType, StructType)):
            retrieved_overwrite, retrieved_patterns = mapping.get_mapping(x.name, x.dataType)
            if field_name_mapping_overwrite and x.name in field_name_mapping_overwrite.keys():
                searched_name = field_name_mapping_overwrite[x.name]
            elif retrieved_overwrite:
                searched_name = retrieved_overwrite
            else:
                searched_name = x.name
            if searched_name in input_dict.keys():
                output[x.name] = _type_caster(input_dict[searched_name], x.dataType, retrieved_patterns)
        elif isinstance(x.dataType, StructType):
            retrieved_mapping, _ = mapping.get_mapping(
                x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            transformation_result = transformations(
                input_row,
                x.dataType,
                mapping,
                field_name_mapping_overwrite=retrieved_mapping,
                parent_column_path=x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            if not([v for v in transformation_result.values() if v is not None]) and x.nullable:
                output[x.name] = None
            else:
                output[x.name] = transformation_result
        elif isinstance(x.dataType, ArrayType):
            retrieved_mapping, _ = mapping.get_mapping(
                x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            result = transformations(
                input_row,
                x.dataType.elementType,
                mapping,
                field_name_mapping_overwrite=retrieved_mapping,
                parent_column_path=x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            not_null_values = [x for x in result.values() if x is not None]
            if not_null_values:
                output[x.name] = [result]
            else:
                output[x.name] = None
    return output


def add_technical_columns(input_dict: Dict, original_file_name: str, owning_company: str,
                          owning_member: str, ro_companies: str):
    input_dict[TWAIN_ORIGINAL_FILE_NAME] = original_file_name
    input_dict[TWAIN_DATA_OWNING_COMPANY] = owning_company
    input_dict[TWAIN_DATA_OWNING_MEMBER] = owning_member
    input_dict[TWAIN_DATA_RO_ALLOWED_COMPANIES] = ro_companies
    return input_dict


class SparkRunner(SparkBaseRunner):
    def __init__(self):
        super().__init__()
        self.log = logging.getLogger(self.__class__.__name__)

    def _sample_test(self):
        row_dict = {'name': 'd', 'surname': 'a', 'location_latitude': 12.45, 'location_longitude': 19.2399,
                    'something_else': 'dedwv', 'szybkość wiatru': '-0.23142',
                    'event_timestamp': '2024-02-01 10:00:02.907535'}
        row = Row(**row_dict)
        self.log.info(f'Test output: {transformations(row, EXPECTED_SCHEMA, self.mapping)}')

    def _put_files_in_s3(self, table_name, file_path):
        bucket_name = "datalake"
        self.mm.create_bucket(bucket_name)
        data_file_name = file_path.split('/')[-1]
        self.mm.put_object(bucket_name, f'{table_name}/data/{data_file_name}', file_path)

    def write_files_directly_on_s3(self, table_name: str, input_df: spark_df, owning_company_name: str,
                                   owning_member: str):
        file_name = os.path.basename(self.input_data_file).split('.')[-2]
        local_path = f'output/{table_name}'
        local_path_avsc = f'output/{table_name}/schemas/{file_name}.avsc'
        write_path_base = f's3a://{self.bucket_name}/{local_path}'
        write_path = '/'.join([
            f'{write_path_base}',
            f'{TWAIN_DATA_OWNING_COMPANY}={owning_company_name}',
            f'{TWAIN_DATA_OWNING_MEMBER}={owning_member}'
        ])
        output_avro_schema_path = self.am.compile_schema_from_spark_df(self.spark, input_df)
        self.mm.put_object(self.bucket_name, local_path_avsc, output_avro_schema_path)
        input_df = self._drop_partition_columns(input_df)
        input_df.write.parquet(path=write_path, mode='append')
        self.log.info(f'Wrote file to {write_path}')
        return write_path_base, local_path_avsc

    def write_files_on_s3(self, input_df: spark_df):
        table_name = os.path.basename(self.input_data_file).split('.')[-2]
        local_path = f'output/common_data_schema/{table_name}.parquet'
        # input_df.toPandas().to_parquet(path=local_path)
        fp_write(local_path, input_df.toPandas(), compression='GZIP', object_encoding='utf8')
        self._put_files_in_s3(table_name=table_name, file_path=local_path)

    def _drop_partition_columns(self, input_df: spark_df):
        return input_df.drop(*TWAIN_PARTITION_COLUMNS)

    def run(self):
        target_table_name = 'common_data_model'
        input_dataframe = self.spark.read.format('csv').\
            option('header', 'true').\
            option("skipRows", "2").\
            load(self.input_data_file)
        self.log.info(f"There are {input_dataframe.count()} records in the file")
        self.log.info(f"{input_dataframe.schema}")
        owning_company = 'EDF'
        ro_companies = DEFAULT_DATA_SHARING_AGREEMENT[owning_company]

        owning_member = self.file_owner
        input_data_file_name = self.input_data_file_name
        mapping = copy.copy(self.mapping)
        transformed_df = input_dataframe.rdd.map(
            lambda x: transformations(x, EXPECTED_SCHEMA, mapping)).\
            map(lambda x: add_technical_columns(x,
                                                original_file_name=input_data_file_name,
                                                owning_company=owning_company,
                                                owning_member=owning_member,
                                                ro_companies=ro_companies)).\
            toDF(schema=EXPECTED_SCHEMA)
        self.log.info(f"There are {transformed_df.count()} records in the file")
        self.log.info(f"{transformed_df.schema}")
        self.log.info(transformed_df.show())
        # self.write_files_on_s3(transformed_df)
        remote_parquet_file_path, remote_avro_schema_path =\
            self.write_files_directly_on_s3(target_table_name, transformed_df, owning_company, owning_member)
        self.tm.create_table_and_schema(
            target_table_name, remote_parquet_file_path, remote_avro_schema_path, self.bucket_name)


def read_parquet(runner):
    df = runner.spark.read.\
        option("basePath", "s3a://datalake/output/common_data_model/").\
        parquet("s3a://datalake/output/common_data_model/twain*")
    df.show()
    df_count = df.count()
    runner.log.info(f"There are {df_count} records in parquets.")
    if df_count < 100:
        print(df.toPandas().to_dict())


if __name__ == "__main__":
    logger = logging.getLogger('read_engie_sample_data')
    logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level="INFO", stream=sys.stdout)
    start_timestamp = datetime.datetime.now(tz=pytz.UTC)
    try:
        runner = SparkRunner()
        runner._sample_test()
        runner.run()
        if runner:
            runner.log.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
        read_parquet(runner)
    except Exception as e:
        raise e
    finally:
        if 'runner' in globals():
            runner.log.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
            runner.tm.close()
        else:
            logger.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
