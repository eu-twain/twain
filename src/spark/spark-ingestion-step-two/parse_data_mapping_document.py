import pandas as pd
import os
import yaml
from pyspark.sql.types import Row, StructField, DataType, StructType, StringType, DoubleType, FloatType, \
    ArrayType, StructType, TimestampType

# BASE_FILE_PATH = r'C:\Users\prslys\Twain\data mapping\ENGIE'
BASE_FILE_PATH = os.getenv('TWAIN_NOMENCLATURE_LOCAL_DIR')
mapping_df = pd.read_excel(
    os.path.join(BASE_FILE_PATH, 'TWAIN_Nomenclature_Signals_v1.xlsx'), sheet_name='Signal_Name_Dict')

name_description_mapping = mapping_df[['Standardized Name', 'Definition']].to_dict(orient='records')

print(name_description_mapping)

schema = StructType([
    StructField('timestamp', TimestampType(), nullable=True, metadata={'description': 'Date and time'})])
for column_data in name_description_mapping:
    if column_data['Standardized Name'] == 'timestamp':
        continue
    schema.add(
        StructField(
            name=column_data['Standardized Name'].strip(),
            dataType=FloatType(),
            nullable=True,
            metadata={'description': column_data['Definition']})
    )

print(schema.jsonValue())

for x in schema.fields:
    print(f"StructField('{x.name}'," + str(x.dataType) + ", nullable=True, " +
          "metadata={'description': '" + x.metadata['description'] +"'}),")


def _manage_mapping_v1(mapping_df: pd.DataFrame):
    name_description_mapping = mapping_df[
        ['Standardized Name', 'Definition',
         'Other name 1', 'Other name 2', 'Other name 3', 'Other name 4',
         'Other name 5', 'Other name 6', 'Other name 7', 'Other name 8'
         ]].fillna('NA').to_dict(orient='records')

    output_dict = dict()
    for x in name_description_mapping:
        local_list = list()
        name = x['Standardized Name'].replace(" ", "_").replace(",", "_").replace(".", "_").lower()
        local_list.append(name)
        for y in ['Other name 1', 'Other name 2', 'Other name 3', 'Other name 4', 'Other name 5', 'Other name 6', 'Other name 7', 'Other name 8']:
            if x[y] and x[y] != 'NA':
                alternative_name = x[y].replace(" ", "_").replace(",", "_").replace(".", "_").lower()
                if alternative_name.endswith("?"):
                    alternative_name = alternative_name[:-1]
                local_list.append(alternative_name)
        output_dict[x['Standardized Name']] = local_list

    a = yaml.dump(output_dict)
    print(a)


def _validate_mapping_v1(file_name: str):
    with open(file_name, 'r') as stream:
        mapping = yaml.safe_load(stream)
    print(mapping)


_manage_mapping_v1(mapping_df)
_validate_mapping_v1(os.path.join('..', 'minio_config', 'mappings', 'consumption_layer_data_mapping.yaml'))
