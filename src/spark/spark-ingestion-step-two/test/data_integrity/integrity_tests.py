

def read_parquet(runner):
    df = runner.spark.read.\
        option("basePath", "s3a://datalake/output/bronze_layer/bronze_fake_data_ps_024_tabulated_csv_minikube_ts_present/").\
        parquet("s3a://datalake/output/bronze_layer/bronze_fake_data_ps_024_tabulated_csv_minikube_ts_present/twain*")
    df.show()
    df_count = df.count()
    runner.log.info(f"There are {df_count} records in parquets.")
    if df_count < 100:
        print(df.toPandas().to_dict())

