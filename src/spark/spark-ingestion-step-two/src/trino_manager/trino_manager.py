import json
import logging
import re
import trino.exceptions
from trino.dbapi import connect
from trino.auth import BasicAuthentication
from trino.constants import HTTPS as TRINO_HTTPS
from ..config.technical_columns import TWAIN_PARTITION_COLUMNS
from ..avro_manager.avro_processor import trino_create_table_statement


class TrinoManager(object):
    def __init__(self, host, port, username, password, catalog, schema):
        self.log = logging.getLogger(self.__class__.__name__)
        self.avsc_location = None
        self.location_pattern = re.compile('^[ ]+external_location = \'(.*)\',')
        self.connection = connect(
            host=host,
            port=port,
            user=username,
            auth=BasicAuthentication(username, password),
            catalog=catalog,
            schema=schema,
            http_scheme=TRINO_HTTPS,
            verify=False
        )

    def get_file_location(self, table_name, transform_file_system: bool = False):
        cur = self.connection.cursor()
        cur.execute(f"show create table {table_name}")
        rows = cur.fetchall()
        for z in rows:
            for x in z:
                for y in x.split('\n'):
                    matches = re.search(self.location_pattern, y)
                    if matches:
                        self.log.info(f"Reading will commence from directory {y}")
                        if not transform_file_system:
                            return matches.group(1)
                        else:
                            return self._transform_data_location_to_visible_outside_of_trino(matches.group(1))
        return None

    def _sample_query(self):
        cur = self.connection.cursor()
        cur.execute("SELECT * FROM hive.default.spark_to_df_with_array")
        rows = cur.fetchall()
        for index_i, x in enumerate(rows):
            show_number = max(int(len(rows)/100), 1)
            if index_i % show_number == 0:
                self.log.info(x)

    def create_table_from_avsc(self, table_name: str, avsc_location: str, bucket: str,
                               protocol_header: str = 's3://', avro_schema_dict: dict = None):
        self.log.info(f"Creating table {table_name}")
        cur = self.connection.cursor()
        self.log.info(f"avro schema dump before parsing into plain SQL create table statement: "
                      f"{json.dumps(avro_schema_dict, indent=2, ensure_ascii=False)}")
        try:
            create_table_query = trino_create_table_statement(
                avro_schema_dict, only_str=False, overwrite_table_name=table_name)
        except Exception as e:
            self.log.error(f"Got error of type {type(e)}; {e.args}")
            raise e
        # create_table_query = f"""CREATE TABLE {table_name} (
        #     id BIGINT
        #  )
        #  WITH (
        #     format = 'AVRO',
        #     avro_schema_url = '{protocol_header}{bucket}/{avsc_location}'
        #  )"""
        self.log.info(create_table_query)
        try:
            cur.execute(create_table_query)
        except trino.exceptions.TrinoUserError as tue:
            self.log.info(tue.error_name)
            if tue.error_name == 'TABLE_ALREADY_EXISTS':
                self.log.info(f'Table \'{table_name}\' already exists.')
                return
            else:
                raise tue
        rows = cur.fetchall()
        for x in rows:
            self.log.info(x)
        self.log.info(f"Created table {table_name}")

    def create_data_table(self, table_name: str, schema_table_name: str, data_location: str,
                          add_default_partition: bool = True):
        self.log.info(f"Creating table {table_name}")
        cur = self.connection.cursor()
        create_table_query = f"""CREATE TABLE {table_name} (
                    LIKE {schema_table_name}
                 )
                 WITH (
                    format = 'PARQUET',
                    external_location = '{data_location}'"""
        if add_default_partition:
            partition_string = '\', \''.join(TWAIN_PARTITION_COLUMNS)
            partition_string = "\'" + partition_string + "\'"
            create_table_query = create_table_query + f""",
                    partitioned_by = ARRAY[{partition_string}]
                 )"""
        else:
            create_table_query = create_table_query + ")"
        self.log.info(create_table_query)
        try:
            cur.execute(create_table_query)
        except trino.exceptions.TrinoUserError as tue:
            self.log.info(tue.error_name)
            if tue.error_name == 'TABLE_ALREADY_EXISTS':
                self.log.info(f'Table \'{table_name}\' already exists.')
                self._synch_partitions(table_name)
                return
            else:
                raise tue
        # TODO:
        # identify timestamps wrongly typed as integers
        #
        # attempt 1: ALTER TABLE [ IF EXISTS ] name ALTER COLUMN column_name SET DATA TYPE new_type
        # result: Error:
        #   This connector does not support setting column types
        # 	at io.trino.spi.connector.ConnectorMetadata.setColumnType(ConnectorMetadata.java:636)
        self._synch_partitions(table_name)
        self.log.info(f"Created table {table_name}")

    def _synch_partitions(self, table_name: str):
        cur = self.connection.cursor()
        synch_table_query = f"CALL system.sync_partition_metadata('{self.connection.schema}', '{table_name}', 'ADD')"
        try:
            cur.execute(synch_table_query)
        except trino.exceptions.TrinoUserError as tue:
            self.log.info(tue.error_name)
            raise tue
        self.log.info(f"Added partitions to table {table_name}")

    @staticmethod
    def _transform_data_location_to_visible_by_trino(input_location: str):
        search_string = 's3a://'
        if input_location.startswith(search_string):
            input_location = 's3://' + input_location[len(search_string):]
        return input_location

    @staticmethod
    def _transform_data_location_to_visible_outside_of_trino(input_location: str):
        search_string = 's3://'
        if input_location.startswith(search_string):
            input_location = 's3a://' + input_location[len(search_string):]
        return input_location

    def create_table_and_schema(self, table_name: str, data_location: str, avsc_location: str,
                                bucket: str, avro_schema_dict: dict):
        self.create_table_from_avsc(table_name + '_schema', avsc_location, bucket,
                                    protocol_header='s3://', avro_schema_dict=avro_schema_dict)
        data_location = self._transform_data_location_to_visible_by_trino(data_location)
        self.create_data_table(table_name, table_name + '_schema', data_location)

    def close(self):
        self.connection.close()
