import os


def create_dir_if_not_exists(input_dir):
    if os.path.exists(input_dir):
        return
    try:
        os.mkdir(input_dir)
    except FileNotFoundError as fnf:
        create_dir_if_not_exists(os.path.dirname(input_dir))
        os.mkdir(input_dir)
