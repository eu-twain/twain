import datetime
import os
from typing import Dict
from pyspark.sql.types import Row, DataType, StringType, DoubleType, FloatType, TimestampType, \
    ArrayType, StructType, _make_type_verifier as type_verifier, AtomicType
from ..config.mappings.test_schema_mappings import DataMapping


def _parse_timestamp(input_value: str, accepted_patterns: str):
    if not accepted_patterns:
        return None
    for x in accepted_patterns.split(";"):
        try:
            output_value = datetime.datetime.strptime(input_value, x)
        except ValueError as ve:
            continue
        if output_value:
            return output_value
    return None


def _type_caster(input_value, expected_schema_field_type: DataType, timestamp_patterns: str = None):
    if isinstance(input_value, str) and not input_value:
        output_value = None
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, StringType):
        output_value = input_value
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, FloatType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, DoubleType):
        output_value = float(input_value)
    elif isinstance(input_value, str) and isinstance(expected_schema_field_type, TimestampType):
        output_value = _parse_timestamp(input_value, timestamp_patterns)
    elif isinstance(input_value, str):
        output_value = float(input_value)
    elif isinstance(input_value, int) and isinstance(expected_schema_field_type, FloatType):
        output_value = float(str(input_value))
    else:
        output_value = input_value
    try:
        type_verifier(expected_schema_field_type)(output_value)
    except Exception as e:
        raise e
    return output_value


def transformations(input_row: Row, expected_schema, mapping: DataMapping,
                    field_name_mapping_overwrite: Dict = None, parent_column_path: str = None):
    output = {}
    input_dict = input_row.asDict()
    for x in expected_schema.fields:
        output[x.name] = None
        if isinstance(x.dataType, AtomicType):  # or isinstance(x.dataType, StructType)):
            retrieved_overwrite, retrieved_patterns = mapping.get_mapping(x.name, x.dataType)
            if field_name_mapping_overwrite and x.name in field_name_mapping_overwrite.keys():
                searched_name_list = field_name_mapping_overwrite[x.name]
            elif retrieved_overwrite:
                searched_name_list = retrieved_overwrite
            else:
                searched_name_list = [x.name]
            for searched_name in searched_name_list:
                if searched_name in input_dict.keys():
                    retrieved_value = _type_caster(input_dict[searched_name], x.dataType, retrieved_patterns)
                    if retrieved_value:
                        output[x.name] = retrieved_value
                        break
        elif isinstance(x.dataType, StructType):
            retrieved_mapping, _ = mapping.get_mapping(
                x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            transformation_result = transformations(
                input_row,
                x.dataType,
                mapping,
                field_name_mapping_overwrite=retrieved_mapping,
                parent_column_path=x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            if not([v for v in transformation_result.values() if v is not None]) and x.nullable:
                output[x.name] = None
            else:
                output[x.name] = transformation_result
        elif isinstance(x.dataType, ArrayType):
            retrieved_mapping, _ = mapping.get_mapping(
                x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            result = transformations(
                input_row,
                x.dataType.elementType,
                mapping,
                field_name_mapping_overwrite=retrieved_mapping,
                parent_column_path=x.name if not parent_column_path else f'{parent_column_path}.{x.name}'
            )
            not_null_values = [x for x in result.values() if x is not None]
            if not_null_values:
                output[x.name] = [result]
            else:
                output[x.name] = None
    return output


if __name__ == "__main__":
    raise NotImplementedError(f"This script {os.path.basename(__file__)} is not supposed to be run outside of Spark.")
