from pyspark.sql.types import TimestampType
from src.minio_uploader.minio_uploader import MinioManager
import os
import yaml


FAKE_SSP_CSV_DATA_SAMPLE = {
    'locality': {
        'latitude': 'location_latitude',
        'longitude': 'location_longitude'
    },
    'some_embedded_field': {
        'latitude': 'location_latitude',
        'event_timestamp': {
            'search_for_column': 'event_timestamp',
            'parsing_patterns': '%d/%m/%Y;%Y/%m/%d;%Y-%m-%d %H:%M:%S.%f;%Y-%m-%d %H:%M:%S'
        }
    },
    'time_series_data': {
        'wind_speed': 'szybkość wiatru'
    },
    'time_series_data.wind_direction': {
        'wind_direction_x': 'it_blows_from_x',
        'wind_direction_y': 'it_blows_from_y'
    },
    'wind_direction': 'kierunek wiatru',
    'wind_direction_x': 'kierunek wiatru',
    'event_timestamp': {
        'search_for_column': 'event_timestamp',
        'parsing_patterns': '%d/%m/%Y;%Y/%m/%d;%Y-%m-%d %H:%M:%S.%f;%Y-%m-%d %H:%M:%S'
    },
    'name_with_dots': 'name.with.dots'
}

TWAIN_COMMON_DATA_MODEL = {
    'wtur_Id': 'mac_code',
    'timestamp': 'event_timestamp',
    'wtur_W_min': 'wind_speed'
}

COMMON_DATA_SCHEMA_MAPPING = {
    'FAKE_SSP_CSV_DATA_SAMPLE': FAKE_SSP_CSV_DATA_SAMPLE,
    'TWAIN_COMMON_DATA_MODEL': TWAIN_COMMON_DATA_MODEL
}


class DataMapping(object):
    LOCAL_FILE_MODE = 'local_file_mode'
    EXTERNAL_FILE_MODE = 'external_file_mode'
    EXTERNAL_BUCKET_NAME = 'datalake'

    def __init__(self, mode, mapping_name, minio_handle=None):
        if mode == DataMapping.LOCAL_FILE_MODE:
            self.mapping = COMMON_DATA_SCHEMA_MAPPING[mapping_name]
        elif mode == DataMapping.EXTERNAL_FILE_MODE:
            if not minio_handle or not isinstance(minio_handle, MinioManager):
                raise ValueError("When using external data mapping a minio client is needed.")
            mapping_file_local_dir = os.path.join(os.getcwd(), 'mapping_file.yaml')
            mapping_file_external_dir = 'admin_consumption_data_mapping/consumption_layer_data_mapping.yaml'
            minio_handle.log.info(f"Reading file from bucket {DataMapping.EXTERNAL_BUCKET_NAME},"
                                  f" path: {mapping_file_external_dir}.")
            minio_handle.client.fget_object(
                DataMapping.EXTERNAL_BUCKET_NAME,
                mapping_file_external_dir,
                mapping_file_local_dir
            )
            self.mapping = yaml.safe_load(open(mapping_file_local_dir))[mapping_name]
            minio_handle.log.info(self.mapping)

    def _get_mapping(self, expected_column_name: str):
        if expected_column_name in self.mapping.keys():
            return self.mapping[expected_column_name]
        else:
            return None

    def get_mapping(self, expected_column_name: str, expected_column_type=None):
        if not expected_column_type or not isinstance(expected_column_type, TimestampType):
            return self._get_mapping(expected_column_name), None
        elif isinstance(expected_column_type, TimestampType):
            # this below worked for text to timestamp transformation
            # timestamp_config = self._get_mapping(expected_column_name)
            # return timestamp_config['search_for_column'], timestamp_config['parsing_patterns']
            return self._get_mapping(expected_column_name), None
        else:
            raise NotImplementedError(f'Option not implemented')

