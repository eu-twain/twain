from pyspark.sql.types import Row, StructField, DataType, StructType, StringType, DoubleType, FloatType, \
    ArrayType, StructType, TimestampType, IntegerType


EXPECTED_SCHEMA = StructType([
    StructField('timestamp',TimestampType(), nullable=True, metadata={'description': 'Date and time'}),
    StructField('wtur_Id',StringType(), nullable=True, metadata={'description': 'WT Identifier'}),
    StructField('wtur_W_avg',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after converter and trafo (average)'}),
    StructField('wtur_W_max',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after converter and trafo (max)'}),
    StructField('wtur_W_min',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after converter and trafo (min)'}),
    StructField('wtur_W_std',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after converter and trafo (std)'}),
    StructField('wtur_TurSt',FloatType(), nullable=True, metadata={'description': 'Wind turbine status'}),
    StructField('wtur_WLim',FloatType(), nullable=True, metadata={'description': 'Limit of active power generation - wind turbine'}),
    StructField('wtur_StrEvt',FloatType(), nullable=True, metadata={'description': 'Start up has ocurred'}),
    StructField('wtur_StopEvt',FloatType(), nullable=True, metadata={'description': 'Shut down has ocurred'}),
    StructField('wgen_W_avg',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after generator (average)'}),
    StructField('wgen_W_max',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after generator (max)'}),
    StructField('wgen_W_min',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after generator (min)'}),
    StructField('wgen_W_std',FloatType(), nullable=True, metadata={'description': 'Active power generation delivered by the WT after generator (std)'}),
    StructField('wgen_RotSpd_avg',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine generator (average)'}),
    StructField('wgen_RotSpd_max',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine generator (max)'}),
    StructField('wgen_RotSpd_min',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine generator (min)'}),
    StructField('wgen_RotSpd_std',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine generator (std)'}),
    StructField('wgen_Torq_avg',FloatType(), nullable=True, metadata={'description': 'Generator torque from the transmision system (average)'}),
    StructField('wgen_Torq_max',FloatType(), nullable=True, metadata={'description': 'Generator torque from the transmision system (max)'}),
    StructField('wgen_Torq_min',FloatType(), nullable=True, metadata={'description': 'Generator torque from the transmision system (min)'}),
    StructField('wgen_Torq_std',FloatType(), nullable=True, metadata={'description': 'Generator torque from the transmision system (std)'}),
    StructField('wgen_ProdTms',FloatType(), nullable=True, metadata={'description': 'Generator production time'}),
    StructField('wgen_Gn1ProdTms',FloatType(), nullable=True, metadata={'description': 'Generator 1 connection time'}),
    StructField('wgen_Gn2ProdTms',FloatType(), nullable=True, metadata={'description': 'Generator 2 connection time'}),
    StructField('wmet_WdSpd_avg',FloatType(), nullable=True, metadata={'description': 'Wind Speed (average)'}),
    StructField('wmet_WdSpd_max',FloatType(), nullable=True, metadata={'description': 'Wind Speed (max)'}),
    StructField('wmet_WdSpd_min',FloatType(), nullable=True, metadata={'description': 'Wind Speed (min)'}),
    StructField('wmet_WdSpd_std',FloatType(), nullable=True, metadata={'description': 'Wind Speed (std)'}),
    StructField('wmet_WdDir_avg',FloatType(), nullable=True, metadata={'description': 'Wind Direction (average)'}),
    StructField('wmet_WdDir_max',FloatType(), nullable=True, metadata={'description': 'Wind Direction (max)'}),
    StructField('wmet_WdDir_min',FloatType(), nullable=True, metadata={'description': 'Wind Direction (min)'}),
    StructField('wmet_WdDir_std',FloatType(), nullable=True, metadata={'description': 'Wind Direction (std)'}),
    StructField('wmet_AirTmp_avg',FloatType(), nullable=True, metadata={'description': 'Ambient Temperature (average)'}),
    StructField('wmet_AirTmp_max',FloatType(), nullable=True, metadata={'description': 'Ambient Temperature (max)'}),
    StructField('wmet_AirTmp_min',FloatType(), nullable=True, metadata={'description': 'Ambient Temperature (min)'}),
    StructField('wmet_AirTmp_std',FloatType(), nullable=True, metadata={'description': 'Ambient Temperature (std)'}),
    StructField('wmet_WdTrbInt_avg',FloatType(), nullable=True, metadata={'description': 'Turbulence Intensity (average)'}),
    StructField('wmet_AirDen_avg',FloatType(), nullable=True, metadata={'description': 'Air density (average)'}),
    StructField('wmet_AirPres_avg',FloatType(), nullable=True, metadata={'description': 'Air pressure (average)'}),
    StructField('wmet_AirPres_max',FloatType(), nullable=True, metadata={'description': 'Air pressure (max)'}),
    StructField('wmet_AirPres_min',FloatType(), nullable=True, metadata={'description': 'Air pressure (min)'}),
    StructField('wmet_AirPres_std',FloatType(), nullable=True, metadata={'description': 'Air pressure (std)'}),
    StructField('wmet_AirHum_avg',FloatType(), nullable=True, metadata={'description': 'Humidity (average)'}),
    StructField('wmet_AirHum_max',FloatType(), nullable=True, metadata={'description': 'Humidity (max)'}),
    StructField('wmet_AirHum_min',FloatType(), nullable=True, metadata={'description': 'Humidity (min)'}),
    StructField('wmet_AirHum_std',FloatType(), nullable=True, metadata={'description': 'Humidity (std)'}),
    StructField('wmet_VerWdSpd_avg',FloatType(), nullable=True, metadata={'description': 'Vertical Wind Speed (average)'}),
    StructField('wmet_VerWdSpd_max',FloatType(), nullable=True, metadata={'description': 'Vertical Wind Speed (max)'}),
    StructField('wmet_VerWdSpd_min',FloatType(), nullable=True, metadata={'description': 'Vertical Wind Speed (min)'}),
    StructField('wmet_VerWdSpd_std',FloatType(), nullable=True, metadata={'description': 'Vertical Wind Speed (std)'}),
    StructField('wmet_RnFll_avg',FloatType(), nullable=True, metadata={'description': 'Rain precipitation (average)'}),
    StructField('wmet_RnFll_max',FloatType(), nullable=True, metadata={'description': 'Rain precipitation  (max)'}),
    StructField('wmet_RnFll_min',FloatType(), nullable=True, metadata={'description': 'Rain precipitation  (min)'}),
    StructField('wmet_RnFll_std',FloatType(), nullable=True, metadata={'description': 'Rain precipitation  (std)'}),
    StructField('wmet_RnFllSt',FloatType(), nullable=True, metadata={'description': 'Rain precipitation status'}),
    StructField('wnac_WdDirRel_avg_deprecated',FloatType(), nullable=True, metadata={'description': 'Wind Direction with respect to nacelle orientation (average), deprecated'}),
    StructField('wnac_WdDirRel_max_deprecated',FloatType(), nullable=True, metadata={'description': 'Wind Direction with respect to nacelle orientation (max), deprecated'}),
    StructField('wnac_WdDirRel_min_deprecated',FloatType(), nullable=True, metadata={'description': 'Wind Direction with respect to nacelle orientation (min), deprecated'}),
    StructField('wnac_WdDirRel_std_deprecated',FloatType(), nullable=True, metadata={'description': 'Wind Direction with respect to nacelle orientation (std), deprecated'}),
    StructField('wnac_Dir_avg',FloatType(), nullable=True, metadata={'description': 'Nacelle Orientation (average)'}),
    StructField('wnac_Dir_max',FloatType(), nullable=True, metadata={'description': 'Nacelle Orientation (max)'}),
    StructField('wnac_Dir_min',FloatType(), nullable=True, metadata={'description': 'Nacelle Orientation (min)'}),
    StructField('wnac_Dir_std',FloatType(), nullable=True, metadata={'description': 'Nacelle Orientation (std)'}),
    StructField('wrot_BlPthAngVal_avg',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blades (average)'}),
    StructField('wrot_BlPthAngVal_max',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blades (max)'}),
    StructField('wrot_BlPthAngVal_min',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blades (min)'}),
    StructField('wrot_BlPthAngVal_std',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blades (std)'}),
    StructField('wrot_Bl1PthAngVal_avg',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 1 (average)'}),
    StructField('wrot_Bl1PthAngVal_max',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 1 (max)'}),
    StructField('wrot_Bl1PthAngVal_min',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 1 (min)'}),
    StructField('wrot_Bl1PthAngVal_std',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 1 (std)'}),
    StructField('wrot_Bl2PthAngVal_avg',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade2 (average)'}),
    StructField('wrot_Bl2PthAngVal_max',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 2 (max)'}),
    StructField('wrot_Bl2PthAngVal_min',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 2 (min)'}),
    StructField('wrot_Bl2PthAngVal_std',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 2 (std)'}),
    StructField('wrot_Bl3PthAngVal_avg',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade3 (average)'}),
    StructField('wrot_Bl3PthAngVal_max',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 3 (max)'}),
    StructField('wrot_Bl3PthAngVal_min',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 3 (min)'}),
    StructField('wrot_Bl3PthAngVal_std',FloatType(), nullable=True, metadata={'description': 'Pitch angle for blade 3 (std)'}),
    StructField('wrot_RotSpd_avg',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine rotor (average)'}),
    StructField('wrot_RotSpd_max',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine rotor (max)'}),
    StructField('wrot_RotSpd_min',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine rotor (min)'}),
    StructField('wrot_RotSpd_std',FloatType(), nullable=True, metadata={'description': 'Rotational Speed of wind turbine rotor (std)'}),
    StructField('wyaw_YwAng_avg',FloatType(), nullable=True, metadata={'description': 'Yaw bearing rotational angle relative to nominal true North (avg)'}),
    StructField('wyaw_YwAng_min',FloatType(), nullable=True, metadata={'description': 'Yaw bearing rotational angle relative to nominal true North (min)'}),
    StructField('wyaw_YwAng_max',FloatType(), nullable=True, metadata={'description': 'Yaw bearing rotational angle relative to nominal true North (max)'}),
    StructField('wyaw_YwAng_std',FloatType(), nullable=True, metadata={'description': 'Yaw bearing rotational angle relative to nominal true North (std)'}),
    StructField('wslg_EvtStrDtTms_deprecated',FloatType(), nullable=True, metadata={'description': 'Event start date time, deprecated'}),
    StructField('wslg_EvtStopDtTms_deprecated',FloatType(), nullable=True, metadata={'description': 'Event stop date time, deprecated'}),
    StructField('wslg_EvtId',FloatType(), nullable=True, metadata={'description': 'Event identifier'})
    # columns added in March 2025
    , StructField('wtur_Eng_avg', FloatType(), nullable=True, metadata={'description': 'Energy delivered by the WT after converter and trafo (average)'})
    , StructField('wtur_Eng_max', FloatType(), nullable=True, metadata={'description': 'Energy delivered by the WT after converter and trafo (max)'})
    , StructField('wtur_Eng_min', FloatType(), nullable=True, metadata={'description': 'Energy delivered by the WT after converter and trafo (min)'})
    , StructField('wtur_Eng_std', FloatType(), nullable=True, metadata={'description': 'Energy delivered by the WT after converter and trafo (std)'})
    , StructField('wavl_AvlTmsRs', IntegerType(), nullable=True, metadata={'description': 'Availability time in seconds'})
    , StructField('wtur_VAr_avg', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after converter and trafo (average)'})
    , StructField('wtur_VAr_max', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after converter and trafo (max)'})
    , StructField('wtur_VAr_min', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after converter and trafo (min)'})
    , StructField('wtur_VAr_std', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after converter and trafo (std)'})
    , StructField('wtur_DmdWSpt', FloatType(), nullable=True, metadata={'description': 'Active power generation set-point'})
    , StructField('wtur_DmdVArSpt', FloatType(), nullable=True, metadata={'description': 'Reactive power generation set-point'})
    , StructField('wgen_VAr_avg', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after generator (average)'})
    , StructField('wgen_VAr_max', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after generator (max)'})
    , StructField('wgen_VAr_min', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after generator (min)'})
    , StructField('wgen_VAr_std', FloatType(), nullable=True, metadata={'description': 'Reactive power generation delivered by the WT after generator (std)'})
    , StructField('wcnv_GnHz', FloatType(), nullable=True, metadata={'description': 'Generator side frequency (Hz)'})
    , StructField('wcnv_GriHz', FloatType(), nullable=True, metadata={'description': 'Grid side frequency (Hz)'})
    , StructField('wrot_Bl1PthAngTgt', FloatType(), nullable=True, metadata={'description': 'Pitch angle set-point for blade 1'})
    , StructField('wrot_Bl2PthAngTgt', FloatType(), nullable=True, metadata={'description': 'Pitch angle set-point for blade 2'})
    , StructField('wrot_Bl3PthAngTgt', FloatType(), nullable=True, metadata={'description': 'Pitch angle set-point for blade 3'})
    , StructField('wrot_Bl1AziAngVal', FloatType(), nullable=True, metadata={'description': 'Blade azimuthal position for blade 1'})
    , StructField('wrot_Bl2AziAngVal', FloatType(), nullable=True, metadata={'description': 'Blade azimuthal position for blade 2'})
    , StructField('wrot_Bl3AziAngVal', FloatType(), nullable=True, metadata={'description': 'Blade azimuthal position for blade 3'})
    , StructField('wrot_Bl1Rad0InPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root in-plane bending moment for blade 1'})
    , StructField('wrot_Bl2Rad0InPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root in-plane bending moment for blade 2'})
    , StructField('wrot_Bl3Rad0InPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root in-plane bending moment for blade 3'})
    , StructField('wrot_Bl1Rad0OutPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root out-of-plane bending moment for blade 1'})
    , StructField('wrot_Bl2Rad0OutPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root out-of-plane bending moment for blade 2'})
    , StructField('wrot_Bl3Rad0OutPlnMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade root out-of-plane bending moment for blade 3'})
    , StructField('wyaw_RotSpd_avg', FloatType(), nullable=True, metadata={'description': 'Yaw system rotational speed (average)'})
    , StructField('wyaw_YwOp', FloatType(), nullable=True, metadata={'description': 'Yaw system set-point'})
    , StructField('wslg_EvtStrDtTms',TimestampType(), nullable=True, metadata={'description': 'Event start date time'})
    , StructField('wslg_EvtStopDtTms',TimestampType(), nullable=True, metadata={'description': 'Event stop date time'})
    , StructField('wtrm_LoSpdShftRotXMnt_avg', FloatType(), nullable=True, metadata={'description': 'Low speed shaft torsional moment in x direction, rotatory coordinate system (avg)'})
    , StructField('wtrm_LoSpdShftRotYMnt_avg', FloatType(), nullable=True, metadata={'description': 'Low speed shaft bending moment in y direction, rotatory coordinate system (avg)'})
    , StructField('wtrm_LoSpdShftRotZMnt_avg', FloatType(), nullable=True, metadata={'description': 'Low speed shaft bending moment in z direction, rotatory coordinate system (avg)'})
    , StructField('wtow_HASSMnt_avg', FloatType(), nullable=True, metadata={'description': 'Tower side-side bending moment at height A (% height, integer from 0 to 100) (avg)'})
    , StructField('wtow_HAFAMnt_avg', FloatType(), nullable=True, metadata={'description': 'Tower fore-aft bending moment at height A (% height, integer from 0 to 100) (avg)'})
    , StructField('wrot_BlARadBFlpMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade flapwise bending moment in blade A (integer for blade number 1, 2, 3) at radius B (% radius, integer from 0 to 100) (avg)'})
    , StructField('wrot_BlARadBEdgMnt_avg', FloatType(), nullable=True, metadata={'description': 'Blade edgewise bending moment in blade A (integer for blade number 1, 2, 3) at radius B (% radius, integer from 0 to 100) (avg)'})

    , StructField('twain_original_file_name', StringType(), nullable=True, metadata={'description': 'File name of the original file containing the data'})
    , StructField('twain_data_ro_allowed_companies', StringType(), nullable=True, metadata={'description': 'Companies/entities allowed to query the data'})
    , StructField('twain_data_owning_company', StringType(), nullable=False, metadata={'description': 'Company/entity owning the data'})
    , StructField('twain_data_owning_member', StringType(), nullable=False, metadata={'description': 'Personal Account owning the data'})
])
