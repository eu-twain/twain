import re
from .mapping.parquet_avro_mapping import PYSPARK_TO_AVRO_TYPES, AVRO_TO_PYSPARK_TYPES
from pyspark.sql.types import AtomicType, StructType, ArrayType


def guess_pyspark_type(avro_type):
    if avro_type in list(AVRO_TO_PYSPARK_TYPES.keys()):
        return AVRO_TO_PYSPARK_TYPES[avro_type]()
    else:
        raise ValueError(f"Could not find correct pyspark type for avro type {avro_type}")


class RePatterns(object):
    def __init__(self):
        self.starts_with_number = re.compile('^[0-9]')


class GenericAVSCBuilder(object):
    def __init__(self, namespace, name):
        self.namespace = namespace
        self.type = 'record'
        self.name = name
        self.fields = []

    def build(self):
        return {"namespace": self.namespace,
                "type": self.type,
                "name": self.name,
                "fields": self.fields
                }


class FlatFieldConverter(object):
    def __init__(self, patterns: RePatterns, column_name: str, column_type):
        self.column_name = column_name.replace(" ", "_").replace(",", "_").lower()
        self.fields = None
        self.items = None
        if re.match(patterns.starts_with_number, self.column_name):
            self.column_name = 'no_' + self.column_name
        self.column_type = self._convert_type(type(column_type))
        if not isinstance(column_type, AtomicType):
            if isinstance(column_type, StructType):
                self.fields = [FlatFieldConverter(patterns, x.name, x.dataType).build() for x in column_type.fields]
            elif isinstance(column_type, ArrayType):
                self.items = []
                if isinstance(column_type.elementType, AtomicType):
                    self.items.append(FlatFieldConverter(
                        patterns, column_type.elementType.name, column_type.elementType.dataType).build())
                else:
                    for x in column_type.elementType.fields:
                        self.items.append(FlatFieldConverter(patterns, x.name, x.dataType).build())

    def build(self):
        base = {"name": self.column_name, "type": self.column_type}
        if self.fields:
            base.update({'fields': self.fields})
        if self.items:
            base.update({'items': self.items})
        return base

    @staticmethod
    def _convert_type(pyspark_type):
        if pyspark_type in list(PYSPARK_TO_AVRO_TYPES.keys()):
            return PYSPARK_TO_AVRO_TYPES[pyspark_type]
        else:
            return pyspark_type
