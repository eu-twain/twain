#!/usr/bin/env python

# AVRO_TO_TRINO_TYPE_MAP: dict[str, str] = {
#     "boolean": "BOOLEAN",
#     "long": "BIGINT",
#     "int": "INT",
#     "float": "REAL",
#     "double": "DOUBLE",
#     "string": "VARCHAR",
#     "bytes": "VARCHAR",
#     "date": "TIMESTAMP(6)",
# }
#
# LOGICAL_TYPES_MAP: dict[str, str] = {
#     "decimal": "DECIMAL",
#     "uuid": "UUID",
#     "timestamp-millis": "TIMESTAMP(3)",
#     "timestamp-micros": "TIMESTAMP(6)",
# }

AVRO_TO_TRINO_TYPE_MAP = {
    "boolean": "BOOLEAN",
    "long": "BIGINT",
    "int": "INT",
    "float": "REAL",
    "double": "DOUBLE",
    "string": "VARCHAR",
    "bytes": "VARCHAR",
    "date": "TIMESTAMP(6)",
}

LOGICAL_TYPES_MAP = {
    "decimal": "DECIMAL",
    "uuid": "UUID",
    "timestamp-millis": "TIMESTAMP(3)",
    "timestamp-micros": "TIMESTAMP(6)",
}


CREATE_TABLE_STATEMENT = """
CREATE TABLE {table_name} (
{columns}
)
"""

COLUMN_STATEMENT = "    {column_name} {column_type} {comment}"

COMMENT_STATEMENT = "COMMENT '{comment_text}'"


class UnsupportedType(Exception):
    def __init__(self, field):
        type_kind = "logicalType" if "logicalType" in field else "type"
        message = f"Unsupported {type_kind} type {field['type']} for field {field['name']}"

        self.message = message
        super().__init__(self.message)


def trino_type_from_field(field: dict) -> str:
    field_type = field["type"]
    if isinstance(field_type, list):
        for x in field_type:
            if isinstance(x, str):
                if x.lower() == 'null':
                    continue
                else:
                    field_type = x
                    break
            elif isinstance(x, dict):
                if "logicalType" in x.keys():
                    field['logicalType'] = x['logicalType']
                    field_type = x['type']
                    break
    elif isinstance(field_type, dict):
        if "logicalType" in field_type.keys():
            field['logicalType'] = field_type['logicalType']
            field_type = field_type['type']

    trino_type = AVRO_TO_TRINO_TYPE_MAP[field_type]
    if "logicalType" in field:
        trino_type = LOGICAL_TYPES_MAP[field["logicalType"]]
    return trino_type


def field_to_typed_column(field: dict) -> str:
    try:
        trino_type = trino_type_from_field(field)
        return COLUMN_STATEMENT.format(
            column_name=field['name'],
            column_type=trino_type,
            comment=COMMENT_STATEMENT.format(comment_text=field["doc"]) if "doc" in field else ""
        )
    except KeyError as e:
        raise UnsupportedType(field) from e


def field_to_str_column(field: dict) -> str:
    return COLUMN_STATEMENT.format(
        column_name=field['name'],
        column_type='VARCHAR',
        comment=COMMENT_STATEMENT.format(comment_text=field["doc"]) if "doc" in field else ""
    )


def trino_create_table_statement(schema: dict, only_str=False, overwrite_table_name: str = None) -> str:
    """
    Function returns a string containing  'CREATE TABLE'
    statement with corresponding types

    :param dict schema: Twain Avro~ish schema
    :param bool only_str: if set to True, the column
         types in output will be only of varchar types.
         Otherwise the types will correspond to the ones defined in schema
    :param str overwrite_table_name: An overwrite for table name

    :return: trino SQL string
    :rtype: str

    :raises UnsupportedType: if the type provided in avro is unsupported

    """

    mapper = field_to_str_column if only_str else field_to_typed_column
    columns = [
        mapper(field)
        for field in schema['fields']
    ]

    return CREATE_TABLE_STATEMENT.format(
        table_name=schema["name"] if not overwrite_table_name else overwrite_table_name,
        columns=",\n".join(columns)
    )


if __name__ == "__main__":
    test_avro = {
      "type": "record",
      "namespace": "dk.dtu.twain",
      "name": "static_info_la_haute_borne",
      "doc": "Static information about La Haute Borne wind farm",
      "format": "CSV",
      "csv_separator": ";",
      "skip_header_line_count": "1",
      "fields": [
        {"name": "commissioning_date", "type": "date", "doc": "Commission date", "parsing_pattern": "%Y-%m-%d"},
        {"name": "rated_power", "type": "int", "doc": "Rated power (kW)."},
        {"name": "rotor_diameter", "type": "int", "doc": "Rotor diameter (m)."},
        # { "name": "broken_field", "type": "unsupported_"},  # this should raise UnsupportedType exception
        {"name": "rotor_diameter_nullable", "type":  ["null", "double"], "doc": "Rotor diameter (m)."},
        {
            "name": "technically_correct_but_no_db_can_handle_type_alternatives",
            "type": ["null", "string", "Foo"],
            "doc": "Rotor diameter (m)."
        },
      ]
    }

    print(trino_create_table_statement(test_avro, only_str=False))

    test_avro_2 = {
       "type": "record",
       "name": "namespace",
       "fields": [
         {
           "name": "wtur_id",
           "type": "int",
           "doc": "a fake turbine  identifier 470 or something"
         },
         {
           "name": "twain_name",
           "type": "string",
           "doc": "altered name to twain name field description from airfow  minikube"
         },
         {
           "name": "surname",
           "type": [
             "string",
             "null"
           ],
           "doc": "surname of the person"
         },
         {
           "name": "location_name",
           "type": [
             "string",
             "null"
           ],
           "doc": "some comment set in file 075"
         },
         {
           "name": "grd_prod_pwr_std",
           "type": [
             "double",
             "null"
           ],
           "doc": "some comment of the columns for fake_data_ps_047_engie_csv_minikube"
         },
         {
           "name": "date",
           "type": [
             {
               "type": "long",
               "logicalType": "timestamp-micros"
             },
             "null"
           ],
           "doc": "started using datetime, a non-existing type in avro since file 076"
         },
         {
           "name": "date_nulls_first",
           "type": [
             "null",
             {
               "type": "long",
               "logicalType": "timestamp-micros"
             }
           ],
           "doc": "started using datetime, a non-existing type in avro since file 076"
         },
         {
           "name": "hcnt_avg_gen1",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "gen_rpm_avg",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "wgen_rotspd_avg",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "location_latitude",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "location_longitude",
           "type": [
             "double",
             "null"
           ]
         },
         {
           "name": "twain_original_file_name",
           "type": [
             "string",
             "null"
           ]
         },
         {
           "name": "twain_data_ro_allowed_companies",
           "type": [
             "string",
             "null"
           ]
         },
         {
           "name": "twain_data_owning_company",
           "type": "string"
         },
         {
           "name": "twain_data_owning_member",
           "type": "string"
         }
       ],
       "owner": "Unknown",
       "retention": 90
     }
    print(trino_create_table_statement(test_avro_2, only_str=False, overwrite_table_name='other_table_name'))

    test_avro_3 = {
  "type": "record",
  "name": "namespace",
  "fields": [
    {
      "name": "wind_turbine_name",
      "type": "string",
      "doc": "Wind turbine name"
    },
    {
      "name": "date_time",
      "type": {
        "type": "long",
        "logicalType": "timestamp-micros"
      },
      "doc": "Timestamp of the measurement event"
    },
    {
      "name": "ba_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pitch angle for blades (average)"
    },
    {
      "name": "ba_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pitch angle for blades (minimal)"
    },
    {
      "name": "ba_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pitch angle for blades (maximal)"
    },
    {
      "name": "ba_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pitch angle for blades (standard deviation)"
    },
    {
      "name": "rt_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rt_avg"
    },
    {
      "name": "rt_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rt_min"
    },
    {
      "name": "rt_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rt_max"
    },
    {
      "name": "rt_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rt_std"
    },
    {
      "name": "dcs_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "DCs_avg"
    },
    {
      "name": "dcs_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "DCs_min"
    },
    {
      "name": "dcs_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "DCs_max"
    },
    {
      "name": "dcs_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "DCs_std"
    },
    {
      "name": "cm_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cm_avg"
    },
    {
      "name": "cm_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cm_min"
    },
    {
      "name": "cm_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cm_max"
    },
    {
      "name": "cm_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cm_std"
    },
    {
      "name": "p_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Active power generation delivered by the WT after converter and trafo (average)"
    },
    {
      "name": "p_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Active power generation delivered by the WT after converter and trafo (minimal)"
    },
    {
      "name": "p_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Active power generation delivered by the WT after converter and trafo (maximal)"
    },
    {
      "name": "p_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Active power generation delivered by the WT after converter and trafo (standard deviation)"
    },
    {
      "name": "q_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Q_avg"
    },
    {
      "name": "q_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Q_min"
    },
    {
      "name": "q_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Q_max"
    },
    {
      "name": "q_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Q_std"
    },
    {
      "name": "s_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "S_avg"
    },
    {
      "name": "s_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "S_min"
    },
    {
      "name": "s_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "S_max"
    },
    {
      "name": "s_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "S_std"
    },
    {
      "name": "cosphi_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cosphi_avg"
    },
    {
      "name": "cosphi_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cosphi_min"
    },
    {
      "name": "cosphi_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cosphi_max"
    },
    {
      "name": "cosphi_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Cosphi_std"
    },
    {
      "name": "ds_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine generator (average)"
    },
    {
      "name": "ds_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine generator (minimal)"
    },
    {
      "name": "ds_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine generator (maximal)"
    },
    {
      "name": "ds_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine generator (standard deviation)"
    },
    {
      "name": "db1t_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db1t_avg"
    },
    {
      "name": "db1t_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db1t_min"
    },
    {
      "name": "db1t_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db1t_max"
    },
    {
      "name": "db1t_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db1t_std"
    },
    {
      "name": "db2t_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db2t_avg"
    },
    {
      "name": "db2t_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db2t_min"
    },
    {
      "name": "db2t_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db2t_max"
    },
    {
      "name": "db2t_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Db2t_std"
    },
    {
      "name": "dst_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Dst_avg"
    },
    {
      "name": "dst_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Dst_min"
    },
    {
      "name": "dst_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Dst_max"
    },
    {
      "name": "dst_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Dst_std"
    },
    {
      "name": "gb1t_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb1t_avg"
    },
    {
      "name": "gb1t_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb1t_min"
    },
    {
      "name": "gb1t_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb1t_max"
    },
    {
      "name": "gb1t_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb1t_std"
    },
    {
      "name": "gb2t_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb2t_avg"
    },
    {
      "name": "gb2t_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb2t_min"
    },
    {
      "name": "gb2t_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb2t_max"
    },
    {
      "name": "gb2t_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gb2t_std"
    },
    {
      "name": "git_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Git_avg"
    },
    {
      "name": "git_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Git_min"
    },
    {
      "name": "git_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Git_max"
    },
    {
      "name": "git_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Git_std"
    },
    {
      "name": "gost_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gost_avg"
    },
    {
      "name": "gost_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gost_min"
    },
    {
      "name": "gost_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gost_max"
    },
    {
      "name": "gost_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Gost_std"
    },
    {
      "name": "ya_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nacelle direction in angular degrees, average"
    },
    {
      "name": "ya_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nacelle direction in angular degrees, minimal"
    },
    {
      "name": "ya_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nacelle direction in angular degrees, maximal"
    },
    {
      "name": "ya_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nacelle direction in angular degrees, standard deviation"
    },
    {
      "name": "yt_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Yt_avg"
    },
    {
      "name": "yt_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Yt_min"
    },
    {
      "name": "yt_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Yt_max"
    },
    {
      "name": "yt_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Yt_std"
    },
    {
      "name": "ws1_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws1_avg"
    },
    {
      "name": "ws1_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws1_min"
    },
    {
      "name": "ws1_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws1_max"
    },
    {
      "name": "ws1_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws1_std"
    },
    {
      "name": "ws2_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws2_avg"
    },
    {
      "name": "ws2_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws2_min"
    },
    {
      "name": "ws2_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws2_max"
    },
    {
      "name": "ws2_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ws2_std"
    },
    {
      "name": "ws_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Speed (average)"
    },
    {
      "name": "ws_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Speed (minimal)"
    },
    {
      "name": "ws_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Speed (maximal)"
    },
    {
      "name": "ws_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Speed (standard deviation)"
    },
    {
      "name": "wa_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Direction with respect to nacelle orientation (average)"
    },
    {
      "name": "wa_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Direction with respect to nacelle orientation (minimal)"
    },
    {
      "name": "wa_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Direction with respect to nacelle orientation (maximal)"
    },
    {
      "name": "wa_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind Direction with respect to nacelle orientation (standard deviation)"
    },
    {
      "name": "va1_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va1_avg"
    },
    {
      "name": "va1_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va1_min"
    },
    {
      "name": "va1_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va1_max"
    },
    {
      "name": "va1_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va1_std"
    },
    {
      "name": "va2_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va2_avg"
    },
    {
      "name": "va2_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va2_min"
    },
    {
      "name": "va2_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va2_max"
    },
    {
      "name": "va2_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va2_std"
    },
    {
      "name": "va_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va_avg"
    },
    {
      "name": "va_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va_min"
    },
    {
      "name": "va_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va_max"
    },
    {
      "name": "va_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Va_std"
    },
    {
      "name": "ot_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ambient Temperature (average)"
    },
    {
      "name": "ot_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ambient Temperature (minimal)"
    },
    {
      "name": "ot_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ambient Temperature (maximal)"
    },
    {
      "name": "ot_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Ambient Temperature (standard deviation)"
    },
    {
      "name": "nf_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nf_avg"
    },
    {
      "name": "nf_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nf_min"
    },
    {
      "name": "nf_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nf_max"
    },
    {
      "name": "nf_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nf_std"
    },
    {
      "name": "nu_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nu_avg"
    },
    {
      "name": "nu_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nu_min"
    },
    {
      "name": "nu_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nu_max"
    },
    {
      "name": "nu_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Nu_std"
    },
    {
      "name": "rs_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine rotor (average)"
    },
    {
      "name": "rs_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine rotor (minimal)"
    },
    {
      "name": "rs_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine rotor (maximal)"
    },
    {
      "name": "rs_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rotational Speed of wind turbine rotor (standard deviation)"
    },
    {
      "name": "rbt_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rbt_avg"
    },
    {
      "name": "rbt_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rbt_min"
    },
    {
      "name": "rbt_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rbt_max"
    },
    {
      "name": "rbt_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rbt_std"
    },
    {
      "name": "rm_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rm_avg"
    },
    {
      "name": "rm_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rm_min"
    },
    {
      "name": "rm_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rm_max"
    },
    {
      "name": "rm_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Rm_std"
    },
    {
      "name": "pas_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pas_avg"
    },
    {
      "name": "pas_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pas_min"
    },
    {
      "name": "pas_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pas_max"
    },
    {
      "name": "pas_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Pas_std"
    },
    {
      "name": "wa_c_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind direction in angular degrees, average"
    },
    {
      "name": "wa_c_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind direction in angular degrees, minimal"
    },
    {
      "name": "wa_c_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind direction in angular degrees, maximal"
    },
    {
      "name": "wa_c_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Wind direction in angular degrees, standard deviation"
    },
    {
      "name": "na_c_avg",
      "type": [
        "double",
        "null"
      ],
      "doc": "Na_c_avg"
    },
    {
      "name": "na_c_min",
      "type": [
        "double",
        "null"
      ],
      "doc": "Na_c_min"
    },
    {
      "name": "na_c_max",
      "type": [
        "double",
        "null"
      ],
      "doc": "Na_c_max"
    },
    {
      "name": "na_c_std",
      "type": [
        "double",
        "null"
      ],
      "doc": "Na_c_std"
    },
    {
      "name": "twain_original_file_name",
      "type": [
        "string",
        "null"
      ]
    },
    {
      "name": "twain_data_ro_allowed_companies",
      "type": [
        "string",
        "null"
      ]
    },
    {
      "name": "twain_data_owning_company",
      "type": "string"
    },
    {
      "name": "twain_data_owning_member",
      "type": "string"
    }
  ],
  "owner": "Unknown",
  "retention": 90
}
    print(trino_create_table_statement(test_avro_3, only_str=False, overwrite_table_name='la_haute_table_schema'))
