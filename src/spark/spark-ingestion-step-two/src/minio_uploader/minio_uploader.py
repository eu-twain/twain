import logging
import os
from minio import Minio
from minio.error import S3Error


class MinioManager(object):
    def __init__(self, endpoint: str):
        self.log = logging.getLogger(self.__class__.__name__)
        self.log.info(f"Connecting Minio Manager to {endpoint}.")
        self.access_key = os.getenv('MINIO_USER')
        self.secret_key = os.getenv('MINIO_PASSWORD')
        self.client = Minio(endpoint=endpoint,
                            access_key=self.access_key,
                            secret_key=self.secret_key,
                            secure=False)

    def create_bucket(self, bucket_name):
        if not self.client.bucket_exists(bucket_name):
            self.client.make_bucket(bucket_name)
            self.log.info(f"Created bucket {bucket_name}")
        else:
            self.log.info(f"Bucket {bucket_name} already exists")

    def put_object(self, bucket_name: str, minio_file_path: str, local_file_abs_path: str):
        self.log.info(f"Attempting to put Local file {local_file_abs_path} as object {minio_file_path}"
                      f" to bucket {bucket_name}.")
        self.client.fput_object(bucket_name, minio_file_path, local_file_abs_path)
        self.log.info(f"Local file {local_file_abs_path} successfully uploaded as object {minio_file_path}"
                      f" to bucket {bucket_name}")


def run():
    bucket_name = "datalake"
    mm = MinioManager()
    mm.create_bucket(bucket_name)

    # client.fput_object(bucket_name, destination_file, source_file)
    # client.fput_object(bucket_name, destination_schema, source_schema)
    # print(
    #     source_file, "successfully uploaded as object",
    #     destination_file, "to bucket", bucket_name,
    # )


if __name__ == '__main__':
    run()
