import logging
import sys
import datetime
import pytz
import json
import os
import copy
from pyspark.sql import SQLContext, Row
from pyspark.sql.dataframe import DataFrame as spark_df
from src.spark_base_runner.spark_base_runner import SparkBaseRunner, trinoHost, TRINO_DEFAULT_SCHEMA
from src.utils.spark_transformations import transformations
from src.config.common_data_schema import EXPECTED_SCHEMA
from src.config.data_layers import CONSUMPTION_LAYER_NAME
from src.config.technical_columns import *
from src.config.mappings.test_schema_mappings import DataMapping
from src.utils.os_utils import create_dir_if_not_exists
from src.trino_manager.trino_jdbc_reader import TrinoJDBCReader

GET_TABLE_ORIGINAL_FILES_QUERY = """
  select
    twain_original_file_name,
    twain_data_owning_company,
    twain_data_owning_member,
    count(1) row_count
  from {}
  group by
    twain_original_file_name,
    twain_data_owning_company,
    twain_data_owning_member
  order by 1 desc"""

GET_TABLE_PARQUET_FILES_QUERY = """
 select
  twain_original_file_name,
  parquet_file_path
 from (
 select
   "$path" parquet_file_path,
   twain_original_file_name
 from
  hive.default.common_data_model
 where
  twain_data_owning_company = '{twain_data_owning_company}'
  and twain_data_owning_member = '{twain_data_owning_member}'
  and twain_original_file_name = '{twain_original_file_name}'
 )
 group by parquet_file_path, twain_original_file_name
 order by twain_original_file_name
"""


class SparkETL2Runner(SparkBaseRunner):
    """
    Class representing the step 1 in ETL process for Twain project.
    The goal is to move data from raw layer (where it is stored in a dummy string type) to bronze layer where
    data has correct types set up by the pseudo-avro schema.
    """
    def __init__(self):
        super().__init__()
        self.log = logging.getLogger(self.__class__.__name__)
        self.version = "1.0.1"
        self.dead_letters_bucket_name = "ingestion-results"
        self.trino_jdbc_reader = TrinoJDBCReader(self.spark, self.tm, trinoHost, TRINO_DEFAULT_SCHEMA)
        self.mapping = DataMapping(
            DataMapping.EXTERNAL_FILE_MODE,
            'default',
            self.mm
        )
        self.target_table_name = None
        config_text = os.getenv('INPUT_DATA_CONFIG').replace('\'', "\"")
        if not config_text:
            self.log.error(f"Could not find argument INPUT_DATA_CONFIG")
            raise ValueError("Could not find argument INPUT_DATA_CONFIG")
        try:
            self.input_data_config = json.loads(config_text)
        except Exception as e:
            self.log.error(f"Could not parse argument {config_text}")
            raise e
        self.csv_table_prefix = os.getenv('CSV_TABLE_PREFIX', default='csv_')
        self.avro_table_prefix = os.getenv('AVRO_TABLE_PREFIX', default='avro_')
        self.bronze_table_prefix = os.getenv('BRONZE_TABLE_PREFIX', default='bronze_')
        self.incorrect_tables = list()
        self.correct_tables = list()

    def _infer_file_owner(self):
        """
        Function not relevant for SparkETL1Runner
        :return:
        """
        return "Unknown"

    def _sample_test_schema_parsing(self):
        self.log.info(f"test_pyspark_schema: {EXPECTED_SCHEMA}")
        self.log.info(f"test_expected_schema: {EXPECTED_SCHEMA}")
        row_dict = {'name': 'd', 'surname': 'a', 'location': 'Loc Muinne',
                    'event_timestamp': '2024-02-01 10:00:02.907535', 'wind_speed': '1.0093',
                    'widn_direction': '78.0492', 'location_x': '30.000', 'location_y': '-100.302',
                    'grd_prod_pwr_std': '-29.0012',
                    'timestamp': datetime.datetime.strptime('2024-04-05 10:00:02.907535', '%Y-%m-%d %H:%M:%S.%f')}
        row = Row(**row_dict)
        self.log.info(f'Test output: {transformations(row, EXPECTED_SCHEMA, self.mapping)}')

    def write_files_directly_on_s3(self, data_layer: str, table_name: str, input_df: spark_df):
        """
        Function writes avro schema and parquet data files in proper places in s3 permanent storage.
        :param data_layer: the name of the data layer the file has to be stored at
        :param table_name: the name of the table
        :param input_df: the spark dataframe holding the data
        :param avro_schema_dict_for_df: original schema of df, needed to pass description of fields
        :return: Tuple with first element being the s3a path where data was written and
                 the second element is the path within the container where te avro schema was generated to
        """
        file_name = f'{table_name}.avsc'
        local_path = f'output/{data_layer}/{table_name}'
        local_path_avsc = f'output/{data_layer}/{table_name}/schemas/{file_name}.avsc'
        write_path_base = f's3a://{self.bucket_name}/{local_path}'
        output_avro_schema_path, compiled_avro_schema = \
            self.am.compile_schema_from_spark_df(self.spark, input_df, avro_schema_dict_for_df=EXPECTED_SCHEMA)
        self.mm.put_object(self.bucket_name, local_path_avsc, output_avro_schema_path)
        # input_df = self._drop_partition_columns(input_df)
        # limiting the amount of rows in one parquet file:  option("maxRecordsPerFile", 10). \
        input_df.write.\
            partitionBy(TWAIN_DATA_OWNING_COMPANY, TWAIN_DATA_OWNING_MEMBER).\
            parquet(path=write_path_base, mode='append')
        self.log.info(f'Wrote file to {write_path_base}')
        return write_path_base, local_path_avsc, compiled_avro_schema

    @staticmethod
    def _drop_partition_columns(input_df: spark_df):
        return input_df.drop(*TWAIN_PARTITION_COLUMNS)

    def set_static_xcom(self, xcom_contents: dict):
        """
        Function sets the contents of a dictionary into a file on local directory /airflow/xcom/return.json in order
        to pass it to xcom.
        :param xcom_contents: dictionary of xcom variable
        :return:
        """
        local_mode = json.loads((os.getenv('LOCAL_MODE', 'False').lower()))
        if not local_mode:
            xcom_file_location = os.path.join('/', 'airflow', 'xcom')
        else:
            xcom_file_location = os.path.join(os.getcwd(), 'output', 'airflow', 'xcom')
        try:
            os.mkdir(xcom_file_location)
        except FileExistsError as fee:
            self.log.info(f'directory {xcom_file_location} already exists, skipping creation.')
        except FileNotFoundError as fnf:
            create_dir_if_not_exists(xcom_file_location)
        xcom_file_path = os.path.join(xcom_file_location, 'return.json')
        with open(xcom_file_path, 'w+', encoding='UTF-8') as xcom_file:
            xcom_file.write(json.dumps(xcom_contents))
        self.log.info(f"Written file {xcom_file_path}")

    def _interpret_results(self):
        """
        Function interprets the results of running step 2 of ETL job on a set of tables. Passes the resulting
        dictionary to xcom variable. Raises KeyError if at least one table was not ingested into common_data_model.
        :return:
        """
        xcom_contents = dict()
        if self.incorrect_tables:
            xcom_contents['incorrect_tables'] = self.incorrect_tables
        if self.correct_tables:
            xcom_contents['correct_tables'] = self.correct_tables
            self._store_dead_letters(self.correct_tables)
        if xcom_contents.keys():
            self.set_static_xcom(xcom_contents)
            if self.incorrect_tables:
                raise KeyError(f"There are incorrect tables. Look into xcom to get the table lst and into "
                               f"Airflow logs to identify the reason.")

    def _store_dead_letters(self, correct_table_names: list):
        for bronze_table_name in correct_table_names:
            self.log.info(f"Collecting original file names for table {bronze_table_name}")
            query = GET_TABLE_ORIGINAL_FILES_QUERY.format(bronze_table_name)
            original_file_names_df = self.trino_jdbc_reader.run_query(query)
            original_file_names = [row.asDict(True) for row in original_file_names_df.collect()]
            self.log.info(f"original_file_names: {original_file_names}")
            previous_name = None
            parquet_paths = list()
            for row in original_file_names:
                if previous_name and previous_name != row['twain_original_file_name']:
                    self._store_dead_letter(previous_name, parquet_paths)
                    parquet_paths = list()
                parquet_paths.extend(self._get_partition_parquets(row))
                previous_name = row['twain_original_file_name']
            self._store_dead_letter(previous_name, parquet_paths)

    def _get_partition_parquets(self, input_dict: dict):
        original_file_name = input_dict['twain_original_file_name']
        self.log.info(f"Collecting parquet file names for file {original_file_name}")
        query = GET_TABLE_PARQUET_FILES_QUERY.format(
            twain_data_owning_company=input_dict['twain_data_owning_company'],
            twain_data_owning_member=input_dict['twain_data_owning_member'],
            twain_original_file_name=original_file_name)
        parquet_file_names_df = self.trino_jdbc_reader.run_query(query)
        parquet_file_names = [row.asDict(True) for row in parquet_file_names_df.collect()]
        self.log.info(f"parquet_file_names: {parquet_file_names}")
        return [x['parquet_file_path'] for x in parquet_file_names]

    def _store_dead_letter(self, file_name: str, file_contents_list: list):
        # get rid of duplicates
        file_contents_list = list(set(file_contents_list))
        file_content = ",".join(file_contents_list)
        output_file_full_path = f"success_ingestion_{file_name}.txt"
        local_file_path = f"output/{output_file_full_path}"
        with open(local_file_path, 'w+', encoding='utf8') as output_file:
            output_file.write(file_content)
        self.mm.put_object(self.dead_letters_bucket_name, output_file_full_path, local_file_path)

    def _run_one_table(self, table_name: str, target_table_name: str):
        """
        Function transforms data from single bronze data layer table into consumption layer.
        :param table_name: the name of the bronze layer table
        :param target_table_name: the name of the table in consumption layer
        :return:
        """
        query = f'select * from {table_name} a'
        input_dataframe = self.trino_jdbc_reader.run_query(query)
        self.log.info(f"There are {input_dataframe.count()} records in the bronze table {table_name}.")
        self.log.info(f"Raw data schema: {input_dataframe.schema}")

        mapping = copy.copy(self.mapping)
        transformed_df = input_dataframe.rdd.map(
            lambda x: transformations(x, EXPECTED_SCHEMA, mapping)). \
            toDF(schema=EXPECTED_SCHEMA).persist()
        self.log.info(f"There are {transformed_df.count()} records in the transformed bronze layer table.")
        self.log.info(f"{transformed_df.schema}")

        remote_parquet_file_path, remote_avro_schema_path, compiled_avro_schema = \
            self.write_files_directly_on_s3(CONSUMPTION_LAYER_NAME, target_table_name, transformed_df)
        self.tm.create_table_and_schema(
            target_table_name, remote_parquet_file_path, remote_avro_schema_path,
            self.bucket_name, avro_schema_dict=compiled_avro_schema)

    def run(self):
        """
        Main function of the step 2 of the Twain ingestion process. The processing occurs consecutively
        for each item in the configuration. Said configuration is the python list which elements are the tables
        from bronze data layer that are supposed to be merged into consumption layer.
        :return:
        """
        self.log.info(f"Running ETL step 2 version {self.version}.")
        target_table_name = 'common_data_model'
        for table_name in self.input_data_config:
            try:
                self._run_one_table(table_name, target_table_name)
                self.correct_tables.append(table_name)
            except Exception as e:
                self.log.error(e)
                self.incorrect_tables.append(table_name)
        self._interpret_results()


if __name__ == "__main__":
    logger = logging.getLogger('read_engie_sample_data')
    logging.basicConfig(format='%(asctime)s,%(msecs)-3d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level="INFO", stream=sys.stdout)
    start_timestamp = datetime.datetime.now(tz=pytz.UTC)
    try:
        runner = SparkETL2Runner()
        runner.run()
        if runner:
            runner.log.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
    except Exception as e:
        raise e
    finally:
        if 'runner' in globals():
            runner.log.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
            runner.tm.close()
        else:
            logger.info(f"The process took {datetime.datetime.now(tz=pytz.UTC) - start_timestamp}.")
